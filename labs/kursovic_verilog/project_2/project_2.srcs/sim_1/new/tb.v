`timescale 1ns / 1ps


module tb;
    
    reg clk;
    reg rst;
    
    reg [4 : 0] in;
    
    wire [7 : 0] out1;
    wire [7 : 0] out2;
    
    wire cmp;
    
    initial 
    begin
      #1;
      rst = 0;
      #1;
      rst = 1;
    end
    
    
    always 
    begin
        #10;
        clk = 'b0;
        #10;
        clk = 'b1;
    end
    
    assign cmp = |(out1 ^ out2);
    
    
     main main1 ( clk, rst, in, out1 );
     main main2 ( clk, rst, in, out2 );
    
    reg [7 : 0] st_mh;
    reg [7 : 0] delay;
    
    always @( posedge clk or negedge rst )
    begin
        if ( ~rst )
        begin
            st_mh <= 0;
            in <= 0;
            delay <= 0;
        end else
        begin
            case ( st_mh )
                'd0:
                    begin
                        delay <= 0;
                        in <= in + 1;
                        st_mh <= st_mh + 1;
                    end
                
                'd1:
                    begin
                        delay <= delay + 1;
                        if ( delay >= 3 )
                            st_mh <= st_mh + 1;    
                
                    end
                    
                'd2:
                    begin
                        if ( cmp )
                            $stop;
                            
                        st_mh <= 0;
                    end
            endcase
        end
    end
    
endmodule
