`timescale 1ns / 1ps


module main(
    clk, rst, in,
    out
    );
    
    input clk;
    input rst;
    
    input [4 : 0] in;
    output [7 : 0] out;
    
    reg [7 : 0] st_mh;
    
    reg [4 : 0] in_work;
    reg [7 : 0] out_work;
    
    reg [3 : 0] high_dig;
    reg [3 : 0] low_dig;
    
    assign out = out_work;
    //assign out = (( in / 10 ) << 4) ^ ( in % 10 );
    
    always @( posedge clk or negedge rst )
    begin
        if ( ~rst )
        begin
            st_mh <= 0;
            in_work <= 0;
            out_work <= 0;
            
            high_dig <= 0;
            low_dig <= 0;
        end else
        begin
            case ( st_mh )
                'd0:
                    begin
                        in_work <= in;
                        st_mh <= st_mh + 1;
                    end
                    
                'd1:
                    begin
                        high_dig <= in_work / 10;
                        low_dig <= in_work % 10;
                        
                        st_mh <= st_mh + 1;
                    end
                    
                'd2:
                    begin
                        out_work <= (high_dig << 4) ^ low_dig;
                        st_mh <= 0;
                    end
                   
            endcase
        end
    end
    
endmodule

/*
module add3(in,out);
input [3:0] in;
output [3:0] out;
reg [3:0] out;

always @ (in)
case (in)
4'b0000: out <= 4'b0000;
4'b0001: out <= 4'b0001;
4'b0010: out <= 4'b0010;
4'b0011: out <= 4'b0011;
4'b0100: out <= 4'b0100;
4'b0101: out <= 4'b1000;
4'b0110: out <= 4'b1001;
4'b0111: out <= 4'b1010;
4'b1000: out <= 4'b1011;
4'b1001: out <= 4'b1100;
default: out <= 4'b0000;
endcase
endmodule

module binary_to_BCD(A,ONES,TENS);
input [5:0] A;
output [3:0] ONES, TENS;

wire [3:0] c1,c2,c3;
wire [3:0] d1,d2,d3;

assign d1 = {1'b0,A[5:3]};
assign d2 = {c1[2:0],A[2]};
assign d3 = {c2[2:0],A[1]};
add3 m1(d1,c1);
add3 m2(d2,c2);
add3 m3(d3,c3);
assign ONES = {c3[2:0],A[0]};
assign TENS = {1'b0,c1[3],c2[3],c3[3]};
endmodule
*/
