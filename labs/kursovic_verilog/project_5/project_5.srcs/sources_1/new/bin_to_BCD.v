`timescale 1ns / 1ps

module bin_to_BCD(
        clk, rst, in,
        out
    );
    
    input wire clk;
    input wire rst;
    input wire [ 4 : 0 ] in;
    
    output reg [ 5 : 0 ] out;
    

    reg [ 7 : 0 ] state = 0;
    
    reg [ 3 : 0 ] MSB;
    reg [ 3 : 0 ] LSB;
    
    reg [ 4 : 0 ] in_buf;
    
    always @(posedge clk or negedge rst )
    begin
        if ( ~rst )
        begin
            state <= 0;
            LSB <= 0;
            MSB <= 0;
            
            out <= 0;
        end else
        begin
            case ( state )
                'd0:
                    begin
                        in_buf <= in;
                        LSB <= (in << 1) % 10;
                        state <= 'd1;
                    end
                    
                'd1:
                    begin
                        MSB <= (in_buf << 1) / 10;
                        state <= 'd2;
                    end
                    
                'd2:
                    begin
                        out <= (MSB << 3) | (LSB >> 1 );
                        state <= 0;
                    end
            endcase
        end    
    end
    
endmodule
