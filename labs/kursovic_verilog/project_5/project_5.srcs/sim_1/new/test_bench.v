`timescale 1ns / 1ps


module test_bench();

    reg clk;
    reg rst;
    reg [ 4 : 0 ] in;
    
    wire [ 5 : 0 ] out1;
    wire [ 5 : 0 ] out2; 
  
    
    reg [ 7 : 0 ] state;
    reg [ 7 : 0 ] i;
    
    wire verify;
    assign verify = ( out1 == out2 ) ? 0 : 1;
    
    initial
    begin
        state = 0;
        in = 0;
        clk = 0;
        rst = 1;
        i = 0;
    end
    
    always
    begin
        #10;
        clk = 0;
        #10;
        clk = 1;
    end
    
    always @(posedge clk or negedge rst)
    begin
        if ( ~rst )
        begin
            state = 0;
            in = 0;
            i <= 0;
            
        end else
        begin
            case ( state )
                'd0:
                    begin
                        in <= in + 1;     
                        i <= 0;
                        state <= 'd1;                   
                    end
                    
                'd1:    
                    begin
                        i <= i + 1;
                        if ( i == 1 )
                            state <= 0;
                    end
                
            endcase
            
            if ( verify )
                $stop;
        end
    end 
    
    bin_to_BCD bin_to_BCD_1 ( clk, rst, in, out1 );
    bin_to_BCD bin_to_BCD_2 ( clk, rst, in, out2 ); 
endmodule
