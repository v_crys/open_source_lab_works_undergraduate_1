`timescale 1ns / 1ps

module main(
    clk, rst, A_START, B_START, LOADBAR,
    Sum
    );
    
    input wire clk;
    input wire rst;
    input wire [3 : 0] A_START;
    input wire [3 : 0] B_START;
    input wire LOADBAR;
    
    reg [3 : 0] A;
    reg [3 : 0] B;
    
    output reg [3 : 0] Sum = 0;
    
    reg [7 : 0] state = 0;
    
    always @(posedge clk or negedge rst)
    begin
        if ( ~rst )
        begin
            Sum <= 0;
            state <= 0;
            
            A <= 0;
            B <= 0;
            
        end else
        begin
            case ( state )
                'd0:
                    begin
                    if ( LOADBAR )
                    begin
                        A <= A_START;
                        B <= B_START;
                    end
                 
                    state <= state + 1;
                    end
                    
                'd1:
                    begin
                        Sum <= A + B;
                        state <= 0;
                    end
                    
                default:
                    begin
                        state <= 0;
                    end
                    
            endcase
        end
    end
    
endmodule
