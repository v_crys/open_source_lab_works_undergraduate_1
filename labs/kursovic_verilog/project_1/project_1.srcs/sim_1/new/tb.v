`timescale 1ns / 1ps


module tb;
    reg clk;
    reg rst;
    
    reg LOADBAR;
    wire [ 3 : 0 ] Sum1;
    wire [ 3 : 0 ] Sum2;

    reg [ 7 : 0 ] state;

    reg [ 3 : 0 ] A_LOAD;
    reg [ 3 : 0 ] B_LOAD;

    reg [ 3 : 0 ] delay;

    initial
    begin
        A_LOAD = 0;
        B_LOAD = 0;
        
        state = 0;
    end
    
    always
    begin
        #10;
        clk = 0;
        #10;
        clk = 1;
    end

    always @( posedge clk  )
    begin
        case ( state )     
            'd0:
                begin
                    rst <= 0;
                    state <= state + 1;
                    delay <= 0;
                
                    A_LOAD <= 0;
                    B_LOAD <= 0;
                
                    LOADBAR <= 0;
                end
        
        
            'd1:
                begin
                    rst <= 1;
                    LOADBAR <= 1;
                    
                    if ( A_LOAD == 'd15 )
                        B_LOAD <= B_LOAD + 1;
                        
                    A_LOAD <= A_LOAD + 1;
                    
                    state <= 'd2;
                    delay <= 0;
                end
                
            'd2:
                begin
                    LOADBAR <= 0;                       
                    state <= 'd1;  
                end
           
                
            default:
                begin
                    state <= 0;
                end
        endcase
        
        
         if ( Sum1 != Sum2 )
                $stop;  
    
   
    end

    main main1 ( clk, rst, A_LOAD, B_LOAD, LOADBAR, Sum1 );
    main main2 ( clk, rst, A_LOAD, B_LOAD, LOADBAR, Sum2 );
endmodule

