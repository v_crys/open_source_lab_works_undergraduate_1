`timescale 1ns / 1ps


module testbench();

    reg clk;
    reg load = 0;
    reg cten = 0;
    reg du = 0;
    
    reg [3 : 0] IN = 0;
    
    wire [3 : 0] OUT1;
    wire [3 : 0] OUT2;
    
    wire MIN_MAX1;
    wire MIN_MAX2;
    
    reg [7 : 0] st_mh = 0;
    
    reg [7 : 0] cnt = 0;
    
    wire ver;
    assign ver = (|(OUT1 ^ OUT2)) | (MIN_MAX1 ^ MIN_MAX2);  
    
    initial
    begin
        load = 0;
        IN = 'd0;
    end
    
    always
    begin
        #10;
        clk = 0;
        #10;
        clk = 1;
        
        load = 1;
    end

    always @( posedge clk )
    begin
        case ( st_mh )
            'd0:
                begin
                    du <= 0;
                    if ( cnt == 'd30 )
                        st_mh <= 'd1;
                        
                    cnt <= cnt + 1;
                    
                    if ( ver )
                        $stop;
                end
                
                
            'd1: 
                begin
                    du <= 1;
                    if ( cnt == 'd60 )
                    begin                        
                        st_mh <= 0;
                        cnt <= 0;
                    end else
                        cnt <= cnt + 1;
                        
                    if ( ver )
                        $stop;
                end
        
        endcase
    end
    
    counter counter1 ( clk, load, cten, du, IN, OUT1, MIN_MAX1 );
    counter counter2 ( clk, load, cten, du, IN, OUT2, MIN_MAX2 );
endmodule
