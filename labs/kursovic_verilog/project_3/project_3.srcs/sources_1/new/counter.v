`timescale 1ns / 1ps


module counter(
    clk, load, cten, du, IN,
    OUT, MIN_MAX
    );
    
    input clk;
    input load;
    input cten;
    input du;
    input [3 : 0] IN;
    
    output reg [3 : 0] OUT;
    output reg MIN_MAX;
    
    reg [7 : 0] st_mh;
    
    reg du_buf;
    
    always @( posedge clk or negedge load )
    begin
        if ( ~load )
        begin
            OUT <= IN;
            MIN_MAX <= 0;
            st_mh <= 0;
            
            du_buf <= 0;
        end else
        begin
            if ( cten == 0 )
            begin
            
            case ( st_mh )
                'd0:
                    begin
                        du_buf <= du;
                        st_mh <= 'd1;
                    end
            
                'd1:
                    begin
                        if ( ~du_buf )
                        begin
                            OUT <= OUT + 1;
                            
                            if ( OUT == 'd8 )
                                st_mh <= st_mh + 1;
                            else
                                st_mh <= 0;
                        end else
                        begin
                            OUT <= OUT - 1;
                            
                            if ( OUT == 'd1 )
                                st_mh <= st_mh + 1;
                            else
                                st_mh <= 0;
                        end
                        
                        MIN_MAX <= 0;
                    end
                    
                'd2:
                    begin
                        MIN_MAX <= 1;
                        
                        if ( OUT == 'd9 )
                        begin
    
                                OUT <= 0;
                            
                        end 
                        
                        if ( OUT == 'd0 )
                        begin
                                OUT <= 'd9;     
                        end
                                                   
                        st_mh <= 'd0;
                    end
            endcase
            
            end
        end
    end
    
endmodule
