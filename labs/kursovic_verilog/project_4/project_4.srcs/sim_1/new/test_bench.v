`timescale 1ns / 1ps


module test_bench();

    reg clk;
    reg rst;
    
    reg [ 3 : 0 ] A;
    reg [ 3 : 0 ] B;
    
    wire out_b1;
    wire out_m1;
    wire out_e1;
    
    wire out_b2;
    wire out_m2;
    wire out_e2;
    
    reg [ 7 : 0 ] st_mh;
    
    reg [ 7 : 0 ] busy;
    
    wire verify;
    
    assign verify = ( { out_b1, out_m1, out_e1 } == { out_b2, out_m2, out_e2 } ) ? 0 : 1;
    
    initial
    begin
        clk = 0;
        rst = 1;
        
        A = 0;
        B = 0;
        
        st_mh = 0;
        
        busy = 0;
    end
    
    always
    begin
        #10;
        clk <= 0;
        #10;
        clk <= 1;
    end
    
    
    always @( posedge clk or negedge rst )
    begin
        if ( ~rst )
        begin
            st_mh <= 0;
        end else
        begin
            case ( st_mh )
                'd0:
                    begin
                        A <= A + 1;
                    
                        if ( A == 'd15 )
                        begin
                            B <= B + 1;
                        end
                        
                        
                        busy <= 0;
                        
                        st_mh <= st_mh + 1;
                    end
                    
                'd1:
                    begin
                        busy <= busy + 1;
                        
                        if ( busy == 'd2 )
                            st_mh <= 0;
                    end
                    
            
            endcase
            
            if ( verify )
                $stop;
            
        end
    end
    
    
    
    chip_7485  chip_7485_1 ( clk, rst, A, B, out_b1, out_m1, out_e1 );
    chip_7485  chip_7485_2 ( clk, rst, A, B, out_b2, out_m2, out_e2 );
endmodule
