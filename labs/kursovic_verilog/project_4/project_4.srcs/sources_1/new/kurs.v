`timescale 1ns / 1ps

module chip_7485(
    clk, rst, A, B,
    out_b, out_m, out_e
    );
    
    
    
    input clk;
    input rst;
    
    input [ 3 : 0 ] A;
    input [ 3 : 0 ] B;
    
    output reg out_b;
    output reg out_m;
    output reg out_e;
    
    reg [ 7 : 0 ] st_mh = 0;
    
    
    reg [ 3 : 0 ] buf_A;
    reg [ 3 : 0 ] buf_B;
    
    always @( posedge clk or negedge rst )
    begin
        if ( ~rst )
        begin
            out_b <= 0;
            out_m <= 0;
            out_e <= 0;
            
            st_mh <= 0;
            
            buf_A <= 0;
            buf_B <= 0;
        end else
        begin
            case ( st_mh )
                'd0:
                    begin
                        buf_A <= A;
                        buf_B <= B;
                        
                        st_mh <= st_mh + 1;
                    end
            
                'd1:
                    begin
                        if ( buf_A > buf_B )
                        begin
                            out_b <= 1;
                        end else
                        begin
                            out_b <= 0;
                        end
                        
                        st_mh <= st_mh + 1;
                    end
                    
                    
                'd2:
                    begin
                        if ( buf_A < buf_B )
                        begin
                            out_m <= 1;
                        end else
                        begin
                            out_m <= 0;
                        end
                      
                        st_mh <= st_mh + 1;
                        
                    end
                    
                'd3:
                    begin
                        if ( buf_A == buf_B )
                        begin
                            out_e <= 1;
                        end else
                        begin
                            out_e <= 0;
                        end
                             
                        st_mh <= 0;           
                    end
                   
                   
                default:
                    begin
                        st_mh <= 0;
                    end 
                    
            endcase
        end
    end 
    
    
    
endmodule
