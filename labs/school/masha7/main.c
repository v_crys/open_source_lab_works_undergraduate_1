#include <stdio.h>
#include <windows.h>

#define DOWN 1
#define TOP 3
#define LEFT 4
#define RIGHT 2

#define SIZE 10

int main ()
{
    int maze [ SIZE ][ SIZE ];
    int i, j, N;
    FILE *fin;

    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    printf ( "To enter the maze from the keyboard press 1. Load from file - 2.\n" );
    scanf ( "%d", &N );

    if ( N == 1 )
    {
        printf ( "Enter the maze\n\n" );
        for ( i = 0; i < SIZE; i ++ )
        {
            for ( j = 0; j < SIZE; j++)
            {
            scanf( "%d", &maze[i][j]);
            }
        }
    }
    else
    {
        fin = fopen( "in.txt", "rt" );
        for ( i = 0; i < SIZE; i ++ )
        {
            for ( j = 0; j < SIZE; j++)
            {
            fscanf( fin, "%d", &maze[i][j]);
            }
        }
    }

    printf("\n");

    N = 1;
    i = 1;
    j = 1;
    int iend = SIZE - 2;
    int jend = SIZE - 2;
    maze[i][j] = 2;
    //maze[iend][jend] = 2;

    //while ( i != iend || j != jend )
    while ( maze[ iend ][ jend ] != 2 )
    {
        if ( N == DOWN )
        {
            if ( ( maze[i][j - 1] == 1 ) && (maze[i + 1][j] != 1 ) )
            {
                if ( maze[i + 1][j - 1] == 1 )
                {
                    i++;
                    maze[i][j] = 2;
                }
                else
                {
                    i++;
                    j--;
                    maze[i][j] = 2;
                    maze[i][j + 1] = 2;

                    N = LEFT;
                }
                continue;
            }
                else N = RIGHT;

            continue;
        }

        if ( (N == RIGHT) )
        {
        if ( ( maze[i + 1][j] == 1 ) &&  ( (maze[i][j + 1] == 0) || (maze[i][j + 1] == 2)) )
        {
            if ( maze[i + 1][j + 1] == 1 )
            {
                j++;
                maze[i][j] = 2;
            }
            else
            {
                i++;
                j++;
                maze[i][j] = 2;
                maze[i - 1][j] = 2;
                N = DOWN;
            }
            continue;
        }
        else N = TOP;
        }

        if ( N == TOP )
        {
        if ( ( maze[i][j + 1] == 1 ) && ( (maze[i - 1][j] == 0) || (maze[i - 1][j] == 2) ) )
        {
            if ( maze[i - 1][j + 1] == 1 )
            {
                i--;
                maze[i][j] = 2;
            }
            else
            {
                i--;
                j++;
                maze[i][j] = 2;
                maze[i][j - 1] = 2;
                N = RIGHT;
            }
            continue;
        }
        else N = LEFT;
        }

        if ( (N == LEFT) )
        {
        if ( ( maze[i - 1][j] == 1 ) && ( (maze[i][j - 1] == 0) || (maze[i][j - 1] == 2) ) )
        {
            if ( maze[i - 1][j - 1] == 1 )
            {
                j--;
                maze[i][j] = 2;
            }
            else
            {
                i--;
                j--;
                maze[i][j] = 2;
                maze[i + 1][j] = 2;
                N = TOP;
            }
            continue;
        }
        else N = TOP;
        }
    }

    for ( i = 0; i < SIZE; i++)
    {
        for ( j = 0; j < SIZE; j++)
        {
            if (maze[i][j] == 2)
            {
                maze[i][j] = 0;
                SetConsoleTextAttribute(console, 1);
                printf( "%d ", maze [i][j] );
            }
            else
            {
            SetConsoleTextAttribute(console, 10);
            printf( "%d ", maze [i][j] );
            }

        }
        printf( "\n" );
    }


}
