#include <stdio.h>

#define SIZE 100
#define N_KEY 4

struct type_cod{
char text [SIZE];
int Nt;
char key [SIZE];
};

void coder ( struct type_cod );
void decoder ( struct type_cod );

int main ()
{
    int i, P;
    struct type_cod my_cod;

    printf( "Enter the key\n" );

    for ( i = 0; i < N_KEY; i++ )
        scanf ( "%c", &my_cod.key[i] );
    //my_cod.key[ i ] = getch();

    printf ( "coder - 1, decoder - 2\n" );
    scanf ( "%d", &P );

    printf ( "Enter the length of the text\n" );
    scanf ( "%d", &my_cod.Nt );

    printf ( "Enter the text\n" );

    for ( i = 0; i < my_cod.Nt; i++ )
        scanf ( "%c", &my_cod.text[i] );
        //my_cod.text[ i ] = getch();

    if ( P == 1)
        coder ( my_cod );
    else
       decoder ( my_cod );
}

void coder ( struct type_cod coder_cod)
{
    int i;
    for ( i = 0; i < coder_cod.Nt; i++ )
    {
        if ( coder_cod.text[i] == ' ' )
            printf ( "%c", coder_cod.key[0] );
        else
            printf ( "%c", coder_cod.key[ coder_cod.text[i] - 'a' + 1 ] );
    }

}

void decoder ( struct type_cod decoder_cod)
{
    int i, j;
    for ( i = 0; i < decoder_cod.Nt; i++ )
    {
        if ( decoder_cod.text[i] == decoder_cod.key[0] )
        {
            printf ( " " );
            continue;
        }

        for ( j = 1; j < N_KEY; j++)
        {
            if ( decoder_cod.text[i] == decoder_cod.key[j] )
                printf ( "%c",  j + 'a' - 1 );
        }
    }
}


