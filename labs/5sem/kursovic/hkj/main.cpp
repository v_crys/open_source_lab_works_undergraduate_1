#include <stdio.h>
#include <stdlib.h>

void minimum ( int **mas )
{
    int N = 6;
    int i, j, k;
    int min_;

    for ( i = 0; i < N; i ++ )
    {
        for ( j = 0; j < N; j ++ )
        {
            min_ = 8000;
            for ( k = 0; k < N; k ++ )
            {

                if ( min_  > (mas[i][k] + mas[k][j]) )
                    min_ = (mas[i][k] + mas[k][j]);
            }
            printf( "%d ", min_ );
        }
        printf( "\n" );
    }
}

int main()
{
    int N = 6;
    int i, j;
    int **mas_1 = (int **)calloc( 6, sizeof(int *) );

    for ( i = 0; i< 6; i++ )
        mas_1[ i ] = (int *) calloc( 6, sizeof( int ) );

    FILE *IN;

    IN = fopen ( "in.txt", "r");

    for ( i = 0; i < N; i ++ )
    {
        for ( j = 0; j < N; j ++ )
        {
            fscanf( IN, "%d", &mas_1[i][j] );
        }
    }

    fclose( IN );

  //  minimum( mas_1 );
    return 0;

}
