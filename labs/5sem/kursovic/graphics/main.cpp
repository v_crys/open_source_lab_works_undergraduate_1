#include "pir4.h"
int main() //     ГЛАВНАЯ ФУНКЦИЯ MAIN() !!!
{
  int gdriver = DETECT, gmode, errorcode;
  initgraph(&gdriver, &gmode,"");
  errorcode = graphresult();
  if (errorcode != grOk){
     cout << "Graphics error: %s\n";
     getch();
     return 0;
  }
  setbkcolor(WHITE);
  pir4 pir;
  pir.Compute();
  pir.Compute_3();
  cleardevice();
  pir.DrawPix();              // вывести исходную фигуру
  while (1){
    char W = getche();
    system("CLS");
    switch (W){
      case 27:{
                return 0;}
      case 'a':{
             	pir.ShiftX -= 3;
                break;}
      case 'd':{
            	pir.ShiftX += 3;
                break;}
      case 'w':{
             	pir.ShiftY -= 3;
                break;}
      case 's':{
             	pir.ShiftY += 3;
                break;}
      case '+':{
              	pir.Scale += 0.1;
              	break;}
      case '-':{
                if (pir.Scale > 0.0)
                    pir.Scale -= 0.1;
                break;}
      case 'x':{
               if (pir.RotX > 0) // полный круг 360 градусов
                    pir.RotX -= 3;
               else
                    pir.RotX = 357;
               break;}
      case 'y':{
               if (pir.RotY < 357)
                    pir.RotY += 3;
               else
                    pir.RotY = 0;
               break;}
      case 'z':{
               if (pir.RotZ < 357)
                    pir.RotZ += 3;
               else
                    pir.RotZ = 0;
               break;}
      case 72:{
               pir.SMz=pir.SMz+3;
               break;}
      case 80:{
               pir.SMz=pir.SMz-3;
               break;}
      case 75:{
               pir.SMx=pir.SMx-3;
               break; }
      case 77: {
               pir.SMx=pir.SMx+3;
               break; }
    } //switch
    pir.Compute();
    pir.Compute_3();
    cleardevice();
    pir.DrawPix();    // рисуем
  }
  closegraph();
  return 0;
}
