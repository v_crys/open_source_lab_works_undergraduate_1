#ifndef PIR4_H_INCLUDED
#define PIR4_H_INCLUDED
#include <iostream>
#include <conio.h>
#include <graphics.h>
#include <cmath>
#include <cstdlib>
#define sizeP4 5
#define sizeP3 4
using namespace std;
class pir4{
    int pix[sizeP4][3];
    int newpix[sizeP4][3];
    int Tx,Ty,Tz;
    int nTx,nTy,nTz;
    int pix_3[sizeP3][3];
    int newpix_3[sizeP3][3];
    int Tx_3,Ty_3,Tz_3;
    int nTx_3,nTy_3,nTz_3;
public:
    int SMx=0,SMz=0;
    int RotX, RotY, RotZ;   // углы поворота по X,Y,Z в градусах
    int ShiftX, ShiftY;
    int ShiftX_3, ShiftY_3;     // сдвиг начала координат
    long double Scale;      // масштабирование
    pir4();
    void Compute_3();
    void DrawPix_os_3();
    void DrawPix_st_3(int k);
    float getMidZ_3(int k);
    void DrawPix_ten_3();
    void Compute();
    void DrawPix_os();
    void DrawPix_st(int k);
    float getMidZ(int k);
    void DrawPix_ten();
    void DrawPix();
    void V_FAB4R (int grn_pix, int new_pix, int x_isx, int y_isx);
};
pir4::pir4(){
    int bufpix[sizeP4][3] ={{-25,25,-25}, {25,25,-25}, {25,25,25}, {-25,25,25}, {0, -25, 0}};
    for (int i=0;i<sizeP4;i++){
        for (int j=0;j<3;j++){
            pix[i][j]=bufpix[i][j];
        }
    }
    Tx=0;
    Ty=25;
    Tz=50;
    RotX = 0;
    RotY = 0;
    RotZ = 0;
    ShiftX = 225;
    ShiftY = 225;
    Scale = 2.0;
    this->Compute();
    int bufpix1[sizeP3][3] ={{-25,25,-25}, {25,25,-25}, {0,25,25}, {0, -25, 0}};
    for (int i=0;i<sizeP3;i++){
        for (int j=0;j<3;j++){
            pix_3[i][j]=bufpix1[i][j];
        }
    }
    Tx_3=0;
    Ty_3=25;
    Tz_3=50;
    ShiftX_3 = 425;
    ShiftY_3 = 225;
    this->Compute_3();
}
void pir4::Compute(){
  double sinx = sin((RotX *M_PI) /180.0);
  double siny = sin((RotY *M_PI) /180.0);
  double sinz = sin((RotZ *M_PI) /180.0);
  double cosx = cos((RotX *M_PI) /180.0);
  double cosy = cos((RotY *M_PI) /180.0);
  double cosz = cos((RotZ *M_PI) /180.0);
  double x,y,z,x1,y1,z1;
  for (register int i = 0;i < sizeP4;i++)
  {
    x = pix[i][0];   //Исходное направление
    y = pix[i][1];
    z = pix[i][2];
    x1 = x*cosz - y*sinz;       // вокруг Z
    y1 = x*sinz + y*cosz;
    z1 = z;
    x = x1;                     // вокруг X
    y = y1*cosx + z1*sinx;
    z = -y1*sinx + z1*cosx;
    x1 = x*cosy + z*siny;       // вокруг Y
    y1 = y;
    z1 = -x*siny + z*cosy;
    newpix[i][0] = x1*Scale;
    newpix[i][1] = y1*Scale;
    newpix[i][2] = z1*Scale;
    newpix[i][0] += ShiftX;
    newpix[i][1] += ShiftY;
  }
    x = Tx+SMx;   //Исходное направление
    y = Ty;
    z = Tz+SMz;
    x1 = x*cosz - y*sinz;       // вокруг Z
    y1 = x*sinz + y*cosz;
    z1 = z;
    x = x1;                     // вокруг X
    y = y1*cosx + z1*sinx;
    z = -y1*sinx + z1*cosx;
    x1 = x*cosy + z*siny;       // вокруг Y
    y1 = y;
    z1 = -x*siny + z*cosy;
    nTx = x1*Scale;
    nTy = y1*Scale;
    nTz = z1*Scale;
    nTx += ShiftX;
    nTy += ShiftY;
}
void pir4::DrawPix_os(){
  register int i;
  int poly[10];
  setcolor(YELLOW);
  setfillstyle(1,YELLOW);
  for (i = 0; i < 4;i++)      // Рисует основание пирамиды
    {
      poly[i*2] = newpix[i][0];
      poly[i*2+1] = newpix[i][1];
    }
    poly[8]=poly[0];
    poly[9]=poly[1];
  fillpoly(5,poly);
}
void pir4::DrawPix_st(int k){
    register int i,j,m;
    int poly[8];
    setcolor(k+1);
    setfillstyle(1,k+1);
    poly[0]=newpix[4][0];
    poly[1]=newpix[4][1];
    m=0;
    for (i = k; i < k+2;i++){
      j = (i < 4) ? (i) : 0;
      poly[m*2+2] = newpix[j][0];
      poly[m*2+3] = newpix[j][1];
      m++;
    }
    poly[6]=poly[0];
    poly[7]=poly[1];
    fillpoly(4,poly);

}
float pir4::getMidZ(int k){
    float mid_z=0;
    int j, i;
    if (k==4){
        for (i = 0; i < 4;i++){
            j = (i < 4) ? (i) : 0;
            mid_z += newpix[j][2];
        }
        mid_z=mid_z/4;
    }
    else {
        for (i = k; i < k+2;i++){
            j = (i < 4) ? (i) : 0;
            mid_z += newpix[j][2];
        }
        mid_z+=newpix[4][2];
        mid_z=mid_z/3;
    }
    return mid_z;
}
void pir4::DrawPix_ten(){
    register int i,j,m;
    int poly[8];
    setcolor(DARKGRAY);
    setfillstyle(1,DARKGRAY);
    poly[0]=nTx;
    poly[1]=nTy;
    for (j=0;j<2;j++){
        m=0;
        for (i = 0; i < 2;i++){
            poly[2*m+2] = newpix[2*i+j][0];
            poly[2*m+3] = newpix[2*i+j][1];
            m++;
        }
        poly[6]=poly[0];
        poly[7]=poly[1];
        fillpoly(4,poly);
    }
}
void pir4::Compute_3(){
  double sinx = sin((RotX *M_PI) /180.0);
  double siny = sin((RotY *M_PI) /180.0);
  double sinz = sin((RotZ *M_PI) /180.0);
  double cosx = cos((RotX *M_PI) /180.0);
  double cosy = cos((RotY *M_PI) /180.0);
  double cosz = cos((RotZ *M_PI) /180.0);
  double x,y,z,x1,y1,z1;
  for (register int i = 0;i < sizeP3;i++)
  {
    x = pix_3[i][0];   //Исходное направление
    y = pix_3[i][1];
    z = pix_3[i][2];
    x1 = x*cosz - y*sinz;       // вокруг Z
    y1 = x*sinz + y*cosz;
    z1 = z;
    x = x1;                     // вокруг X
    y = y1*cosx + z1*sinx;
    z = -y1*sinx + z1*cosx;
    x1 = x*cosy + z*siny;       // вокруг Y
    y1 = y;
    z1 = -x*siny + z*cosy;
    newpix_3[i][0] = x1*Scale;
    newpix_3[i][1] = y1*Scale;
    newpix_3[i][2] = z1*Scale;
    newpix_3[i][0] += ShiftX_3;
    newpix_3[i][1] += ShiftY_3;
  }
    x = Tx_3+SMx;   //Исходное направление
    y = Ty_3;
    z = Tz_3+SMz;
    x1 = x*cosz - y*sinz;       // вокруг Z
    y1 = x*sinz + y*cosz;
    z1 = z;
    x = x1;                     // вокруг X
    y = y1*cosx + z1*sinx;
    z = -y1*sinx + z1*cosx;
    x1 = x*cosy + z*siny;       // вокруг Y
    y1 = y;
    z1 = -x*siny + z*cosy;
    nTx_3 = x1*Scale;
    nTy_3 = y1*Scale;
    nTz_3 = z1*Scale;
    nTx_3 += ShiftX_3;
    nTy_3 += ShiftY_3;
}
void pir4::DrawPix_os_3(){
  register int i;
  int poly[8];
  setcolor(YELLOW);
  setfillstyle(1,YELLOW);
  for (i = 0; i < 3;i++)      // Рисует основание пирамиды
    {
      poly[i*2] = newpix_3[i][0];
      poly[i*2+1] = newpix_3[i][1];
    }
    poly[6]=poly[0];
    poly[7]=poly[1];
  fillpoly(4,poly);
}
void pir4::DrawPix_st_3(int k){
    register int i,j,m;
    int poly[8];
    setcolor(k+1);
    setfillstyle(1,k+1);
    poly[0]=newpix_3[3][0];
    poly[1]=newpix_3[3][1];
    m=0;
    for (i = k; i < k+2;i++){
      j = (i < 3) ? (i) : 0;
      poly[m*2+2] = newpix_3[j][0];
      poly[m*2+3] = newpix_3[j][1];
      m++;
    }
    poly[6]=poly[0];
    poly[7]=poly[1];
  fillpoly(4,poly);

}
float pir4::getMidZ_3(int k){
    float mid_z=0;
    int j, i;
    if (k==3) {
        mid_z+=newpix_3[1][2];
        k--;
    }
    else  mid_z+=newpix_3[3][2];

    for (i = k; i < k+2;i++){
        j = (i < 3) ? (i) : 0;
        mid_z += newpix_3[j][2];
    }
    mid_z=mid_z/3;
    return mid_z;
}
void pir4::DrawPix_ten_3(){
    register int i,j,m;
    int poly[8];
    setcolor(DARKGRAY);
    setfillstyle(1,DARKGRAY);
    poly[0]=nTx_3;
    poly[1]=nTy_3;
    for (j=0;j<2;j++){
        m=0;
        for (i = 0; i < 2;i++){
            poly[2*m+2] = newpix_3[i+j][0];
            poly[2*m+3] = newpix_3[i+j][1];
            m++;
        }
        poly[6]=poly[0];
        poly[7]=poly[1];
        fillpoly(4,poly);
    }
}
void pir4::DrawPix(){
    float arr[2][sizeP4]={{0,1,2,3,4}, {getMidZ(0),getMidZ(1),getMidZ(2),getMidZ(3),getMidZ(4)}};
    float buf=0, bufi=0;
    for(int i=0; i<5; i++){
        for (int j=0; j<(4-i); j++){
            if (arr[1][j]<arr[1][j+1]) {
                buf=arr[1][j+1];
                bufi=arr[0][j+1];
                arr[1][j+1]=arr[1][j];
                arr[0][j+1]=arr[0][j];
                arr[1][j] = buf;
                arr[0][j] = bufi;
            }
        }
    }
    this->DrawPix_ten();
    for(int i=0;i<sizeP4;i++){
            if (arr[0][i]==4) this->DrawPix_os();
            else this->DrawPix_st(arr[0][i]);
    }
    float arr1[2][sizeP3]={{0,1,2,3}, {getMidZ_3(0),getMidZ_3(1),getMidZ_3(2),getMidZ_3(3)}};
    for(int i=0; i<sizeP3; i++){
        for (int j=0; j<(3-i); j++){
            if (arr1[1][j]<arr1[1][j+1]) {
                buf=arr1[1][j+1];
                bufi=arr1[0][j+1];
                arr1[1][j+1]=arr1[1][j];
                arr1[0][j+1]=arr1[0][j];
                arr1[1][j] = buf;
                arr1[0][j] = bufi;
            }
        }
    }
    this->DrawPix_ten_3();
    for(int i=0;i<sizeP3;i++){
        if (arr1[0][i]==3) this->DrawPix_os_3();
        else this->DrawPix_st_3(arr1[0][i]);
    }
}
void pir4::V_FAB4R (int grn_pix, int new_pix, int x_isx, int y_isx){
    if (getpixel (x_isx, y_isx) != grn_pix && getpixel (x_isx, y_isx) != new_pix){
      putpixel (x_isx, y_isx, new_pix);
      V_FAB4R (grn_pix, new_pix, x_isx+1, y_isx);
      V_FAB4R (grn_pix, new_pix, x_isx,   y_isx+1);
      V_FAB4R (grn_pix, new_pix, x_isx-1, y_isx);
      V_FAB4R (grn_pix, new_pix, x_isx,   y_isx-1);
    }
} /* V_FAB4R */
#endif // PIR4_H_INCLUDED
