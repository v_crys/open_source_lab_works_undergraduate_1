#include <iostream>
#include <stdio.h>
#include <Windows.h>
#include <conio.h>

const int N = 10;
void Input(int *mas);//ручной ввод
void Output(int *mas);
void Quest(int *mas);

int main() {
	setlocale(LC_ALL, "rus");
	int *mas;
	mas = new int[N];
	Input(mas);
	Quest(mas);
	Output(mas);
	_getch();
	return 0;
}

void Input(int *mas) {

	char *textov = "¬ведите массив из 10 чисел:";
	char *Format = "%s\n";
	char *forma = "%d";
	__asm {
		push textov
		push Format
		call printf
		add esp, 8
	}
	_asm {
		mov esi, mas
		mov ecx, N
		c :
		push ecx
		push esi
		push forma
		call scanf
		add esp, 8
		pop ecx
		add esi, 4
		loop c
	}
}

void Output(int *mas) {

	char *forma = "%4d ";
	_asm {
		mov esi, mas
		mov ecx, 10
		c :
		push ecx
		mov  eax, [esi]
		push eax
		push forma
		call printf
		add esp, 8
		pop ecx
		add esi, 4
		loop c
	}
}

	void Quest(int *mas)
	{
		int max = 1;
		_asm
		{
			mov esi, mas
			mov ebx, [esi]
			mov edi, esi
			inc esi
			inc esi
			inc esi
			inc esi

			mov ecx, 9
			mov edx, 2
			c:
			mov eax, [esi]
			cmp eax, ebx
			jle p
			mov edi, esi
			mov ebx, [edi]
			mov max, edx

				p:
				inc esi
				inc esi
				inc esi
				inc esi
				inc edx
				loop c

				mov esi, mas
				mov ecx, max
				cmp ecx, 1
				je p2
				dec ecx

					c1:
			mov edx, 0
				mov [esi], edx
				inc esi
				inc esi
				inc esi
				inc esi
				loop c1
				p2:
				mov ecx, 11
				sub ecx, max
				mov eax, [edi]
				c2:
					mov [esi],eax
					inc esi
					inc esi
					inc esi
					inc esi
					loop c2
		}
	}
