#include "p16f877A.inc"
    
    __CONFIG _HS_OSC & _WDTE_OFF & _PWRTE_OFF & _BOREN_ON & _LVP_OFF & _CPD_OFF & _WDT_OFF & _CP_OFF
    
ORG 0

NAPRAVL	EQU 20h
i EQU 21h
j EQU 22h
k EQU 23h

START:
	MOVLW 0
	BSF STATUS, RP0
	CLRF TRISD

	MOVLW 0f0
	MOVWF TRISB

	BCF STATUS, RP0
	
	BCF STATUS, C
	MOVLW	1
	MOVWF PORTD
	
	MOVLW 0f0
	MOVWF PORTB
	
	RUN:
		CALL DELAY1
	
		BTFSS PORTB, 4
		GOTO BUTTON
	
	SHIFT:
		BTFSC NAPRAVL, 0 
		GOTO LEFT
		
		GOTO RIGHT
		
		
	BUTTON:
		BTFSC PORTD, 3 
		GOTO MENYEM
		
		BTFSC PORTD, 4
		GOTO MENYEM
		
		GOTO SHIFT
	
	
	MENYEM:
		MOVF NAPRAVL
		XORLW 1
		MOVWF NAPRAVL
		GOTO SHIFT
		

	LEFT:
		RLF PORTD
		GOTO RUN
		
	RIGHT:
		RRF PORTD
		GOTO RUN
		
		
	DELAY1: 
	MOVLW 0f0
	MOVWF i 
	CALL LOOP
	RETURN
		
		
	LOOP: 
	CALL LOOP1
	MOVLW 0ff
	MOVWF j
        DECFSZ i,F 
	GOTO LOOP 
	RETURN
	
    LOOP1:
	CALL LOOP2
	MOVLW 0ff
	MOVWF k
	DECFSZ j, F
	GOTO LOOP1
	RETURN
	
    LOOP2:
	DECFSZ k, F
	GOTO LOOP2
	RETURN
END
		
;/*#include "p16f877A.inc"
;ORG 0
;START: BSF STATUS, RP0
		;MOVLW 0
		;CLRF TRISD
		
		;MOVLW 255
		;MOVWF TRISB
		
		;BCF STATUS, RP0
		
;MAIN_LOOP: MOVF PORTB, W
;			BTFSS PORTB, RB7
;			GOTO MAIN_LOOP
			
;			BSF PORTD, RD0
;			GOTO MAIN_LOOP
;			END*/
		
;#include "p16f877A.inc"
;
;; CONFIG
;; __config 0xFFFB
; __CONFIG _HS_OSC & _WDTE_OFF & _PWRTE_OFF & _BOREN_ON & _LVP_ON & _CPD_OFF & _WRT_OFF & _CP_OFF
;		
;ORG 0
 
		
;i EQU 20h
;j EQU 21h
;k EQU 22h
		
;START: 
	 ;  BSF STATUS, RP0
          ; CLRF TRISD
           ;CLRF TRISB
           ;BCF STATUS, RP0
	   ;CLRF PORTB
	   ;CLRF PORTD
	  
           ;BSF PORTB, RB4 
           ;BSF PORTB, RB5 
           ;BSF PORTB, RB6 
           ;BSF PORTB, RB7 
	   
	   ;BSF PORTD, 0
	   ;BCF STATUS, 0
;MAIN_LOOP:
 ;BTFSC PORTB, RB4
 ;GOTO B2
 ;RLF PORTD, 1
 ;CALL DELAY
 ;GOTO MAIN_LOOP
 
 ;B2:
 ;BTFSC PORTB, RB5
 ;GOTO B3
 ;RRF PORTD, 1
 ;CALL DELAY
 ;GOTO MAIN_LOOP
 
; B3:
 ;BTFSC PORTB, RB6
 ;GOTO B4
 ;RLF PORTD, 1
 ;CALL DELAY1
 ;GOTO MAIN_LOOP
  
 ;B4:
 ;BTFSC PORTB, RB7
 ;GOTO MAIN_LOOP
 ;RRF PORTD, 1
; CALL DELAY1
;
;GOTO MAIN_LOOP
; 
;    DELAY: 
;	MOVLW 20 
;	MOVWF i 
;	MOVLW 10
;	MOVWF j
;	MOVLW 10
;	MOVWF k
;
;	CALL LOOP
;	RETURN
;	
; 
; 
;    DELAY1: 
;	MOVLW 10
;	MOVWF i 
;	MOVLW 10
;	MOVWF j
;	MOVLW 10
;	MOVWF k
;
;	CALL LOOP
;	RETURN
; 
;	
;    LOOP: 
;	CALL LOOP1
;        DECFSZ i,F 
;	GOTO LOOP 
;	RETURN
;	
;    LOOP1:
;	CALL LOOP2
;	DECFSZ j, F
;	GOTO LOOP1
;	RETURN
;	
;    LOOP2:
;	DECFSZ k, F
;	GOTO LOOP2
;	RETURN
; END
 
 
; #include "p16f877A.inc"
;
;; CONFIG
; __CONFIG _HS_OSC & _WDTE_OFF & _PWRTE_OFF & _BOREN_ON & _LVP_OFF & _CPD_OFF & _WDT_OFF & _CP_OFF
;	
; i EQU 030h
; j EQU 031h
;ORG 0
; 
; BSF STATUS, RP0
; CLRF TRISA
; CLRF TRISD
; BCF STATUS, RP0
; 
; CLRF PORTA
; CLRF PORTD
; 
; 
; BCF STATUS, RP0 ;
;BCF STATUS, RP1 ; Bank0
;CLRF PORTA ; Initialize PORTA by
;; clearing output
;; data latches
;BSF STATUS, RP0 ; Select Bank 1
;MOVLW 06h ; Configure all pins
;MOVWF ADCON1; as digital inputs
;BCF TRISA, 0
; 
;BCF STATUS, RP0
; ; button to gnd
; CLRF i
; DEL:
;    INCF i
;    BTFSC i, 6h
;    GOTO DEL
;
;BSF STATUS, RP0
;BSF TRISA, 0
;BCF STATUS, RP0
;   
; LOOP_MAIN:
;    BTFSS PORTA, 0
;    GOTO LOOP_MAIN
;    
;    INF_LOOP:
;    BSF PORTD, 0
;    GOTO INF_LOOP
;    
; 
;DELAY: 
;	MOVLW 50
;	MOVWF i
;	MOVWF 50
;	MOVWF j
;	
;	LOOP:
;	CALL LOOP2
;        DECFSZ i,F 
;	GOTO LOOP 
;	RETURN
;    
;	LOOP2:
;	DECFSZ j,F 
;	GOTO LOOP2
;	RETURN
;	
;	
;	END
;    
;    