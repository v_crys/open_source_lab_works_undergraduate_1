n = input( 'Input order for the matrix: ' );
matrix = rand( n, n ) * 100;
[V,D,W] = eig(matrix);

fprintf( 'Matrix:\n' );
Print_table( matrix, n );

fprintf( 'Result:\n ' );
Print_table( V, n );