function [  ] = Print_table( V, n )
    for i = 1 : n
        for j = 1 : n
            fprintf( '\t%f', V( i, j ) );
        end
        fprintf( '\n' );
    end
end

