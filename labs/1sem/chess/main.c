#include <stdio.h>
#include <stdlib.h>
#include <windows.h>



#define FIELD_SIZE  5
#define MAX_POS     8


#define X           3
#define Y           3


#define FIELD_( var ) ( ( var >= 0 ) && ( var <= ( FIELD_SIZE - 1 ) ) )


struct vector{
    int x;
    int y;
};

void print_chess( int flag_clr, int vec_x, int vec_y );


int chess[ FIELD_SIZE ][ FIELD_SIZE ];
struct vector   moves[ MAX_POS ] = { { 1, -2}, {2, -1}, {2, 1}, {1, 2}, {-1, 2}, {-2, 1}, {-2, -1}, {-1, -2}};

int flag = 0;
HANDLE console;

int cnt_out = 0;

int main()
{
    console = GetStdHandle(STD_OUTPUT_HANDLE);

    int i, j;
    struct vector  location;

    location.x = X;
    location.y = Y;

    for ( i = 0; i < FIELD_SIZE; i++ )
        for ( j = 0; j < FIELD_SIZE; j++ )
            chess[ i ][ j ] = 0;

    chess[ location.x ][ location.y ] = 1;
    step( location );

    return 0;
}



void step( struct vector loc )
{
    static field = 0;

    int i, j, k;
    int buf = 0;
    int flag_call = 0;
    struct vector vec;


    field++;

    //--- finished?
    if ( field == (FIELD_SIZE * FIELD_SIZE) )
    {
            printf( "Print answer:\n");
            print_chess( 0, loc.x, loc.y );
            chess[ loc.x ][ loc.y ] = 0;
            flag = 1;
            return;
    }



    for ( i = 0; i < MAX_POS; i++ )
    {
        vec.x = loc.x + moves[ i ].x;
        vec.y = loc.y + moves[ i ].y ;

        if ( FIELD_( vec.x ) && FIELD_( vec.y) )
            if ( chess[ vec.x ][ vec.y ] == 0 )
            {
                chess[ vec.x ][ vec.y ] = 1;

                step( vec );


                if ( flag )
                {
                    chess[ loc.x ][ loc. y ] = 0;

                    printf( "\n" );
                    print_chess( 0, loc.x, loc.y );
                    cnt_out++;

                    if ( cnt_out > 9 ) { getch(); cnt_out = 0; }

                    field--;
                    return;
                }

                flag_call = 1;
            }
    }

    chess[ loc.x ][ loc.y ] = 0;

    field--;
    return;


}

void print_chess( int flag_clr, int vec_x, int vec_y )
{
    int i, j;


    if ( flag_clr )
        system( "cls" );
SetConsoleTextAttribute(console, 2);
    printf( "_____________\n" );
    for ( i = 0; i < FIELD_SIZE; i++ )
    {
        SetConsoleTextAttribute(console, 2);
        printf("| ");
        for ( j = 0; j < FIELD_SIZE; j++ )
        {
            if ( ( i == vec_x ) && ( j == vec_y ) )
                SetConsoleTextAttribute(console, 2);
            else
            {
                if ( chess[ i ][ j ] == 1 )
                    SetConsoleTextAttribute(console, 0xd);
                else
                    SetConsoleTextAttribute(console, 0xa);
            }

            switch ( chess[ i ][ j ] )
            {
           case 0:
                printf("_ ");
                break;

           case 1:
           //case 2:
                printf("K " );



            }
        }
        SetConsoleTextAttribute(console, 2);
        printf("|");


    printf( "\n" );

    }
    SetConsoleTextAttribute(console, 2);
    printf( "_____________\n" );
}


