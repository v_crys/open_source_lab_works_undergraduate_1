#include <stdio.h>
#include <stdlib.h>

int main()
{
    int *nomb;
    int *summ;
    int N, i;

    printf( "N = " );
    scanf( "%d", &N );

    nomb = ( int * ) calloc( N, sizeof( int ) );
    summ = ( int * ) calloc( N, sizeof( int ) );

    for ( i = 0; i < N; i++ )
    {
        scanf( "%d", &nomb[ i ] );
        scanf( "%d", &summ[ i ] );
    }

    for ( i = 0; i < N; i++ )
    {
        printf( "%d - %d\n", *nomb++, *summ++ );

    }
}
