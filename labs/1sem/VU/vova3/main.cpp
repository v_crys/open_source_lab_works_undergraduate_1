#include <iostream>

using namespace std;

//-------------- Hotel
class Hotel {
    private:
        std::string name;
        int stars;

    public:
        Hotel( std::string name_i, int stars_i )
        {
            name    = name_i;
            stars   = stars_i;
        }
        Hotel()
        {
            name    = "Default";
            stars   = 1;
        }

        void show();
        void Set( std::string name_i, int stars_i );
        std::string get_name();
        int get_stars();
};

void Hotel::show()
{
    std::cout << "\n\tHotel: " << name;
    std::cout << "\tStars: " << stars;
    return;
}

void Hotel::Set( std::string name_i, int stars_i )
{
    name    =   name_i;
    stars   =   stars_i;
    return;
}

int Hotel::get_stars()
{
    return stars;
}

std::string Hotel::get_name()
{
    return name;
}

//--------------- Bus
class Bus {
    private:
        int N_BUS;
        int cost;

    public:
        Bus( int N_BUS_i, int cost_i )
        {
            N_BUS   =   N_BUS_i;
            cost    =   cost_i;
        }
        Bus( )
        {
            N_BUS   =   100;
            cost    =   10;
        }

        void show();
        void Set( int N_BUS_i, int cost_i );
        int get_N();
        int get_cost();
};

void Bus::show()
{
    std::cout << "\n\tBus number: " << N_BUS;
    std::cout << "\tCost bus: " << cost;
    return;
}

void Bus::Set( int N_BUS_i, int cost_i )
{
    N_BUS   =   N_BUS_i;
    cost    =   cost_i;
    return;
}

int Bus::get_N()
{
    return N_BUS;
}

int Bus::get_cost()
{
    return cost;
}


//--------------- Tour
class Tour {
    private:
            std::string Country;
            std::string City;

            int         cnt_hotel;
            Hotel       Hotels[ 10 ];
            Bus         buss;

            int         level_excursion;
            int         level_servic;
            unsigned int cost;

    public:
            //----- methods sets
            void set_country            ( std::string Country_i );
            void set_city               ( std::string City_i );
            void set_cnt_hotel          ( int cnt );
            void set_hotel              ( int n_hotel, std::string name_hot, int stars_hot );
            void set_buss               ( int n_bus, int cost_bus );
            void set_level_excursion    ( int level );
            void set_level_servic       ( int level );
            void set_cost               ( int cost_i );

            //------- methods get
            std::string     get_country( );
            std::string     get_city( );
            int             get_cnt_hotel( );
            Hotel           get_hotel( int n_hotel );
            Bus             get_buss( );
            int             get_level_excursion( );
            int             get_level_servic( );
            int             get_cost( );

            //-------- methods show
            void show( );

            void show_hotel();

};

//----- methods sets
void Tour::set_country            ( std::string Country_i )
{
    Country =   Country_i;
    return;
}

void Tour::set_city               ( std::string City_i )
{
    City    =   City_i;
    return;
}

void Tour::set_cnt_hotel          ( int cnt )
{
    cnt_hotel   =   cnt;
    return;
}

void Tour::set_hotel              ( int n_hotel, std::string name_hot, int stars_hot )
{
    if ( n_hotel > cnt_hotel ) return;

    Hotels[ n_hotel ].Set( name_hot, stars_hot );
    return;
}

void Tour::set_buss               ( int n_bus, int cost_bus )
{
    buss.Set( n_bus, cost_bus );
    return;
}

void Tour::set_level_excursion    ( int level )
{
    level_excursion =   level;
    return;
}

void Tour::set_level_servic       ( int level )
{
    level_servic    =   level;
    return;
}

void Tour::set_cost               ( int cost_i )
{
    cost    =   cost_i;
    return;
}

//----------- get methods
std::string     Tour::get_country( )
{
        return Country;
}

std::string     Tour::get_city( )
{
        return City;
}

int             Tour::get_cnt_hotel( )
{
        return cnt_hotel;
}

Hotel           Tour::get_hotel( int n_hotel )
{
        if ( n_hotel > cnt_hotel ) return Hotels[ 0 ];

        return Hotels[ n_hotel ];
}

Bus             Tour::get_buss( )
{
        return buss;
}

int             Tour::get_level_excursion( )
{
        return level_excursion;
}

int             Tour::get_level_servic( )
{
        return level_servic;
}

int             Tour::get_cost( )
{
        return cost;
}

//-------------- show
void Tour::show()
{
    int i;

    std::cout << "\n--------- Print Tour ---------\n";
    std::cout << "\tCountry: " << Country << "\n";
    std::cout << "\tCity: " << City << "\n";
    std::cout << "--------------------------------\n";
    std::cout << "\tCnt hotels: " << cnt_hotel << "\n";

    for ( i = 0; i < cnt_hotel; i++ )
        Hotels[ i ].show();

    std::cout << "\n--------------------------------\n";
    buss.show();

    std::cout << "\n--------------------------------\n";
    std::cout << "\tLevel excursion: " << level_excursion << "\n";
    std::cout << "\tLevel servic: " << level_servic << "\n";
    std::cout << "\t\tCost: " << cost << "\n";

    return;
}

void Tour::show_hotel()
{
    int i;

    for ( i = 0; i < cnt_hotel; i++ )
        Hotels[ i ].show();

    return;
}






//*****************************************************************
//*****************************************************************
//*****************************************************************

#define N   10

int main()
{
    Tour    tours[ N ];
    int i, j;
    int cnt_tours;

    std::string str_buf;
    int         i_buf, i_buf2;



    //--------------- write
    std::cout << "Print cnt tours: ";
    std::cin >> cnt_tours;


    for ( i = 0; i < cnt_tours; i++ )
    {
        std::cout << "\n---------------------\n" ;
        std::cout << "Print Tour: " << i;

        std::cout << "\nWrite Country: ";
        std::cin >> str_buf;
        tours[ i ].set_country( str_buf );

        std::cout << "Write City: ";
        std::cin >> str_buf;
        tours[ i ].set_city( str_buf );

        std::cout << "Write hotels: ";
        std::cin >> i_buf;
        tours[ i ].set_cnt_hotel( i_buf );

        for ( j = 0; j < tours[ i ].get_cnt_hotel(); j++ )
        {
            std::cout << "Write Hotel: ";
            std::cin >> str_buf;

            std::cout << "Write stars: ";
            std::cin >> i_buf;

            tours[ i ].set_hotel( j, str_buf, i_buf );
        }

        std::cout << "Write bus number: ";
        std::cin >> i_buf;

        std::cout << "Write bus cost: ";
        std::cin >> i_buf2;
        tours[ i ].set_buss( i_buf, i_buf2 );


        std::cout << "Write level excursion: ";
        std::cin >> i_buf;
        tours[ i ].set_level_excursion( i_buf );

        std::cout << "Write level service: ";
        std::cin >> i_buf;
        tours[ i ].set_level_servic( i_buf );

        std::cout << "\nCost: ";
        std::cin >> i_buf;
        tours[ i ].set_cost( i_buf );

    }

    //------------------ print
    for ( i = 0; i < cnt_tours; i++ )
        tours[ i ].show();


    int state = 0;

    while ( 1 )
    {
        std::cout << "\n\n\n\n-------------- Menu --------------\n" ;
        std::cout << "\t0 - Print tour around the country\n" ;
        std::cout << "\t1 - Print tour around the country + cost\n" ;
        std::cout << "\t2 - Print hotel in city\n" ;
        std::cout << "\t3 - Exit\n" ;

        std::cin >> state;

        switch ( state )
        {
            case 0:
                std::cout << "\nMod 1\nPrint Country: ";
                std::cin >> str_buf;

                for ( i = 0; i < cnt_tours; i++ )
                    if ( !tours[ i ].get_country().compare( str_buf ) )
                        tours[ i ].show();
                break;

            case 1:
                std::cout << "\nMod 2\nPrint Country: ";
                std::cin >> str_buf;

                std::cout << "\nPrint cost: ";
                std::cin >> i_buf;

                for ( i = 0; i < cnt_tours; i++ )
                    if ( !tours[ i ].get_country().compare( str_buf ) )
                        if ( tours[ i ].get_cost() < i_buf )
                            tours[ i ].show();
                break;

            case 2:
                std::cout << "\nMod3\nPrint City: ";
                std::cin >> str_buf;

                for ( i = 0; i < cnt_tours; i++ )
                    if ( !tours[ i ].get_city().compare( str_buf ) )
                        tours[ i ].show_hotel();
                break;

            case 3: return 0;

        }
    }



    return 0;
}
