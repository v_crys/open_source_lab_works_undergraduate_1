#include <stdio.h>
#include <stdlib.h>

int main()
{
    int *arr, *buf_arr, *buf_arr2;
    int N, i;

    printf( "N = " );
    scanf( "%d", &N );

    arr = ( int * ) calloc( N, sizeof( int ) );
    buf_arr = arr + 1;
    buf_arr2 = arr;

    for ( i = 0; i < N; i++ )
        scanf( "%d", arr++ );

    arr--;
    while ( *arr & 1 ) arr--;
    //arr++;

    for ( i = 1; i < N - 1; i += 1 )
    {
        if ( *buf_arr & 1 )
            *buf_arr += *arr;
        buf_arr++;
    }

    printf( "\n" );
    for ( i = 0; i < N; i++ )
        printf( "%d\n", *buf_arr2++ );
}
