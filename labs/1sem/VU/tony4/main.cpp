#include <iostream>

using namespace std;


class Video_Film
{
    private:
        string  Name;
        string  Studio;
        string  Genre;
        int     Year;
        string  Directed;
        int     actors_cnt;
        string  Actors[ 5 ];

    public:
        //------- set methods
        void set_name       ( string name_i );
        void set_studio     ( string studio_i );
        void set_genre      ( string genre_i );
        void set_year       ( int year_i );
        void set_directed   ( string directed_i );

        void set_actors_cnt ( int cnt );
        void set_actors     ( int i, string actors );

        //-------- get methods
        string get_name     ();
        string get_studio   ();
        string get_genre    ();
        int    get_year     ();
        string get_directed ();

        int    get_actors_cnt ();
        string get_actors     ( int i );

        //-------- show
        void    show();
        void    show_act();
};

//------- set methods
void Video_Film::set_name       ( string name_i )
{
    Name    =   name_i;
    return;
}

void Video_Film::set_studio     ( string studio_i )
{
    Studio  =   studio_i;
    return;
}

void Video_Film::set_genre      ( string genre_i )
{
    Genre   =   genre_i;
    return;
}

void Video_Film::set_year       ( int year_i )
{
    Year    =   year_i;
    return;
}

void Video_Film::set_directed   ( string directed_i )
{
    Directed    =   directed_i;
    return;
}

void Video_Film::set_actors_cnt ( int cnt )
{
    actors_cnt  =   ( cnt > 4 ) ? 5 : cnt;
    return;
}

void Video_Film::set_actors     ( int i, string actors )
{
    if ( i >= actors_cnt ) return;

    Actors[ i ] =   actors;
    return;
}


//-------- get methods
string Video_Film::get_name     ()
{
        return Name;
}

string Video_Film::get_studio   ()
{
        return Studio;
}

string Video_Film::get_genre    ()
{
        return Genre;
}

int    Video_Film::get_year     ()
{
        return Year;
}

string Video_Film::get_directed ()
{
        return Directed;
}

int    Video_Film::get_actors_cnt ()
{
    return actors_cnt;
}

string Video_Film::get_actors     ( int i )
{
    if ( i > actors_cnt ) return Actors[ 0 ];

    return Actors[ i ];
}

//-------- show
void    Video_Film::show()
{
    int i;

    cout << "--------- Show All data about Video Film -----------\n";
    cout << "\tName: "          << Name;
    cout << "\n\tStudio: "      << Studio;
    cout << "\n\tGenre: "       << Genre;
    cout << "\n\tYear: "        << Year;
    cout << "\n\tDirected: "    << Directed;

    cout << "\n----------- Actors -------------\n";
    for ( i = 0; i < actors_cnt; i++ )
        cout << "\t" << Actors[ i ];

    cout << "\n";
    return;
}

void    Video_Film::show_act()
{
    int i;
    cout << "--------- Show actors in film -----------\n";
    cout << "\tName: "          << Name;
    cout << "\n\tActors:\n";

    for ( i = 0; i < actors_cnt ; i++ )
        cout << "\n\t\t" << Actors[ i ];

    cout << "\n";
    return;
}


//******************************************************************
//******************************************************************
//******************************************************************

#define N   10

int main()
{
    Video_Film  film[ N ];
    int cnt_film;

    int buf_i;
    string buf_str;
    int i, j;

    cout << "Print cnt film: ";
    cin >> cnt_film;

    for ( i = 0; i < cnt_film; i++ )
    {
        cout << "\n-----------------------------\n" ;
        cout << "Film: " << i;

        cout << "\n Write Name: ";
        cin >> buf_str;
        film[ i ].set_name( buf_str );

        cout << "\n Write Studio: ";
        cin >> buf_str;
        film[ i ].set_studio( buf_str );

        cout << "\n Write Genre: ";
        cin >> buf_str;
        film[ i ].set_genre( buf_str );

        cout << "\n Write Year: ";
        cin >> buf_i;
        film[ i ].set_year( buf_i );

        cout << "\n Write Directed: ";
        cin >> buf_str;
        film[ i ].set_directed( buf_str );

        cout << "\n Write actors cnt: ";
        cin >> buf_i;
        film[ i ].set_actors_cnt( buf_i );

        for ( j = 0; j < buf_i; j++  )
        {
            cin >> buf_str;
            film[ i ].set_actors( j, buf_str );
        }
    }


    int state = 0;

    while ( 1 )
    {
        std::cout << "\n\n\n\n-------------- Menu --------------\n" ;
        std::cout << "\t0 - Print films (now year)\n" ;
        std::cout << "\t1 - Print films (with actors)\n" ;
        std::cout << "\t2 - Print films (studio)\n" ;
        std::cout << "\t3 - Exit\n" ;

        std::cin >> state;

        switch ( state )
        {
            case 0:
                std::cout << "\nMod 1\nPrint year: ";
                std::cin >> buf_i;


                for ( i = 0; i < cnt_film; i++ )
                    if ( film[ i ].get_year() == buf_i )
                        film[ i ].show();
                break;

            case 1:
                std::cout << "\nMod 2\nPrint actors: ";
                std::cin >> buf_str;

                char flag;
                for ( i = 0; i < cnt_film; i++ )
                {
                    flag = 0;
                    for ( j = 0 ; j < film[ i ].get_actors_cnt(); j++ )
                        if ( !film[ i ].get_actors( j ).compare( buf_str ) )
                        {
                            flag = 1;
                            break;
                        }

                    if ( flag )
                        film[ i ].show_act();
                }
                break;

            case 2:
                std::cout << "\nMod3\nPrint studio: ";
                std::cin >> buf_str;

                for ( i = 0; i < cnt_film; i++ )
                    if ( !film[ i ].get_studio().compare( buf_str ) )
                        film[ i ].show_act();
                break;

            case 3: return 0;

        }
    }



    return 0;
}
