#include <iostream>

using namespace std;


class Phone
{
    private:
        string Famili;
        string Name;
        string Otchestvo;
        string Address;
        string Number;
        int    Time_internal;
        int    Time_external;

    public:
        //----------- set methods
        void set_familia    ( string famili_i );
        void set_name       ( string name_i );
        void set_otchestvo  ( string otchestvo_i );
        void set_address    ( string address_i );
        void set_number     ( string number_i );
        void set_time_internal ( int time );
        void set_time_external  ( int time );

        //------------ get methods
        string get_familia    ( );
        string get_name       ( );
        string get_otchestvo  ( );
        string get_address    ( );
        string get_number     ( );
        int get_time_internal ( );
        int get_time_external ( );

        //-------------- show
        void    show();
};

//----------- set methods
void Phone::set_familia    ( string famili_i )
{
    Famili  =   famili_i;
    return;
}

void Phone::set_name       ( string name_i )
{
    Name    =   name_i;
    return;
}

void Phone::set_otchestvo  ( string otchestvo_i )
{
    Otchestvo   =   otchestvo_i;
    return;
}

void Phone::set_address    ( string address_i )
{
    Address     =   address_i;
    return;
}

void Phone::set_number     ( string number_i )
{
    Number  =   number_i;
    return;
}

void Phone::set_time_internal ( int time )
{
    Time_internal   =   time;
    return;
}

void Phone::set_time_external  ( int time )
{
    Time_external   =   time;
    return;
}

//------------ get methods
string Phone::get_familia    ( )
{
    return Famili;
}

string Phone::get_name       ( )
{
    return Name;
}

string Phone::get_otchestvo  ( )
{
    return Otchestvo;
}

string Phone::get_address    ( )
{
    return Address;
}

string Phone::get_number        ( )
{
    return Number;
}

int Phone::get_time_internal ( )
{
    return Time_internal;
}

int Phone::get_time_external ( )
{
    return Time_external;
}

//-------------- show
void    Phone::show()
{
    cout << "\n----------------------------\n";
    cout << "-------- Show abonent----------\n";
    cout << "\tFamilia: " << Famili;
    cout << "\n\tName: " << Name;
    cout << "\n\tOtchestvo: " << Otchestvo;
    cout << "\n\tAddress: " << Address;
    cout << "\n\tNumber: " << Number;
    cout << "\n\tTime internal: " << Time_internal;
    cout << "\n\tTime external: " << Time_external;
    cout << "\n";
    return;
}

#define N 10

int main()
{
    Phone phones[ N ];
    int    cnt_phones;
    int i, j;

    int   alf[ N ];

    string  buf_str;
    int     buf_i;

    cout << "Print cnt phone: ";
    cin >> cnt_phones;

    for ( i = 0; i < cnt_phones; i++ )
    {
        cout << "\n---------------------------\n";
        cout << "Phones: " << i;

        cout << "\n\tFamilia:";
        cin >> buf_str;
        phones[ i ].set_familia( buf_str );

        cout << "\tName:";
        cin >> buf_str;
        phones[ i ].set_name( buf_str );

        cout << "\tOtchectvo:";
        cin >> buf_str;
        phones[ i ].set_otchestvo( buf_str );

        cout << "\tAddress:";
        cin >> buf_str;
        phones[ i ].set_address( buf_str );

        cout << "\tNumber:";
        cin >> buf_str;
        phones[ i ].set_number( buf_str );

        cout << "\tTime internal:";
        cin >> buf_i;
        phones[ i ].set_time_internal( buf_i );

        cout << "\tTime external:";
        cin >> buf_i;
        phones[ i ].set_time_external( buf_i );
    }

    int state = 0;

    while ( 1 )
    {
        std::cout << "\n\n\n\n-------------- Menu --------------\n" ;
        std::cout << "\t0 - Print abonents (internal time)\n" ;
        std::cout << "\t1 - Print abonents (external time)\n" ;
        std::cout << "\t2 - Print abonents (alfavit)\n" ;
        std::cout << "\t3 - Exit\n" ;

        std::cin >> state;

        switch ( state )
        {
            case 0:
                std::cout << "\nMod 1\nPrint internal time: ";
                std::cin >> buf_i;


                for ( i = 0; i < cnt_phones; i++ )
                    if ( phones[ i ].get_time_internal() > buf_i )
                        phones[ i ].show();
                break;

            case 1:
                std::cout << "\nMod 2\n";

                for ( i = 0; i < cnt_phones; i++ )
                    if ( phones[ i ].get_time_external() != 0 )
                        phones[ i ].show();
                break;

            case 2:
                std::cout << "\nMod3\n ";
                for ( i = 0; i < cnt_phones; i++ )
                    alf[ i ] = 1;


                for ( i = 0; i < cnt_phones; i++)
                {
                    buf_str = "ZZZZZZZZZ";
                    for ( j = 0; j < cnt_phones; j++ )
                    {
                        if ( alf[ j ] )
                        if ( phones[ j ].get_familia().compare( buf_str ) < 0 )
                        {
                            buf_str = phones[ j ].get_familia();
                            buf_i   =   j;
                        }
                    }

                    alf[ buf_i ] = 0;
                    phones[ buf_i ].show();
                }

                break;

            case 3: return 0;

        }
    }




    return 0;
}
