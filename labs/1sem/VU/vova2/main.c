#include <stdio.h>
#include <stdlib.h>

int main()
{
    char *arr, *buf_arr;
    int N;
    int i;

    printf( "Print N: " );
    scanf( "%d", &N );

    arr = ( char * ) calloc( N + 1, sizeof( char ) );
    buf_arr = arr;
    for ( i = 0; i < N; i++ )
        scanf( "%d", arr++ );



    while( buf_arr != arr )
    {
        *arr = *(arr - 1);
        arr--;
    }


    arr[ 0 ] = arr[ N ];


    printf( "\n\nOut:\n");
    for ( i = 0; i < N; i++ )
        printf( "%d\n", *arr++ );
}
