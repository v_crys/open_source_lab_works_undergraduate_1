#include <stdio.h>
#include <stdlib.h>

int read( FILE *fi, char *str );

int main()
{
    char *main_str;
    int i, j;

    FILE *fi = fopen( "test.c", "rt" );
    if ( !fi )
    {
        printf( "ERROR" );
        return 1;
    }

    i = read( fi, main_str );
    for ( j = 0; j < i; j++ )
        putchar( main_str[ j ] );
    //printf( "%s", main_str );

    return 0;
}

int read( FILE *fi, char *str )
{
    int c;
    int size = 0;

    str = ( char * ) calloc( 1, sizeof( char ) );
    while ( ( c = getc( fi ) ) != EOF )
    {
        str = realloc( str, ( size + 1 ) * sizeof( char ) );
        str[ size ] = c;
        size++;
    }

    return size;
}
