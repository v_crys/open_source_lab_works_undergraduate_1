#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define MAX_STR        100
#define MAX_LINE_STR   1000

int main ()
{
  int i, j;
  char *arr [ MAX_STR ];
  int arr_size = 0;
  int c = 0;
    FILE *fi = fopen( "in.txt", "rt" );

  do
    {
      arr[ arr_size ] = ( char* )calloc( MAX_LINE_STR, sizeof( char ) );
      i = 0;

      while ( ( c = getc ( fi ) ) != '\n')
      {
          if ( c == EOF ) break;
           arr[ arr_size ][ i++ ] = c;
      }

      arr[ arr_size ][ i ] = 0;
      arr_size ++;
    } while ( c != EOF );

  for ( i = 0; i < arr_size; i++)
  {
      if ( arr[ i ][ 100 ] == 100 )
         continue;

      for ( j = i + 1; j < arr_size; j++ )
      {
         if ( strcmp ( arr[ i ], arr[ j ] ) == 0 )
            arr[ j ][ 100 ] = 100;
      }
  }

  FILE *fp = fopen ( "out.txt", "wt");

    for ( i = 0; i < arr_size; i++)
    {
        if ( arr[ i ][ 100 ] != 100)
        {
            for ( j = 0; arr[i][j] != 0; j++ )
               fputc ( arr[ i ][ j ], fp);
               fputc( '\n', fp );
        }

    }
    fclose (fp);
    fclose (fi);

    return 0;
}










