#include <stdio.h>
#include <stdlib.h>

#define CNT_FERZ    8

void set_f( int **table, char x, char y );



int cnt = 0;

int main()
{
    int    *table[ 8 ];
    int     i, j;

    for ( i = 0; i < 8; i++ )
        table[ i ]  =   ( int * )calloc( 8, sizeof( int ) );

    for ( i = 0; i < 8; i++ )
        for ( j = 0; j < 8; j++ )
            table[ i ][ j ] =   0;

    table[ 0 ][ 0 ] = 2;
    set_f( table, 0, 0 );

    return 0;
}

void set_f( int **table, char x, char y )
{
    int i, j;
    cnt++;

    //--------- create new table
    int *table2[ 8 ];
    for ( i = 0; i < 8; i++ )
        table2[ i ]  =   calloc( 8, sizeof( int ) );

    //---------- if finish
    if ( cnt == CNT_FERZ )
    {
        table[ x ][ y ] =   2;
        for ( i = 0; i < 8; i++ )
        {
            for ( j = 0; j < 8; j++ )
                printf( "%d ", table[ i ][ j ] );

            printf( "\n" );
        }
        goto exit_func;
    }


    //------------ copy old table in new table
    for ( i = 0; i < 8; i++ )
        for ( j = 0; j < 8 ; j++ )
            table2[ i ][ j ] = table[ i ][ j ];

    table2[ x ][ y ]    =   2;

    //------ verify and set bit field
    for ( i = 0; i < 8; i++ )
    {
        if ( i != x )
        if ( table2[ i ][ y ] == 2 )
            goto exit_func;
        else
            table2[ i ][ y ] =   1;

        if ( i != y )
        if ( table2[ x ][ i ] == 2 )
            goto exit_func;
        else
            table2[ x ][ i ] =   1;

        if ( ( i != 0 ) && ( ( x + i ) < 8 ) && ( ( x + i ) >= 0 ) && ( ( y + i ) < 8 ) && ( ( y + i ) >= 0 ))
        {
            if ( table2[ x + i ][ y + i ] == 2 )
                goto exit_func;
            else
                table2[ x + i ][ y + i ] =   1;
        }

        if ( ( i != 0 ) && ( ( x - i ) < 8 ) && ( ( x - i ) >= 0 ) && ( ( y - i ) < 8 ) && ( ( y - i ) >= 0 ))
        {
            if ( table2[ x - i ][ y - i ] == 2 )
                goto exit_func;
            else
                table2[ x - i ][ y - i ] =   1;
        }


        if ( ( i != 0 ) && ( ( x + i ) < 8 ) && ( ( x + i ) >= 0 ) && ( ( y - i ) < 8 ) && ( ( y - i ) >= 0 ))
        {
            if ( table2[ x + i ][ y - i ] == 2 )
                goto exit_func;
            else
                table2[ x + i ][ y - i ] =   1;
        }

        if ( ( i != 0 ) && ( ( x - i ) < 8 ) && ( ( x - i ) >= 0 ) && ( ( y + i ) < 8 ) && ( ( y + i ) >= 0 ))
        {
            if ( table2[ x - i ][ y + i ] == 2 )
                goto exit_func;
            else
                table2[ x - i ][ y + i ] =   1;
        }
    }


    for ( i = 0; i < 8 ; i++ )
        for ( j = 0; j < 8; j++ )
        {
            if ( table2[ i ][ j ] == 0 )
                set_f( table2, i, j);

            if ( cnt == CNT_FERZ ) goto exit_func;
        }

    exit_func:;
    for ( i = 0; i < 8 ; i++ )
        free( table2[ i ] );

    if ( cnt != CNT_FERZ )
        cnt--;

    return;
}
