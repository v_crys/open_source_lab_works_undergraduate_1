#include <stdio.h>
#include <stdlib.h>

void next_hod( int **board, int x, int y, int fig);
int verify_wh( int **table, int x, int y );
int verify_bl( int **table, int x, int y );
/*
 1 - white korol
 2 - black ferz
 3 - black korol
 */

 int coord_x[ 8 ] = { -1, 0, 1, 1, 1, 0, -1, -1 };
 int coord_y[ 8 ] = { 1, 1, 1, 0, -1, -1, -1, 0 };

 int stop = 0;

int main()
{
    int *board[ 8 ];
    int i, j;



    for ( i = 0; i < 8; i++)
        board[ i ]  =   (int *) calloc( 8, sizeof( int ));

    for ( i = 0; i < 8; i++ )
        for ( j = 0; j < 8; j++ )
            board[ i ][ j ] = 0;

    board[ 3 ][ 3 ] =   1;
    board[ 4 ][ 5 ] =   2;
    board[ 4 ][ 4 ] =   3;


    next_hod( board, 3, 3, 1);
    return 0;
}

void next_hod( int **board, int x, int y, int fig )
{
    int *board2[ 8 ];
    int i, j;

    int old_cell;

    for ( i = 0; i < 8; i++)
        board2[ i ]  =   (int *) calloc( 8, sizeof( int ));

    for ( i = 0; i < 8; i++ )
        for ( j = 0; j < 8; j++ )
            board2[ i ][ j ] = board[ i ][ j ];

    //------ show
    system( "cls" );
    printf( "%d\n", fig );
    for ( i = 0; i < 8; i++ )
    {
        for ( j = 0; j < 8; j++ )
            printf( "%d ", board2[ i ][ j ] );

        printf( "\n" );
    }

    if ( fig == 1 )
    {
        for ( i = 0; i < 8; i++ )
            if ( verify_wh( board2, x + coord_x[ i ], y + coord_y[ i ] ) )
            {
                old_cell = board2[ x ][ y ] ;
                board2[ x ][ y ]    =   0;

                board2[ x + coord_x[ i ]][ y + coord_y[ i ]] =    old_cell;

                //if ( stop ) goto finish;
                next_hod( board2, x + coord_x[ i ], y + coord_y[ i ], 0 );

                board2[ x ][ y ] = old_cell;
                board2[ x + coord_x[ i ]][ y + coord_y[ i ]] = 0;
            }


        goto finish;
    } else {

        for ( i = 0; i < 8; i++ )
            if ( verify_bl( board2, x + coord_x[ i ], y + coord_y[ i ] ) )
            {

                old_cell = board2[ x ][ y ];
                board2[ x + coord_x[ i ]][ y + coord_y[ i ]] =    old_cell;
                board2[ x ][ y ] =   0;

              //  if ( stop ) goto finish;

                next_hod( board2, x + coord_x[ i ], y + coord_y[ i ], 1 );

                board2[ x ][ y ] = old_cell;
                board2[ x + coord_x[ i ]][ y + coord_y[ i ]] = 0;
            }
        goto finish;
    }

    finish:;
    for ( i = 0; i < 8; i++)
        free( board2 [ i ] );


    return;
}

int verify_wh( int **table, int x, int y )
{
    int i, j;

    if ( table[ x ][ y ] != 0) return 0;

    for ( i = 0; i < 8; i++ )
    {
        if ( table[ x + coord_x[ i ] ][ y + coord_y[ i ]] == 3 )
            return 0;
    }

    for ( i = 0; i < 8; i++ )
    {
        if ( table[ i ][ y ] == 2 )
            return 0;

        if ( table[ x ][ i ] == 2 )
            return 0;

        if ( ( i != 0 ) && ( ( x + i ) < 8 ) && ( ( x + i ) >= 0 ) && ( ( y + i ) < 8 ) && ( ( y + i ) >= 0 ))
            if ( table[ x + i ][ y + i ] == 2 )
                return 0;

        if ( ( i != 0 ) && ( ( x - i ) < 8 ) && ( ( x - i ) >= 0 ) && ( ( y - i ) < 8 ) && ( ( y - i ) >= 0 ))
            if ( table[ x - i ][ y - i ] == 2 )
                return 0;

        if ( ( i != 0 ) && ( ( x + i ) < 8 ) && ( ( x + i ) >= 0 ) && ( ( y - i ) < 8 ) && ( ( y - i ) >= 0 ))
            if ( table[ x + i ][ y - i ] == 2 )
                return 0;

        if ( ( i != 0 ) && ( ( x - i ) < 8 ) && ( ( x - i ) >= 0 ) && ( ( y + i ) < 8 ) && ( ( y + i ) >= 0 ))
            if ( table[ x - i ][ y + i ] == 2 )
                return 0;
    }

    return 1;
}

int verify_bl( int **table, int x, int y )
{
        int i, j;

    if ( table[ x ][ y ] != 0) return 0;

    for ( i = 0; i < 8; i++ )
    {
        if ( table[ x + coord_x[ i ] ][ y + coord_y[ i ]] == 1 )
            return 0;
    }

    return 1;
}
