/*
 *  Name project:   Lab2
 *  Generated:      15/10/2016
 *
 *  Description:
 *
 */

#ifndef DB_PRINT_H_INCLUDED
#define DB_PRINT_H_INCLUDED

    #include <stdio.h>

    #include "DB_print.c"

    void print_DB( Product *p_prod, unsigned int categ, unsigned int p_prod_size );
    void print_DB_alf( Product *p_prod, unsigned int categ, unsigned int p_prod_size );

#endif // DB_PRINT_H_INCLUDED
