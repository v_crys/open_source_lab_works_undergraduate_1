//#include "selection_product.h"
#include <stdio.h>

void select_prod( Product *p_prod, unsigned char *p_prod_set, unsigned int p_prod_size, unsigned int p_prod_set_size )
{
    unsigned int coast=0;
    int i,j,min,min_index,buf;
    buf = 0;
    printf("Selected products: \n");
    printf("|\tName\t|\tCoast\t|\tCategory\t|\tNumber\t|\n");
    printf("-------------------------------------------------------------------------\n");

    for (i=0;i<p_prod_set_size;i++)
    {


        min=MAX_COAST;
        for (j=0;j<p_prod_size;j++)
        {
            if ( p_prod[ j ].number == 0) continue;

            buf=1;
            if (p_prod[j].category==p_prod_set[i])
                if (min>p_prod[j].coast)
                {
                    min=p_prod[j].coast;
                    min_index=j;
                }
        }

        if (buf==0) continue;
        printf("|\t%-8s|\t%-8d|\t%-8d\t|\t%-8d|\n",p_prod[min_index].name,p_prod[min_index].coast,
               p_prod[min_index].category,p_prod[min_index].number);

        coast=coast+p_prod[min_index].coast;
    }

    printf("Final coast:%d",coast);
    return;
}
