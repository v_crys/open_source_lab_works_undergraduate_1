#include <stdio.h>
#include <string.h>


    void add_record( Product *p_prod, Product *p_added_prod, unsigned int *p_prod_size )
    {
        if ( strlen( p_added_prod->name ) > 8 )
            p_added_prod->name[ 8 ] = 0;

        p_prod[ (*p_prod_size)++ ] = *p_added_prod;
        return;
    }


    void del_record( Product *p_prod, unsigned int number_prod, unsigned int *p_prod_size )
    {
        int i;
        (*p_prod_size)--;

        for ( i = number_prod ; i < *p_prod_size; i++)
            p_prod[ i ] = p_prod[ i + 1 ];

        p_prod[ i ].coast = 0;
        return;
    }
