/*
 *  Name project:   Lab2
 *  Generated:      15/10/2016
 *
 *  Description:
 *
 */

#ifndef SELECTION_PRODUCT_H_INCLUDED
#define SELECTION_PRODUCT_H_INCLUDED

    #include <stdio.h>
    #include "selection_product.c"

    void select_prod( Product *p_prod, unsigned char *p_prod_set, unsigned int p_prod_size, unsigned int p_prod_set_size );


#endif // SELECTION_PRODUCT_H_INCLUDED
