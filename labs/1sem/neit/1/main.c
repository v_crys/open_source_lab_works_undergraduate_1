#include <stdio.h>    // Input / output
#include <stdint.h>   // Integer types
//#include <termios.h>  // For getch()
//#include <unistd.h>   // For getch()

#define CLEAR_SCREEN "\e[1;1H\e[2J"
#define VISITED      "\e[1;31m" // Red bold
#define KNIGHT       "\e[1;32m" // Green bold
#define REGULAR      "\e[0m"
#define SIZE 8
#define START_X 0
#define START_Y 0

#define uint8_t unsigned int

//#define DISPLAY
//#define WAIT

/*int getch(void) // Reads from keypress, doesn’t echo
{
	struct termios oldattr, newattr;
	int ch;
	tcgetattr(STDIN_FILENO, &oldattr);
	newattr = oldattr;
	newattr.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
	return ch;
}*/
/*
void display(uint8_t (*board)[SIZE], const uint8_t (*figures)[15])
{


	printf(CLEAR_SCREEN);
	printf("  |");
	for (uint8_t j = 0; j < SIZE; ++j)
		printf(" %c", ('A' + j));
	printf("\n−−|");
	for (uint8_t j = 0; j < SIZE; ++j)
		printf("−−");
	putchar('\n');
	for (uint8_t i = SIZE; i > 0; --i)
	{
		printf("%d | ", i);
		for (uint8_t j = 0; j < SIZE; ++j)
			printf("%s ", figures[board[i-1][j]]);
		printf("\n");
	}

	#ifdef WAIT
	getch();
	#endif
}*/

char flag = 1;

int turn(uint8_t x1, uint8_t y1, FILE *log)
{
    int i, j;
	static uint8_t board[SIZE][SIZE];

	if ( flag  )
    {
        for ( i = 0; i < 8; i++ )
            for ( j = 0; j < 8; j++ )
                board[ i ][ j ] =0;

        flag = 0;
    }
	//static const uint8_t figures[3][15] = {"o", VISITED"x"REGULAR,
	//                                                KNIGHT"♞"REGULAR};
	static uint8_t count = SIZE * SIZE ;

	/*static const int8_t dy[8] = {-2, -2, -1, -1,  1, 1,  2, 2},
	                    dx[8] = {-1,  1, -2,  2, -2, 2, -1, 1};
*/
    static const int8_t dy[8] = { -2, -1, 1, 2, 2, 1, -1, -2},
                        dx[8] = { 1, 2, 2, 1, -1, -2,-2,-1};



	board[x1][y1] = 2;
	//fprintf(log, "%c%d\n", ('A' + x1), (y1 + 1));
	#ifdef DISPLAY
	display(board, figures);
	#endif
	--count;


	if (!count)
    {
        for ( i = 0; i < 8; i++ )
        {
            for ( j = 0; j < 8; j++ )
                printf( "%d ", board[ i ][ j ] );

            printf("\n" );
        }
        return 0;
    }


	//uint8_t i = 7;
	i = 7;
	for ( i = 0; i < 8; i++ )
	{
		uint8_t x2 = x1 + dx[i],
		        y2 = y1 + dy[i];

		if (x2 < SIZE && y2 < SIZE && !board[x2][y2])
		{
			board[x1][y1] = 1;

			if (turn(x2, y2, log) == 0)
            {
                fprintf(log, "%c%d\n", ('A' + x1), (y1 + 1));
                return 0;
            }


			else
			{
				++count;
				board[x2][y2] = 0;
				board[x1][y1] = 2;
				#ifdef DISPLAY
				display(board, figures);
				#endif
				//fseek(log, -3, SEEK_CUR);
			}
		}
	}
	//while (i--);

	return 1;
}

int main()
{
	FILE *log = fopen("log.txt", "w");
	if (turn(START_X, START_Y, log))
		printf("NO WAY!\n");
    else printf( "OK" );
	fclose(log);
}
