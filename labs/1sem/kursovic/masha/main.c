#include <stdio.h>
#include <stdlib.h>

struct formula
{
    int mask[ 10 ];
    int f[ 10 ];
    int f_vs[ 10 ];
    int pos;
};

struct element
{
    int x, y;

    char type; // ---- 0 - free, 1 - int, 2 - formul
    struct formula fs;
    int value;
};


struct element * add_element( struct element *elements, int *cnt_element )
{
    int i;

    (*cnt_element)++;
    elements = realloc( elements, *cnt_element * sizeof( struct element ) );


    printf( "Input X: " );
    scanf( "%d", &elements[ *cnt_element - 1].x );

    printf( "Input Y: " );
    scanf( "%d", &elements[ *cnt_element - 1].y );

    printf( "Input type ( 0 - free, 1 - value, 2 - formul ): " );
    scanf( "%d", &elements[ *cnt_element - 1].type );

    switch (elements[ *cnt_element - 1 ].type)
    {
    case 0:
        elements[ *cnt_element - 1 ].value = 0;
        break;

    case 1:
        printf( "Input value: " );
        scanf( "%d", &elements[ *cnt_element - 1 ].value );
        break;

    case 2:
        elements[ *cnt_element - 1 ].value = 0;
        printf( "Input cnt position: " );
        scanf( "%d", &elements[ *cnt_element - 1 ].fs.pos );

        for ( i = 0; i < elements[ *cnt_element - 1 ].fs.pos; i++ )
        {
            printf( "Input type (0 - value, 1 - znak, 2 - element): " );
            scanf( "%d", &elements[ *cnt_element - 1 ].fs.mask[ i ] );

            switch ( elements[ *cnt_element - 1 ].fs.mask[ i ] )
            {
            case 0:
                printf( "Input value: " );
                scanf( "%d", &elements[ *cnt_element - 1 ].fs.f[ i ] );
                break;

            case 1:
                printf( "Input znak: " );
                scanf( "%d", &elements[ *cnt_element - 1 ].fs.f[ i ] );
                break;

            case 2:
                printf( "Input X: " );
                scanf( "%d", &elements[ *cnt_element - 1 ].fs.f[ i ] );
                printf( "Input Y: " );
                scanf( "%d", &elements[ *cnt_element - 1 ].fs.f_vs[ i ] );
                break;
            }

        }
    }
    return elements;
}

struct element * delete_element( struct element *elements, int *cnt_element )
{
    int deles_x, deles_y, i, j;
    printf( "Input deleted element X:" );
    scanf( "%d", &deles_x );
    printf( "Input deleted element Y:" );
    scanf( "%d", &deles_y );

    for ( i = 0; i < *cnt_element; i++ )
    {
        if ( ( elements[ i ].x == deles_x ) && ( elements[ i ].y == deles_y ) )
        {
            for ( j = i; j < *cnt_element; j++ )
                elements[ i ] = elements [ i + 1 ];

            break;
        }

    }

    *cnt_element--;
    elements = realloc( elements, *cnt_element * sizeof( struct element ) );

    return elements;
}

void show_table( struct element *elements, int *cnt_element )
{
    if ( elements == NULL ) { printf( "ERROR " ); return; }

    int max_x = elements[ 0 ].x;
    int max_y = elements[ 0 ].y;
    int i, j, z;
    int flag;

    //------- find size table
    for ( i = 0 ; i < *cnt_element; i++ )
    {
        if ( max_x < elements[ i ].x )
            max_x = elements[ i ].x;

        if ( max_y < elements[ i ].y )
            max_y = elements[ i ].y;
    }

    //-------- print
    for ( i = 0; i < max_y + 1; i++ )
    {
        for ( j = 0; j < max_x + 1; j++ )
        {
            flag = 0;
            for ( z = 0; z < *cnt_element; z++ )
            {
                if ( ( elements[ z ].x == j ) && ( elements[ z ].y == i  ) )
                {
                    switch ( elements[ z ].type )
                    {
                    case 0:
                        printf( "___ " );
                        break;

                    case 2:
                        printf( "%d ", elements[ z ].value );
                        break;
                    case 1:
                        printf( "%d ", elements[ z ].value );
                    }
                    flag = 1;
                }

                if ( flag )
                    break;
            }

            if ( !flag )
                printf( "___ " );


        }
        printf( "\n" );
    }

}

void update_form( struct element *elements, int *cnt_element )
{
    int i, j, z, k;
    int out = 0;
    int new_znak = -1;
    int buf;

    for ( i = 0; i < *cnt_element; i++ )
    {
        if ( elements[ i ].type == 2 ) //------ if formula
        {
            out = 0;
            for ( j = 0; j < elements[ i ].fs.pos; j++ )
            {
                switch ( elements[ i ].fs.mask[ j ] )
                {
                    case 0:
                        switch ( new_znak )
                        {
                            case 0:
                                out += elements[ i ].fs.f[ j ];
                                break;

                            case 1:
                                out -= elements[ i ].fs.f[ j ];
                                break;

                            case 2:
                                out *= elements[ i ].fs.f[ j ];
                                break;

                            case 3:
                                out /= elements[ i ].fs.f[ j ];
                                break;

                            case -1:
                                out = elements[ i ].fs.f[ j ];
                        }
                        break;

                    case 1:
                        new_znak = elements[ i ].fs.f[ j ];
                        break;

                    case 2:
                        for ( k = 0; k < *cnt_element; k++ )
                        {
                            if ( ( elements[ k ].x == elements[ i ].fs.f[ j ] ) &&
                                ( elements[ k ].y == elements[ i ].fs.f_vs[ j ] ) )
                            {
                                buf = elements[ k ].value;
                                break;
                            }
                        }

                        switch ( new_znak )
                        {
                        case 0:
                            out += buf;
                            break;

                        case 1:
                            out -= buf;
                            break;

                        case 2:
                            out *= buf;
                            break;

                        case 3:
                            out /= buf;
                            break;

                        case -1:
                            out = buf;
                        }

                        break;

                }
            }

            elements[ i ].value = out;
        }


    }


}



int main()
{
    struct element *ARR_EL = NULL;
    int ARR_EL_cnt = 0;
    int i;
    int state;


    while ( 1 )
    {
        printf( "--------- MENU ------------\n" );
        printf("1 - Add element\n") ;
        printf("2 - Delete element\n") ;
        printf("3 - Update table\n") ;
        printf("4 - Show table\n" );
        printf("e - exit\n" );

        state = getch();
        switch ( state )
        {
            case '1':
                ARR_EL = add_element( ARR_EL, &ARR_EL_cnt );
                if ( ARR_EL == NULL ) printf("FAIL");
                break;

            case '2':
                ARR_EL = delete_element( ARR_EL, &ARR_EL_cnt );
                break;

            case '3':
                update_form( ARR_EL, &ARR_EL_cnt );
                break;

            case '4':
                show_table( ARR_EL, &ARR_EL_cnt );
                break;

            case 'e': return 0;
        }
    }


    return 0;
}
