#include <stdio.h>
#include <stdlib.h>

#define     MAX_LEN 100

struct birthday
{
    unsigned char   day;
    unsigned char   month;
    unsigned int    year;
};

struct Sportsmen
{
    char *soname;
    char *name;

    struct birthday     birth;

    unsigned char weigth;
    unsigned char growth;

    unsigned char pol; //----- 0 - women; 1 - men;
};




int main()
{
    int i, j;

    unsigned char mode;

    struct Sportsmen    *S;
    unsigned int        cnt;

    unsigned int buf_arr[ 4 ];
    unsigned int bolshe_menshe_arr[ 4 ];
    unsigned int mode2;

    struct Sportsmen    buf_sportsmen;
    buf_sportsmen.soname = ( char * ) calloc( MAX_LEN, sizeof( char ) );
    buf_sportsmen.name = ( char * ) calloc( MAX_LEN, sizeof( char ) );

    //---------------- input
    printf( "Print counter: " );
    scanf( "%d", &cnt );
    S = (struct Sportsmen *) calloc( cnt, sizeof( struct Sportsmen ) );

    for ( i = 0; i < cnt; i++ )
    {
        S[ i ].soname = ( char * ) calloc( MAX_LEN, sizeof( char ) );
        S[ i ].name = ( char * ) calloc( MAX_LEN, sizeof( char ) );

        printf( "\n\nSportsmen[ %d ]:\n", i );
        printf( "Soname: " );
        scanf( "%s", (S[ i ].soname) );
        printf( "Name: " );
        scanf( "%s", (S[ i ].name) );

        printf( "Birthday: \n" );
        printf( "\tDay: " );
        scanf( "%d", &S[ i ].birth.day );
        printf( "\tMonth: " );
        scanf( "%d", &S[ i ].birth.month );
        printf( "\tYear: " );
        scanf( "%d", &S[ i ].birth.year );

        printf( "Weigth: " );
        scanf( "%d", &S[ i ].weigth );
        printf( "Growth: " );
        scanf( "%d", &S[ i ].growth );

        printf( "Pol: " );
        scanf( "%d", &S[ i ].pol );

    }


    //-------------- menu
    while( 1 )
    {
        printf( "\n-------------- MENU --------------\n" );
        printf( "\t1 - Sorting array\n" );
        printf( "\t2 - Request of sportsmen\n" );
        printf( "\t3 - Add sportsmen\n" );
        printf( "\t4 - Create list of request\n" );
        printf( "\te - exit" );

        mode = getch();

        switch ( mode )
        {
            case '1':
                for ( i = 0; i < cnt; i++ )
                    for ( j = 0; j < cnt; j++ )
                        if ( S[ i ].weigth < S[ j ].weigth )
                        {
                            buf_sportsmen = S[ i ];
                            S[ i ] = S[ j ];
                            S[ j ] = buf_sportsmen;
                        }
                printf( "\n---------- Sorthing --------\n" );
                break;

            case '2':
                printf( "\nPrint id sportsmen: " );
                scanf( "%d", &j );


                unsigned int mode2;
                while( 1 )
                {
                    printf( "1 - Soname\n" );
                    printf( "2 - Name\n" );
                    printf( "3 - Birthday\n" );
                    printf( "4 - Weigth\n" );
                    printf( "5 - Growth\n" );
                    printf( "6 - Pol\n" );
                    printf( "n - next\n" );

                    mode2 = getch( );
                    if ( mode2 == 'n' ) break;

                    switch ( mode2 )
                    {
                        case '1':
                            printf( "\n\tSoname[ %d ]: %s\n", j, S[ j ].soname );
                            break;

                        case '2':
                            printf( "\n\tName[ %d ]: %s\n", j, S[ j ].name );
                            break;

                        case '3':
                            printf( "\n\tBirthday:\n\t\tDay: %d\n\t\tMonth: %d\n\t\tYear: %d\n",
                                   S[ j ].birth.day, S[ j ].birth.month, S[ j ].birth.year );

                            break;

                        case '4':
                            printf( "\nWeigth: %d\n", S[ j ].weigth );
                            break;

                        case '5':
                            printf( "\nGrowth: %d\n", S[ j ].growth );
                            break;

                        case '6':
                            printf( "\nPol (0 - women, 1 - men): %d\n", S[ j ].pol );
                            break;

                    }

                }

                break;

            case '3':
                printf( "\n\nPrint new sportsmen: \n");
                printf( "Soname: " );
                scanf( "%s", buf_sportsmen.soname );
                printf( "Name: " );
                scanf( "%s", buf_sportsmen.name );

                printf( "Birthday: \n" );
                printf( "\tDay: " );
                scanf( "%d", &buf_sportsmen.birth.day );
                printf( "\tMonth: " );
                scanf( "%d", &buf_sportsmen.birth.month );
                printf( "\tYear: " );
                scanf( "%d", &buf_sportsmen.birth.year );

                printf( "Weigth: " );
                scanf( "%d", &buf_sportsmen.weigth );
                printf( "Growth: " );
                scanf( "%d", &buf_sportsmen.growth );

                printf( "Pol: " );
                scanf( "%d", &buf_sportsmen.pol );

                for ( i = 0; i < cnt; i++ )
                {
                    if ( S[ i ].weigth > buf_sportsmen.weigth )
                    {
                        S = (struct Sportsmen *) realloc( S, cnt * sizeof( struct Sportsmen ) );
                        cnt++;

                        for ( j = cnt - 1; j != i + 1; j-- )
                            S[ j ] = S[ j - 1 ];

                        S[ i ].soname = ( char * ) calloc( MAX_LEN, sizeof( char ) );
                        S[ i ].name = ( char * ) calloc( MAX_LEN, sizeof( char ) );

                        S[ i ] = buf_sportsmen;
                        break;
                    }
                }

                break;

            case '4':

                for ( i = 0; i < 4; i++ ) buf_arr[ i ] = 0;


                for ( i = 0; i < 4; i++ ) bolshe_menshe_arr[ i ] = 0;

                while( 1 )
                {
                    printf( "\n1 - Weigth\n" );
                    printf( "2 - Growth\n" );
                    printf( "3 - Pol\n" );
                    printf( "n - next\n" );

                    mode2 = getch(  );
                    if ( mode2 == 'n' ) break;

                    if ( mode2 != '3' )
                    {
                        printf( "\n1 - Bolhe, 2 - menshe, 3 - ravno: ");
                        scanf( "%d", &bolshe_menshe_arr[ mode2 - '1' ] );
                    } else { bolshe_menshe_arr[ 2 ] = 1;}

                    printf( "Value: ");
                    scanf( "%d", &buf_arr[ mode2 - '1' ] );

                }

                for ( i = 0; i < cnt; i++ )
                {
                    if ( bolshe_menshe_arr[ 0 ] != 0 )
                    {
                        printf( "yes" );
                        switch ( bolshe_menshe_arr[ 0 ] )
                        {
                            case 1:
                                if ( S[ i ].weigth <= buf_arr[ 0 ] ) continue;
                                break;

                            case 2:
                                if ( S[ i ].weigth >= buf_arr[ 0 ] ) continue;
                                break;

                            case 3:
                                if ( S[ i ].weigth != buf_arr[ 0 ] ) continue;
                                break;
                        }
                    }

                    if ( bolshe_menshe_arr[ 1 ] != 0 )
                    {
                        switch ( bolshe_menshe_arr[ 1 ] )
                        {
                            case 1:
                                if ( S[ i ].growth <= buf_arr[ 1 ] ) continue;
                                break;

                            case 2:
                                if ( S[ i ].growth >= buf_arr[ 1 ] ) continue;
                                break;

                            case 3:
                                if ( S[ i ].growth != buf_arr[ 1 ] ) continue;
                                break;
                        }
                    }

                    if ( bolshe_menshe_arr[ 2 ] != 0 )
                    {
                        if ( S[ i ].pol != buf_arr[ 2 ] ) continue;
                    }


                    printf( "\nSportsmen[ %d ]", i );
                    printf( "\n\tSoname: %s", S[ i ].soname );
                    printf( "\n\tName: %s", S[ i ].name );
                    printf( "\n\tBirthday:\n\t\tDay: %d\n\t\tMonth: %d\n\t\tYear: %d\n",
                                   S[ i ].birth.day, S[ i ].birth.month, S[ i ].birth.year );
                    printf( "\n\tWeigth: %d", S[ i ].weigth );
                    printf( "\n\tGrowth: %d", S[ i ].growth );
                    printf( "\n\tPol (0 - women, 1 - men): %d\n", S[ i ].pol );
                }

                break;

            case 'e':
                return 0;
        }
    }

    return 0;
}
