#include <stdio.h>
#include <stdlib.h>

#define LEN 10

struct Vklad
{
    char *name;
    unsigned int proz;
};

struct Bank
{
    char *name;
    char *addr;
    char *status;

    unsigned int vk_s;
    struct Vklad vklads[ LEN ];

    unsigned int kapital;
};

void constructor( struct Bank *banks )
{
    int i;

    banks->name = calloc( 100, sizeof( char ) );
    banks->addr = calloc( 100, sizeof( char ) );
    banks->status = calloc( 100, sizeof( char ) );


    printf( "Input name: " );
    scanf( "%s", banks->name );

    printf( "Input address: " );
    scanf( "%s", banks->addr );

    printf( "Input status: " );
    scanf( "%s", banks->status );

    printf( "Input cnt vklads: " );
    scanf( "%d", &banks->vk_s );

    for ( i = 0; i < banks->vk_s ; i++ )
    {
        banks->vklads[i].name = calloc( 100, sizeof( char ) );
        printf( "\tInput name vklad: " );
        scanf( "%s", banks->vklads[ i ].name );

        printf( "\tInput prozent vklads: " );
        scanf( "%d", &banks->vklads[ i ].proz );
    }

    printf( "\nInput kapital: " );
    scanf( "%d", &banks->kapital );

    printf( "\n---------------------------\n" );
    return;

}

void destructor( struct Bank *banks )
{
    int i;
    for ( i = 0; i < banks->vk_s; i++ )
        free( banks->vklads[ i ].name );

    free( banks->name );
    free( banks->addr );
    free( banks->status );

    return;
}

void print_bank( struct Bank *banks )
{
    int i;

    printf( "\nName: %s", banks->name );
    printf( "\nAddress: %s", banks->addr );
    printf( "\nStatus: %s", banks->status );

    printf( "\nVklads: %d", banks->vk_s );
    for ( i = 0 ; i < banks->vk_s; i++ )
    {
        printf( "\n\tName: %s", banks->vklads[ i ].name );
        printf( "\n\tProzent: %d", (banks->vklads[ i ].proz) );
    }

    printf( "\nKapital: %d\n\n", banks->kapital );

    return;
}

int binary_find( struct Bank *banks, int bank_cnt, int value )
{
    int i;
    int a;
    int step;

    a = bank_cnt >> 1 ;
    step = a;

    if ( bank_cnt == 0 ) return 0;

    int flag = 0;
    while ( 1 )
    {
        if ( a == bank_cnt - 1 )
            if ( banks[ a ].kapital <= value) return a + 1;


        if ( (banks[ a + 1 ].kapital >= value) && (banks[ a ].kapital <= value) ) return a + 1;
        if ( ( a == 0 ) && ( banks[ a + 1 ].kapital > value ) ) return 0;


        if ( banks[ a ].kapital > value )
        {
            step = ( step & 1 ) ? ( step >> 1 ) + 1 : step >> 1;
            if ( a - step >= 0 )
            a -= step;
        }
        else
        {
            step = ( step & 1 ) ? ( step >> 1 ) + 1 : step >> 1;
            if ( a + step < bank_cnt)
            a += step;
        }

        //if ( flag ) return a;


    }


}

int main()
{
    struct Bank *banks;
    int banks_cnt = 0;
    int i, j,  deletes, buf_number, buf_vklad, flag;
    char buf_str[ 1000 ];

    int banks_cnt_buf = 0;
    int pos_past;
    struct Bank buf;

    printf( "Input cnt banks: " );
    scanf( "%d", &banks_cnt );

    banks = calloc( banks_cnt, sizeof( struct Bank ) );


    for ( i = 0; i < banks_cnt; i++ )
        constructor( &banks[ i ] );

    int state;

    while( 1 )
    {
        printf( "\n----------- MENU -----------------\n" );
        printf( "\ts - sorthing\n" );
        printf( "\tc - select\n" );
        printf( "\tn - new\n" );
        printf( "\td - delete\n" );
        printf( "\tk - delete bankrot");
        printf( "\n\te - exit\n" );

        state = getch();

        switch ( state )
        {
            case 's':
                    banks_cnt_buf = 0;
                    for ( i = 0; i < banks_cnt; i++ )
                    {
                        int int_min = banks[ banks_cnt_buf ].kapital;
                        int pos_min = banks_cnt_buf;
                        //-------- find minimum element
                        for ( j = banks_cnt_buf + 1; j < banks_cnt; j++ )
                        {
                            if ( banks[ j ].kapital < int_min )
                            {
                                int_min = banks[ j ].kapital;
                                pos_min = j;
                            }
                        }

                        //------- binary find
                        pos_past = binary_find( banks, banks_cnt_buf, int_min );

                        //------- relocation
                        buf = banks[ pos_past ];
                        banks[ pos_past ] = banks[ pos_min ];
                        banks[ pos_min ] = buf;

                        banks_cnt_buf++;

                    }

                    //-------- out
                    for ( i = 0 ; i < banks_cnt; i++ )
                    {
                            printf( "Print bank (%d): \n", i );
                            print_bank( &banks[ i ] );
                    }
                break;

            case 'c':
                printf( "Print name vklad: " );
                scanf( "%s", buf_str );

                //buf_number = 0;

                flag = 0;
                for ( i = 0; i < banks_cnt; i++ )
                {
                    for ( j = 0; j < banks[ i ].vk_s; j++  )
                    {
                        if ( strcmp( banks[ i ].vklads[ j ].name, buf_str ) == 0 )
                        {
                            if ( flag )
                            {
                                if ( banks[ i ].vklads[ j ].proz > banks[ buf_number ].vklads[ buf_vklad ].proz  )
                                {
                                    buf_number = i;
                                    buf_vklad = j;
                                }
                            } else {
                                buf_number = i;
                                buf_vklad = j;
                                flag = 1;
                            }


                        }
                    }
                }

                print_bank( &banks[ buf_number ] );

                break;

            case 'n':
                banks_cnt++;
                banks = realloc( banks, banks_cnt * sizeof( struct Bank ) );

                printf( "\nNew bank:\n");
                constructor( &banks[ banks_cnt - 1 ] );

                for ( i = 0 ; i < banks_cnt; i++ )
                {
                    printf( "Print bank (%d): \n", i );
                    print_bank( &banks[ i ] );
                }

                break;

            case 'd':
                printf( "Print number bank: " );
                scanf( "%d", &deletes);
                destructor( &banks[ deletes ] );

                for ( i = deletes; i < banks_cnt; i++ )
                    banks[ i ] = banks[ i + 1 ];

                banks_cnt--;
                banks = realloc( banks, banks_cnt * sizeof( struct Bank));

                for ( i = 0 ; i < banks_cnt; i++ )
                {
                    printf( "Print bank (%d): \n", i );
                    print_bank( &banks[ i ] );
                }
                break;

            case 'e': return 0;

            case 'k':
                for ( i = 0; i < banks_cnt; i++ )
                {
                    f:;
                    if ( i == banks_cnt ) break;
                    if ( banks[ i ].kapital == 0 )
                    {
                        destructor( &banks[ i ] );

                        for ( j = i; j < banks_cnt; j++ )
                            banks[ j ] = banks[ j + 1 ];

                        banks_cnt--;
                        banks = realloc( banks, banks_cnt * sizeof( struct Bank));
                        goto f;
                    }
                }

                for ( i = 0 ; i < banks_cnt; i++ )
                {
                    printf( "Print bank (%d): \n", i );
                    print_bank( &banks[ i ] );
                }

                break;
        }
    }

    return 0;
}
