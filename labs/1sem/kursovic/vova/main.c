#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define MAX_LEN 100

enum vagon { PL, K };

struct time_data
{
    int Year;
    char Month;
    char Day;
    char Hour;
    char Minuts;
};

struct People
{
    char *name;
    char *iniz;
};

struct train
{
    char *dispatch;
    char *destination;
    unsigned int   number;

    struct time_data Time;

    struct People *peopls;
    unsigned int P_s;

    enum vagon vag;

    unsigned int cost;
};

void constructor( struct train *t)
{
    int i;
    char buf[ 10 ];


    printf( "\n----------- Input new train ------------\n" );

    t->dispatch = ( char * ) calloc( MAX_LEN, sizeof( char ) );
    printf( "\tDispatch: " );
    scanf( "%s", t->dispatch );

    t->destination = ( char * ) calloc( MAX_LEN, sizeof( char ) );
    printf( "\tDestination: " );
    scanf( "%s", t->destination );

    printf( "\tNumber: " );
    scanf( "%d", &t->number );

    printf( "\tPrint Data/Time: \n" );
    printf( "\t\tPrint Year: " );
    scanf( "%d", &t->Time.Year );
    printf( "\t\tPrint Month: " );
    scanf( "%d", &t->Time.Month );
    printf( "\t\tPrint Day: " );
    scanf( "%d", &t->Time.Day );
    printf( "\t\tPrint Hour: " );
    scanf( "%d", &t->Time.Hour );
    printf( "\t\tPrint Minuts: " );
    scanf( "%d", &t->Time.Minuts );

    printf( "\tPrint Peopls: " );
    scanf( "%d", &t->P_s );
    t->peopls = ( struct People * ) calloc( t->P_s, sizeof( struct People ) );
    for ( i = 0; i < t->P_s; i++ )
    {
        t->peopls[i].name = ( char * ) calloc( MAX_LEN, sizeof( char ) );
        printf( "\t\tName: " );
        scanf( "%s", t->peopls[i].name);

        t->peopls[i].iniz = ( char * ) calloc( MAX_LEN, sizeof( char ) );
        printf( "\t\tInizials: " );
        scanf( "%s", t->peopls[i].iniz );
    }

    printf( "\tPrint type vagon: " );
    scanf( "%s", buf );
    if ( strcmp( buf, "PL" ) == 0 ) t->vag = PL; else  t->vag = K;

    printf( "\tPrint cost: " );
    scanf( "%d", &t->cost );

    return;
}


int main()
{
    struct train *TR;
    struct train TR_buf;
    int TR_s;
    int i, j;
    char state;
    char state_sort;

    char buf[ MAX_LEN ];
    char destin[ MAX_LEN ];
    enum vagon vag;
    unsigned int cost;

    char flags[ 10 ];
    char disp_buf[ MAX_LEN ];
    char dest_buf[ MAX_LEN ];
    unsigned int number_buf;
    struct time_data time_buf;
    enum vagon vagon_buf;
    unsigned int cost_buf;

    //------------ create trains
    printf( "Print cnt trains: " );
    scanf( "%d", &TR_s );

    TR = ( struct train *) calloc ( TR_s, sizeof( struct train ) );

    for ( i = 0; i < TR_s; i++ )
        constructor( &TR[ i ] );


            for ( i = 0; i < TR_s; i++ )
                {
                    printf( "\n----------------------------\n");
                    printf( "\tDispatch: %s\n\tDestination: %s\n\tNumber: %d", TR[ i ].dispatch, TR[ i ].destination, TR[ i ].number );
                    printf( "\n\tData(time):\n\t\tYear: %d\n\t\tMonth: %d\n\t\tDay: %d\n\t\tHour: %d\n\t\tMinuts: %d", TR[ i ].Time.Year, TR[ i ].Time.Month, TR[ i ].Time.Day, TR[ i ].Time.Hour, TR[ i ].Time.Minuts );
                    printf( "\n\tPeoples:");
                    for ( j = 0; j < TR[ i ].P_s; j++ )
                    {
                        printf( "\n\t\tName: %s", TR[ i ].peopls[ j ].name );
                        printf( "\n\t\tInizials: %s", TR[ i ].peopls[ j ].iniz );
                    }
                    printf( "\n\tType vagon: %s", ( TR[ i ].vag == PL ) ? "PL" : "K" );
                    printf( "\n\tCost: %d", TR[ i ].cost );
            }



    //----------- MENU
    while ( 1 )
    {
        printf( "\n\n------------- MENU ---------------\n" );
        printf( "\t1 - sorthing \n" );
        printf( "\t2 - create list train \n" );
        printf( "\t3 - report of the day \n" );
        printf( "\te - exit \n" );

        state = getch();
        switch ( state )
        {
            case '1':
                printf( "\n\nMenu 1:\n" );
                printf( "Input type sorthing: \n");
                printf( "\t 1 - destination \n" );
                printf( "\t 2 - type vagone \n" );
                printf( "\t 3 - cost \n" );
                 state_sort = getch();



                for ( i = 0; i < TR_s; i++  )
                {
                    for ( j = 0; j < TR_s; j++ )
                    {
                        switch ( state_sort )
                        {
                            case '1':
                                if ( strcmp( TR[ i ].destination, TR[ j ].destination ) < 0 )
                                {
                                    TR_buf = TR[ i ];
                                    TR[ i ] = TR[ j ];
                                    TR[ j ] = TR_buf;
                                }
                                break;

                            case '2':
                                if ( TR[ i ].vag < TR[ j ].vag )
                                {
                                    TR_buf = TR[ i ];
                                    TR[ i ] = TR[ j ];
                                    TR[ j ] = TR_buf;
                                }
                                break;

                            case '3':
                                if ( TR[ i ].cost < TR[ j ].cost )
                                {
                                    TR_buf = TR[ i ];
                                    TR[ i ] = TR[ j ];
                                    TR[ j ] = TR_buf;
                                }
                                break;
                        }
                    }
                }

                for ( i = 0; i < TR_s; i++ )
                {
                    printf( "\n----------------------------\n");
                    printf( "\tDispatch: %s\n\tDestination: %s\n\tNumber: %d", TR[ i ].dispatch, TR[ i ].destination, TR[ i ].number );
                    printf( "\n\tData(time):\n\t\tYear: %d\n\t\tMonth: %d\n\t\tDay: %d\n\t\tHour: %d\n\t\tMinuts: %d", TR[ i ].Time.Year, TR[ i ].Time.Month, TR[ i ].Time.Day, TR[ i ].Time.Hour, TR[ i ].Time.Minuts );
                    printf( "\n\tPeoples:");
                    for ( j = 0; j < TR[ i ].P_s; j++ )
                    {
                        printf( "\n\t\tName: %s", TR[ i ].peopls[ j ].name );
                        printf( "\n\t\tInizials: %s", TR[ i ].peopls[ j ].iniz );
                    }
                    printf( "\n\tType vagon: %s", ( TR[ i ].vag == PL ) ? "PL" : "K" );
                    printf( "\n\tCost: %d", TR[ i ].cost );
                }
                break;

            case '2':
                printf( "\nMENU: 2\n");
                printf( "Input criterii: \n" );
                printf( "\t0 - dispatch\n ");
                printf( "\t1 - destination\n ");
                printf( "\t2 - number\n ");
                printf( "\t3 - day\n ");
                printf( "\t4 - vagon\n ");
                printf( "\t5 - max_cost\n ");
                printf( "\t6 - next\n ");
                for ( i = 0; i < 6; i++ ) flags[ i ] = 0;

                while( 1 )
                {
                    state_sort = getch();
                    printf( "\n--- OK " );
                    if ( state_sort == '6' ) break;
                    flags[ state_sort - '0' ] = 1;
                }

                if ( flags[ 0 ] == 1 )
                {
                    printf( "\n\t\tDispatch: " );
                    scanf( "%s", disp_buf );
                }

                if ( flags[ 1 ] == 1 )
                {
                    printf( "\n\t\tDestination: " );
                    scanf( "%s", dest_buf );
                }

                if ( flags[ 2 ] == 1 )
                {
                    printf( "\n\t\tNumber: " );
                    scanf( "%d", &number_buf );
                }

                if ( flags[ 3 ] == 1 )
                {
                    printf( "\n\t\tYear: " );
                    scanf( "%d", &time_buf.Year );
                    printf( "\n\t\tMonth: " );
                    scanf( "%d", &time_buf.Month );
                    printf( "\n\t\tDay: " );
                    scanf( "%d", &time_buf.Day );
                }

                if ( flags[ 4 ] == 1 )
                {
                    printf( "\n\t\tVagon: " );
                    scanf( "%s", buf );
                    if ( strcmp( buf, "PL") == 0 ) vagon_buf = PL; else vagon_buf = K;
                }

                if ( flags[ 5 ] == 1 )
                {
                    printf( "\n\t\tMAX_cost: " );
                    scanf( "%d", &cost_buf );
                }

                for ( i = 0; i < TR_s; i++ )
                {
                    if ( flags[ 0 ] )
                        if ( strcmp( disp_buf, TR[ i ].dispatch ) )
                            continue;

                    if ( flags[ 1 ] )
                        if ( strcmp( dest_buf, TR[ i ].destination ) )
                            continue;

                    if ( flags[ 2 ] )
                        if ( number_buf != TR[ i ].number )
                            continue;

                    if ( flags[ 3 ] )
                        if ( ( TR[ i ].Time.Year != time_buf.Year ) || ( TR[ i ].Time.Month != time_buf.Month )
                                || ( TR[ i   ].Time.Day != time_buf.Day ))
                            continue;

                    if ( flags[ 4 ] )
                        if ( vagon_buf != TR[ i ].vag ) continue;

                    if ( flags[ 5 ] )
                        if ( TR[ i ].cost > cost_buf )
                            continue;

                    printf( "\n----------------------------\n");
                    printf( "\tDispatch: %s\n\tDestination: %s\n\tNumber: %d", TR[ i ].dispatch, TR[ i ].destination, TR[ i ].number );
                    printf( "\n\tData(time):\n\t\tYear: %d\n\t\tMonth: %d\n\t\tDay: %d\n\t\tHour: %d\n\t\tMinuts: %d", TR[ i ].Time.Year, TR[ i ].Time.Month, TR[ i ].Time.Day, TR[ i ].Time.Hour, TR[ i ].Time.Minuts );
                    printf( "\n\tPeoples:");
                    for ( j = 0; j < TR[ i ].P_s; j++ )
                    {
                        printf( "\n\t\tName: %s", TR[ i ].peopls[ j ].name );
                        printf( "\n\t\tInizials: %s", TR[ i ].peopls[ j ].iniz );
                    }
                    printf( "\n\tType vagon: %s", ( TR[ i ].vag == PL ) ? "PL" : "K" );
                    printf( "\n\tCost: %d", TR[ i ].cost );
                }

                break;

            case '3':
                printf( "\nMENU: 3\n");
                printf( "Input day:\n" );
                printf( "\tYear: " );
                scanf( "%d", &time_buf.Year );
                printf( "\tMonth: " );
                scanf( "%d", &time_buf.Month );
                printf( "\tDay: " );
                scanf( "%d", &time_buf.Day );


                for ( i = 0; i < TR_s; i++ )
                {
                    if ( ( TR[ i ].Time.Year != time_buf.Year ) || ( TR[ i ].Time.Month != time_buf.Month )
                                || ( TR[ i ].Time.Day != time_buf.Day )) continue;


                    printf( "\n----------------------------\n");
                    printf( "\tDispatch: %s\n\tDestination: %s\n\tNumber: %d", TR[ i ].dispatch, TR[ i ].destination);
                    printf( "\n\tPeoples:");
                    for ( j = 0; j < TR[ i ].P_s; j++ )
                    {
                        printf( "\n\t\tName: %s", TR[ i ].peopls[ j ].name );
                        printf( "\n\t\tInizials: %s", TR[ i ].peopls[ j ].iniz );
                    }
                    printf( "\n\tType vagon: %s", ( TR[ i ].vag == PL ) ? "PL" : "K" );
                }
                break;

            case 'e':
                return 0;

        }
    }

	return 0;
}
