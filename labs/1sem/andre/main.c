#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>

#define N 100
#define N1 50

int my_remove();
int my_save();
int my_read();
int my_buses();
int my_list_streets();
int my_list_bus();
int my_delete();
int my_add();
int search();




int I_max = 0, J_max = 0; // I_max - кол-во заполненых ячеек в массиве структур "BUSES"; J_max - кол-во заполненых ячеек в массиве структур "STREETS"

struct streets
{
	char name[N1];
}STREETS[N];

struct Bus // Структура автобуса
{
	char number[4];
	int ID[10]; // Номера улиц из массива улиц
} BUSES[N];


int main()
{
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

	char c;
	int i;

	if (my_read() == 0)
		return 1;

	while (1)
	{
		system("cls");
		SetConsoleTextAttribute(console, 10);
		printf("Please select the item you need\n");
		SetConsoleTextAttribute(console, 15);
		printf("________________________________________\n");
		printf("1.list of buses                         |\n");
		printf("2.list of streets                       |\n");
		printf("3.to remove the bus                     |\n");
		printf("4.add bus                               |\n");
		printf("5.search                                |\n");
		printf("                                        |\n");
		printf("9.SAVE                                  |\n");
		printf("0.EXIT                                  |\n");
		printf("Backspace.Delete file                   |\n");
		printf("________________________________________|");
		if ((c = getch()) == '1')
		{
			my_list_bus();
		}
		if (c == '2')
		{
			my_list_streets();
		}
		if (c == '3')
		{
			my_remove();
		}
		if (c == '4')
		{
			my_add();
		}
		if (c == '5')
		{
			search();
		}
		if (c == '\b')
		{
			my_delete();
		}
		if (c == '9')
		{
			my_save();
		}
		if (c == '0')
		{
			my_save();
			system("cls");
			SetConsoleTextAttribute(console, 4);
			for (i = 0; i < 4; i++)
			{
				printf("GOOD BYE!!!");
				Sleep(500);
				system("cls");
				Sleep(500);
			}
			SetConsoleTextAttribute(console, 15);
			c = 1;
			for (i = 0; i < 10000; i++)
			{
				printf("%c", c++);
			}
			break;
		}
	}

	return 0;
}



int my_save() // сохранение базы
{
	system("cls");
	FILE *fi = fopen("basedata", "wb");
	if (fi == NULL)
	{
		printf("Error reading file to exit, press any key");
		getch();
		return 0;
	}
	fwrite(&I_max, sizeof(int), 1, fi);
	fwrite(&J_max, sizeof(int), 1, fi);
	fwrite(&STREETS,sizeof(struct streets), J_max, fi);
	fwrite(&BUSES,sizeof(struct Bus), I_max, fi);
	fclose(fi);
	printf("Save completed");
	Sleep(1000);
	return 1;
}
// сделано*****

int my_read() // чтение из файла
{

	FILE *fi = fopen("basedata", "rb");
	if (fi == NULL)
	{
		printf("Error reading file to exit, press any key");
		getch();
		return 0;
	}
	fread(&I_max, sizeof(int), 1, fi);
	fread(&J_max, sizeof(int), 1, fi);
	fread(&STREETS[0], sizeof(struct streets), J_max , fi);
	fread(&BUSES[0], sizeof (struct Bus), I_max , fi);
	fclose(fi);
	return 1;
}
// сделано*****

int my_buses()
{
	int i;
	for (i = 0; i != I_max; i++)
	{
		printf("%d.%s\n", i + 1, BUSES[i].number);
	}
	return 1;
}
// сделано*****

int my_list_streets()
{
	while (1)
	{
		system("cls");
		char c;
		int j, i;
		for ( i = 0; i < )
		/*for (j = 0; j < J_max; j++)
		{

			printf("%d.%s\n", j + 1, STREETS[j].name);
		}*/
		printf("\n\n0.To return to the main menu");
		if ((c = getch()) == '0') return 1;
	}
}
// сделано*****

int my_list_bus() // вывод базы номеров автобусов на экран
{
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	while (1)
	{
	breake:
		system("cls");
		char c;
		int i, j;
		my_buses();
		printf("\n\n0.To return to the main menu\nTo see the bus route enter the room: ");
		if ((c = getch()) == '0') return 1;
		scanf("%d", &j);
		if ((j >= 1) && (j <= I_max))
		{
			system("cls");
			for (i = 0; i < 10; i++)
			{
				if ((BUSES[j - 1].ID[i+1]) == '\0' )
				{
					printf("%s\n", STREETS[BUSES[j - 1].ID[i]].name);
					printf("\n\n9.Back");
					while (1)
					{
						if ((c = getch()) == '9') goto breake;
					}
				}
				printf("%s\n", STREETS[BUSES[j - 1].ID[i]].name);
			}
		}
		system("cls");
		SetConsoleTextAttribute(console, 10);
		printf("There is no such number in the list. Please try again");
		SetConsoleTextAttribute(console, 15);
		Sleep(1500);
	}
}
 // сделано*****

int my_remove() // удаление
{
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

	char c;
	while (1)
	{
		system("cls");
		int j;
		my_buses();
		printf("\n\n0.To return to the main menu\nTo see the bus route enter the room: ");
		if ((c = getch()) == '0') return 1;
		scanf("%d", &j);
		if ((j >= 1) && (j <= I_max))
		{
			system("cls");
			for ( ; j < I_max; j++)
			{
				BUSES[j-1] = BUSES[j];
			}
			I_max--;
		}
		else
		{
			system("cls");
			SetConsoleTextAttribute(console, 10);
			printf("There is no such number in the list. Please try again");
			SetConsoleTextAttribute(console, 15);
			Sleep(1500);
		}
	}
}
 // сделано***

int my_delete() // очищение файла
{
	system("cls");
	FILE *fi = fopen("basedata", "w");
	printf("The deletion is performed");
	Sleep(1000);
	fclose(fi);
	return 1;
}
// сделано*****

int my_add() // добавление
{
	system("cls");
	int i,j = 0;
	char c, buf[N1] = "";
	printf("Enter number bus  ");
	scanf("%4s", BUSES[I_max].number); // сделать чтобы проверяла нету ли такого же номера
	printf("Enter the name of the streets. To exit enter the Save\n");
	while (1)
	{
	next:
		memset(buf, 0, sizeof(char)*N1);
		scanf("%s", buf);
		if ((strcmp(buf, "Save")) == 0)
		{
			I_max++;
			return 1;
		}
		for (i = 0; i < J_max; i++)
		{
			if ((strcmp(buf, STREETS[i].name)) == 0)
			{
				BUSES[I_max].ID[j++] = i;
				goto next;
			}
		}
		for (i = 0; i < N1; i++)
		{
			if (buf[i] == '\0')
				break;
			STREETS[J_max].name[i] = buf[i];
		}
		BUSES[I_max].ID[j++] = J_max++;
	}
}
// сделано*****

int search_1()
{
	while (1)
	{
		system("cls");
		char c;
		printf("\n\n9.Back\n0.To return to the main menu");
		if ((c = getch()) == '9') return 6;
		if (c == '0') return 5;
	}
}

/*Начальная точка: фыафыафы
Конечная точка:  фыафыаф

9.Назад
0.В меню*/

int search_2()
{
	while (1)
	{
		system("cls");
		char c;
		printf("\n\n9.Back\n0.To return to the main menu");
		if ((c = getch()) == '9') return 6;
		if (c == '0') return 5;
	}
}

int search_3()
{
	while (1)
	{
		system("cls");
		char c;
		printf("\n\n9.Back\n0.To return to the main menu");
		if ((c = getch()) == '9') return 6;
		if (c == '0') return 5;
	}
}

int search_4()
{
	while (1)
	{
		system("cls");
		char c;
		printf("\n\n9.Back\n0.To return to the main menu");
		if ((c = getch()) == '9') return 6;
		if (c == '0') return 5;
	}
}

int search() // интерфейс поиска
{
	while (1)
	{
		system("cls");
		char c;
		printf("1.Route without a transfer\n2.A route with one change\n3.The route with two more transfers\n4.Buses crossing the particular street\n\n0.To return to the main menu");
		if ((c = getch()) == '1')
		{
			if (search_1() == 5)
				return 1;
		}
		if (c == '2')
		{
			if (search_2() == 5)
				return 1;
		}
		if (c == '3')
		{
			if (search_3() == 5)
				return 1;
		}
		if (c == '4')
		{
			if (search_4() == 5)
				return 1;
		}
		if (c == '0')
		{
			return 1;
		}
	}
}


