#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>


typedef char* string;

int main()
{
    string *p_arr;
    int i, count;

    scanf( "%d", &count );

    p_arr = ( string * ) calloc( count, sizeof( string ) );
    for ( i = 0; i < count; i++ )
        p_arr[ i ] = ( char* ) calloc( 100, sizeof( char ) );


        //--------------------
    printf( "Input array: \n");
    for ( i = 0; i < count; i++ )
        scanf( "%s", p_arr[ i ] );

    printf( "Output array:\n" );

    for ( i = 0; i < count; i++ )
        printf( "%s\n", p_arr[ i ] );

    //------------------------

    for ( i = 0; i < count; i++ )
        free( p_arr[ i ] );

    free( p_arr );

    return 0;
}
