#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>



int main()
{

    //char strings[10][32], str[32];
    char **strings = NULL;
    char *buf_ptr;

    FILE *fi, *fo;
    int count = 0, i, j;
    if((fi = fopen ("input.txt", "rt")) == NULL){
        return 1;
    }
    if((fo = fopen ("out.txt", "wt")) == NULL){
        return 1;
    }

    /* Считываем слова из файла */

    while (feof( fi ) == 0 || count > 10)
    {
        strings = ( char ** ) realloc( strings, count + 1 );
        strings[ count ] = ( char * ) calloc( 32, sizeof( char ) );
        fscanf ( fi, "%s", strings[ count++ ] );
       // strcpy(strings[count++], str);
    }
    fclose( fi );
    /* Выводим считаные строки */
    for(i = 0; i < count; i++)
        printf("%s\n", strings[i]);
    putchar('\n');
    /* Сортировка методом пузырька */
    for(i = 1; i < count; i++)
        for(j = 0; j < count - i; j++)
            if(strcmp(strings[j], strings[j+1]) > 0){
               /* strcpy(str, strings[j]);
                strcpy(strings[j], strings[j+1]);
                strcpy(strings[j+1], str);*/
                buf_ptr = strings[ j ];
                strings[ j ] = strings[ j + 1 ];
                strings[ j + 1 ] = buf_ptr;

            }
    /* Выводим отсортированные строки */
    FILE *fp = fopen ( "output.txt", "wt");
    for(i = 0; i < count; i++)
        fprintf( fo, "%s\n", strings[i]);
        fclose (fp);
    fclose (fi);
    getch();
    return 0;
}


