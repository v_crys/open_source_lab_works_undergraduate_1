#include <stdio.h>
#include <windows.h>

int main( )
{
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(console, 15);

    int a = 0;
    int b = 0;
    int c = 0;
    int D = 0;

    printf ( " Enter a, b, c:\n\n  a = " );
    scanf( "%d", &a);
    printf ( "  b = " );
    scanf( "%d", &b );
    printf( "  c = " );
    scanf( "%d", &c );

    D = ( b*b - 4*a*c ) ;
    printf( "\n  D = %d\n\n", D );

    printf( " The number of real roots of the equation  %dx^2 + %dx + %d = 0:  ", a, b,c );

    SetConsoleTextAttribute(console, 12);
    if ( D > 0)
        printf( " 2 \n\n\n\n\n\n" );
    else if ( D =0 )
        printf( " 1 \n\n\n\n\n\n" );
    else
        printf( " 0 \n\n\n\n\n\n" );
    SetConsoleTextAttribute(console, 15);
}
