#include <stdio.h>

int main ()
{

    float y = 0;
    float Y = 0;
    float X = 0;

    printf ( "Enter x and y:\n\n x = " );
    scanf ( "%f", &X );
    printf ( " y = " );
    scanf ( "%f", &Y );

    if ( Y >= 0 )
    {
        if ( X >= 0 )
        {
            y = -2*X + 10;
                if ( Y <= y )
                    printf( "\n\nPoint (%4.2f,%4.2f) are in the realm\n\n", X, Y );
                else
                    printf( "\n\nPoint (%4.2f,%4.2f) are not in the realm\n\n", X, Y );
        }
        else
            printf( "\n\nPoint (%4.2f,%4.2f) are not in the realm\n\n", X, Y );
    }
    else
        printf( "\n\nPoint (%4.2f,%4.2f) are not in the realm\n\n", X, Y );

}
