#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <windows.h>

int main()
{
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    FILE *fi = fopen( "in.txt", "r" );
    int str[ 80 ] ;
    int str_i;

    int i, j , k, z, c;
    char flag_ex = 0;
    char flag = 0;

    char cnt_str = 20;
    char del_color = 0;

    for( ;; )
    {
        //-------- save str
        str_i = 0;
        while ( 1 )
        {

            c = getc( fi );

            if ( c == EOF )
            {
                flag_ex = 1;
                break;
            }

            if ( ( c == '\n' ) || ( str_i == 79 ) )
            {
                str[ str_i++ ] = '\n';
                str[ str_i++ ] = 0;
                break;
            }

            str[ str_i++ ] = c;
        }


        for ( i = 0; i < str_i; i++ )
        {
            if ( ( str[ i ] == '/' ) && ( str[ i + 1 ] == '/' ) && ( flag == 0 ) )
                flag = 1;
            else if ( ( str[ i ] == '/' ) && ( str[ i + 1] == '*' ) && ( flag == 0 ))
                flag = 2;
            else if ( ( str[ i ] == '*' ) && ( str[ i + 1 ] == '/' ) && ( flag == 2))
            {
                del_color = 2;
                flag = 0;
            }
            else if ( ( str[ i ] == 0 ) && ( flag == 1 ) )
                flag = 0;
            else if ( ( str[ i - 1 ] != '\\' ) &&  ( str[ i ] == '"'  ) && ( flag == 0 ) )
                flag = ( flag == 3 ) ? 0 : 3;



            if ( del_color == 0 )
            {
                if ( ( flag == 1 ) || ( flag == 2 ) )
                    SetConsoleTextAttribute(console, 10);
                else
                    SetConsoleTextAttribute(console, 15);
            } else del_color--;

            putchar( str[ i ] );
        }


        if ( cnt_str == 0 )
        {
            getch();
            cnt_str = 20;
        }

        cnt_str--;

        if ( flag_ex ) break;
    }

    fclose( fi );
    return 0;
}
