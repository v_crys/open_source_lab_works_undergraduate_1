#include <stdio.h>
#include <stdlib.h>

struct time {
    int hour;
    int min;
};

struct person {
    int num;
    struct time st;
    int del;
};

int minuts_sub( struct time tm1, struct time tm2 );

int main()
{
    struct time time_open, time_close, active_time, buf_time;
    struct person pers[ 100 ];

    int i, c, j, fl, i_buf;
    int numb = 1;

    printf( "Scan open:\n\tHour: " );
    scanf( "%d", &time_open.hour );
    printf( "\tMinutes: " );
    scanf( "%d", &time_open.min );

    printf( "Scan close:\n\tHour: " );
    scanf( "%d", &time_close.hour );
    printf( "\tMinutes: " );
    scanf( "%d", &time_close.min );
    printf( "\n\n" );

    active_time.hour = time_open.hour;
    active_time.min = time_open.min;

    while ( 1 )
    {
        c = getch();

        switch ( c )
        {
            case 't':
                printf( "Scan time start:\n\tHour: " );
                scanf( "%d", &pers[ numb ].st.hour );
                printf( "\tMinutes: " );
                scanf( "%d", &pers[ numb ].st.min );
                printf( "Delay: " );
                scanf( "%d", &pers[ numb ].del );

                if ( minuts_sub( pers[numb].st, active_time ) < 0 )
                {
                    printf( "\nLate\n" );
                    break;

                }

                if ( minuts_sub( time_open, pers[ numb ].st ) > 0 )
                {
                    printf( "Don't worked\n\n" );
                    break;
                }

                if ( ( minuts_sub( time_close, pers[ numb ].st ) - pers[ numb ].del ) < 0 )
                {
                    printf( "Don't worked\n\n" );
                    break;
                }

                //------------------
                fl = 0;
                for ( i = 1; i < numb; i++ )
                {
                    if ( ( minuts_sub( pers[ numb ].st, pers[ i ].st ) < 0 )
                         && ( ( minuts_sub( pers[ numb ].st, pers[ i ].st ) * -1 ) < pers[ numb ].del ))
                            fl = 1;

                    if ( ( minuts_sub( pers[ numb ].st, pers[ i ].st ) > 0 ) &&
                        ( ( minuts_sub( pers[ numb ].st, pers[ i ].st ) ) < pers[ i ].del ))
                            fl = 1;

                    if ( fl ) break;
                }
                if ( fl ) {
                        printf( "\nBUSY\n");
                        break;
                }

                pers[ numb ].num = numb;
                printf( "Your number tickets: %d\n\n", numb++ );

                break;


            case 'o':
                buf_time.hour = 0;
                buf_time.min = 0;

                i_buf = 0;
                for ( i = 1; i < numb; i++ )
                {
                    if ( minuts_sub( pers[ i ].st, active_time ) <= 0 ) continue;

                    if ( minuts_sub( pers[ i ].st, active_time ) < minuts_sub( pers[ i ].st, buf_time ) )
                    {
                        i_buf = i;
                        buf_time = pers[ i ].st;
                    }
                }

                if ( i_buf == 0 )
                {
                    printf( "\nDon't clients\n" );
                    break;
                }
                printf( "\nClient: %d\n\tHour: %d\n\tMinutes: %d\n\tDelay: %d\n\n", pers[ i_buf ].num, pers[ i_buf ].st.hour, pers[ i_buf ].st.min, pers[ i_buf ].del );

                active_time= pers[ i_buf ].st;

                break;
        }
    }

    return 0;
}


int minuts_sub( struct time tm1, struct time tm2 )
{
    return ( ( tm1.hour - tm2.hour ) * 60 ) + ( tm1.min - tm2.min ) ;
}
