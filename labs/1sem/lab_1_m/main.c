#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>

#define NAME_FILE       "test.c"


#define MAX_LEN_LINE    80
#define MAX_DEF         100

//------- COLOR
#define NORM            0
#define COMM_1          1
#define COMM_2_ST       2
#define COMM_2_END      3
#define LITERS          4
#define MY_DEF          5


#define FIND_LIT(lit, color)                                                    \
        for ( i = 0; i < MAX_LEN_LINE; i++ )                                    \
        {                                                                       \
            if ( strstr( &arr[ i ], ( lit ) ) == NULL ) break;                  \
            i = ( strstr( &arr[ i ], ( lit ) ) - arr );                         \
            if ( i != 0 )                                                       \
                if ( ( arr[ i - 1 ] == '\\' ) || ( arr[ i - 1 ] == '\'' ) ){    \
                                                                                \
                    continue;                                                   \
                }                                                               \
            map_line[ i ] = ( color );                                          \
                                                                                \
        }

#define CMP_W(var) ( ( ( (var) >= '0' ) && ( (var) <= '9' ) ) ||                \
                    ( ( (var) >= 'a' ) && ( (var) <= 'z' ) ) ||                 \
                    ( ( (var) >= 'A' ) && ( (var) <= 'Z' ) ) ||                 \
                    ( (var) == '_' ) )

int main()
{
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    int flag = NORM;

    char arr[ MAX_LEN_LINE ];
    int map_line[ MAX_LEN_LINE ];
    char *words[ MAX_DEF ];

    int cnt_words = 0;
    char flag_exit = 0;
    int c, i, j, k, delay_col = 0;

    FILE *fi = fopen( NAME_FILE, "rt" );
    if ( fi == NULL ) {
            printf( "Don't file!" );
            return 0;
    }

    while ( 1 )
    {
    //---- INIT
        //----- input lines
        c = 0;
        for ( i = 0; ( ( i < MAX_LEN_LINE ) && ( c != '\n' ) && ( c != EOF ) ); i++ )
        {
            c = getc( fi );
            arr[ i ] = c;
        }

        if ( c == EOF ) flag_exit = 1;

        //----- clear map
        for ( i = 0; i < MAX_LEN_LINE; i++ )
            map_line[ i ] = 0;

    //---- WORKED CODE
        //------- save define
        if ( strstr( arr, "#define" ) != NULL )
        {
            for ( i = ( strstr( arr, "#define" ) - arr + 7 ); ( ( i < MAX_LEN_LINE ) && ( arr[ i ] != '\n' ) ); i++ )
                if ( CMP_W( arr [ i ] ) )
                        break;

            words[ cnt_words ] = ( char * ) calloc( MAX_DEF, sizeof( char ) );

            for ( j = i ; ( ( j < MAX_LEN_LINE ) && ( arr[ j ] != '\n' ) ); j++ )
            {
                if ( !( CMP_W( arr[ j ] ) ) )
                            break;

                words[ cnt_words ][ j - i ] = arr[ j ];
            }
            words[ cnt_words][ j ] = 0;

            cnt_words++;
        }


        //----- create map
        FIND_LIT("//", COMM_1);
        FIND_LIT("/*", COMM_2_ST);
        FIND_LIT("*/", COMM_2_END);

        FIND_LIT( "\"", LITERS );

        for ( j = 0; j < cnt_words; j++ )
        {
            //FIND_LIT( words[ j ], MY_DEF );

            for ( i = 0; i < MAX_LEN_LINE; i++ )
            {
                if ( strstr( &arr[ i ], words[ j ] ) == NULL ) break;
                i = ( strstr( &arr[ i ], words[ j ] ) - arr );

                if CMP_W( arr[ i - 1 ] )
                    continue;

                if CMP_W( arr[ i + strlen( words[ j ] ) ] )
                    continue;

                map_line[ i ] = MY_DEF;
            }
        }




        //----- output text & map
        c = 0;
        for ( i = 0; ( ( i < MAX_LEN_LINE ) && ( c != '\n' ) && ( c != EOF ) ); i++ )
        {
                    c = arr[ i ];

                    //------ flag set
                    switch ( flag )
                    {
                        case NORM:
                            flag = map_line[ i ];
                            break;

                        case COMM_2_ST:
                            if ( map_line[ i ] == COMM_2_END )
                            {
                                delay_col = 2;
                                flag = NORM;
                            }
                            break;

                        case LITERS:
                            if ( map_line[ i ] == LITERS )
                            {
                                delay_col = 1;
                                flag = NORM;
                            }
                            break;

                        case MY_DEF:
                            if ( !CMP_W( c ) )
                                    flag = NORM;

                            break;

                    }


                    //---- coloring
                    if ( delay_col == 0 )
                    {
                        switch ( flag )
                        {
                            case NORM:
                                SetConsoleTextAttribute(console, 0xF);
                                break;

                            case COMM_1:
                                SetConsoleTextAttribute(console, 1);
                                break;

                            case COMM_2_ST:
                                SetConsoleTextAttribute(console, 2);
                                break;

                            case LITERS:
                                SetConsoleTextAttribute(console, 3);
                                break;

                            case MY_DEF:
                                SetConsoleTextAttribute(console, 4);
                                break;
                        }
                    } else delay_col--;

                    putchar( c );
        }

        //----- flag refresh
        if ( flag == COMM_1 )
            flag = NORM;

        getch();
        if ( flag_exit ) break;
    }

    return 0;
}
