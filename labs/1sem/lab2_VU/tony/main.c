#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <string.h>
#include <ctype.h>

#define N 50

int i_max = 0;

struct record
{
	int House_number;
	char Street[N];
	int Rooms_number;
	float Cost;
	float Distance_metro;
} Record[N];

int add_record(struct record *p)
{
	int i = 0;
	if ((p->Cost == 0) || (p->Distance_metro == 0) || (p->House_number == 0) || (p->Rooms_number == 0) || (p->Street[0] == '\0'))
		return 1;
	Record[i_max].Cost = p->Cost;
	Record[i_max].Distance_metro = p->Distance_metro;
	Record[i_max].House_number = p->House_number;
	Record[i_max].Rooms_number = p->Rooms_number;
	for (i = 0; i <= N; i++)
	{
		if (p->Street[i] == '\0')
			break;
		Record[i_max].Street[i] = p->Street[i];
	}

	i_max++;
	return 0;
}

int delete_record()
{
	int n = 0, i = 0;
	printf("Enter the id record.\nid = ");
	scanf("%d", &i);
	if ((i < 0) || (i >= i_max))
		return 1;

	if (i == (i_max - 1))
	{
		i_max--;
		return 0;
	}

	for (; i < (i_max - 1); i++)
	{
		Record[i].Cost = Record[i + 1].Cost;
		Record[i].Distance_metro = Record[i + 1].Distance_metro;
		Record[i].House_number = Record[i + 1].House_number;
		Record[i].Rooms_number = Record[i + 1].Rooms_number;
		for (n = 0; n < N; n++)
		{
			if (Record[i + 1].Street[n] == '\0')
				break;
			Record[i].Street[n] = Record[i + 1].Street[n];
		}
	}

	i_max--;
	return 0;
}

int search(struct record *p)
{
	int mod_s = 0,
		mod_c = 0,
		mod_d = 0,
		mod_hn = 0,
		mod_rn = 0,
		num = 0;
	char street[N];

	if (p->Street[0] == '-') mod_s = 1;
	if (p->Distance_metro == 0) mod_d = 1;
	if (p->Cost == 0) mod_c = 1;
	if (p->House_number == 0) mod_hn = 1;
	if (p->Rooms_number == 0) mod_rn = 1;

	int k;
	for (k = 0; k < i_max; k++)
	{
		strcpy(street, Record[k].Street);
		if ((mod_s || !strcmp(_strlwr(p->Street), _strlwr(street))) && (mod_d || (p->Distance_metro == Record[k].Distance_metro)) && (mod_c || (p->Cost == Record[k].Cost)) && (mod_hn || (p->House_number == Record[k].House_number)) && (mod_rn || (p->Rooms_number == Record[k].Rooms_number)))
			printf("%d. %s Street - House %d\n Rooms: %d\n Distance: %3.2f\n Cost: %3.2f\n ID[%d]\r\n", ++num, Record[k].Street, Record[k].House_number, Record[k].Rooms_number, Record[k].Distance_metro, Record[k].Cost, k);
	}
	if (!num) return 1; else return 0;
}

void SAVE()
{
	FILE *fi = fopen("C:\\Record","wb");
	if (fi == NULL)
	{
		printf("Warning\n");
		return;
	}
	fwrite(&Record[0], sizeof(struct record), i_max, fi);
	fclose(fi);
}

void read_file()
{
	int i = 0, n = 0;
	FILE *fi = fopen("C:\\Record", "rb");
	if (fi == NULL)
	{
		printf("Warning\n");
		return;
	}
	i_max = fread(&Record[0], sizeof(struct record), 50, fi);
	fclose(fi);
}

int enter(struct record *p)
{
	int i = 0;
	system("cls");
	p->House_number = 0; p->Rooms_number = 0; p->Cost = 0; p->Distance_metro = 0;
	for (i = 0; i <= N; i++)
		p->Street[i] = '\0';

	printf("Enter the house number: ");
	scanf("%d", &p->House_number);

	printf("Enter the cost: ");
	scanf("%f", &p->Cost);

	printf("Enter the number of rooms: ");
	scanf("%d", &p->Rooms_number);

	printf("Enter the distance to the metro (in meters): ");
	scanf("%f", &p->Distance_metro);

	printf("Enter the street name (without spaces): ");
	scanf("%s", &p->Street);

	if ((p->Cost < 0) || (p->Cost > 2147483646) || (p->Rooms_number < 0) || (p->Rooms_number > 2147483646))
		return 1;
	if ((p->Distance_metro < 0) || (p->Distance_metro > 2147483646) || (p->House_number < 0) || (p->House_number > 2147483646))
		return 1;

	system("cls");
	return 0;
}

void sorting()
{
	int i = 0, j = 0, n = 0, flag = 0;
	char Street1[N] = "", Street2[N] = "";
	struct record temp;

	for (j = 0; j < i_max; j++)
	{
		flag = 0;
		for (i = 0; i < (i_max - 1); i++)
		{
			for (n = 0; n < N; n++)
			{
				Street1[n] = Record[i].Street[n];
				Street1[n] = (char)tolower(Street1[n]);
				Street2[n] = Record[i+1].Street[n];
				Street2[n] = (char)tolower(Street2[n]);
			}
			for (n = 0; n < N; n++)
			{
				if (Street1[n] == Street2[n])
					continue;
				if (Street1[n] < Street2[n])
					break;

				temp.Cost = Record[i].Cost; temp.House_number = Record[i].House_number;
				temp.Distance_metro = Record[i].Distance_metro; temp.Rooms_number = Record[i].Rooms_number;
				for (n = 0; n < N; n++)
					temp.Street[n] = Record[i].Street[n];

				Record[i].Cost = Record[i+1].Cost; Record[i].House_number = Record[i+1].House_number;
				Record[i].Distance_metro = Record[i+1].Distance_metro; Record[i].Rooms_number = Record[i+1].Rooms_number;
				for (n = 0; n < N; n++)
					Record[i].Street[n] = Record[i+1].Street[n];

				Record[i + 1].Cost = temp.Cost; Record[i + 1].House_number = temp.House_number;
				Record[i + 1].Distance_metro = temp.Distance_metro; Record[i + 1].Rooms_number = temp.Rooms_number;
				for (n = 0; n < N; n++)
					Record[i + 1].Street[n] = temp.Street[n];
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			break;
	}

}

int Enter_display()
{
	int i = 0;
	if (i_max == 0)
	{
		printf("No records\n");
		return 1;
	}

	sorting();

	for (i = 0; i < i_max; i++)
	{
		printf("Street: %s\n", Record[i].Street);
		printf("House number: %u\n", Record[i].House_number);
		printf("Rooms: %u\n", Record[i].Rooms_number);
		printf("Cost: %3.2f\n", Record[i].Cost);
		printf("Distance to the metro: %3.2f\n", Record[i].Distance_metro);
		printf("id = %d\n\n", i);
	}
	return 0;
}

int main()
{
	char c = 0;
	struct record Record_temp;
	struct record *p;
	p = &Record_temp;
	read_file();

	while (1)
	{
		system("cls");
		printf("To search record -  enter '1'.\nTo add record, enter '2'.\nTo delete a record, enter '3'.\nDisplays all records on the screen, enter '4'\nSave the result, enter '5'\nTo exit the program, tap '6'.\n");
		c = _getch();
		system("cls");

		if (c == '6')
			break;

		if (c == '2')
		{
			if (enter(p) == 1)
			{
				printf("Error entered data.\nPress any key to continue.");
				_getch();
				continue;
			}
			if (add_record(p) == 1)
				printf("Sorry, the record can't be add.\nPress any key to continue.");
			else
				printf("The record was added successfully.\nPress any key to continue.");
			_getch();
			continue;
		}

		if (c == '1')
		{
			enter(p);
			if (search(p) == 1)
				printf("This record wasn't found.\n");
			printf("\nPress any key to continue.");
			_getch();
			continue;
		}

		if (c == '3')
		{

			if (delete_record() == 1)
				printf("This record was not successfully deleted.\nPress any key to continue.");
			else
				printf("This record was successfully deleted.\nPress any key to continue.");
			_getch();
			continue;
		}
		if (c == '4')
		{
			Enter_display();
			printf("Press any key to continue.");
			_getch();
		}
		if (c == '5')
			SAVE();
	}

	SAVE();
	return 0;
}
