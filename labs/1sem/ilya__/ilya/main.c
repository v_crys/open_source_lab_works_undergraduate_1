#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    //---- input data
    float a, b, c, d, x0, h, m;
    //---- calculated
    float p, x, out;
    //---- other
    int i, k;
    float buf, buf2;

    printf( "Print val: \n\ta: " );
    scanf( "%f", &a );

    printf( "\tb: ");
    scanf( "%f", &b );

    printf( "\tc: ");
    scanf( "%f", &c );

    printf( "\td: ");
    scanf( "%f", &d );

    printf( "\tx0: ");
    scanf( "%f", &x0 );

    printf( "\th: ");
    scanf( "%f", &h );

    printf( "\tm: ");
    scanf( "%f", &m );

    //---- p calculated
    //---- p = min( a, b, c ) + d
    if ( a < b )
        p = ( a < c ) ? a : c;
    else
        p = ( b < c ) ? b : c;

    p += d;

    printf( "\np = %f", p );

    //---- x calculated
    //---- x = x0 + ( i - 1 ) * h
    //---- i = 1 .. m
    x = x0;
    for ( i = 1; i <= m ; i++ )
        x += ( i - 1 ) * h;

    printf( "\nx = %f", x );

    //---- calculated main
    out = 0;
    for ( k = 1; k <= 6; k += 2 )
    {
        buf = pow( ( -1 ), k - 2 ); //---- ( -1 ) ^ (k - 2)
        buf *= pow( x, 2 * k ); //----- * ( -1 ) ^ (k - 2) ) * x^2k
        buf *= exp( -( p * k ) );//--- top

        //---- fact

        buf2 = 1;
        for ( i = ( ( 2 * k ) - 1 ); i != 0; i-- )
            buf2 *= i;

        buf = ( float )buf / ( float )buf2;
        out += 1 + buf;

    }

    printf( "\nOut: %f", out );
    return 0;
}
