#include <stdio.h>
#include <stdlib.h>

int integral( int a, int b, int (*f)( int x ) );
int func( int x );

int main()
{
    int a, b;
    printf("Print A = " );
    scanf( "%d", &a );

    printf("Print B = " );
    scanf( "%d", &b );

    printf( "Otvet = %d", integral( a, b, func ) );
    return 0;
}

int func( int x )
{
    return x;
}

int integral( int a, int b, int (*f)( int x ) )
{
    int S = 0;
    int i;

    for ( i = a; i < b; i ++ )
        S += (*f)( i );

    return S;
}
