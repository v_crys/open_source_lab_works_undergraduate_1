//Abiturient: Фамилия, Имя, Отчество, Адрес, Оценки. Создать массив объектов.
//Вывести:
//а) список абитуриентов, имеющих неудовлетворительные оценки;
//б) список абитуриентов, сумма баллов у которых не меньше заданной;
//в) cписок абитуриентов, имеющих полупроходной балл.
#include <iostream>
#include <cstring>
#include <ctime>
using namespace std;
#define N 3 // kol-vo ludej
#define M 5 // kol-vo ocenok
#define PROHOD 12
#define len 16
class Abiturient {
	string last_name;
    string name;
    string patrname;
    string adress;
    int mark[ M ];

public:

    //---------- constructor
    Abiturient ();

    //---------- set function
    void set_last_name  ( string n);
    void set_name       ( string n );
    void set_patrname   ( string n );
    void set_adress     ( string n );
    void set_mark       ( );

    //------------- get function
	string get_last_name ();
	string get_name ();
	string get_patrname ();
	string get_adress ();
	int*   get_mark ();


    int summ_ball ();
	void show_abit();
};

void Abiturient::set_last_name  ( string n ) { last_name = n;  }
void Abiturient::set_name       ( string n ) { name = n; }
void Abiturient::set_patrname   ( string n ) { patrname = n; }
void Abiturient::set_adress     ( string n ) { adress = n; }
void Abiturient::set_mark       ()           { int i=0; for (i=0; i<M; i++) cin>>mark[i]; }

string Abiturient::get_last_name    () { return last_name; }
string Abiturient::get_name         () { return name; }
string Abiturient::get_patrname     () { return patrname; }
string Abiturient::get_adress       () { return adress; }
int* Abiturient::get_mark           () { int *p=mark; return p; }

int Abiturient::summ_ball () {int i=0, s=0; for (i=0; i<M; i++) s+=mark[i]; return s;}
void Abiturient::show_abit()
	{
	    int i=0;
		cout<<"ABITURIENT\n";
		cout<<"Full name: "<<last_name<<" "<<name<<" "<<patrname<<endl;
		cout<<"Adress: "<<adress<<endl;
		cout<<"Marks: ";
		for (i=0; i<M; i++)
		    cout<<mark[i]<<" ";
		cout<<endl;
	}

Abiturient::Abiturient ()
{
	/*int i=0, j=0;
	//char n[32];
	//for (j=0; j<num; j++)
	{
		cout<<"New abit:\n";
		cout<<"Input last name: "; cin>>n; a[j].set_last_name(n);
		cout<<"Input name: "; cin>>n; a[j].set_name(n);
		cout<<"Input patrname: "; cin>>n; a[j].set_patrname(n);
		cout<<"Input mark: "; a[j].set_mark();
		cout<<"Input adress: "; cin>>n; a[j].set_adress(n);
	}*/

	string str_buf;

		cout << "New abit:\n";
		cout << "Input last name: ";  cin >> str_buf; set_last_name( str_buf );
		cout << "Input name: ";       cin >> str_buf; set_name( str_buf );
		cout << "Input patrname: ";   cin >> str_buf; set_patrname( str_buf);
		cout << "Input mark: ";       set_mark();
		cout << "Input adress: ";     cin >> str_buf; set_adress( str_buf );

}
void printNeud (Abiturient *a)
{
	int i=0, j=0;
	int *ptr;
	cout<<"\n\nNeud:\n";
	for (i=0; i<N; i++)
		{
		ptr=a[i].get_mark();
		for (j=0; j<M; j++)
		    if (*(ptr+j)<3)
			    {
			        a[i].show_abit();
			        break;
			    }
		}
}
void printPoluprohod (Abiturient *a)
{
    int i=0;
    for (i=0; i<N; i++)
     if (a[i].summ_ball()/2>PROHOD)
        a[i].show_abit();
}
void printBySumm (Abiturient *a, int ball)
{
	int i;
	cout<<"\nSumm higher than "<<ball<<":\n";
	for (i=0; i<N; i++)
		if (a[i].summ_ball()>=ball)
			a[i].show_abit();
}

int main ()
{
	Abiturient abit[N];
	int i=0;
//	read_abit(abit, N);
	for (i=0; i<N; i++)
		abit[i].show_abit();
	printNeud(abit);
	cout<<"\n\nInput ball: ";
	cin>>i;
	printBySumm(abit, i);
	cout<<"\n\nImeut poluprohodnoj ball:\n";
	printPoluprohod(abit);
}
