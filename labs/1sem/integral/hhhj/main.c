#include <stdio.h>
#include <stdlib.h>

void str_compound( char *str1, char *str2);
int main()
{
    FILE *fi = fopen("in.txt","rt");
    FILE *fo = fopen("out.txt","wt");

    char arr[100];
    int str_parity = 0;
    int i,j;
    int c;
    i = 0;
    while ((c = fgetc(fi)))
    {
        if((c == '\n')||(c == EOF))
        {
            if(str_parity)
            {
                for(j = i - 1; j>=0; j--)
                    fputc(arr[j],fo);
            }else{
                for(j = 0; j<i;j++)
                    fputc(arr[j],fo);
            }

            str_parity ^= 1;
            i = 0;

            if(c == EOF)break;
            fputc(0x0D,fo);
            continue;
        }
        arr[i++] = c;
    }
return 0;
}

