//Вычислить сумму первых N элементов ряда последовательности:
//S=3/1-4/4+5/7-6/10+...
#include <stdio.h>

int main (void)
{
	int S;
	int n;
	int a1;
	int d;
	int Sn;
	a1 = 3;
	d = -1;
	printf("Vvod N");
	scanf("%d", &n);

	if ( n < 0 )
	{
		printf( "ERROR!" );
		return 0;
	}

	if ( n == 0 )
	{
		printf( "Summ = 0" );
		return 0;
	}

//За основу взята формула Sn=(2a1+d(n-1))/2
	if ( n > 0 )
	{
		Sn = ( 2 * a1 + d * ( n - 1) ) / 2;
		printf( "Summ = %d", Sn );
	}

    return 0;
}
