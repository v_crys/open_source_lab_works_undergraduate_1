#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <windows.h>

int main()
{
    //------- detecting words
    char *WORD[ ]  = { "return", "break", "continue", "if", "for", "while", "switch", "case", "else" };

    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    FILE *fi = fopen( "in.c", "r" );
    char str[ 80 ] ;
    int str_i;

    int i, j , k, c;
    char flag_ex = 0; //--- if last str flag up
    char flag = 0; //--- overall flag

    char cnt_str = 20; //---- str show

    char cmp = 0;

    for ( i = 0; i < 10; i++ );
    printf( "%d", i );
    getch();

    //---- looping
    for( ;; )
    {
        //-------- save str
        str_i = 0;
        while ( 1 )
        {

            c = getc( fi );

            if ( c == EOF )
            {
                flag_ex = 1;
                break;
            }

            if ( ( c == '\n' ) || ( str_i == 78 ) )
            {
                str[ str_i++ ] = '\n';
                str[ str_i++ ] = 0;
                break;
            }

            str[ str_i++ ] = c;
        }


        //----- each symbol work
        for ( i = 0; i < str_i; i++ )
        {
            if ( ( str[ i ] == '/' ) && ( str[ i + 1 ] == '/' ) && ( flag == 0 ) )
                flag = 1;
            else if ( ( str[ i ] == '/' ) && ( str[ i + 1] == '*' ) && ( flag == 0 ))
                flag = 2;
            else if ( ( str[ i ] == '*' ) && ( str[ i + 1 ] == '/' ) && ( flag == 2))
            {
              //  del_color = 2;
                flag = 0;
            }
            else if ( ( str[ i ] == 0 ) && ( flag == 1 ) )
                flag = 0;
            else if ( ( str[ i - 1 ] != '\\' ) &&  ( str[ i ] == '"'  )  )
                flag = ( flag == 3 ) ? 0 : 3;
            else if ( ( flag == 10 ) && ( ( str[ i ] == ' ' ) || ( str[ i ] == '\t' ) || ( str[ i ] == '(' ) || ( str[ i ] == ';') || ( str[ i ] == '\n' ) )  )
                flag = 0;
            else
            {
                if ( flag == 0 )
                {
                    //---------- key word brute
                    for ( j = 0; j < 9 ; j++ )
                    {
                        if ( i != 0 )
                            if ( ( str[ i - 1 ] != ' ' ) && ( str[ i - 1] != '\t' ) && ( str[ i - 1 ] != '(' )  ) break;


                        cmp = 1;
                        for ( k = 0; ; k++ )
                        {
                            if ( WORD[ j ][ k ] == 0 )
                                break;

                            if ( str[ i + k ] != WORD[ j ][ k ] )
                            {
                                cmp = 0;
                                break;
                            }
                        }

                        if ( ( cmp == 1 ) && ( ( str[ i + strlen( WORD[ j ] ) ] == ' ' ) || ( str[ i + strlen( WORD[ j ] ) ] == '\t' ) || ( str[ i + strlen( WORD[ j ] ) ] == ';' ) || ( str[ i + strlen( WORD[ j ] ) ] == '(' ) || ( str[ i + strlen( WORD[ j ] ) ] == '\n' ) ) )
                        {
                            flag = 10;
                            break;
                        }

                    }
                }
            }


                if ( flag == 10 )
                    SetConsoleTextAttribute(console, 10);
                else
                    SetConsoleTextAttribute(console, 15);



            putchar( str[ i ] );
        }


        if ( cnt_str == 0 )
        {
            getch();
            cnt_str = 20;
        }

        cnt_str--;

        if ( flag_ex ) break;
    }

    fclose( fi );
    return 0;
}
