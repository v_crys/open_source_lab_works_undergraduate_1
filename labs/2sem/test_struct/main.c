#include <stdio.h>
#include <stdlib.h>

#define uint8_t unsigned char
#define uint16_t unsigned short int

#define SPI_HandleTypeDef uint8_t
#define SPI_TypeDef uint8_t
#define GPIO_TypeDef uint8_t
#define uint32_t int

typedef struct _LoRa_LowLevel_SPI {
    SPI_HandleTypeDef hspi; //!---- don't change!!!
    SPI_TypeDef *SPI;   //----- example SPI1

    int Pin_MISO;  //----- Example GPIO_PIN_6
    uint16_t Pin_MOSI;
    uint16_t Pin_SCK;
    GPIO_TypeDef *PORT; //----- example GPIOA
    uint8_t PORT_AF;    //----- example GPIO_AF0_SPI1
} LoRa_LowLevel_SPI;

typedef struct _LoRa_LowLevel {
    LoRa_LowLevel_SPI ll_SPI;

    uint16_t Pin_RES;
    GPIO_TypeDef *Port_RES;
    uint16_t Pin_NSS;
    GPIO_TypeDef *Port_NSS;
    uint16_t Pin_DIO0;
    GPIO_TypeDef *Port_DIO0;
    uint16_t Pin_DIO1;
    GPIO_TypeDef *Port_DIO1;
    uint16_t Pin_DIO2;
    GPIO_TypeDef *Port_DIO2;

    uint16_t Pin_ANT_CN1;
    GPIO_TypeDef *Port_ANT_CN1;
    uint16_t Pin_ANT_CN2;
    GPIO_TypeDef *Port_ANT_CN2;
} LoRa_LowLevel;

typedef struct _LoRa_HighLevel {
    struct _LoRa_LowLevel ll_LoRa;

    uint32_t frequency;
    uint16_t  bandwidth;
    uint8_t power;
    uint16_t SpreadingFactor;
} LoRa_HighLevel;

typedef struct _LoRa_Client{
    uint8_t data_buf[ 256 ];
    struct _LoRa_HighLevel HAL_LoRa;


    uint8_t size_buf;

    uint8_t ID_server;
    uint8_t data_flag;

    uint8_t TS_client;
    uint8_t TS_empty;
} LoRa_Client;

void TEST( struct _LoRa_HighLevel *hh_LoRa )
{
    printf( "%d", hh_LoRa->ll_LoRa.ll_SPI.Pin_MISO );
}

int main()
{
    LoRa_Client test;

    test.HAL_LoRa.ll_LoRa.ll_SPI.Pin_MISO = 20;

    TEST( &(test.HAL_LoRa) );

    printf("Hello world!\n");
    return 0;
}
