Сей луг, уставленный душистыми скирдами, где светлые ручьи в кустарниках шумят.
Висит между цветов, пришлец осиротелый, и час их красоты - его паденья час!