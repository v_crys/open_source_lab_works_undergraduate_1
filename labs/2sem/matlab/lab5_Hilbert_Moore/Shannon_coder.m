function [ Table_coding, SI ] = Shannon_coder( Px )

    %----- sorting ascending
    [ Px_s, SI ] = sort( -Px );
    Px_s = -Px_s;
    
    %----- create servic matrix
    Len = zeros( 1, size( Px, 2 ) );
    Q = zeros( 1, size( Px, 2 ) );
    
    %----- calculation len and cumulative probability
    Q( 1 ) = 0;
    for i = 1 : size( Px, 2 )
        Len( i ) = -floor( log( Px_s( i ) ) / log( 2 ) ); 
        
        if ( i > 1 )
            Q( i ) = Q( i - 1 ) + Px_s( i - 1 );
        end
    end
    
    %------ expension in two pow (create table coding)
    Table_coding = zeros( 1, size( Px, 2 ) );
    for i = 1 : size( Px, 2 )
        Table_coding( i ) = 2;
        my_P = Q( i );
        for j = 1 : Len( i )
            Table_coding( i ) = Table_coding( i ) * 10;
            if ( my_P - ( 2 ^ ( -j ) ) >= 0 )
                Table_coding( i ) = Table_coding( i ) + 1;
                my_P = my_P - ( 2 ^ ( -j ) );
            end
        end
    end
    
    
end

