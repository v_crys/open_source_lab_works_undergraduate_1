function [ c,N,x,ax,Nx,Px,Ix,IxPx,y,ay,Ny,Py,Iy,IyPy,z,az,Nz,Pz,Iz,IzPz] = Lab1(~)
[c,N,x,y,z]=ReadFromFile;
ax=unique(x);
ay=unique(y);
az=unique(z);
[ Nx,Px,Ix,IxPx ] = Frequency( N, x, ax );
[ Ny,Py,Iy,IyPy ] = Frequency( N-1, y, ay );
[ Nz,Pz,Iz,IzPz ] = Frequency( N-2, z, az );

end