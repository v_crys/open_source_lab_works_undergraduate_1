function [ ci ] = split( c, a, P, N )
ci=char('');
cn=char('');
for i=1:(length(c)/6)
    for j=1:(6/N)
        cn{j}=c(j*N-N+1+6*(i-1));
        if(N>1)
            for k=1:N-1
                 cn{j}=strcat(char(cn{j}),char(c(j*N-N+1+k+6*(i-1))));
            end
        end
    end
    [ cnv ] = Arithmetic( cn, a, P);
    ci=strcat(ci,cnv);
end


end

