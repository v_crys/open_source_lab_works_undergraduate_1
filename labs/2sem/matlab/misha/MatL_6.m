function [ output_args ] = MatL_6( ~ )
[c,N,x,ax,Nx,Px,Ix,IxPx,y,ay,Ny,Py,Iy,IyPy,z,az,Nz,Pz,Iz,IzPz] = Lab1;
[cx]=split(c,ax,Px,1);
fprintf('\nCX=%s\n',cx);
Lx=length(cx)/(length(c)/1);
Rx=Lx/1;

[cy]=split(c,ay,Py,2);
fprintf('\nCY=%s\n  ',cy);
Ly=length(cy)/(length(c)/2);
Ry=Ly/2;


[cz]=split(c,az,Pz,3);
fprintf('\nCZ=%s\n',cz);
Lz=length(cz)/(length(c)/3);
Rz=Lz/3;

fprintf('\nLx=%f\tRx=%f\nLy=%f\tRy=%f\nLz=%f\tRz=%f\n',Lx,Rx,Ly,Ry,Lz,Rz);
end

