function [ Kod ] = inc_cod( Kod, n )
N=length(Kod);
if (Kod(N-n+1)=='0')
    Kod(N-n+1)='1';
else
    Kod(N-n+1)='0';
    [ Kod ] = inc_cod( Kod, n+1 );
end

end

