function [ output_args ] = Lab8sregkod(~)
[~,~,~,ax,Nx,Px,~,~,~,~,~,~,~,~,~,~,~,~,~,~] = Lab1;
Lx = huffmandict(ax,Px);
[ Kod, sim ] = sort( ax );
for i=1:length(Lx)
    Lxs(i)=length((Lx{sim(i),2}));
    Nxs(i)=Nx(sim(i)) ;
    Pxs(i)=Px(sim(i));
end
[ Lxs, sim ] = sort( Lxs );
Mn=min(Lxs);
Mx=max(Lxs);
n=1;
axn=char('');
for i=Mn:Mx
    k(i)=length(findstr(i,Lxs));
    for j=n:(n+k(i)-1)
        axn{j}=char(Kod(sim(j)));
        Nxn(j)=Nxs(sim(j)); 
        Pxn(j)=Pxs(sim(j));
    end
    n=n+k(i);
end
Kod='';
K(1)=1;  
for i=1:length(Nx)
    Kodx{i}='';
end
for i=1:Lxs(1)
    for j=1:K(i)
        Kod=strcat(Kod,'0');
    end
    K(i+1)=K(i)*2;
	Kodx{1}=strcat(Kodx{1},'0');
end
for i=2:length(Lxs);
    [ Kodx{i} ] = inc_cod( Kodx{i-1}, 1 );
    while( (length(Kodx{i}))<(Lxs(i)))
        Kodx{i}=strcat(Kodx{i},'0');
    end
end
for i=(Mn):(Mx)
    for j=1:k(i)
        Kod=strcat(Kod,'1');
    end
    if((k(i)+1)<=K(i+1))
        for j=(k(i)+1):K(i+1)
        	Kod=strcat(Kod,'0');
        end
    end
    if(i<Mx)
        K(i+2)=(K(i+1)-k(i))*2;
    end
end
for j=1:k(i)
	Kod=strcat(Kod,'1');
end
ki(1)=0;
for i=1:length(k)
    ki(i+1)=k(i);
end
L1=0;
SVI=0;
KS=256;
for i=1:(length(K))
    S=2;
    L1=L1+1;
    while((K(i)+1)>S)
        S=S*2;
        L1=L1+1;
    end
    n=1;
    if(ki(i)>0)
        SV(i)=ceil(log2(nchoosek(KS, ki(i))));
    else
        SV(i)=0;
    end
    SVI=SVI+SV(i); 
    KS=KS-ki(i);
end
L1=L1+SVI;
L2=0;
for i=1:length(Nxn)
    L2=L2+Nxn(i)*Lxs(i);
end
LI=L1+L2;
for i=1:length(axn)
    fprintf(' %s \t  %d \t %f \t %d \t %s\n', char(axn{i}),Nxn(i),Pxn(i),Lxs(i),char(Kodx(i)));
end
fprintf('\n');
fprintf('Kod yzlov: %s \n',char(Kod));
fprintf('LI=L1+L2 \n%d=%d+%d \n',LI,L1,L2);
end

