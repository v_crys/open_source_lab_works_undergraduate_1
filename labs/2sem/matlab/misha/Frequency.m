function [ Nx,Px,Ix,IxPx ] = Frequency( N, x, a )
for i=1:length(a)
    Nx(i)=0;
    for j=1:N
        if(char(x(j))==char(a(i)))
            Nx(i)= Nx(i)+1;
        end
    end
    %as=char(a(i));
    %str=strfind(x,as);
    %Nx(i)=length(strfind(x(),as));
    Px(i)=Nx(i)/N;
    Ix(i)=-log2(Px(i));
    IxPx(i)=mtimes(Ix(i),Px(i));
    %fprintf(' %c \t %d \t %f \t %f\n',a{i},Nx(i),Px(i),IxPx(i))
end
end

