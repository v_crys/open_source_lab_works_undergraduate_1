function [ output_args ] = Lab7(~)

[~,~,~,ax,Nx,Px,~,~,~,~,~,~,~,~,~,~,~,~,~,~] = Lab1;
LH = huffmandict(ax,Px);
for i=1:length(ax)
    Lx(i)=length((LH{i,2}));
    kodx{i}='';
    for j=1:Lx(i)
        kd=LH{i,2};
        if(kd(j)==0)
            kodx(i)=strcat(kodx(i),'0');
        else
            kodx(i)=strcat(kodx(i),'1');
        end
    end
end

%----------����������----------
[ K, sim ] = sort( kodx );
for i=1:length(Lx)
    Lxs(i)=Lx(sim(i));
end
[ L, simL ] = sort( Lxs );
for i=1:length(Lx) 
    kodxs(i)=K(simL(i));
end
%----------����� ��������� ���������----------
PY=1;       % ��������, �� �������� ���������� � ���������������� ������� ����(kodxs)
Kod='0';    % ������ ����, ������ ����
Mx=max(Lx); % ������������ ����� ����
znach(1)=0; % znach - ������ ��������:
znach(2)=0;
%----------�������� ������----------
for i=1:Mx
    for j=1:(2^i)
        if(znach(j)==0) %�������� ���������� (����������� ����� �����)
            %�������� �����, ��� ���������
            KodP='';
            stk=j-1; 
            for k=1:i
                if(mod(stk,2)==0)
                    KodP=strcat('0',KodP);
                    stk=stk/2;
                else
                    KodP=strcat('1',KodP);
                    stk=(stk-1)/2;
                end
            end
            %��������� ����� � ���������� (� ������ ����������� ���� ��
            %����� ���������� �����)
            if(length(char(KodP))== length(char(kodxs(PY))))
                if(char(KodP)==char(kodxs(PY)))
                    znach(j)=1;
                    PY=PY+1;
                end
            else
                break;
            end
        end
    end
    %��������� ������� �������� � ����������� � 2 ���� (�����
    %�������������� ������ znachN)
    for j=1:length(znach)
        if(znach(j)==0) %� ������ 0 ����������� '0' � ��� � 2 ������ ���� � ������
            Kod=strcat(Kod,'0');
            znachN(j*2)=0;
            znachN(j*2-1)=0;
        else
            if(znach(j)==1) %� ������ 1 ����������� '1' � ���
                Kod=strcat(Kod,'1');
            end
            %� ������ ����������� 2 ������ �������� (����� ���������)
            znachN(j*2)=2;
            znachN(j*2-1)=2;
        end
    end
    for j=1:length(znachN)
        znach(j)=znachN(j);
    end
end
%������������ ����� ����
L1=length(Kod)+8*length(Nx);
L2=0;
for i=1:length(Nx)
    L2=L2+Nx(i)*Lx(i);
end
LI=L1+L2;
for i=1:length(ax)
    fprintf(' %s \t  %d \t %f \t %d \t %s\n', char(ax{i}),Nx(i),Px(i),Lx(i),char(kodx(i)));
end
fprintf('\n');
fprintf('Kod yzlov: %s \n',char(Kod));
fprintf('LI=L1+L2 \n%d=%d+%d \n',LI,L1,L2);
end

