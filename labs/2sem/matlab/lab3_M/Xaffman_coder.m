function [ Coding_words ] = Xaffman_coder( Nx, N )
    Propability_vertex = [ Nx, zeros( 1, 1000 ) ];
    
    Branch_binary = zeros( 1, 1000 );
    Vertex_parent = zeros( 1, 1000 );
    
    size_Nx = size( Nx, 2 );
    Vertexs = size_Nx;
    
    for j = 1 : size_Nx - 1
        %------ find minimum propability double vertex
        [ min_val_1, pos_min_val_1 ] = min( Propability_vertex( 1, 1 : Vertexs ) );
        Propability_vertex( pos_min_val_1 ) = 99999;
        
        [ min_val_2, pos_min_val_2 ] = min( Propability_vertex( 1, 1 : Vertexs) );
        Propability_vertex( pos_min_val_2 ) = 99999;        
        
        %---- rewrite old elements and create new element
        %------- calculated new Probability
        Vertexs = Vertexs + 1;
        Propability_vertex( Vertexs ) = min_val_1 + min_val_2;
        
        %-------- save edge
        Branch_binary( pos_min_val_1 ) = 0;
        Branch_binary( pos_min_val_2 ) = 1;
        
        %-------- save parents for minimals element
        Vertex_parent( pos_min_val_1 ) = Vertexs;
        Vertex_parent( pos_min_val_2 ) = Vertexs;
        
       if ( Propability_vertex( Vertexs ) == N ) 
           break;
       end
    end
    
    %---------- create table for coding
    Coding_words = zeros( 1, size_Nx );
    
    for i = 1 : size_Nx        
        Coding_words( 1, i ) = 2;
        buf = i;
        while ( buf ~= Vertexs )
            Coding_words( 1, i ) = Coding_words( 1, i ) * 10 + Branch_binary( buf );
            buf = Vertex_parent( buf );
        end
    end

end
