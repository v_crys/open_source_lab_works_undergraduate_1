function [ L, C, Q, pr ] = Gilber_Moor( Px ) 

    pr = zeros(1, size (Px, 2));
    Q = zeros(1 , size( Px, 2));
    pr(1) = 0;
    L = ceil((-log2(Px))+1);    
    for i = 1 : size (Px, 2)
        Q(i) = pr(i) + Px(i)/2;
        pr(i+1) = pr(i) + Px(i);
    end
    
    for i = 1 : size (Px, 2)
        [ C(i) ] = bin_coder(Q(i), L(i));
    end
    
end
