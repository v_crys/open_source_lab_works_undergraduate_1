function [ counter ] = len_bin_code(bin)

counter = 0;

while(1)
    if((mod(bin,10) == 1) || (mod(bin,10) == 0))
        counter = counter + 1;
        bin = fix(bin/10);
        continue;
    end
    break; 
end