function [ bin ] = mon( e )

bin = bin_code_int(e);
bin = prav(bin);
bin = fix(bin/10);
bin = prav(bin);
l = len_bin_code(bin) + 1;
k = unar(l);
[ bin ] = glue_int(k,bin);