function [  ] = lab_10( mas )
fileID = fopen('Output_lab10.txt','w');
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end
d = 0; temp = 0; z = 1; bin = 0; e = 0; i = 1;  sum = 0;
ASCII_a = string('                                 !"#$%& ()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_ abcdefghijklmnopqrstuvwxyz{|}~                                         �               ��      ����������������������������������������������������������������');
ASCII_c = zeros(1, 256);
a = cell(1,1);
code = zeros(1,1);
%mas_temp(1) = mas(1);


for k = 1 : 256
    ASCII_c(k) = bin_code_int(k - 1);
end

k = 0;

while(i <= size(mas,2))
    k = k + 1;
    if (srav_word_to_string(mas(i), a) <= size(a,2) )
        [ e, d, a, z ] = sam_code( mas, i, a, z );
        bin = mon(e);
        bin = glue_int(equalizer(bin_code_int(d),ceil(log2(k))),bin);
        bin = prav(bin)*10+1;
        bin = prav(bin);
        sum = sum + len_bin_code(bin);
        fprintf(fileID ,'\t%d\t : \t%d\t : \t%s\t : \t%d\t : \t%d\t : \t%d\t : \t%d\r\n', k, 1, a{z-1}, d, e, len_bin_code(bin), bin);
        i = i + e;
        continue;
    else
        a{z} = mas(i);
        z = z + 1;
        bin = ASCII_c(srav_word(mas(i),ASCII_a));
        fprintf(fileID ,'\t%d\t : \t%d\t : \t%s\t : \t%s\t : \t%s\t : \t%s\t : \t%d\r\n', k, 0, a{z-1}, '-', '0', '9', equalizer((bin),9));
        i = i + 1;
    end
    
    
end

fprintf(fileID, '\r\n\r\n%d', sum );

fclose(fileID);