function [ bin ] = glue_int(bin,Code)
Code = prav(Code);
while(mod(Code,10) ~= 2)
    if(mod(Code,10) == 1)
        bin = bin*10+1;
    else
        bin = bin*10;
    end
    Code = fix(Code/10);
end