function [ result ] = inc_bin( M )

result = M;
k = 10;


for i = 1 : 300;
   
    if(mod(M,10) == 2)
       break; 
    end    
    if(mod(M,10) == 0)
        result = result + k/10;
        break;
    end
    if(mod(M,10) == 1)
        result = result - k/10;
    end
    
    k = k * 10;
    M = fix(M/10);
end