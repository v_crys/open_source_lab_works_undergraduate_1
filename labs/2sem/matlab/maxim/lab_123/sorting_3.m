function [ mas1 , mas2 ] = sorting_3 ( mas1 , mas2 )

	for i = 1 : size ( mas1 , 2 )
		min = 999999999999;
		for j = i : size ( mas1 , 2 )
           		if ( mas1( j ) < min ) 
               		min = mas1( j );
                	pos_min = j;
                end
        end
        temp = mas1( pos_min );
		mas1( pos_min ) = mas1( i );
		mas1( i ) = temp;

		temp = mas2( pos_min );
		mas2( pos_min ) = mas2( i );
		mas2( i ) = temp;

	end
