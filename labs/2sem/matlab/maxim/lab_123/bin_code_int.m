function [ bin ] = bin_code_int( chislo )

bin = 2;
    if(chislo == 0)
        bin = 20;
        return;
    end
    while(chislo ~= 0)
        if(mod(chislo,2) == 0)
            bin = bin*10;
        else
            bin = bin * 10 + 1;
        end
        chislo = fix(chislo/2);
    end
    bin = prav( bin );
end

