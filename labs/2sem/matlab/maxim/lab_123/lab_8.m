function [ result_code ] = lab_8( C, a, N, n )

[ C,a,n ] = lexicographic_sorting(C,a,n);

Codes = zeros (1, 1000);
Codes(1) = 2;
temp = 0;
max = 1; min = 1; k = 0; counter = 1; x = 1; z = 1; Sum2 = 0; Sum1 = 0;
len = zeros(1,3);
new_words = string(' ');
new_codes = zeros(1,3);

while (counter ~= 0)
    counter = counter * 2;
    k = 0;
    for i = min : max
        temp = Codes(i)*10;
        if(len_bin_code(temp) == len_bin_code(C(z)))
            counter = counter - 1;
            new_words(z) = a(z);
            new_codes(z) = temp;
            z = z + 1;
        else    
            k = k + 1;
            Codes(max + k) = temp;
            
        end
        temp = Codes(i)*10+1;
        if(len_bin_code(temp) == len_bin_code(C(z)))
            counter = counter - 1;
            new_words(z) = a(z);
            new_codes(z) = temp;
            z = z + 1;
        else    
            k = k + 1;
            Codes(max + k) = temp;
            
        end
    end
    max = max + counter;
    min = min + x;
    x = k;
end

for i = 1 : size(new_codes,2)
    len(i) = len_bin_code(new_codes(i));
end

    fileID = fopen('Output_lab8.txt','w');
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end
    for i = 1 : size(new_codes,2)
        fprintf ( fileID, '%s : %d : %d\r\n', new_words(i), len(i), new_codes(i));
    end
   
    fprintf ( fileID, '\r\n\r\n\r\n');
    fprintf ( fileID, '\t0\t : \t1\t : \t0\t : \t0\t : \t0..1\t : \t1\t :\r\n');
    counter = 1; j = 1; k = 1; temp1 = 0; temp2 = 0;
    
    for i = 1 : len(size(new_codes,2))
        max = 0;
        counter = counter * 2;
        while(1)
            if(j > size(new_codes,2))
                break;
            end
            if(len_bin_code(new_codes(j)) == i)
                max = max + 1;
                j = j + 1;
                continue;
            end
            break;
        end
        temp1 = ceil(log2(nchoosek(N,max)));
        temp2 = ceil(log2(counter+1));
        Sum1 = Sum1 + (temp1 + temp2);
        fprintf ( fileID, '\t%d\t : \t%d\t : \t%d\t : \t%d\t : \t0..%d\t : \t%d\t :\r\n', i, counter, max, temp1, counter, temp2);
        k = k + temp2;
        counter = counter - max;
        N = N - max;
    end
    fprintf ( fileID, '\t����\t : \t\t : \t\t : \t%d\t : \t\t :\t%d���\t :\r\n', Sum1, k);
    for i = 1 : size(len,2)
        Sum2 = Sum2 + (n(i) * len(i));
    end
    fprintf ( fileID, 'L = %d ; l_1 = %d; l_2 = %d', Sum1+Sum2, Sum2, Sum1);
    fclose(fileID);
end