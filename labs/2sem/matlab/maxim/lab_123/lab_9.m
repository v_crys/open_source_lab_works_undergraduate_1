function [ result_code ] = lab_9( C, a, Mas ) % Mas - string

    fileID = fopen('Output_lab9.txt','w');
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end

result_code = zeros(1,1);
result_temp = zeros(1,1);

ASCII_a = string('                                 !"#$%& ()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_ abcdefghijklmnopqrstuvwxyz{|}~                                         �               ��      ����������������������������������������������������������������');
ASCII_c = zeros(1, 256);
ASCII_a_copy = ASCII_a;


for i = 1 : 256
    ASCII_c(i) = bin_code_int(i - 1);
end
%[ C, a ] = sorting_3( C, a );

for i = 1 : size(Mas,2)
    [ j ] = srav_word(Mas(i),ASCII_a);
    [ x ] = srav_word(Mas(i),ASCII_a_copy);
    fprintf( fileID, '%d\t : \t%s\t : \t%d\t : \t%d\t : \t%d\t : \t%d\t\r\n', i, ASCII_a(j), j, len_bin_code(ASCII_c(j)), x, ASCII_c(j));
    [ result_code ] = glue_string_int(result_code,ASCII_c(j));
    result_temp(i) = ASCII_c(j);
    [ ASCII_a ] = shake(ASCII_a,j);
end

 Len = (size(result_code,2) - 1);
 
 i = 1;
    fprintf(fileID, '\r\n\r\n');
    for i = 1 : Len
        fprintf ( fileID, '%d', result_code(i) );
    end
    fprintf ( fileID, '\r\n%d', Len );
    fclose(fileID);
