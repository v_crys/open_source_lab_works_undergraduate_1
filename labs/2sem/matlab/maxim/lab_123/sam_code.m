function [ e, d, a, max_a ] = sam_code( mas,poz,a,max_a )

min = 1; z = 1; temp = 0; flag = 0;
poz_mas = zeros(2,1);

for i = 1 : (poz - 1)
    if(mas(i) == mas(poz))
        for j = 1 : (poz - i )
            if((poz + j - 1) > size(mas,2))
                if(poz == size(mas,2))
                    poz_mas(1,z) = i;
                    poz_mas(2,z) = j;
                    z = z + 1;
                    break;
                end
                poz_mas(1,z) = i;
                poz_mas(2,z) = j + 1;
                z = z + 1;
                break;
            end
            if(mas(i+j-1) ~= mas(poz+j-1))
                poz_mas(1,z) = i;
                poz_mas(2,z) = j;
                z = z + 1;
                break;
            end

        end
    end
end

z = z - 1;

for i = 2 : z
    if(poz_mas(2,min) <= poz_mas(2,i))
        min = i;
    end
end

for i = 1 : (poz_mas(2,min) - 1)
    a{max_a}(i) = mas(poz_mas(1,min)+i-1); 
end
max_a = max_a + 1;
e = poz_mas(2,min) - 1;
d = poz - poz_mas(1,min) - 1;
%d = poz - poz_mas(1,size(poz_mas,2)) - 1;
%d = poz - poz_mas(1,min);