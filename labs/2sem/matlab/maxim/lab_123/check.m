function [ R ] = check( a, Mas )

R = 0;

for i = 1 : size(Mas, 2)
    if(a == Mas(i))
        R = 1;
        break;
    end
end