function [ Mas ] = shake( Mas, j )
if(j == 1) 
    return; 
end

temp = Mas(1);
Mas(1) = Mas(j);

while(j ~= 1)
    Mas(j) = Mas(j - 1);
    j = j - 1;
end

Mas(2) = temp;