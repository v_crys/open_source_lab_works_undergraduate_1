function [  ] = lab_11( mas )
fileID = fopen('Output_lab11.txt','w');
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end
temp = 0; z = 1; bin = 0; i = 1;  sum = 0; poz_a = 0;
ASCII_a = string('                                 !"#$%& ()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_ abcdefghijklmnopqrstuvwxyz{|}~                                         �               ��      ����������������������������������������������������������������');
ASCII_c = zeros(1, 256);
a = cell(1,1);
code = zeros(1,1);
%mas_temp(1) = mas(1);


for k = 1 : 256
    ASCII_c(k) = bin_code_int(k - 1);
end

k = 0;

while(i <= size(mas,2))
    k = k + 1;
    if (srav_word_to_string(mas(i), a) <= size(a,2) )
        [ poz_a, a, i, z] = sam_code_2( mas, i, a, z );
        bin = bin_code_int(poz_a);
        bin = equalizer(bin,ceil(log2(z-2)));
        sum = sum + len_bin_code(bin);
        fprintf(fileID ,'\t%d\t : \t%s\t : \t%d\t : \t%d\t : \t%d\r\n', k, a{z-1}, poz_a, len_bin_code(bin), bin);
        continue;
    else
        a{z} = mas(i);
        z = z + 1;
        bin = ASCII_c(srav_word(mas(i),ASCII_a));
        fprintf(fileID ,'\t%d\t : \t%s\t : \t%s\t : \t%s\t : \t%d\r\n', k, a{z-1}, '-', '8', equalizer((bin),8));
        i = i + 1;
    end
    
    
end

fprintf(fileID, '\r\n\r\n%d', sum );

fclose(fileID);