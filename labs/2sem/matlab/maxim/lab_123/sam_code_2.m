function [ poz_a, a, poz, max_a ] = sam_code_2( mas,poz,a,max_a )

temp = cell(1,1);
poz_a = 0; temp{1} = mas(poz); counter = 1;

for i = 1 : size(a,2)
    if(strcmp(temp{1},a(i)))
        if(poz + counter >= size(mas,2))
            poz_a = i;
            counter = counter + 1;
            break;
        end
        temp{1}(counter + 1) = mas(poz + counter);
        counter = counter + 1;
        poz_a = i;
    end
end
a{max_a} = temp{1};
max_a = max_a + 1;
poz = poz + counter - 1;
%poz = poz + length(temp) - 1;