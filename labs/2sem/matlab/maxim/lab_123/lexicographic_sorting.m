function [ M1,M2,M3 ] = lexicographic_sorting(M1,M2,M3)

max = 0; i = 1;
 M1_copy = M1;
[ M1, M2 ] = sorting_3(M1,M2);
[ M1_copy, M3 ] = sorting_3(M1_copy,M3);

while( i < size(M1,2))
    max = 1;
    if((i + max) > size(M1,2) )
        break;
    end
    while(1)
        if((i + max) > size(M1,2))
            break;
        end
        if(len_bin_code(M1(i + max - 1)) == len_bin_code(M1(i + max)))
            max = max + 1;
            continue;
        end
        break;
    end
    for k = i : (i + max - 1)
        min = 999999999999;
        if(k >= size(M1,2))
            break;
        end
		for j = k : (i + max - 1)
           	if ( M2( j ) < min ) 
              	min = M2( j );
                pos_min = j;
            end
        end
        temp = M2( pos_min );
		M2( pos_min ) = M2( k );
		M2( k ) = temp;

		temp = M1( pos_min );
		M1( pos_min ) = M1( k );
		M1( k ) = temp;
        
        temp = M3( pos_min );
		M3( pos_min ) = M3( k );
		M3( k ) = temp;
    end
    i = i + max;
end