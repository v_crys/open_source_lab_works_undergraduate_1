function [ M, counter, z ] = srav( M, C, L, j, counter, z )

temp = 2 * power(10,L(j));

for i = 1 : power(2,L(j))
    
    if(temp == C(j))
       M(z) = 1;
       z = z + 1;
       counter = counter - 1;
       [ temp ] = inc_bin( temp );
       if(j<size(C,2))
          j = j + 1;
       end
       continue;
    end
    
    z = z + 1;
    [ temp ] = inc_bin( temp );
end