global c N  x y z   ax ay az    Nx Ny Nz    Px Py Pz    Ix Iy Iz
global PxIx  PyIy  PzIz  H_x  H_x_n  H_conv_x  H_y  H_y_n  H_conv_y  H_z  H_z_n  H_conv_z
global Xaffman_x  Xaffman_y  Xaffman_z  L_x  e_x  L_y  e_y  L_z  e_z  R_x  R_y  R_z  Tree
global X_l  Y_l  Z_l  Px_s  Py_s  Pz_s  ax_s  ay_s  az_s  Cx  Cy  Cz Qx  Qy  Qz % _s(sorting)
global Gilbert_x  Gilbert_y  Gilbert_z  Code_x  Code_y  Code_z  pr_x  pr_y  pr_z
global C_tree

[ c N x y z ] = ReadFromFile;

[ ax, Nx, Px, Ix, PxIx ] = statistics_x( x, N );
[ H_x, H_x_n, H_conv_x] = f_H_x(PxIx);
[ ay, Ny, Py, Iy, PyIy ] = statistics_y( y, N );
[ H_y, H_y_n, H_conv_y] = f_H_y(PyIy, H_conv_x);
[ az, Nz, Pz, Iz, PzIz ] = statistics_z( z, N );
[ H_z, H_z_n, H_conv_z] = f_H_z(PzIz, H_conv_x, H_conv_y);

WriteToFileX( ax, Nx, Px, Ix, PxIx );
WriteToFileY( ay, Ny, Py, Iy, PyIy );
WriteToFileZ( az, Nz, Pz, Iz, PzIz );
WriteToFile_H(H_x, H_x_n, H_conv_x, H_y, H_y_n, H_conv_y, H_z, H_z_n, H_conv_z);

[ Xaffman_x ] = Xaffman_coder(Nx);
[ Xaffman_y ] = Xaffman_coder(Ny);
[ Xaffman_z ] = Xaffman_coder(Nz);
[ L_x, e_x, R_x, L_y, e_y, R_y, L_z, e_z, R_z ] = LEN( Xaffman_x, Xaffman_y, Xaffman_z, Nx, Px, e_x, R_x, Ny, Py, e_y, R_z, Nz, Pz, e_z, R_z );
WriteToFileXaffman( Xaffman_x, Xaffman_y, Xaffman_z, ax, Px, L_x, e_x, R_x, ay, Py, L_y, e_y, R_y, az, Pz, L_z, e_z, R_z );

[ Px_s, ax_s] = sorting( Px, ax);
[ Py_s, ay_s] = sorting_2( Py, ay);
[ Pz_s, az_s] = sorting_2( Pz, az);
[ X_l,  Cx, Qx ] = Shennon_coder(Px_s);
[ Y_l,  Cy, Qy ] = Shennon_coder(Py_s);
[ Z_l,  Cz, Qz ] = Shennon_coder(Pz_s);
WriteToFileShennon(Px_s, ax_s, X_l, Cx, Qx, Py_s, ay_s, Y_l, Cy, Qy, Pz_s, az_s, Z_l, Cz, Qz);

[ Gilbert_x, Code_x, Qx, pr_x] = Gilber_Moor(Px);
[ Gilbert_y, Code_y, Qy, pr_y] = Gilber_Moor(Py);
[ Gilbert_z, Code_z, Qz, pr_z] = Gilber_Moor(Pz);
WriteToFileGilbertMoor(Px, ax, Gilbert_x, Code_x, Qx, pr_x, Py, ay, Gilbert_y, Code_y, Qy, pr_y, Pz, az, Gilbert_z, Code_z, Qz, pr_z);

Arithmetic_coder(c, ax, Px, ay, Py, az, Pz);

lab_7( Xaffman_x, Nx, L_x, ax );
lab_8( Xaffman_x, ax, N, Nx );
lab_9( Xaffman_x, ax, c );
lab_10( c );
lab_11( c );
%[ C_tree ] = lab_7( Xaffman_y );