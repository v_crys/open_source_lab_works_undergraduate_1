function [ Mas_tree ] = lab_7( C, N, L, a )

Codes = zeros (1, 1000);
Codes(1) = 2;
temp = 0; Sum1 = 0; Sum2 = 0; Len = 0;
max = 1; min = 1; k = 0; counter = 1; x = 1; z = 3;
Mas_tree = zeros(1,3);
Mas_tree(2) = 2;
while (counter ~= 0)
    counter = counter * 2;
    k = 0;
    for i = min : max
        temp = Codes(i)*10;
        if(check(temp, C))
            counter = counter - 1;
            Mas_tree(z) = 1;
            z = z + 1;
        else    
            k = k + 1;
            Codes(max + k) = temp;
            Mas_tree(z) = 0;
            z = z + 1;
        end
        temp = Codes(i)*10+1;
        if(check(temp, C))
            counter = counter - 1;
            Mas_tree(z) = 1;
            z = z + 1;
        else    
            k = k + 1;
            Codes(max + k) = temp;
            Mas_tree(z) = 0;
            z = z + 1;
        end
    end
    Mas_tree(z) = 2;
    z = z + 1;
    max = max + counter;
    min = min + x;
    x = k;
end

for i = 1 : size(N,2)
    Sum1 = Sum1 + (N(i) * L(i));
end
for i = 1 : size(Mas_tree,2)
    if(Mas_tree(i) == 2)
       continue; 
    end
    Len = Len + 1;
end
Sum2 = Len + size(N,2)*8;

    fileID = fopen('Output_lab7.txt','w');
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end
    for i = 1 : size(N,2)
        fprintf ( fileID, '\t%s\t : \t%d\t : \t%d\t :\r\n', a(i), N(i), L(i));
    end
    for i = 1 : (z - 1)
        if(Mas_tree(i) == 2)
            fprintf ( fileID, ' ');
        else
        fprintf ( fileID, '%d', Mas_tree(i) );
        end
    end
    fprintf ( fileID, '\r\nL = %d ; l_1 = %d, l_2 = %d', Sum1+Sum2, Sum1, Sum2 );
    fclose(fileID);