function [ result ] = glue_string_int(result,Code)

max = size(result,2);

[ Code ] = prav (Code);

while(mod(Code,10) ~= 2)
    if(mod(Code,10) == 1)
        result(max) = 1;
    else
        result(max) = 0;
    end
    max = max + 1;
    Code = fix(Code/10);
end

result(max) = 0;