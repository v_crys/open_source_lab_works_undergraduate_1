/*
 * Name Project: Class for Vectors
 * IDE: Code::Blocks
 * Compiler: GCC
 * Verion: 1.0
 * Generated: 26.03.2017
 * Author: V.Hrustalev
 */


#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <stdlib.h>


class Vector {
//-------- variables
    int *_data;
    int _size;

public:
//------- constructors & destructor
    Vector() : _data( NULL ), _size( 0 ) {}
    Vector( int SIZE ) : _data( NULL ), _size( SIZE ) { set_size( SIZE ); }
    ~Vector() { set_size( 0 ); };

//------- constructor copy
    Vector( const Vector &src );

//------- binary operators
    Vector & operator=( const Vector &src );
    Vector operator+( const Vector &src_r );
    Vector operator-( );
    Vector operator-( const Vector &src_r );
    Vector operator*( const Vector &src_r );
    Vector operator*( int );
    friend Vector operator*( int l, const Vector &src_r );

    Vector operator>>( int );
    Vector operator<<( int );

//-------- thread operators
    friend std::ostream & operator<< ( std::ostream &OS, const Vector &src_r );
    friend std::istream & operator>> ( std::istream &OS, Vector &src_r );

//-------------
    friend bool operator== ( const Vector &src_l, const Vector &src_r );
    friend bool operator!= ( const Vector &src_l, const Vector &src_r );
    Vector & operator+= ( const Vector &src_r );
    Vector & operator-= ( const Vector &src_r );
    Vector & operator*= ( const Vector &src_r );
    Vector & operator*= ( int src );
    Vector & operator<<= ( int src );
    Vector & operator>>= ( int src );

    int &operator[]( int );

    Vector ret_part( int x1, int x2 );
    Vector del_elem( int ind );
    void set_size( int SIZE );
    int get_size();
private:
    void Print();
};


#endif // VECTOR_H
