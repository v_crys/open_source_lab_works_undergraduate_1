#include "Vector.h"

int &Vector::operator[]( int Number )
{
    if ( ( Number >= _size ) || ( Number < 0 ) )
        exit( EXIT_FAILURE );

    return (_data[ Number ]);
}

Vector::Vector( const Vector &src ) :
    _size( 0 ), _data( NULL )
{
    int i;
    set_size( src._size );

    for ( i = 0; i < src._size; i++ )
        _data[ i ] = src._data[ i ];
}

Vector & Vector::operator=( const Vector &src )
{
    if ( this == &src )
        return *this;


    set_size( src._size );
    int i;

    for ( i = 0; i < src._size; i++ )
        _data[ i ] = src._data[ i ];

    return *this;
}


Vector Vector::operator+( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    Vector res( _size );
    int i;
    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] + src_r._data[ i ];

    return res;
}

Vector Vector::operator-( )
{
    Vector res( _size );
    int i;
    for ( i = 0; i < _size; i++ )
        res._data[ i ] = -_data[ i ];

    return res;
}

Vector Vector::operator-( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    Vector res( _size );
    res = src_r;
    res = *this + ( -res );

    return res;
}

Vector Vector::operator*( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    int i;
    Vector res( _size );

    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] * src_r._data[ i ];

    return res;
}

Vector Vector::operator*( int r )
{
    Vector res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] * r;

    return res;
}

Vector operator*( int l, const Vector &src_r )
{
    Vector res( src_r._size );
    res = src_r;
    res = res * l;
    return res;
}

Vector Vector::operator>>( int SHR )
{
    if ( SHR < 0 )
        exit ( EXIT_FAILURE );

    Vector res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        if ( i < SHR )
            res[ i ] = 0;
        else
            res[ i ] = _data[ i - SHR ];

    return res;
}

Vector Vector::operator<<( int SHL )
{
    if ( SHL < 0 )
        exit( EXIT_FAILURE );

    Vector res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        if ( i + SHL >= _size )
            res[ i ] = 0;
        else
            res[ i ] = _data[ i + SHL ];

    return res;
}

std::ostream & operator<< ( std::ostream &OS, const Vector &src_r )
{
    int i;
    for ( i = 0; i < src_r._size; i++ )
        OS << src_r._data[ i ] << " ";
    OS << std::endl;
    return OS;
}

std::istream & operator>> ( std::istream &OS, Vector &src_r )
{
    int i, loc_size;
    OS >> loc_size;

    src_r.set_size( loc_size );
    for ( i = 0; i < src_r._size; i++ )
        OS >> src_r._data[ i ];
    return OS;
}


bool operator== ( const Vector &src_l, const Vector &src_r )
{
    int i;
    if ( src_l._size != src_r._size )
        return false;

    for ( i = 0; i < src_l._size; i++ )
        if ( src_l._data[ i ] != src_r._data[ i ] )
            return false;

    return true;
}

bool operator!=( const Vector &src_l, const Vector &src_r )
{
    return !( src_l == src_r );
}

Vector & Vector::operator+= ( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] += src_r._data[ i ];

    return *this;
}

Vector & Vector::operator-= ( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] -= src_r._data[ i ];

    return *this;
}

Vector & Vector::operator*= ( const Vector &src_r )
{
    if ( _size != src_r._size )
        exit( EXIT_FAILURE );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] *= src_r._data[ i ];

    return *this;
}

Vector & Vector::operator*= ( int src )
{
    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] *= src;

    return *this;
}

Vector & Vector::operator<<= ( int SHL )
{
    int i;

    if ( SHL < 0 )
        exit ( EXIT_FAILURE );

    for ( i = 0; i < _size; i++ )
        if ( i + SHL >= _size )
            _data[ i ] = 0;
        else
            _data[ i ] = _data[ i + SHL ];

    return *this;
}

Vector & Vector::operator>>= ( int SHR )
{
    int i;

    if ( SHR < 0 )
        exit ( EXIT_FAILURE );

    for ( i = _size - 1; i >= 0; i-- )
        if ( i < SHR )
            _data[ i ] = 0;
        else
            _data[ i ] = _data[ i - SHR ];

    return *this;
}


//----------------- private methods
void Vector::set_size( int SIZE )
{
    if ( _data != NULL )
    {
        delete _data;
        _data = NULL;
    }

    if ( SIZE <= 0 )
    {
        _size = 0;
        _data = NULL;
        return;
    }

    _size = SIZE;
    _data = new int [ SIZE ];
}

void Vector::Print()
{
    int i;
    for ( i = 0; i < _size; i++ )
        std::cout << _data[ i ] << " ";

    std::cout << std::endl;

}

int Vector::get_size()
{
    return _size;
}

Vector Vector::ret_part( int x1, int x2 )
{
    if ( ( x1 > x2 ) || ( x1 < 0 ) || ( x2 >= _size ) )
        exit( EXIT_FAILURE );

    int i;
    Vector buf( x2 - x1 + 1 );

    for ( i = x1; i <= x2; i++ )
        buf[ i - x1 ] = _data[ i ];

    return buf;
}

Vector Vector::del_elem( int ind )
{
    if ( ( ind < 0 ) || ( ind >= _size ) )
        exit( EXIT_FAILURE );

    Vector buf( _size - 1 );
    int i;

    for ( i = 0; i < ind; i++ )
        buf[ i ] = _data[ i ];

    for ( i = ind + 1; i < _size; i++ )
        buf[ i - 1 ] = _data[ i ];


    return buf;
}
