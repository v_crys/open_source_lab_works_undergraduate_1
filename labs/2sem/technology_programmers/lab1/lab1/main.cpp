#include <iostream>
#include "Matrix.h"

using namespace std;


int main()
{
    Matrix A( 3, 3 );

    A[ 0 ][ 0 ] = 2;
    A[ 0 ][ 1 ] = 3;
    A[ 0 ][ 2 ] = 4;

    A[ 1 ][ 0 ] = 9;
    A[ 1 ][ 1 ] = 8;
    A[ 1 ][ 2 ] = 7;

    A[ 2 ][ 0 ] = 1;
    A[ 2 ][ 1 ] = 7;
    A[ 2 ][ 2 ] = 3;
    cout << "A = " << endl << A;
    cout << "det( A ) = " << A.Determinant() << endl << endl;

    Matrix B( 3, 3 );
    B[ 0 ][ 0 ] = 1;
    B[ 0 ][ 1 ] = 2;
    B[ 0 ][ 2 ] = 3;

    B[ 1 ][ 0 ] = 4;
    B[ 1 ][ 1 ] = 5;
    B[ 1 ][ 2 ] = 6;

    B[ 2 ][ 0 ] = 7;
    B[ 2 ][ 1 ] = 8;
    B[ 2 ][ 2 ] = 9;
    cout << "B = " << endl << B;
    cout << "det( B ) = " << B.Determinant() << endl << endl;

    cout << "A + B" << endl << A + B << endl;
    cout << "A - B" << endl << A - B << endl;
    cout << "A * B" << endl << A * B << endl;
    cout << "2 * A" << endl << 2 * A << endl;
    cout << "A * 2" << endl << A * 2 << endl;
    cout << "!A" << endl << !A << endl;
    cout << "-A" << endl << -A << endl;
    cout << "A[ 0 ][ 2 ] = " << A[ 0 ][ 2 ] << endl;

    return 0;
}
