#include <iostream>

#include <cmath>
class Fraction{
    private:
            int _integer;
            int _numerator;
            int _denominator;
            bool _znak;

            void Simpli_normal();
            int NOD( int a, int b );
            void znak_set();
    public:

            Fraction() : _integer( 0 ), _numerator( 0 ), _denominator( 1 ), _znak( true ) {}
            Fraction( int val ) : _integer( std::abs(val) ), _numerator( 0 ), _denominator( 1 ), _znak ( val >= 0 ) {}
            Fraction( double val );
            Fraction( int nom, int den );
            Fraction( int integ, int nom, int den );


            int get_int();
            int get_num();
            int get_den();

            int get_num_int() const;


            friend std::ostream & operator<< ( std::ostream &OS, const Fraction &src_r );

            Fraction &operator++();
            Fraction &operator--();
            Fraction operator++( int dummy );
            Fraction operator--( int dummy );
            Fraction operator-() const ;


            Fraction operator+( const Fraction &src_r ) const;
            Fraction operator-( const Fraction &src_r ) const;
            Fraction operator*( const Fraction &src_r ) const;
            Fraction operator/( const Fraction &src_r ) const;

            Fraction &operator+=( const Fraction &src_r );
            Fraction &operator-=( const Fraction &src_r );
            Fraction &operator*=( const Fraction &src_r );
            Fraction &operator/=( const Fraction &src_r );


            Fraction operator+( int src_r ) const;
            Fraction operator-( int src_r ) const;
            Fraction operator*( int src_r ) const;
            Fraction operator/( int src_r ) const;

            Fraction &operator+=( int src_r );
            Fraction &operator-=( int src_r );
            Fraction &operator*=( int src_r );
            Fraction &operator/=( int src_r );


            friend Fraction operator+( int l, const Fraction &src_r );
            friend Fraction operator-( int l, const Fraction &src_r );
            friend Fraction operator*( int l, const Fraction &src_r );
            friend Fraction operator/( int l, const Fraction &src_r );


            operator int() const;
            operator float() const;
            operator double() const;

            bool operator== ( const Fraction &src_r );
            bool operator!= ( const Fraction &src_r );
            bool operator< ( const Fraction &src_r );
            bool operator> ( const Fraction &src_r );
            bool operator<= ( const Fraction &src_r );
            bool operator>= ( const Fraction &src_r );
};

void Fraction::znak_set()
{
    /*_znak = ( ( (_integer == 0) ? 1 : _integer ) * ( ( _numerator == 0 ) ? 1 : _numerator ) *
             _denominator ) >= 0 ? _znak : !_znak;*/
    int znak = 0;
    if ( _integer < 0 ) { znak++; _integer = -_integer;}
    if ( _numerator < 0 ) { znak++; _numerator = -_numerator; }
    if ( _denominator < 0 ) { znak++; _denominator = -_denominator; }

    if ( _znak == false ) znak++;

    if ( ( znak & 1 ) == 0 )
        _znak = true;
    else
        _znak = false;
}

Fraction::Fraction( double val )
{
    double buf;
    int k = 0;

    _znak = ( val < 0 ) ? false : true;


    val = std::abs( val );
    _integer = ( int )val;
    if ( _integer == val ) { _numerator = 0; _denominator = 1; return; }

    buf = val - _integer;

    for ( k = 0; k < 5; k++)
        buf *= 10;



    _denominator = pow( 10, k ) + 0.5;
    _numerator = buf + pow( 0.1, k );

    Simpli_normal();
}

Fraction::Fraction( int nom, int den )
{
    if ( den == 0 ) throw( "Division by zero" );
    _znak = ( (nom * den) < 0 ) ? false : true;

    nom = std::abs( nom );
    den = std::abs( den );

    _integer = 0;
    _numerator = nom;
    _denominator = den;

    Simpli_normal();
}

Fraction::Fraction( int integ, int nom, int den )
{
    if ( den == 0 ) throw( "Division by zero" );

    _znak = ( (integ * nom * den) < 0 ) ? false : true;

    integ = std::abs( integ );
    nom = std::abs( nom );
    den = std::abs( den );

    _integer = integ;
    _numerator = nom;
    _denominator = den;

    Simpli_normal();
}

void Fraction::Simpli_normal()
{
    int buf = _numerator / _denominator;

    _numerator -= _denominator * buf;
    _integer += buf;

    int nod = NOD( _numerator, _denominator );
    _numerator /= nod;
    _denominator /= nod;
}

int Fraction::NOD( int a, int b )
{
    int buf;

    if ( b == 0 ) return a;
    if ( a == 0 ) return b;

    if ( a < b )
    {
        buf = a;
        a = b;
        b = buf;
    }

    do {
        buf = b;
        b = a - ( a / b ) * b;
        a = buf;
    } while ( b != 0 );

    return a;
}

int Fraction::get_int()
{ return _integer; }

int Fraction::get_num()
{ return _numerator; }

int Fraction::get_den()
{ return _denominator; }

int Fraction::get_num_int() const
{ return _integer * _denominator + _numerator; }


std::ostream & operator<< ( std::ostream &OS, const Fraction &src_r )
{
    OS << "[";
    if ( src_r._znak == false ) OS << "-";
    OS << src_r._integer ;
    if ( src_r._numerator != 0 )
        OS << "(" << src_r._numerator << "/" << src_r._denominator << ")";
    OS << "]";
    return OS;
}

Fraction Fraction::operator-() const
{
    Fraction res = *this;
    res._znak = !res._znak;
    return res;
}

Fraction &Fraction::operator++()
{
    if ( _integer == 0 ) { _znak = !_znak; return *this; }

    if ( _znak )
        _integer++;
    else
        _integer--;

    return *this;
}

Fraction &Fraction::operator--()
{
    if ( _integer == 0 ) { _znak = !_znak; return *this; }

    if ( _znak )
        _integer--;
    else
       _integer++;

    return *this;
}

Fraction Fraction::operator++( int dummy )
{
    Fraction res = (*this);
    ++(*this);
    return res;
}

Fraction Fraction::operator--( int dummy )
{
    Fraction res = (*this);
    --(*this);
    return res;
}

Fraction Fraction::operator+( const Fraction &src_r ) const
{
    Fraction res;
    int num1 = get_num_int();
    int num2 = src_r.get_num_int();

    if ( _znak == false ) num1 = -num1;
    if ( src_r._znak == false ) num2 = -num2;

    res._denominator = _denominator * src_r._denominator;

    res._numerator = num1 * src_r._denominator + num2 * _denominator;

    res.znak_set();
    res.Simpli_normal();
    return res;
}

Fraction Fraction::operator-( const Fraction &src_r ) const
{
    Fraction res = *this + ( -src_r );
    return res;
}

Fraction Fraction::operator*( const Fraction &src_r ) const
{
    int num1 = get_num_int();
    int num2 = src_r.get_num_int();

    Fraction res;

    res._numerator = num1 * num2;
    res._denominator = _denominator * src_r._denominator;

    res._znak = ( ( _znak && src_r._znak ) || ( !_znak && !src_r._znak ) ) ? true : false;
    res.Simpli_normal();
    return res;
}

Fraction Fraction::operator/( const Fraction &src_r ) const
{
    int num1 = get_num_int();
    int num2 = src_r.get_num_int();

    Fraction res;

    res._numerator = num1 * src_r._denominator;
    res._denominator = _denominator * num2;

    res._znak = ( ( _znak && src_r._znak ) || ( !_znak && !src_r._znak ) ) ? true : false;
    res.Simpli_normal();
    return res;
}


Fraction &Fraction::operator+=( const Fraction &src_r )
{
    *this = *this + src_r;
    return *this;
}

Fraction &Fraction::operator-=( const Fraction &src_r )
{
    *this = *this - src_r;
    return *this;
}

Fraction &Fraction::operator*=( const Fraction &src_r )
{
    *this = *this * src_r;
    return *this;
}

Fraction &Fraction::operator/=( const Fraction &src_r )
{
    *this = *this / src_r;
    return *this;
}

Fraction Fraction::operator+( int src_r ) const
{
        Fraction buf( src_r );
        Fraction res;
        res = *this + buf;
        return res;
}

Fraction Fraction::operator-( int src_r ) const
{
        Fraction buf( src_r );
        Fraction res;
        res = *this - buf;
        return res;
}

Fraction Fraction::operator*( int src_r ) const
{
        Fraction buf( src_r );
        Fraction res;
        res = *this * buf;
        return res;
}

Fraction Fraction::operator/( int src_r ) const
{
        Fraction buf( src_r );
        Fraction res;
        res = *this / buf;
        return res;
}

Fraction &Fraction::operator+=( int src_r )
{
    *this = *this + src_r;
    return *this;
}

Fraction &Fraction::operator-=( int src_r )
{
    *this = *this - src_r;
    return *this;
}

Fraction &Fraction::operator*=( int src_r )
{
    *this = *this * src_r;
    return *this;
}

Fraction &Fraction::operator/=( int src_r )
{
    *this = *this / src_r;
    return *this;
}

Fraction operator+( int l, const Fraction &src_r )
{
    Fraction res = src_r + l;
    return res;
}

Fraction operator-( int l, const Fraction &src_r )
{
    Fraction res = l + -src_r;
    return res;
}

Fraction operator*( int l, const Fraction &src_r )
{
    Fraction res = src_r * l;
    return res;
}

Fraction operator/( int l, const Fraction &src_r )
{
    Fraction buf( l );
    Fraction res = buf / src_r;
    return res;
}

Fraction::operator int() const
{
    return ( _integer + ( _numerator / _denominator ) ) * ( ( _znak ) ? 1 : -1 );
}

Fraction::operator double() const
{
    return ( (double)_integer + ( (double)_numerator / _denominator ) ) * ( ( _znak ) ? 1 : -1 );
}

Fraction::operator float() const
{
    return (float)(double)*this;
}

bool Fraction::operator== ( const Fraction &src_r )
{
    if ( ( _znak == src_r._znak ) && ( _integer == src_r._integer ) &&
        ( _numerator == src_r._numerator ) && ( _denominator == src_r._denominator ) )
          return true;
    else
        return false;
}

bool Fraction::operator!= ( const Fraction &src_r )
{
    return !( *this == src_r );
}

bool Fraction::operator< ( const Fraction &src_r )
{
    return ( (double) *this ) < ( (double) src_r );
}

bool Fraction::operator> ( const Fraction &src_r )
{
    return ( (double) *this ) > ( (double) src_r );
}

bool Fraction::operator<= ( const Fraction &src_r )
{
    return ( *this < src_r ) || ( *this == src_r );
}

bool Fraction::operator>= ( const Fraction &src_r )
{
    return ( *this > src_r ) || ( *this == src_r );
}


using namespace std;

int main()
{

    Fraction A( 10.6 );
    Fraction B( -5 );
    cout << "A = " << A << endl;
    cout << "B = " << B << endl;
    cout << "++A = " << ++A << endl;
    cout << "--B = " << --B << endl;
    cout << "A++ = " << A++ << endl;
    cout << "A = " << A << endl << endl;

    cout << "-A = " << -A << endl << endl;


    cout << "A = " << A << endl;
    cout << "B = " << B << endl;
    cout << "A + B = " << A + B << endl;
    cout << "A - B = " << A - B << endl;
    cout << "A * B = " << A * B << endl;
    cout << "A / B = " << A / B << endl << endl;

    A += B;
    cout << "A += B, A = " << A << endl;
    A -= B;
    cout << "A -= B, A = " << A << endl;
    A *= B;
    cout << "A *= B, A = " << A << endl;
    A /= B;
    cout << "A /= B, A = " << A << endl << endl;

    cout << "A + 7 = " << A + 7 << endl;
    cout << "7 - A = " << 7 - A << endl;
    cout << "A * 3 = " << A * 3 << endl;
    cout << "100 / A = " << 100 / A  << endl << endl;

    A += 7;
    cout << "A += 7, A = " << A << endl;
    A -= 20;
    cout << "A -= 20, A = " << A << endl;
    A *= -2;
    cout << "A *= -2, A = " << A << endl;
    A /= 5;
    cout << "A /= 5, A = " << A  << endl << endl;

    A = 7.35;
    cout << "A = 7.35;, A = " << A << endl;
    cout << "(int)A = " << (int)A << endl;
    cout << "(double)A = " << (double)A << endl <<endl;

    cout << "A = " << A << " B = " << B << endl;
    cout << "A == B = " << (A == B) << endl;
    cout << "A != B = " << (A != B) << endl;
    cout << "A > B = " << (A > B) << endl;
    cout << "A <= B = " << ( A <= B )  << endl << endl;
    return 0;
}
