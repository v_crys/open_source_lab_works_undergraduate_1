// str.cpp: определяет точку входа для консольного приложения.
//

#include <string>
#include <iostream>
using namespace std;
class String  {
private:
	char *pStr;
	int _size;
public:
	String()  {
		pStr = NULL;
		_size = 0;
		//cout << "Constr " << this << endl;
	}
	String(char *str)  {
		_size = strlen(str);
		pStr = new char[_size + 1];
		strcpy(pStr, str);
		//cout << "Constr " << this << endl;
	}
	String(const String &obj)  {
		_size = obj._size;
		pStr = new char[_size + 1];
		strcpy(pStr, obj.pStr);
		//cout << "Copy " << this << endl;
	}
	~String()  {
		delete[]pStr;
		//cout << "Destr " << this << endl;
	}
	int size()  {
		return _size;
	}

	String operator + (const String &obj)  {
		char *temp = new char[_size + obj._size + 1];
		strcpy(temp, this->pStr);
		strcat(temp, obj.pStr);
		String result(temp);
		delete[]temp;
		return result;
	}
	String operator + (const char c)  {
		int t_size;
		if (_size != 0)
			t_size = this->_size + 2;
		else
			t_size = 2;
		char *temp = new char[t_size];
		if (pStr)
			strcpy(temp, this->pStr);
		temp[t_size - 2] = c;
		temp[t_size - 1] = '\0';
		String result(temp);
		delete[]temp;
		return result;
	}
	/*operator char*()  {
	return pStr;
	}*/
	String &operator = (const String &obj)  {
		if (&obj == this)
			return *this;
		delete[]pStr;
		_size = obj._size;
		pStr = new char[_size + 1];
		strcpy(pStr, obj.pStr);
		return *this;
	}
	friend istream &operator >> (istream &is, String &obj)  {
		int ios_size = 0;
		is.sync();
		String temp;
		while (is.peek() != 10)  {
			temp = temp + (char)is.get();
			ios_size++;
		}
		delete[]obj.pStr;
		obj._size = temp._size;
		obj.pStr = new char[obj._size];]
		obj = temp;
		return is;
	}
	friend ostream &operator << (ostream &os, String &obj)  {
		for (int i = 0; i < obj.size(); i++)
			os << obj.pStr[i];
		return os;
	}
};

int main(int argc, _TCHAR* argv[])
{
	return 0;
}
