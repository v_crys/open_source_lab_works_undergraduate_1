#include "MyMatrix.h"


MyMatrix::MyMatrix()
{
	SetSize(AskSize());
	SetDefault();
}

MyMatrix::MyMatrix(const MyMatrix& matr)
{
	SetSize(matr._size);
	_mat = new int*[_size];
	for (int i = 0; i < _size; ++i)
		_mat[i] = new int[_size];
	for (int i = 0; i < _size; ++i)
	{
		for (int j = 0; j < _size; ++j)
		{
			_mat[i][j] = matr._mat[i][j];
		}
	}
}

MyMatrix::MyMatrix(int size)
{
	SetSize(size);
	SetDefault();
}
int MyMatrix::AskSize()
{
	int temp = 0;
	do {
		cout << "\nEnter size (>0 and <40):\n";
		cin >> temp;
	} while (temp <= 0 || temp>40);
	return temp;
}

void MyMatrix::Full()
{
	int i, j;

	for (i = 0; i < _size; i++)
	{
		for (j = 0; j < _size; j++)
		{
			if (j != i)
				_mat[i][j] = i + 1 + j + 1;
			else
				_mat[i][j] = 0;
		}
	}
	for (i = 0; i < _size; i++)
	{
		for (j = 0; j < _size; j++)
		{
			if (j == i)
				for (int k = 0; k < _size; ++k)
				{
					if (k != i)
						_mat[i][j] += _mat[i][k]+_mat[k][j];
				}
		}
	}
	return;
}

void MyMatrix::Print()
{
	int i, j;

	for (i = 0; i < _size; i++)
	{
		for (j = 0; j < _size; j++)
		{
			cout << _mat[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl << endl;
	return;
}
int MyMatrix::det( )
{
	int d = 0;
	MyMatrix b( _size - 1 );
	if ( _size == 2 )
	{
		return ( _mat[0][0] * _mat[1][1] - _mat[0][1] * _mat[1][0]);
	}
	for (int i = 0; i < _size; i++)
	{
		for (int y = 1; y < _size; y++)
			for (int x = 0; x < _size; x++)
			{
				if (x == i) continue;
				if (x < i)
					b._mat[y - 1][x] = _mat[y][x];
				else
					b._mat[y - 1][x - 1] = _mat[y][x];
			}

		d += ( ( (i & 1) == 0 ) ? 1 : -1 ) * _mat[0][i] * b.det();

	}
	return d;
}

MyMatrix operator*(int x, MyMatrix &matr)
{
	int tempsize = matr.GetSize();
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp[i][j] *= x;
		}
	}
	return temp;
}

std::ostream &operator<<(std::ostream& cout, const MyMatrix& matr)
{
    int i;
    for ( i = 0; i < matr._size; ++i)
	{
		for (int j = 0; j < matr._size; ++j)
		{
			cout << matr[i][j]<< " ";
		}
		cout << endl;
	}
	return cout;
}


void  MyMatrix::Rotate()
{
	for (int i = 0; i < _size / 2; ++i)
	{
		for (int j = 0; j < (_size + 1) / 2; j++)
		{
			int tmp = _mat[i][j];
			_mat[i][j] = _mat[_size - 1 - j][i];
			_mat[_size - 1 - j][i] = _mat[_size - 1 - i][_size - 1 - j];
			_mat[_size - 1 - i][_size - 1 - j] = _mat[j][_size - 1 - i];
			_mat[j][_size - 1 - i] = tmp;
		}
	}
	return;
}
void  MyMatrix::Rotate(int n)
{
	for (int i = 0; i < n; ++i)
	{
		Rotate();
	}
}

void MyMatrix::SetDefault()
{
	_mat = new int*[_size];
	for (int i = 0; i < _size; ++i)
		_mat[i] = new int[_size];
	Full();
}
void MyMatrix::ReSize()
{
	for (int i = 0; i < _size; ++i)
		delete[]_mat[i];
	delete[]_mat;
	SetSize(AskSize());
	SetDefault();
}

void MyMatrix::ReSize(int size)
{
	for (int i = 0; i < _size; ++i)
		delete[]_mat[i];
	delete[]_mat;
	SetSize(size);
	SetDefault();
}

void MyMatrix::Assign(MyMatrix mat)
{
	for (int i = 0; i < _size; ++i)
		delete[]_mat[i];
	delete[]_mat;
	cout << _size << endl;
	SetSize(mat._size);
	cout << _size << endl;
	_mat = new int*[_size];
	for (int i = 0; i < _size; ++i)
		_mat[i] = new int[_size];
	for (int i = 0; i < _size; ++i)
	{
		for (int j = 0; j < _size; ++j)
		{
			_mat[i][j] = mat._mat[i][j];
		}
   }
}

MyMatrix MyMatrix::operator+(const MyMatrix matr) const
{
	if (_size != matr._size)
	{
		cout << "The matrix should be the same size";
		return 0;
	}
	int tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp._mat[i][j] = _mat[i][j] + matr._mat[i][j];
		}
	}
	return temp;
}
MyMatrix MyMatrix::operator+(const int x) const
{
	int tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp._mat[i][j] +=x;
		}
	}
	return temp;
}
MyMatrix MyMatrix::operator-(const MyMatrix matr) const
{
	if (_size != matr._size)
	{
		cout << "The matrix should be the same size";
		return 0;
	}
	int tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp._mat[i][j] = _mat[i][j] - matr._mat[i][j];
		}
	}
	return temp;
}

MyMatrix MyMatrix::operator-(const int x) const
{
	int tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp._mat[i][j] -= x;
		}
	}
	return temp;
}
MyMatrix MyMatrix::operator*(const MyMatrix matr) const
{
	if (_size != matr._size)
	{
		cout << "The matrix should be the same size";
		return 0;
	}
	int n, tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int k = 0; k < tempsize; k++)
		{
			n = 0;
			for (int j=0; j<tempsize; j++)
			{
			n += _mat[i][j] * matr._mat[j][k];
			}
			temp._mat[i][k] = n;
		}
	}
	return temp;
}


MyMatrix MyMatrix::operator*(const int x) const
{
	int tempsize = _size;
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp._mat[i][j] *= x;
		}
	}
	return temp;
}

MyMatrix& MyMatrix::operator=(const MyMatrix& matr)
{
	if (this == &matr)
		return *this;
	for (int i = 0; i < _size; ++i)
		delete[]_mat[i];
	delete[]_mat;
	SetSize(matr._size);
	_mat = new int*[_size];
	for (int i = 0; i < _size; ++i)
		_mat[i] = new int[_size];
	for (int i = 0; i < _size; ++i)
	{
		for (int j = 0; j < _size; ++j)
		{
			_mat[i][j] = matr._mat[i][j];
		}
	}
	return *this;

}
int* MyMatrix::operator[](int index) const
{
	if (index<0 || index >_size)
		return 0;
	return _mat[index];
}
MyMatrix::~MyMatrix()
{
	for (int i = 0; i < _size; ++i)
		delete[]_mat[i];
	delete[]_mat;
}
