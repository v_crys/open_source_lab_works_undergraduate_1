#include "Global.h"

MyMatrix operator*(int x, MyMatrix matr)
{
	int tempsize = matr.GetSize();
	MyMatrix temp(tempsize);
	for (int i = 0; i < tempsize; ++i)
	{
		for (int j = 0; j < tempsize; j++)
		{
			temp[i][j] *= x;
		}
	}
	return temp;
}

ostream &operator<<(ostream& cout, MyMatrix& matr)
{
	for (int i = 0; i < matr.GetSize(); ++i)
	{
		for (int j = 0; j < matr.GetSize(); ++j)
		{
			cout << matr[i][j]<< " ";
		}
		cout << endl;
	}
	return cout;
}
