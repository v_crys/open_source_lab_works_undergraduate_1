#pragma once
#include<iostream>
using namespace std;

class MyMatrix
{
	int **_mat;
	int _size;
	void Full();
	void SetSize(int size) { _size = size; }
	void SetDefault();
	int AskSize();
public:
	int **GetMat() { return _mat; }
	int GetSize() { return _size; }
	MyMatrix operator+(const MyMatrix) const;
	MyMatrix operator+(const int) const;
	MyMatrix operator-(const MyMatrix) const;
	MyMatrix operator-(const int) const;
	MyMatrix operator*(const MyMatrix) const;
	MyMatrix operator*(const int) const;
	MyMatrix& operator=(const MyMatrix&);
	int* operator[](int) const;
	void Assign(MyMatrix);
	void Rotate(int);
	void Rotate();
	void Print();
	void ReSize();
	void ReSize(int);
	int det(MyMatrix, int);
	MyMatrix();
	MyMatrix(const MyMatrix&);
	MyMatrix(int);
	~MyMatrix();
};


