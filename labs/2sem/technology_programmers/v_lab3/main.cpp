#include <iostream>

class String {
    private:
        char *_str;
        int _len;

        int *_copy_str;

        void set_size( int size );
        void create_str( char * );
        int len_str( char * ) const;
    public:
        //------ constructor & destructor
        String( ) : _str( NULL ), _len( 0 ) { _copy_str = new int( 1 );}
        String( char *s ) : _str( NULL ), _len( 0 ) { _copy_str = new int( 1 ); create_str( s ); }
        ~String();

        //------ copy constructor
        String( const String &src );

        String &operator=( const String &src );
        String &operator=( char *src );

        String operator+( const String &src ) const ;
        String operator+( char *src ) const ;
        friend String operator+( char *src, const String &src_r );

        String &operator+= ( const String &src );

        char &operator[]( int );

        bool operator==( const String &src ) const;
        bool operator!=( const String &src ) const;

        bool operator==( char *src ) const;
        bool operator!=( char *src ) const;

        friend bool operator==( char *src, const String &src_r );
        friend bool operator!=( char *src, const String &src_r );

        int get_copys( ) { return *_copy_str; }
        //-------- thread operators
        friend std::ostream & operator<< ( std::ostream &OS, const String &src_r );
        friend std::istream & operator>> ( std::istream &OS, String &src_r );
        friend void swap( String &src1, String &src2 );
};

int String::len_str( char *s ) const
{
    int i = 0;
    while ( *s != 0 )
    {
        i++;
        s++;
    }

    return i;
}

void String::create_str( char *s )
{
    _len = len_str( s );
    set_size( _len );
    _str = s;
}

void String::set_size( int size )
{
    if ( size < 0 ) throw( "Set_size: input size < 0" );
    if ( (*_copy_str) > 1 ) throw( "Set_size: _copy_str > 1" );
    if ( _str != NULL )
        delete [] _str;

    _len = size;
    _str = new char[ _len + 1 ];

    return;
}

String::~String()
{
    (*_copy_str)--;
    if ( ( *_copy_str ) != 0 ) return;

    delete [] _str;
    delete _copy_str;
}


String::String( const String &src )
{
    (*(src._copy_str))++;
    _copy_str = src._copy_str;
    _str = src._str;
    _len = src._len;
}

String &String::operator=( const String &src )
{
    int i;

   (*this).~String();

    _len = src._len;
    _str = src._str;
    (*(src._copy_str))++;
    _copy_str = src._copy_str;

    return *this;
}

String &String::operator=( char *src )
{
    int i;

   (*this).~String();

    _len = (*this).len_str( src );
    _str = src;
    _copy_str = new int( 1 );
}

String String::operator+( const String &src ) const
{
    String res;
    int i;

    res._len = _len + src._len;

    res.set_size( res._len );

    for ( i = 0; i < res._len; i++ )
        res._str[ i ] = ( i < _len ) ? _str[ i ] : src._str[ i - _len ];

    res._str[ i ] = 0;

    return res;
}

String String::operator+( char *src ) const
{
    String res;
    String buf( src );

    res = *this + buf;
    return res;
}

String operator+( char *src, const String &src_r )
{
    String buf( src );
    return ( buf + src_r );
}

String &String::operator+= ( const String &src )
{
    *this = *this + src;
    return *this;
}

char &String::operator[]( int i )
{
    if ( (*_copy_str) > 1 ) throw( "operator[]: error (please copy str)" );
    if ( ( i < 0 ) || ( i >= _len ) ) throw( "operator[]: error index" );
    return _str[ i ];
}

bool String::operator==( const String &src ) const
{
    if ( _str == src._str ) return true;
    if ( _len != src._len ) return false;

    int i;
    for ( i = 0; i < _len; i++ )
        if ( _str[ i ] != src._str[ i ] )
            return false;

    return true;
}

bool String::operator!=( const String &src ) const
{
    return !( *this == src );
}

bool String::operator==( char *src ) const
{
    if ( _len != len_str( src ) ) return false;

    int i;
    for ( i = 0; i < _len; i++ )
        if ( _str[ i ] != src[ i ] )
            return false;

    return true;
}

bool String::operator!=( char *src ) const
{
    return !( *this == src );
}

bool operator==( char *src, const String &src_r )
{
    return (src_r == src);
}

bool operator!=( char *src, const String &src_r )
{
    return !( src_r == src );
}


std::ostream & operator<< ( std::ostream &OS, const String &src_r )
{
    int i;
    for ( i = 0; i < src_r._len; i++ )
        OS << src_r._str[ i ];

    return OS;
}

std::istream & operator>> ( std::istream &OS, String &src_r )
{
    int i;
    int len = src_r._len;
    src_r.~String();

    src_r._copy_str = new int( 1 );
    src_r._len = len;
    src_r.set_size( len );

    for ( i = 0; i < src_r._len; i++ )
    {
        OS >> src_r._str[ i ];
        if ( src_r._str[ i ] == '\n' )
            break;
    }

    src_r._str[ i ] = 0;
    return OS;
}

void swap( String &src1, String &src2 )
{
    if ( src1._str == src2._str ) return;

    std::swap( src1._len, src2._len );
    std::swap( src1._copy_str, src2._copy_str );
    std::swap( src1._str, src2._str );
}

using namespace std;

int main()
{
    String A( "hello_" );
    String B( "world" );
    String C;

    cout << "A = " << A << endl;
    cout << "B = " << B << endl;
    cout << "A + B = " << A + B << endl;
    cout << "A + \"world\" = " << A + "world" << endl;
    cout << "\"Hello\" + B = " << "Hello" + B << endl;
    C = "SUAI";
    cout << "C = \"SUAI\"; C = " << C << endl;

    C += A;
    cout << "C += A; C = " << C << endl;

    swap( C, A );
    cout << "swap( C, A ); C = " << C << endl << endl;


    cout << "C == \"hello_\", = " << ( C == "hello_" ) << endl;
    cout << "C != A, = " << ( C != A ) << endl << endl;

    B = A;
    C = A;
    cout << "B = A; C = A;, A.get_copys() = " << A.get_copys()
                << " B.get_copys() = " << B.get_copys() << " C.get_copys() = " << C.get_copys() << endl;

    B = "abra";
    cout << "B = \"abra\";, A.get_copys() = " << A.get_copys()
                << " B.get_copys() = " << B.get_copys() << " C.get_copys() = " << C.get_copys() << endl;

    A = "z";
    cout << "A = \"z\";, A.get_copys() = " << A.get_copys()
                << " B.get_copys() = " << B.get_copys() << " C.get_copys() = " << C.get_copys() << endl;

    return 0;
}
