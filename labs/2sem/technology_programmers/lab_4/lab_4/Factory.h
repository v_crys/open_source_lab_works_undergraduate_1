#pragma once
#include "Doc.h"
#include "Student_card.h"
#include "Passport.h"
#include "International_passport.h"

#define MAX_DOC_INCLUDE ( sizeof( Docs_created_factory ) / 4 ) // ����� ������������ ����������

class Factory {
public:
	virtual Doc* created() = 0;
};

template <class T>
class Factory_doc : public Factory {
public:
	Doc* created() { return (new T); }
};
//class Add {
//public:
//	char **Name;
//	int MAX_DOC_INCLUDE;
//	Factory *Docs_created_factory[];
//
//	Add() {
//
//		Factory *Docs_created_factory[] = {
//			new Factory_doc<Passport>,
//			new Factory_doc<international_passport>,
//			new Factory_doc<student_card>
//		};
//
//		MAX_DOC_INCLUDE = (sizeof(Docs_created_factory) / 4);
//	}
//};

Factory *Docs_created_factory[] =
{
	new Factory_doc<Passport>,
	new Factory_doc<international_passport>,
	new Factory_doc<student_card>
};