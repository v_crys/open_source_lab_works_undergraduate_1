#pragma once
#include "Doc.h"
#include <iostream>

#define ID_Student_Card 3
#define NAME_Student_Card "Student Card"

class student_card : public Doc
{
protected:
	char *Name;
	char *Surname;
	char *Name_university;
	char *Group_number;
	int ID() { return ID_Student_Card; };
	void NAME() { std::cout << NAME_Student_Card; };
public:
	void show();
	student_card();
	~student_card();
};
