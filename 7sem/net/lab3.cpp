#include "stdafx.h"

#define MaxM 5
#define MaxE 6

#define INF 9999 // бесконечность

int matrM[MaxM][MaxM] = { { 0, INF, INF, INF, 60 },{ INF, 0, 10, INF, INF },{ 25, 10, 0, 15, 30 },{ INF, INF, INF, 0, 20 },{ 60, 90, 30, 20, 0 } };
int matrE[MaxE][MaxE] = { { 0, 30, INF, 20, INF, INF },{ INF, 0, 15, 15, INF, INF },{ INF, 15, 0, 20, 25, INF },{ 20, 15, 20, 0, INF, 25 },{ INF, INF, 25, 10, 0, 40 },{ 15, INF, INF, 25, 40, 0 } };

int matrMD[MaxM][MaxM] = { { INF, INF, INF, INF, 60 },{ INF, INF, 10, INF, INF },{ 25, 10, INF, 15, 30 },{ INF, INF, INF, INF, 20 },{ 60, 90, 30, 20, INF } };
int matrED[MaxE][MaxE] = { { INF, 30, INF, 20, INF, INF },{ INF, INF, 15, 15, INF, INF },{ INF, 15, INF, 20, 25, INF },{ 20, 15, 20, INF, INF, 25 },{ INF, INF, 25, 10, INF, 40 },{ 15, INF, INF, 25, 40, INF } };



bool SravMatr(int **m1, int **m2, int max) {

	for (int i = 0; i < max; ++i) {
		for (int j = 0; j < max; ++j) {
			if (m1[i][j] != m2[i][j])
				return false;
		}
	}
	return true;
}

void print(int **matr, int max) {

	for (int i = 0; i < max; ++i) {
		for (int j = 0; j < max; ++j) {
			printf("%d ", matr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	return;
}

void Floid(int **res, int **m1, bool V, int max) {
	int min = INF;
	int minK = 0;

	for (int i = 0; i < max; ++i) {
		for (int j = 0; j < max; ++j) {
			min = INF;
			minK = 0;

			for (int k = 0; k < max; ++k) {
				if ((m1[k][j] + (V ? matrMD[i][k] : matrED[i][k])) < min) {
					min = m1[k][j] + (V ? matrMD[i][k] : matrED[i][k]);
					minK = k + 1;
				}
			}

			res[i][j] = minK;
		}
	}

	return;
}

void CalcNextDegree(int **m1, int **m2, int max) {

	int min = INF;

	for (int i = 0; i < max; ++i) {
		for (int j = 0; j < max; ++j) {
			min = INF;

			for (int k = 0; k < max; ++k) {
				if ((m1[i][k] + m1[k][j]) < min) {
					min = m1[i][k] + m1[k][j];
				}
			}

			if (min >= INF)
				m2[i][j] = INF;
			else
				m2[i][j] = min;
		}
	}

	return;
}

int main()
{
	int **matrLM = new int*[MaxM];
	for (int i = 0; i < MaxM; ++i) {
		matrLM[i] = new int[MaxM];
		for (int j = 0; j < MaxM; ++j)
			matrLM[i][j] = matrM[i][j];
	}
	int **matrLMT = new int*[MaxM];
	for (int i = 0; i < MaxM; ++i) {
		matrLMT[i] = new int[MaxM];
		for (int j = 0; j < MaxM; ++j)
			matrLMT[i][j] = 0;
	}
	int **matrGM = new int*[MaxM];
	for (int i = 0; i < MaxM; ++i) {
		matrGM[i] = new int[MaxM];
		for (int j = 0; j < MaxM; ++j)
			matrGM[i][j] = 0;
	}

	int **matrLE = new int*[MaxE];
	for (int i = 0; i < MaxE; ++i) {
		matrLE[i] = new int[MaxE];
		for (int j = 0; j < MaxE; ++j)
			matrLE[i][j] = matrE[i][j];
	}
	int **matrLET = new int*[MaxE];
	for (int i = 0; i < MaxE; ++i) {
		matrLET[i] = new int[MaxE];
		for (int j = 0; j < MaxE; ++j)
			matrLET[i][j] = 0;
	}
	int **matrGE = new int*[MaxE];
	for (int i = 0; i < MaxE; ++i) {
		matrGE[i] = new int[MaxE];
		for (int j = 0; j < MaxE; ++j)
			matrGE[i][j] = 0;
	}

	for (int k = 1; k < MaxM; ++k) {
		
		CalcNextDegree(matrLM, matrLMT, MaxM);

		printf("MatrL %d\n", k);
		print(matrLM, MaxM);
		printf("Matr Temp\n");
		print(matrLMT, MaxM);
		printf("\n");

		if (SravMatr(matrLM, matrLMT, MaxM)) {
			break;
		}
		for (int i = 0; i < MaxM; ++i) 
			for (int j = 0 ; j < MaxM; ++j)
				matrLM[i][j] = matrLMT[i][j];
	}


	printf("\nEV\n\n");


	for (int k = 1; k < MaxE; ++k) {

		CalcNextDegree(matrLE, matrLET, MaxE);

		printf("MatrL %d\n", k);
		print(matrLE, MaxE);
		printf("Matr Temp\n");
		print(matrLET, MaxE);
		printf("\n");

		if (SravMatr(matrLE, matrLET, MaxE)) {
			break;
		}
		for (int i = 0; i < MaxE; ++i)
			for (int j = 0; j < MaxE; ++j)
				matrLE[i][j] = matrLET[i][j];
	}

	Floid(matrGM, matrLM, true, MaxM);
	Floid(matrGE, matrLE, false, MaxE);

	printf("\nMax:\n");
	print(matrGM, MaxM);
	printf("\nEv:\n");
	print(matrGE, MaxE);
	printf("\n");

	delete(matrGE); 
	delete(matrGM);
	delete(matrLE);
	delete(matrLET);
	delete(matrLM);
	delete(matrLMT);

    return 0;
}

