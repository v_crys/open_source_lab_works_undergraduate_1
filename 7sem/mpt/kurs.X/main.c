 // For LOK-4 (see difference in description) 
//pin description
 
//   PORT  SIGNAL  _MODULE  in/out

//   PORTA    
//   RA0 - free or ADC_0   _ADC   (IN)      
//   RA1 - free or ADC_1   _ADC   (IN)      
//   RA2 - free or ADC_2   _ADC   (IN)      
//   RA3 - free or ADC_3   _ADC   (IN)      
//   RA4 - free
//   RA5 - free or ADC_4   _ADC   (IN)      
 

//  RB0 -  IR_transmitter  (OUT)
//  RB1 -   D/~I   _LCD    (OUT) 1- data,  0 =instruction
//  RB2 -   E     _LCD    (OUT)  0-1-0
//  RB3 -  Ring(OUT)  IR-receiver(IN)
//  RB4 - KEY_B4/Data _LCD    (OUT)
//  RB5 - KEY_B5/Data _LCD    (OUT)
//  RB6 - KEY_B6/Data _LCD    (OUT)
//  RB7 - KEY_B7/Data _LCD    (OUT)

// PortC

//  RC0 - free
//  RC1 - free
//  RC2 - free
//  RC3 - SCL      _I2C    (IN)
//  RC4 - SDA      _I2C    (IN)
//  RC5 - free
//  RC6 - TX       _COM    (OUT)
//  RC7 - RX       _COM    (IN)
// 
// PortD
//  RD0 - free
//  RD1 - free
//  RD2 - free
//  RD3 - free
//  RD4 - free
//  RD5 - free
//  RD6 - free
//  RD7 - free


#include	<pic.h>
#include <xc.h>

#include <string.h>

//#include <lcd.inc>

#define byte unsigned char 
#define Freq 20  // =  main frequency  (MHz)
//#define LOK_1


#define  testbit(var, bit)   ((var) & (1 <<(bit)))
#define  setbit(var, bit)    ((var) |= (1 << (bit)))
#define  clrbit(var, bit)    ((var) &= ~(1 << (bit)))

//#define _F84
//#define _F628
//#define _F870
//#define _F873
#define _F877
//#define +F72

#ifdef _F877
#define DIP_40
#endif

#ifdef _F873|_F870|_F72
#define DIP_28
#endif

#ifdef _F84
#define DIP_18
#endif


#ifdef _F84
	__CONFIG(0x3FFA);
#endif
#ifdef _F628
	__CONFIG(0x3F6A);
#endif
#ifdef _F873
	__CONFIG(0x3D7A);
#endif
#ifdef _F870
	__CONFIG(0x3D72);
#endif
#ifdef _F877
//	__CONFIG(0x3972);
#endif
#ifdef _F72
	__CONFIG(0x3FF2);
#endif

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)


#define Simple_I2C

#ifdef Simple_I2C

//#include "simple_I2C.h"


void init_I2C(void); 
byte IN_BYTE_I2C(void);
byte IN_BYTE_ACK_I2C(void);//IN_BYTE+ACK
byte IN_BYTE_NACK_STOP_I2C(void);//IN_BYTE+NACK+STOP
void OUT_BYTE_I2C(byte);
void ACK_I2C(void);
void NACK_I2C(void);
void START_I2C(void);
void STOP_I2C(void);
void LOW_SCL_I2C(void);
void HIGH_SCL_I2C(void);
void LOW_SDA_I2C(void);
void HIGH_SDA_I2C(void);
void CLOCK_PULSE_I2C(void);
void Init_WRITE_I2C(unsigned int);
void Init_READ_I2C(unsigned int);
void Check_ACK_I2C(void);
void OUT_BYTE_PAGE_I2C(byte);
#define SCL 3
#define SDA 4
byte Ch_ACK;
#endif

byte D_Read(void);
void D_Write(byte);
void D_Reset(void);
void Global_Init_DS1821(void);

#define ClockValue 1 
//Speed_I2 =   (((FOSC/(Clock+1))/4) -1) 
#define	SET_HIGH_SPEED_I2C SSPADD=ClockValue

#define SET_I2C_TIMER	Slave_ADR_RW_I2C=0xD0
#define SET_I2C_EEPROM	Slave_ADR_RW_I2C=0xA0
#define SET_I2C_TERMO_DS1621 Slave_ADR_RW_I2C=0x92 

#define cache_size_I2C 0x40 


//-----------------------------------------------
void Delay(unsigned int);
void Delay_L(unsigned int);
void Delay_LL(unsigned int);
void Delay_Long_Break(unsigned int);
void Beep(void); 
byte Check_buttons(void);
byte Check_Sensor_buttons(void);
//,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
void Init_LCD(void);
void Show_String_LCD(const char *);
void Send_Byte_LCD  (byte );
void Clr_LCD(void);
void Send_Command_LCD  (byte);
void Set_Coord_LCD(byte,byte);
//,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
void General_Init_I2C (void);
void Init_Write_I2C(unsigned int);
void Init_Read_I2C(unsigned int);
void Start_I2C(void );void Start2_I2C(void );void Start2R_I2C(void );
void Stop_I2C(void );void Stop2_I2C(void );
void Send_Slave_Addr_I2C(void);
void GeneralCheck_I2C(void);
void Write_I2C(byte); 
void Write_Cache_I2C(byte);
void Write_Byte_I2C(byte);
byte Read_I2C(void);byte Read2_I2C(void);byte Read2N_I2C(void);
void N_Ack_I2C(void);
void Ack_I2C(void);

byte tmp_buffer_I2C,Slave_ADR_RW_I2C,tmp_I2C;
unsigned int Adr_I2C;

//,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
void Show_String_COM(const char *);
void myTransmit_COM(byte);
void Delay_Fast(byte);
//----------------------------------------------
void Send_Burst_IR(byte);

byte check_buttons(void);
//=============================================
static const char str_TEST[]=
"Test of all!";
static const char str_TEST_EEPROM[]=
"Test of serial  EEPROM ";
static const char str_Writing[]=
"Writing.....";
static const char str_Reading[]=
"Reading.....";
static const char str_PRESS[]=
" Press any key!";
static const char str_B4[]=
"Press B4";
static const char str_B5[]=
"Press B5";
static const char str_B6[]=
"Press B6";
static const char str_B7[]=
"Press B7";

static const char str_Test_COM[]=
"Test of COM-PORT ";
static const char str_Test_IR[]=
"Test of Sensor  button ";
static const char str_Termo[]=
"t= ";
static const char str_Test_Termo[]=
"Test of thermometer: ";
static const char str_OK[]=
" OK!";
static const char str_BLANK[]=
"                ";
static const char str_Select[]=
"Select Key :";


static const char str_Hi[]= "Hello, BOSS ! ";
static const char str_Hi_1[]= "dearie Sergey Ivanovich !!!!";


void performer(int *symbol, int *number_symbol, char *up_line, char *down_line);
long pow2(long num, long degree);


void Pulse_LCD(unsigned int x);
//---------------------------
void Init_LCD(void);
void Send_B_LCD  (byte tmp);
void Send_Byte_LCD  (byte tmp);
void Send_Command_LCD  (byte tmp);
void Clr_LCD(void);
void Set_Coord_LCD(byte i,byte j);
void Show_String_LCD(const char * mySTRING);

byte Current_ind;
byte Key_Press;
//===================================
/*
void Check_Led(void) 
{byte i,tmp_PORTB=PORTB,tmp_TRISB=TRISB;
TRISB=0;
for(i=1;i>0;i>>1){PORTB=i;Delay(100);PORTB=0;}
#ifdef DIP_18
  goto ret;
#endif 

ret:
PORTB=tmp_PORTB;
TRISB=tmp_TRISB;
}
*/
//--------------------------------------
void Start_Thermo(byte tmp)
{START_I2C();OUT_BYTE_I2C(0x92);OUT_BYTE_I2C(tmp);}

//===================================
void main(void)
{
  byte button = 0;
    int symbol = 0, number_symbol = 0;
    
    //TRISA=0xFF;
    TRISB=0xFF;
    //TRISC=0x9B;
    //TRISB=0x00;
    TRISD=0x00;

    PORTD=14;
    //PORTD=0;
    PORTB=0;
    Delay(300);
    char up_line[]={0x30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    char down_line[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    
    
    Init_LCD();
    Clr_LCD();
    General_Init_I2C(); 
    

//    show_string_LCD(load_old_data);
    //TRISD=0;PORTD=0;
    
    //i2c_send_byte(0x0000,"fdf");
    
    Delay(10);
         
    while(1)
    {
        button = check_buttons();
        if(button == 0x1)          //left button 0001
        {
           
            Clr_LCD();
            Set_Coord_LCD(0,0);
            Show_String_LCD(" Read from   I2C ");
            
            Set_Coord_LCD(1,0);Delay(100);
            Show_String_LCD("fd");
            
            Delay(10);
            
            Clr_LCD();
           /*

            //load_data(up_line, down_line);
            set_coord_LCD(0,0);
            show_string_LCD(up_line);
            set_coord_LCD(1,0);
            show_string_LCD(down_line);
            delay_MS(1000);*/
            break;
        }

        if(button == 0x8)
        {         //right button (not need load) //1000
            Clr_LCD();
            Show_String_LCD(up_line);
            break;
        }
    }
    while(1)
        performer(&symbol, &number_symbol, up_line, down_line);
}


//loop
void performer(int *symbol, int *number_symbol, char *up_line, char *down_line)
{
    Delay(10);
    byte button = check_buttons();

    if (button != 0x0)
    {
        if (button == 0x1)
        {
            if (*symbol == 1)
                (*symbol) = 0;       /* binary 0 and 1 */
            else
                ++(*symbol);
        }
       /* change to next position */
        if (button == 0x2)
        {
            if (*number_symbol != 16)
            {
                (*symbol) = 0;
                ++(*number_symbol);
            }
        }

        if (*number_symbol <= 16)
            up_line[*number_symbol] = *symbol + 0x30;   /* numbers start with 0x30 ascii */


        if (button==0x4)
        {                                    /* convert processing */
           /* int pow2 = 1, sum = 0, len = 0;
            len = strlen(upline);
            for (int i = len - 1; i > -1; i--)
                {
                    if (up_line[i] == '1')
                        sum += pow2;
                    pow2 *= 2;
                }*/
            int len = 0,index = 0;
            long sum =0;
            len = strlen(up_line);             /* length of up string */
            int digree = len - 1;
            for (int i = 0; i < len; i++, digree--)
            {
                 sum += (up_line[i] - 0x30) * pow2(2, digree);
            }

            if ((10000 < sum) && (sum < 100000))
            {
                down_line[index] = (sum / 10000) + 0x30;
                sum = sum % 10000;
                index++;
            }
                if ((1000 < sum) && (sum < 10000))
                {
                    down_line[index] = (sum / 1000) + 0x30;
                    sum = sum % 1000;
                    index++;
                }
                if ((100 < sum) && (sum < 1000))
                {
                    down_line[index] = (sum / 100) + 0x30;
                    sum = sum % 100;
                    index++;
                }
                if ((10 < sum) && (sum < 100))
                {
                    down_line[index] = (sum / 10) + 0x30;
                    sum = sum % 10;
                    index++;
                }
                if (sum < 10) {
                    down_line[index] = (sum)+0x30;
                    index++;
                }
        }

       if (button == 0x8)
        {                                //  Save data
            Clr_LCD();
            Set_Coord_LCD(0,0);
            Show_String_LCD("Writing to  I2C ");
            Set_Coord_LCD(1,0);
            Show_String_LCD("                ");
            Delay(2500);
            
            //delay_MS(1000);
            //i2c_send_byte(0x00A0,"a");
            Clr_LCD();
            Delay(500);
        }

        Clr_LCD();
        Set_Coord_LCD(0,0);
        Show_String_LCD(up_line);
        Set_Coord_LCD(1,0);
        Show_String_LCD(down_line);
    }
}

byte check_buttons(void)
{
        byte tmp, tmp_PORTB = PORTB, tmp_TRISB = TRISB;

        TRISB &= 0x0F;        // 0000????
        PORTB |= 0xF0;        // 1111????

        tmp = PORTB^0xFF;     // 1111 ^ 0111 = 1000

        PORTB = tmp_PORTB;
        TRISB = tmp_TRISB;

        return(tmp>>4);       //00001111
}

long pow2(long num, long degree)
{
    long result = 1;
    int i = 0;
    for (i = 0; i < degree; i++)
    {
        result *= num;
    }
    return result;
}


//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//=============================================
void Beep(void)
{byte tmp_TRISB=TRISB,tmp_PORTB=PORTB,i;

TRISB3=0; //clrbit(TRISB,3);
i=12*Freq; while(i--){
                  RB3=1;Delay(8*Freq);      
                  RB3=0;Delay(8*Freq);
                 }
    PORTB=tmp_PORTB;
    TRISB=tmp_TRISB;
}
void Delay(unsigned int tmp) // (tmp=1000) ~  11 mS (4MHz)
{ while(tmp--);return;}
//==================================


//==============================================


byte Check_buttons(void)
{byte tmp,tmp_PORTB=PORTB,tmp_TRISB=TRISB;
TRISB&=0x0F; PORTB|=0xF0;
tmp=PORTB^0xFF; PORTB=tmp_PORTB; TRISB=tmp_TRISB;
return(tmp>>4);
}



byte Check_Sensor_buttons(void)
{byte tmp,
tmp_PORTA=PORTA,tmp_TRISA=TRISA;
PORTA=0;
TRISA=0;
tmp=0;
Delay(6000); 
TRISA=15;

tmp|=PORTA;
tmp|=PORTA;
tmp|=PORTA;
tmp|=PORTA;
//tmp|=PORTA;
//tmp|=PORTA;


Delay(1000); 
PORTA=tmp_PORTA; TRISA=tmp_TRISA;
return((~tmp)&0x0F);
}

#ifndef _F84


//====================================

void General_Init_I2C (void)
{
#ifdef Simple_I2C
init_I2C();
#else
 	SSPEN=0;
	setbit(TRISC,3);
	setbit(TRISC,4);
	SET_HIGH_SPEED_I2C;  // 	; initialize I2C baud rate
	clrbit(SSPSTAT,6);  //	; select I2C input levels
	clrbit(SSPSTAT,7);  //	; enable slew rate
//	setbit(SSPSTAT,7);

	SSPCON=0b00111000;   //	; Master mode, SSP enable
	return; 	//		; return from subroutine

#endif
}

//;--------------------------------------
void Init_Write_I2C(unsigned int uAdr_I2C)
{ Stop_I2C();
 Adr_I2C=uAdr_I2C;
tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);

	Slave_ADR_RW_I2C&=0xFE ; //Write mode
	Start_I2C();

	return;
}
//;=======================================

void Init_Read_I2C(unsigned int uAdr_I2C)
{  Adr_I2C=uAdr_I2C;
tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);


	Init_Write_I2C(Adr_I2C);

//	Stop_I2C();  !!!!!!!!!!!!!!!!!!!!!!!!!
	Slave_ADR_RW_I2C|=1; 
	Start_I2C();
	return;
}

//====================================

void Start_I2C(void ) //  ; Start i2c 
{
#ifdef Simple_I2C
START_I2C();
	Send_Slave_Addr_I2C();

	if((Slave_ADR_RW_I2C&1)==0)	
   {	if(!(Slave_ADR_RW_I2C&16))Write_I2C((byte)(Adr_I2C>>8));
	Write_I2C((byte)Adr_I2C);
   }


#else

	SEN=1; //	; initiate I2C bus start condition
	while(SEN);



	Send_Slave_Addr_I2C();

	if(Slave_ADR_RW_I2C&1)RCEN=1;	
else

   {	if(!(Slave_ADR_RW_I2C&16))Write_I2C((byte)(Adr_I2C>>8));
	Write_I2C((byte)Adr_I2C);
   }
#endif
return;
}

//;=========================================================

void Stop_I2C(void)
{
#ifdef Simple_I2C
STOP_I2C();
#else
	if(Slave_ADR_RW_I2C&1)
{

	while(RCEN);
	while(!STAT_BF);while (STAT_RW);
	//GeneralCheck_I2C();
	tmp_buffer_I2C=SSPBUF;
	N_Ack_I2C();
}
	PEN=1;// 	; initiate I2C bus stop condition
	while(PEN);
#endif
tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);


	return	; 
}
//;===========================================================

//; Generate I2C address write (R/W=0) and read (R/W=1)


void Send_Slave_Addr_I2C(void)
{int tmp;
#ifdef Simple_I2C
Rep:
OUT_BYTE_I2C(Slave_ADR_RW_I2C);
NACK_I2C();
if(testbit(PORTB,SDA)){STOP_I2C();START_I2C();goto Rep;};

#else
tmp=4;
 while(tmp--){

	SSPBUF=Slave_ADR_RW_I2C; //initiate I2C bus write condition
while (STAT_RW);	
//GeneralCheck_I2C();

		
  if(ACKSTAT==0)	return ;
Delay(100);
	Stop_I2C();
	SEN=1;
	while(SEN) ;
           }


#endif
}

//;======================================================
void Write_I2C(byte tmp) 
{
#ifdef Simple_I2C
OUT_BYTE_I2C(tmp);
#else
SSPIF=0;
	SSPBUF=tmp;//	; initiate I2C bus write condition
 while(SSPIF==0);

	//GeneralCheck_I2C();
	while(ACKSTAT);
SSPIF=0;
#endif
	return;
}

//;=======================================================
void Write_Cache_I2C(byte tmp)
{ 
	Write_I2C(tmp);
	Adr_I2C++;
	tmp_buffer_I2C--;
 if(tmp_buffer_I2C)return;

	Stop_I2C();
	tmp_buffer_I2C=cache_size_I2C;

//	clrw


	Start_I2C();

	return;
}
//==============================

void Write_Byte_I2C(byte tmp)
{ 
	Write_I2C(tmp);
	Adr_I2C++;
	Stop_I2C();
	tmp_buffer_I2C=cache_size_I2C;
	Start_I2C();
	return;
}



//;======================================

byte Read_I2C(void)
{byte tmp;
#ifdef Simple_I2C
tmp= IN_BYTE_I2C();
	Adr_I2C++;
#else	
	while(RCEN){};  //	; test

	while(STAT_BF==0);
while (STAT_RW);
//	GeneralCheck_I2C();

	tmp=SSPBUF;
	Adr_I2C++;
	Ack_I2C();
#endif


	return(tmp);
}

//;====================================

//
//; Send Not Acknowledge
void N_Ack_I2C(void)
{
#ifndef Simple_I2C
	ACKDT=1;//	; acknowledge bit state to send (not ack)
	ACKEN=1;// 	; initiate acknowledge sequence
	while(ACKEN);//	; ack cycle complete?
	return;
#endif
}
//;---------------------------------------
//; Send Acknowledge
void Ack_I2C(void)
{
#ifndef Simple_I2C
	ACKDT=0; //; acknowledge bit state to send
	ACKEN=1; //; initiate acknowledge sequence

	while(ACKEN);//	; ack cycle complete?
	RCEN=1;//	; generate receive condition
	return;
#endif
}

//==============================================

//====================================
void Start2_I2C(void ) //  ; Start i2c 
{
#ifndef Simple_I2C
SSPIF=0;
BCLIF=0;
con2:
if(SSPSTAT&16)goto con1;
if(SSPSTAT&8)goto con2;
con1:
	SEN=1; //	; initiate I2C bus start condition
	while(SEN);
//	if(Slave_ADR_RW_I2C&1)RCEN=1;	
 while(SSPIF==0);
SSPIF=0;
return;
#endif
}

void Start2R_I2C(void ) //  ; Start i2c 
{
#ifndef Simple_I2C
	RSEN=1; //	; initiate I2C bus start condition
	while(RSEN);
 while(SSPIF==0);
SSPIF=0;
#endif
return;
}

//;=========================================================

void Stop2_I2C(void)
{byte tmp;
#ifndef Simple_I2C
	PEN=1;// 	; initiate I2C bus stop condition
	while(PEN);
//   tmp=SSPBUF;
 while(SSPIF==0);
SSPIF=0;
#endif
	return	; 
}

//;===========================================================

byte Read2_I2C(void)
{byte tmp;
#ifndef Simple_I2C
SSPIF=0;
	RCEN=1;

//	while(RCEN){};  //	; test
//	while(STAT_BF==0);
while(SSPIF==0);
	tmp=SSPBUF;
SSPIF=0;

//	GeneralCheck_I2C();

	ACKDT=0;ACKEN=1;//while(ACKEN);
while(SSPIF==0);
	Adr_I2C++;
RCEN=0;SSPIF=0;
#endif
	return(tmp);
}




byte Read2N_I2C(void)
{byte tmp;
#ifndef Simple_I2C
SSPIF=0;
	RCEN=1;

//	while(RCEN){};  //	; test
//	while(STAT_BF==0);
while(SSPIF==0);
	tmp=SSPBUF;
SSPIF=0;

//	GeneralCheck_I2C();

	ACKDT=1;ACKEN=1;//while(ACKEN);
while(SSPIF==0);
	Adr_I2C++;
RCEN=0;SSPIF=0;
#endif
	return(tmp);
}
//==============================


#endif  //ifndef _F84



//--------------------------------------
void Delay_Fast(byte  i)
{while (i--);}

//--------------------------------------
byte Indic=1<<4;
void Delay_Long_Break(unsigned int k)
{while(k--){
PORTC=Indic;//PORTC+1+128;
if((Indic&128)==0)Indic+=128;else{
Indic-=128;

if (Indic==1)Indic=1<<4; else
{

if (Indic==0)Indic=1;

if (Indic==(127-(1<<5)-(1<<6)-(1<<2)))Indic=0;

if (Indic==((1<<5)))Indic=127-(1<<5)-(1<<6)-(1<<2);

if (Indic==(1+(1<<5)))Indic=(1<<5);

if (Indic==(1+(1<<1)+(1<<6)))Indic=1+(1<<5);

if (Indic==(1+(1<<3)))Indic=1+(1<<1)+(1<<6);

if (Indic==((1<<2)+(1<<3)))Indic=1+(1<<3);

if (Indic==(127-(1<<2)-(1<<5)))Indic=(1<<3) + (1<<2);

if (Indic==(1<<4))Indic=127-(1<<2)-(1<<5);
}
                                 }
Delay_L(65000);if(Key_Press)break;}
}

void Delay_LL(unsigned int k)
{ while(k--)Delay_Fast(255);}
//=================================
void myTransmit_COM(byte i)
{

#define myH 0
#define myL 1


#define myD 0xaa  // Speed = 4800  ( 208 uS per bit)

RC6=myH; //start bit
Delay_Fast(myD);

if(testbit(i,0))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,1))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,2))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,3))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,4))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,5))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,6))RC6=myL;else RC6=myH;Delay_Fast(myD);
if(testbit(i,7))RC6=myL;else RC6=myH;Delay_Fast(myD);

RC6=myL; //stop bit
Delay_Fast(myD);
Delay_Fast(myD);

return;
}
//==============================================
void Show_String_COM(const char *i)
{
const char *myS;

myS=i;
	while(*myS){
myTransmit_COM(*myS);
myS++;};

myTransmit_COM(0x0D);
myTransmit_COM(0x0A);
	return;

}
//=====================================
/*
void Send_Burst_IR(byte tmp)
{
clrbit(TRISB,0);
	while (tmp--){
           RB0=1; Delay_Fast(0);         
           RB0=0; Delay_Fast(0);         
                     }
//if(RB3)Beep();
}
*/
//===============================================

//======================================
void Delay_L(unsigned int tmp) // (tmp=1000) ~  3 mS (20MHz)
{ byte tmp_1=PORTB,tmp_2=TRISB; Key_Press=0;
TRISB&=0x0F; PORTB|=0xF0;
 while(tmp--)if((PORTB&0xF0)!=0xF0){Key_Press=1;break;}
PORTB=tmp_1; TRISB=tmp_2;
return;
}
//====================================================
//==================================================
// Simple I2C    ===================================
//==================================================

void init_I2C() {
	RC4=0;		
/* set the SDA pin LOW. The SDA pin is then set 
			HIGH by the TRIS command 
*/
	TRISC3=0;	// set SCL line to an OUPUT 
	TRISC4=0;	// set SDA line to an INPUT just to be sure! 
}
	
//=======================================================
//=======================================================

void Init_WRITE_I2C(unsigned int Adr_begin)
{Adr_I2C=Adr_begin;
rep:
START_I2C();Slave_ADR_RW_I2C&=0xFE; 
OUT_BYTE_I2C(Slave_ADR_RW_I2C);
if(Ch_ACK){STOP_I2C();goto rep;}
OUT_BYTE_I2C(Adr_begin>>8);if(Ch_ACK){STOP_I2C();goto rep;}
OUT_BYTE_I2C(Adr_begin);if(Ch_ACK){STOP_I2C();goto rep;}
}
//------------------------------------
void Init_READ_I2C(unsigned int Adr_begin)
{ Init_WRITE_I2C(Adr_begin);
START_I2C ();Slave_ADR_RW_I2C|=1; OUT_BYTE_I2C(Slave_ADR_RW_I2C);
}

//--------------------------------------
void LOW_SCL_I2C(void)
{ //clrbit(PORTC,SCL);
clrbit(PORTC,SCL);Delay_Fast(5);
}
//-----------------------------------------
void HIGH_SCL_I2C(void)
{ //setbit(TRISC,SCL)
 setbit(PORTC,SCL);Delay_Fast(5);
}
//---------------------------------------
void LOW_SDA_I2C(void)
{ clrbit(PORTC,SDA);clrbit(TRISC,SDA);Delay_Fast(5);
}
//-----------------------------------------
void HIGH_SDA_I2C(void)
{ setbit(TRISC,SDA);Delay_Fast(5);
}
//---------------------------------------
void CLOCK_PULSE_I2C(void)
{HIGH_SCL_I2C();LOW_SCL_I2C();}
//--------------------------------------
void STOP_I2C(void)
{LOW_SDA_I2C(); LOW_SCL_I2C(); 
HIGH_SCL_I2C();HIGH_SDA_I2C();LOW_SCL_I2C();}
//----------------------------------------
void START_I2C(void)
{HIGH_SDA_I2C();HIGH_SCL_I2C();LOW_SDA_I2C();LOW_SCL_I2C();}
//---------------------------------------
void ACK_I2C(void)
{ LOW_SDA_I2C(); CLOCK_PULSE_I2C();}
//---------------------------------------
void NACK_I2C(void)
{ HIGH_SDA_I2C(); CLOCK_PULSE_I2C();}
//----------------------------------
void Check_ACK_I2C(void)
{HIGH_SCL_I2C();if(testbit(PORTC,SDA))Ch_ACK=1;else Ch_ACK=0; 
 LOW_SCL_I2C();
}
//---------------------------------------
void OUT_BYTE_I2C(byte t)
{byte tmp;
tmp=8;
while(tmp--){
  if(t & 0x80)HIGH_SDA_I2C();  else LOW_SDA_I2C();
CLOCK_PULSE_I2C(); t+=t;
            }
 HIGH_SDA_I2C(); Check_ACK_I2C();
}
//----------------------------------------
//-----------------------------------------------
byte IN_BYTE_I2C(void)
{byte t,tmp=8;
t=0;HIGH_SDA_I2C();
while (tmp--) {t+=t;       HIGH_SCL_I2C();
if(testbit(PORTC,SDA))t++; LOW_SCL_I2C();
              }
return(t);
}
//----------------
byte IN_BYTE_NACK_STOP_I2C(void)
{byte t;t=IN_BYTE_I2C();NACK_I2C();STOP_I2C();
return(t);
}
//------------------
byte IN_BYTE_ACK_I2C(void)
{byte t;t=IN_BYTE_I2C();ACK_I2C();return(t);
}
//-------------------------------------------
void OUT_BYTE_PAGE_I2C(byte tmp)
{ 
	OUT_BYTE_I2C(tmp);
	Adr_I2C++;
 if((cache_size_I2C-1) & Adr_I2C)return;
	STOP_I2C();
	Init_WRITE_I2C(Adr_I2C);
	return;
}

void Pulse_LCD(unsigned int x){ RB2=1; Delay(x); RB2=0;Delay(x);}
//---------------------------
void Init_LCD(void)
{
Delay(200*Freq); TRISB=0; PORTB=0x30;
Pulse_LCD(20*Freq); Pulse_LCD(20*Freq); Pulse_LCD(20*Freq);

PORTB=0x20;  Pulse_LCD(20*Freq);

Send_Command_LCD  (0x28);
Send_Command_LCD  (0x0C);
Send_Command_LCD  (0x06);
Send_Command_LCD  (0x02);
}

//==========================================
void Send_B_LCD  (byte tmp)
{while (Check_buttons())Delay(500*Freq);
PORTB=(PORTB&0x0F)+ (tmp&0xF0);
Pulse_LCD(2*Freq);
PORTB=(PORTB&0x0F)+ (tmp<<4);
Pulse_LCD(2*Freq);
}
//---------------------------------
void Send_Byte_LCD  (byte tmp)
{Send_B_LCD(tmp);
Current_ind++;
if(Current_ind==16)Set_Coord_LCD(1,0);
//if(Current_ind==32)Set_Coord_LCD(0,0);
}
//======================================
void Send_Command_LCD  (byte tmp)
{ 
RB1=0; Send_B_LCD(tmp);RB1=1; 
Delay(250*Freq);
}
//==================
void Clr_LCD(void)
{
Set_Coord_LCD(0,0);
Show_String_LCD(str_BLANK); 
Show_String_LCD(str_BLANK); 
Set_Coord_LCD(0,0);
}
//========================================================
void Set_Coord_LCD(byte i,byte j)
{
    if(i==0){Current_ind=j;Send_Command_LCD(0x80+j);} 
    else {Current_ind=16+j;Send_Command_LCD(0xC0+j);};
	return;
}
//=========================================================
void Show_String_LCD(const char * mySTRING)
{while(*mySTRING){Send_Byte_LCD(*(mySTRING++));}; }

//=====================================




//================================
//=================================
//===============================

#ifdef DS1821
//****************************************************************************
//D_Reset  -- Resets the 1-wire bus and checks for presence & short cct
//****************************************************************************
void D_Reset(void)
{
 char count=47;
 //-- Reset the status bits
TRISE=0;
 RE0=0;

 //-- Ensure Correct port pin settings
 TRISD=255-5;
 PORTD=4;

 //-- Start the reset Pulse

RD1=0;          //-- Pull Line Low to start reset pulse
TRISD=TRISD&(255-2); 

  Delay(200);      //-- 480uS Delay
TRISD|=2;
 //D_TRIS=1;          //-- Release the line
 Delay(30);       //-- Delay 100uS to about centre of presence pulse

 PORTE=PORTD;  //-- Get Presence status 1=None 0=something there

 Delay(1000);    //-- Rise time + Min Space

}
//******************END OF D_Reset


//****************************************************************************
// D_Write
//****************************************************************************
void D_Write(byte Data)
{byte D_Data;
 char count=8;
 for(;count>0;count--)
 {
  D_Data= Data & 0x01;    //-- Get LSB

  //-- Write the bit to the port
  RD1=0;
TRISD=TRISD&(255-2); 

//  D_TRIS=0;               //-- Lower the port
  Delay(1);             //-- Time slot start time 5 us
  if(D_Data)TRISD|=2; //-- Output the data to the port

Delay(20);
  //DelayUs(50);            //-- Finish Timeslot
  
TRISD|=2;
//  D_TRIS=1;               //-- Ensure Release of Port Pin

  //-- Delay Between Bits
  //DelayUs(D_RiseSpace);             //-- Recovery time between Bits
Delay(2);

  //-- Prep Next Bit
  Data=Data>>1;           //-- Shift next bit into place
 }
// DelayUs(D_RiseSpace);    //-- Rise time + Min Space
Delay(5);
}

//******************END OF D_Write

//****************************************************************************
// D_Read
//****************************************************************************
byte D_Read(void)
{
 byte count=8,data=0;
 for(;count>0;count--)
 {
  //-- Write the bit to the port
 // D_PIN=0;
  RD1=0;
 // D_TRIS=0;               //-- Lower the port
TRISD&=(255-2);
  //DelayUs(5);             //-- Time slot start time
Delay(1);
//  D_TRIS=1;               //-- Release port for reading
TRISD|=2;
//  DelayUs(5);             //-- Get close to center of timeslot
Delay(1);
data = data >> 1;
if(RD1)data = data+128;
//  DelayUs(50);            //-- Finish the timeslot
Delay(10);

  //-- Delay Between Bits
  //DelayUs(D_RiseSpace);             //-- Recovery time between Bits
} 
 //DelayUs(D_RiseSpace);    //-- Rise time + Min Space
Delay(5);
 return(data);

}

//******************END OF D_Read


void Global_Init_DS1821(void)
{
 byte count=16,data=0;
TRISD=0;
PORTD=4;  Delay(50000); 
RD1=1; PORTD=4;  Delay(50000); 
PORTD=2;Delay(50000); 
 
Delay(10);
 for(;count>0;count--)
 {
  PORTD=0;
Delay(1); 
PORTD=2;
Delay(1); 
}

Delay(10);
  PORTD=6;
 TRISD=2;
Delay(10);
}
#endif

#ifdef NIKITA

#include <xc.h>
#include <string.h>
#include "lcd.h"
#include "resource.h"
#include "routine.h"
#include "i2c.c"

static const char load_old_data[]="Load from mem?  <-YES  x x  NO->";
static void performer(int *symbol, int *number_symbol, char *up_line, char *down_line);
static long pow2(long num, long degree);
	
int main()
{
    byte button = 0;
    int symbol = 0, number_symbol = 0;
    
    //TRISA=0xFF;
    TRISB=0xFF;
    //TRISC=0x9B;
    //TRISB=0x00;
    TRISD=0x00;

    PORTD=14;
    //PORTD=0;
    PORTB=0;
    delay_MS(300);
    char up_line[]={0x30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    char down_line[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    
    
    init_LCD();
    clr_LCD();
    General_Init_I2C(); 
    

    show_string_LCD(load_old_data);
    //TRISD=0;PORTD=0;
    
    //i2c_send_byte(0x0000,"fdf");
    
    delay_MS(1000);
         
    while(1)
    {
        button = check_buttons();
        if(button == 0x1)          //left button 0001
        {
           
            clr_LCD();
            set_coord_LCD(0,0);
            show_string_LCD(" Read from   I2C ");
            
            set_coord_LCD(1,0);delay_MS(1000);
            show_string_LCD("fd");
            
            delay_MS(10000);
            
            clr_LCD();
           /*

            //load_data(up_line, down_line);
            set_coord_LCD(0,0);
            show_string_LCD(up_line);
            set_coord_LCD(1,0);
            show_string_LCD(down_line);
            delay_MS(1000);*/
            break;
        }

        if(button == 0x8)
        {         //right button (not need load) //1000
            clr_LCD();
            show_string_LCD(up_line);
            break;
        }
    }
    while(INFINIT)
        performer(&symbol, &number_symbol, up_line, down_line);
}


//loop
static void performer(int *symbol, int *number_symbol, char *up_line, char *down_line)
{
    delay_MS(10);
    byte button = check_buttons();

    if (button != 0x0)
    {
        if (button == 0x1)
        {
            if (*symbol == 1)
                (*symbol) = 0;       /* binary 0 and 1 */
            else
                ++(*symbol);
        }
       /* change to next position */
        if (button == 0x2)
        {
            if (*number_symbol != 16)
            {
                (*symbol) = 0;
                ++(*number_symbol);
            }
        }

        if (*number_symbol <= 16)
            up_line[*number_symbol] = *symbol + 0x30;   /* numbers start with 0x30 ascii */


        if (button==0x4)
        {                                    /* convert processing */
           /* int pow2 = 1, sum = 0, len = 0;
            len = strlen(upline);
            for (int i = len - 1; i > -1; i--)
                {
                    if (up_line[i] == '1')
                        sum += pow2;
                    pow2 *= 2;
                }*/
            int len = 0,index = 0;
            long sum =0;
            len = strlen(up_line);             /* length of up string */
            int digree = len - 1;
            for (int i = 0; i < len; i++, digree--)
            {
                 sum += (up_line[i] - 0x30) * pow2(2, digree);
            }

            if ((10000 < sum) && (sum < 100000))
            {
                down_line[index] = (sum / 10000) + 0x30;
                sum = sum % 10000;
                index++;
            }
                if ((1000 < sum) && (sum < 10000))
                {
                    down_line[index] = (sum / 1000) + 0x30;
                    sum = sum % 1000;
                    index++;
                }
                if ((100 < sum) && (sum < 1000))
                {
                    down_line[index] = (sum / 100) + 0x30;
                    sum = sum % 100;
                    index++;
                }
                if ((10 < sum) && (sum < 100))
                {
                    down_line[index] = (sum / 10) + 0x30;
                    sum = sum % 10;
                    index++;
                }
                if (sum < 10) {
                    down_line[index] = (sum)+0x30;
                    index++;
                }
        }

       if (button == 0x8)
        {                                //  Save data
            clr_LCD();
            set_coord_LCD(0,0);
            show_string_LCD("Writing to  I2C ");
            set_coord_LCD(1,0);
            show_string_LCD("                ");
            delay_MS(2500);
            
            //delay_MS(1000);
            //i2c_send_byte(0x00A0,"a");
            clr_LCD();
            delay_MS(500);
        }

        clr_LCD();
        set_coord_LCD(0,0);
        show_string_LCD(up_line);
        set_coord_LCD(1,0);
        show_string_LCD(down_line);
    }
}

static long pow2(long num, long degree)
{
    long result = 1;
    int i = 0;
    for (i = 0; i < degree; i++)
    {
        result *= num;
    }
    return result;
}
#endif