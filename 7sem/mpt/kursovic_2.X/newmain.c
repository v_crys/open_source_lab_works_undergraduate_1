#include <pic.h>
#include <string.h>
//#include "KERNEL_CONFIG.h"
#define byte unsigned char
#define Freq 20  // main frequency

////////////////////// EEPROM
#define  testbit(var, bit)   ((var) & (1 <<(bit))) //proverka urovnya napryazheniya na SCL i SDA
#define  setbit(var, bit)    ((var) |= (1 << (bit))) //ustanavlivaet v bit-om bite var-a edinicu 
#define  clrbit(var, bit)    ((var) &= ~(1 << (bit))) //ustanavlivaet v bit-om bite var-a nol' (~ - otricanie)

#define SCL 3
#define SDA 4
#define cache_size_I2C 0x40

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)


byte Ch_ACK; //flag opredelyayushchij uroven' napryazheniya na SDA
byte tmp_buffer_I2C,Slave_ADR_RW_I2C,tmp_I2C;
unsigned int Adr_I2C;
void work();
long power(long num, long deg);
//LCD
void send_command_LCD  (byte tmp);
void Send_B_LCD  (byte tmp);
void set_coord_LCD(byte i,byte j);
void show_string_LCD(const char * mySTRING);
void mult(char *a, char *b, char *c);
byte Current_ind = 0;
void hex_to_dec( char * Source, char * out );
//////////////////////OTHER
static const char str_BLANK[]="                  ";
//static const char load_old_data[]="Load from mem?  <-YES  x x  NO->";


char str_line0[]={0x30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//stroki LCD---0
char str_line1[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	//-------------1
char mas[]={'A','B','C','D','E','F'};//?????? ??? ???? ? ????????? ????
char total_sum[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

char s1[]={0,0,0,0,0};//????? ??????? ?? 5, ? ? ???? ???????? ?? -1 ?????? ? ??????? ???????

		

char in_p = 0, in_c = 0, line = 0;//1-poziciya v stroke 2-vyvodimyj na LCD simvol 3-stroka LCD
byte button=0; 

//////////////BUTTONS////////////////////////////////
/*
* FUNKCIYA PROVERKI NAZHATIYA KNOPKI
* oprashivaet vse knopki i formiruet bajt dannyh, soderzhashchij informaciyu o nazhatyh knopkah
* nazhatym knopkah sootvetstvuyut edinicy v maldshyh 4 razryadah, v starshih vsegda nuli (1111 - 7,6,5,4 knopki)
 */
byte Check_buttons(void){
	byte tmp,tmp_PORTB=PORTB,tmp_TRISB=TRISB;
	
	TRISB&=0x0F; // 0000????
	PORTB|=0xF0; // 1111????
	
	tmp=PORTB^0xFF; // 1111 ^ 0111 = 1000
	
	PORTB=tmp_PORTB; TRISB=tmp_TRISB;
	
	return(tmp>>4); //00001111
}

// func stoper (zaderjka)
void delay(unsigned int tmp){ // (tmp=1000) ~  3.2 mS (20MHz) 31265 ~ 100 mS

	while(tmp--);
	return;
}

// func other stoper (drugaya zaderjka)
void delay10(int tmp){
    while(tmp--){
		delay(3126);
	}
    return;
}
///////////////LCD///////////////////////////
/*
* funkciya dlya podachi vhodnogo sinhrosignala, 
* kotoryj obespechivaet schityvanie indikatorom dannyh s ego vhodov
*/
void pulse_LCD(unsigned int x){ 
	RB2=1; 
	delay(x); 
	RB2=0;
	delay(x);
}
// Funkciya inicializacii
void Init_LCD(void){
	delay(200*Freq); 
	TRISB=0; 
	PORTB=0x30; // 0011 0000
	pulse_LCD(20*Freq); 
	pulse_LCD(20*Freq); 
	pulse_LCD(20*Freq);

	PORTB=0x20; // 0010 0000
	pulse_LCD(20*Freq);
	/*
	* ustanavlivaet rezhim otobrazheniya 2-h strok s matricej 5*8 tochek
	* i rabotu s 4-h razryadnoj shinoj dannyh
	*/
	send_command_LCD  (0x28); // 0010 0100
	send_command_LCD  (0x0C); // 0000 1100
	//vklyuchaet otobrazhenie na ehkrane ZHKI-modulya, bez otobrazheniya kursorov
	send_command_LCD  (0x06); // 0000 0101
	//ustanavlivaet rezhim avtomaticheskogo peremeshcheniya kursora sleva-naprvo posle vyvoda kazhdogo simvola
	send_command_LCD  (0x02); // 0000 0010
}
/*
* podprogramma dlya peresylki starshego i mladshego bajtov komandy v ZHKI
* ispol'zuetsya v Send_Command i Send_Byte
*/
void Send_B_LCD  (byte tmp){
	//zhdat', poka nazhaty knopki
	while (Check_buttons()) delay(500*Freq);
	//esli knopki otzhaty- peresylka starshego bajta komandy/dannyh
	PORTB=(PORTB&0x0F) + (tmp&0xF0);
	pulse_LCD(2*Freq);
	//peresylka mladshego bajta komandy/dannyh
	PORTB=(PORTB&0x0F) + (tmp<<4);
	pulse_LCD(2*Freq);
}
//peredacha v ZHKI dannye
void Send_Byte_LCD (byte tmp){
	//posylaem bajt s dannymi (RS = 1)
	Send_B_LCD(tmp);
	Current_ind++; //peremennaya pokazyvaet, v kakoe mesto ZHKI my vyvodim 
	
	if(Current_ind==16) set_coord_LCD(1,0); //esli poslednee mesto v stroke, perekhodim na sleduyushchuyu stroku
	if(Current_ind==32)	set_coord_LCD(0,0); //esli poslednee mesto vo 2 stroke, perekhodim na 1 stroku, 1 poziciya
}

void Send_Byte_LCD_str2  (byte tmp){
	//posylaem bajt s dannymi (RS = 1)
	Send_B_LCD(tmp);
	Current_ind++; //peremennaya pokazyvaet, v kakoe mesto ZHKI my vyvodim 
	
	//if(Current_ind==16) set_coord_LCD(1,0); //esli poslednee mesto v stroke, perekhodim na sleduyushchuyu stroku
	if(Current_ind==32)	set_coord_LCD(0,0); //esli poslednee mesto vo 2 stroke, perekhodim na 1 stroku, 1 poziciya
}

//peredacha v ZHKI komandy
void send_command_LCD  (byte tmp){ 
	RB1=0; //perevod RS v 0 dlya peredachi komandy
	Send_B_LCD(tmp);
	RB1=1; //vozvrashchaem vse nazad
	delay(250*Freq);
}
// clear LCD display
void Clr_LCD(void){
	set_coord_LCD(0,0);
	show_string_LCD(str_BLANK); 
	set_coord_LCD(0,0);
}
//dlya ustanovleniya kursora v nuzhnuyu stroku
void set_coord_LCD(byte i,byte j){
   //1 stroka
	if(i==0){ 
		Current_ind=j;
		send_command_LCD(0x80+j);// (1000 0000) + j
		 //2 stroka
	}else{  
		Current_ind=16+j;
		send_command_LCD(0xC0+j);// (1100 0000) + j
	};
	return;
}
//dlya vyvoda na displej soderzhimoe strokovyh konstant
void show_string_LCD(const char * mySTRING){
	while(*mySTRING){
		Send_Byte_LCD(*(mySTRING));
		mySTRING++;
	};
}
//////////////////////EEPROM & I2C/////////////////////////////
/*
* I2c sostoit iz 2 provodnikov, podtyanutyh k +5V cherez rezistory s soprotivleniem v neskol'ko KOm
* SCL - otvechaet za peredachu impul'sov sinhronizacii  
* SDA - otvechaet za peredachu dannyh mezhdu ustrojstvami
*/
void LOW_SCL_I2C(void){//????????? SCL ?? ?????? ??????????
	//ustanavlivaem v 3 bit(scl) registra PORTC 0
	clrbit(PORTC,SCL);
	delay(5);
}

void HIGH_SCL_I2C(void){//ustanovka SCL na vysokoe napryazhenie
	//ustanavlivaem v 3 bit(scl) registra PORTC 1
	setbit(PORTC,SCL);delay(5);
}

void LOW_SDA_I2C(void){//ustanovka SDA na nizkoe napryazhenie
	//ustanavlivaem v 4 bit(sda) registra PORTC 0
	clrbit(PORTC,SDA);
	clrbit(TRISC,SDA);
	delay(5);
}

void HIGH_SDA_I2C(void){// na vysokoe napryazhenie
	//ustanavlivaem v 4 bit(sda) registra PORTC 1
	setbit(TRISC,SDA);
	delay(5);
}

void CLOCK_PULSE_I2C(void){// sinhroimpul's na SCL
	HIGH_SCL_I2C();
	LOW_SCL_I2C();
}
/*
* otzyv ustrojstva signalom ASK
* vydaet nulevoj bit na SDA vo vremya dejstviya devyatogo sinhroimpul'sa
* posylaetsya posle kazhdogo peredannogo bajta(krome zaversheniya chteniya)
*/
void ACK_I2C(void){
	LOW_SDA_I2C(); 
	CLOCK_PULSE_I2C();
}
/*
* signal nack(no ack - otsutstvie podtverzhdeniya)
* vydaet spec kombinaciyu signalov na sda scl dlya zaversheniya chteniya 
*/
void NACK_I2C(void){
	HIGH_SDA_I2C(); 
	CLOCK_PULSE_I2C();
}

// startovaya kombinaciya - signal START, podaetsya dlya nachal organizacii svyazi po I2C
void START_I2C(void){
	HIGH_SDA_I2C();HIGH_SCL_I2C();
	//perevod linii SDA iz sostoyaniya vysokogo napryazheniya v nizkoe, na SCL vysokoe napryazhenie
	LOW_SDA_I2C();LOW_SCL_I2C();
}
// signal stop, konec svyazi po I2C
void STOP_I2C(void){
	LOW_SDA_I2C(); LOW_SCL_I2C(); 
	//perevod linii SDA iz nulevogo sostoyaniya v sostoyanie vysokogo napryazheniya, na SCL vysokij uroven' napryazheniya
	HIGH_SCL_I2C();HIGH_SDA_I2C();LOW_SCL_I2C();
}

void Check_ACK_I2C(void){//proverka napryazheniya na sda
	HIGH_SCL_I2C(); // ustanavlivaem vysokoe napryazhenie na SCL
	if(testbit(PORTC,SDA))
		Ch_ACK=1; // high
	else Ch_ACK=0; // low
 	LOW_SCL_I2C();
}
//realizaciya peredachi bajta dannyh s pomoshch'yu izmeneniya urovnya napryazheniya na sda
void OUT_BYTE_I2C(byte t){
	byte tmp;
	tmp=8;
	while(tmp--){
  		if(t & 0x80)HIGH_SDA_I2C(); else LOW_SDA_I2C();
		CLOCK_PULSE_I2C(); t+=t;
	}
 	HIGH_SDA_I2C(); 
	Check_ACK_I2C();
}
/*
* realizaciya otpravki ZAPROSNOGO BAJTA
* zaprosnyj bajt sostoit iz adresnogo polya(4 bita - tip ustrojstva, 3 bita - nomer konkretnoj skhemy)
* i bita napravleniya peredachi (0-zapis', 1-chtenie)
*/
void Send_Slave_Addr_I2C(void){
	int tmp;
	Rep:
	OUT_BYTE_I2C(Slave_ADR_RW_I2C);//Poluchaem otvet ot vedomogo s takim-to adresom
	NACK_I2C();
	if(testbit(PORTB,SDA)){
		STOP_I2C();START_I2C();goto Rep;
	};
	tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);
}
/*
* zapis' v bufer bajta, kotoryj posle zaversheniya peredachi(posle signala STOP)
* iz ehtogo bufera budet zapisan v pamyat'(razmer bufera = 64 bajta)
*/
void Write_I2C(byte tmp){ 
	OUT_BYTE_I2C(tmp);
	SSPIF=0;
	SSPBUF=tmp;
 	while(SSPIF==0);
	while(ACKSTAT);
	SSPIF=0;
}

void Start_I2C(void ){ //  ; Start i2c 
	START_I2C();			//Posylaem kombinaciyu START na shinu I2C
	Send_Slave_Addr_I2C();	 //Posylaem na shinu zaprosnyj bajt,soderzhashchij tip, nomer ustrojstva i bit operacii

	if((Slave_ADR_RW_I2C&1)==0){ //Esli 8 bit zaprosnogo bajta 0,to zapisyvaem v pamyat'
		if(!(Slave_ADR_RW_I2C&16))
			Write_I2C((byte)(Adr_I2C>>8));
		Write_I2C((byte)Adr_I2C);
	}
}

void Stop_I2C(void){// Ostanovka operacii
	STOP_I2C();		//Vystavlyaem linii SDA/SCL v nuzhnoe polozhenie
	tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);//vychislyaem i zapisyvaem svobodnoe prostranstvo v bufere
	return;
}

void Init_Write_I2C(unsigned int uAdr_I2C){
	STOP_I2C();
	Adr_I2C=uAdr_I2C;
	tmp_buffer_I2C=cache_size_I2C-(Adr_I2C&63);
	Slave_ADR_RW_I2C&=0xFE ; //write mode
	START_I2C();

	return;
}
//inicializaciya pered zapis'yu dannyh v pamyat'
void Init_WRITE_I2C(unsigned int Adr_begin){
	Adr_I2C=Adr_begin;
	rep:
	START_I2C();Slave_ADR_RW_I2C&=0xFE; 
	OUT_BYTE_I2C(Slave_ADR_RW_I2C);
	if(Ch_ACK){
		STOP_I2C();goto rep;
	}
	OUT_BYTE_I2C(Adr_begin>>8);
	if(Ch_ACK){
		STOP_I2C();goto rep;
	}
	OUT_BYTE_I2C(Adr_begin);
	if(Ch_ACK){
		STOP_I2C();goto rep;
	}
}

void OUT_BYTE_PAGE_I2C(byte tmp){ 
	OUT_BYTE_I2C(tmp);
	Adr_I2C++;
	if((cache_size_I2C-1) & Adr_I2C)
		return;
	STOP_I2C();
	Init_WRITE_I2C(Adr_I2C);
	return;
}

void Init_READ_I2C(unsigned int Adr_begin){//inicializaciya chteniya iz pamyati
	Init_WRITE_I2C(Adr_begin);//imitiruem zapros na zapis'
	START_I2C ();//vystavlyaem startovuyu konfiguraciyu
	Slave_ADR_RW_I2C|=1;//adress
	OUT_BYTE_I2C(Slave_ADR_RW_I2C);
}

byte IN_BYTE_I2C(void){
	byte t,tmp=8;
	t=0;HIGH_SDA_I2C();
	while (tmp--){
		t+=t;
		HIGH_SCL_I2C();
		if(testbit(PORTC,SDA))t++; LOW_SCL_I2C();
	}
	return(t);
}

byte IN_BYTE_ACK_I2C(void){
	byte t;t=IN_BYTE_I2C();ACK_I2C();
	return(t);
}

byte IN_BYTE_NACK_STOP_I2C(void){//zavershenie operacii chteniya iz pamyati:mk formiruet NACK
	byte t;
	t=IN_BYTE_I2C();
	NACK_I2C();
	STOP_I2C();
	return(t);
}


// load data memory
void load_data(){
	Init_READ_I2C(0);//read s adress 0
	
	for (int i = 0; i < 16; i++){//read string 0
		str_line0[i] = IN_BYTE_ACK_I2C();
	}
	
	for (int i = 0; i < 16; i++){//read string 1
		str_line1[i] = IN_BYTE_ACK_I2C();
	}
	
	in_c = IN_BYTE_ACK_I2C();//read last symbol
	in_p = IN_BYTE_ACK_I2C();//read last posis 
	line = IN_BYTE_ACK_I2C();//CHitaem strochku s kotoroj poslednij raz rabotali
	
	IN_BYTE_NACK_STOP_I2C();//stop read mode
}

void hex_to_dec_char( byte Sym, char * out )
{
	switch( Sym )
{
	case '0':
		out[ 0 ] = '0';
		out[ 1 ] = 0;
		break;

	case '1':
		out[ 0 ] = '1';
		out[ 1 ] = 0;
		break;

	case '2':
		out[ 0 ] = '2';
		out[ 1 ] = 0;
		break;

	case '3':
		out[ 0 ] = '3';
		out[ 1 ] = 0;
		break;

	case '4':
		out[ 0 ] = '4';
		out[ 1 ] = 0;
		break;
	case '5':
		out[ 0 ] = '5';
		out[ 1 ] = 0;
		break;

	case '6':
		out[ 0 ] = '6';
		out[ 1 ] = 0;
		break;

	case '7':
		out[ 0 ] = '7';
		out[ 1 ] = 0;
		break;

	case '8':
		out[ 0 ] = '8';
		out[ 1 ] = 0;
		break;


	case '9':
		out[ 0 ] = '9';
		out[ 1 ] = 0;
		break;

	case 'A':
		out[ 0 ] = '1';

		out[ 1 ] = '0';
		out[2]= 0;
		break;
	case 'B':
		out[ 0 ] = '1';
		out[ 1 ] = '1';
		out[ 2 ] = 0;
		break;

	case 'C':
		out[ 0 ] = '1';
		out[ 1 ] = '2';
		out[ 2 ] = 0;
		break;

	case 'D':
		out[ 0 ] = '1';
		out[ 1 ] = '3';
		out[ 2 ] = 0;
		break;

	case 'E':
		out[ 0 ] = '1';
		out[ 1] = '4';
		out[ 2] = 0;
		break;

	case 'F':
		out[ 0 ] = '1';
		out[ 1 ] = '5';
		out[ 2 ] = 0;
		break;
}
}

void Sum(char *a,char *b,char *c)
{
	int lena=strlen(a);//???-?? ???????? ??????? ?????
	int lenb=strlen(b);//???-?? ???????? ??????? ?????
	int k=lena+1;//+lenb;//???-?? ???????? ??????????
//	char *c[k+1];//?????? ??? ??????????
int j;
	c[k]='\0';
	k--;
	int s;//?????? ??????? ?????????? ? ???????? ??????????

	for(int i=lena - 1;i>=0;i--)
	{
		if ( (lenb - ( lena - i) ) >= 0 )
			b[ i ] = b[ lenb - ( lena - i) ];
		else 
			b[ i ] = '0';
	}

	lenb = lena;

	for(int i=0;i<=k;i++)
		c[i]=0;
	for(int i=0;i<lena;i++)
{
	//	for(int j=0;j<lenb;j++)
j = i;
		//	{
				int digit=(a[lena-i-1]-'0')+ (b[lenb-j-1]-'0');
				s=i;//+j;
				while (digit>0)
				{
					c[k-s]+=digit%10;
					if(c[k-s]>9)
					{
					c[k-s]=c[k-s]%10;
					c[k-s-1]++;
					}
					digit/=10;
					s++;
					if(s>k)s=k;
				}
			}
		int begin=k-s;
		while(c[begin]==0 && begin<k)begin++;
		for (int i=begin;i<=k;i++)
			c[i]+='0';

		for ( int i = 0; i <=k; i++ )
			c[ i ] = c[ i + begin];
	return;
}

void hex_to_dec( char * Source, char * out )
{
	char Buf[ 17]={ 0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//	char Buf2[ 17 ];
	char Buf3[ 17 ]={ 0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	char sum[17] = {'0',  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	char dec_16[ 3 ] = {'1', '6', 0};
	char Pow[ 17 ] = { '1', 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	int len_source = strlen( Source );
    int i;
	for ( i = len_source - 1; i >= 0 ; i-- )
	{
		///if ( Source[ i ] != '0' )
		//{
			hex_to_dec_char( Source[ i ], Buf3 ); 	
		
			mult( Pow, Buf3, Buf );

			//if ( i == len_source - 1)
			//{
		//		strcpy( sum, Buf );
			//} else {
				Sum( Buf, sum, Buf3 );
				strcpy( sum, Buf3 );
			//}
		//}
		mult( Pow, dec_16, Buf3 );

		strcpy( Pow, Buf3 );

	}
    sum[ 15 ] = 0;

	strcpy( out, sum );
}

void main(void) {
    
                #ifdef _F877
            TRISD=0;PORTD=0;
            #endif
            SET_I2C_EEPROM; TRISC=0x9B;


            //goto Test_EEPROM;
            Init_I2C(); 
    
//	TRISD=0;
	TRISB=0;
	delay10(30);

	//inicializiruem displej
	Init_LCD();
    //char HEX[ 17 ] = { 'F','F','F','F','F','F','F','F','F','F','F','F', 0 };
	char C[ 17 ] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

   // char A[ 17 ] = { '1','2','3','4','5','6','7','8','9','4','2','4'};
   // char B[ 17 ] = { '1','2','3','4','5','6','7','8','9','4','2','4'};
    
   // Sum( A, B, C );
    
	//hex_to_dec( HEX, C  );


	set_coord_LCD(0,0);
			Clr_LCD();
	byte tmp = 0;
    unsigned int i = 0;

			set_coord_LCD(0,0);
			show_string_LCD(str_line0);
			set_coord_LCD(1,0);
			show_string_LCD(str_BLANK);	
            
	
	while(1){
		button=0;
	button=Check_buttons(); 
 	int count=0;//???-?? ????????? ????
	// buuton is pressed
	if (button != 0x0){
		//symbol input
		if (button==0x1){//uvelichivaem chislo
			if (in_c == 15){ // 15 tak tak 16-aya sis schisl 
				in_c = 0; // esli cifra 15, to obnulyaem
			}
			else{
				in_c++;
			}
		}
		
		if (button==0x2){//perekhod k sleduyushchemu simvolu v tekushchej stroke
			if (in_p != 16 ){//Esli simvolov budet 8, to zakonchim vvod 
				in_c=0;
				in_p++;
				count++;
			}
		}

		//dlya vyvoda vvedennoe chislo nuzhno konvertirovat' v simvol (+0h30)
		if (in_p < 13){
			str_line0[in_p]=in_c+0x30;
		}
		if(in_c>=10 && in_c<16)
		{
		//	str_line0[in_p]=in_c+0x30;
			switch(in_c)
			{
			case 10:
						str_line0[in_p]=mas[0];
						break;
			case 11:
						str_line0[in_p]=mas[1];
						break;

			case 12:
						str_line0[in_p]=mas[2];
						break;

			case 13:
						str_line0[in_p]=mas[3];
						break;

			case 14:
						str_line0[in_p]=mas[4];
						break;

			case 15:
						str_line0[in_p]=mas[5];
						break;

			}
		}
		set_coord_LCD(0,0);
		show_string_LCD(str_line0);

		if (button==0x8){ //main processing
            hex_to_dec( str_line0, C  );
  
            
            set_coord_LCD(1,0);
        	show_string_LCD(C);    
        }
        }
	}
}

				

//main or not main func (fuck)
void work(){


}
//	  Clr_LCD();
//			set_coord_LCD(0,0);
//			show_string_LCD(C);
 

#ifdef sdfdfsfd

	delay(3126);//=10mc
	button=0;
	button=Check_buttons(); 
 	int count=0;//???-?? ????????? ????
	// buuton is pressed
	if (button != 0x0){
		//symbol input
		if (button==0x1){//uvelichivaem chislo
			if (in_c == 15){ // 15 tak tak 16-aya sis schisl 
				in_c = 0; // esli cifra 15, to obnulyaem
			}
			else{
				in_c++;
			}
		}
		
		if (button==0x2){//perekhod k sleduyushchemu simvolu v tekushchej stroke
			if (in_p != 16 ){//Esli simvolov budet 8, to zakonchim vvod 
				in_c=0;
				in_p++;
				count++;
			}
		}

		//dlya vyvoda vvedennoe chislo nuzhno konvertirovat' v simvol (+0h30)
		if (in_p < 10){
			str_line0[in_p]=in_c+0x30;
		}
		if(in_c>=10 && in_c<16)
		{
		//	str_line0[in_p]=in_c+0x30;
			switch(in_c)
			{
			case 10:
						str_line0[in_p]=mas[0];
						break;
			case 11:
						str_line0[in_p]=mas[1];
						break;

			case 12:
						str_line0[in_p]=mas[2];
						break;

			case 13:
						str_line0[in_p]=mas[3];
						break;

			case 14:
						str_line0[in_p]=mas[4];
						break;

			case 15:
						str_line0[in_p]=mas[5];
						break;

			}
		}
		set_coord_LCD(0,0);
		show_string_LCD(str_line0);

		if (button==0x8){ //main processing
			int len0=0;
			int len1=0;
			unsigned long perem=0;
			char ss[]={0,0};
			unsigned int i=0; 
            len0=strlen(str_line0);//length of string
            int temp_1 = len0-1;
			
//??? ????? ?????????
            		for(int i=len0; i>=0; i--)
					{//??????? ? 16-?????? ??????? ????????? ?? 10-?????? ???????
					if (((str_line0[i] - 0x30) >= 0) && ((str_line0[i] - 0x30) <= 9))
						{
					//	s1[0] += (str_line0[i] - 0x30) * power(16, temp_1);// 
							perem += (str_line0[i] - 0x30) * power(16, temp_1);
						}
						else
						{
							translate(i,temp_1);
						}
						temp_1--;
				/*	if ((i>3)&&(i<=6))
					{
						a2=perem;
					}
					if ((i>6)&&(i<=9))
					{
						a3=perem;
					}
				*/


            	}  
				int a=0;
				while(perem/10 != 0)
					{
						perem=perem/10;
						a++;
							
					}
int bb=a;
					for (int i=0;i<=a;i++)
					{
							str_line1[a-i] = bb%10 + 0x30;
							bb = bb/10;
							
					}
//................
         	
				//	char	l=strlen(summ);//length of string
				//	int num=summ;
					//int a=0;

					//for(int i=0;i<5;i++)//???? ?? ???? ????????? ?????????
					//{

						//set_coord_LCD(0,0);
						//show_string_LCD(s1);
						int a2=0;
						//s1[0]=313;
						//s1[0]+=3;
						a2+=(int)s1[0];
					//	a2++;
						char f[]={0,0};
					//	perem=120302302;

						f[0]=perem+ 0x30;
					

			//ss[0]=234534+ 0x30;
		//	ss[1]=3+ 0x30;
			set_coord_LCD(1,0);
			show_string_LCD(str_line1);
	//show_string_LCD(str_line1);
//show_string_LCD(total_sum);
		//	}            	
		}
	}
#endif


long power(long num, long deg){
    unsigned long result = 1;
    long i = 0;
    for(i = 0; i < deg; i++) {
        result *= num;
    }
    return result;
}

void mult(char *a, char *b, char *c)
{
	int lena=strlen(a);//???-?? ???????? ??????? ?????
	int lenb=strlen(b);//???-?? ???????? ??????? ?????
	int k=lena+lenb;//???-?? ???????? ??????????
//	char *c[k+1];//?????? ??? ??????????
	c[k]='\0';
	k--;
	int s;//?????? ??????? ?????????? ? ???????? ??????????
	for(int i=0;i<=k;i++)
		c[i]=0;
	for(int i=0;i<lena;i++)
		for(int j=0;j<lenb;j++)
			{
				int digit=(a[lena-i-1]-'0')* (b[lenb-j-1]-'0');
				s=i+j;
				while (digit>0)
				{
					c[k-s]+=digit%10;
					if(c[k-s]>9)
					{
					c[k-s]=c[k-s]%10;
					c[k-s-1]++;
					}
				digit/=10;
				s++;
				if(s>k)s=k;
				}
			}
		int begin=k-s;
		while(c[begin]==0 && begin<k)begin++;
		for (int i=begin;i<=k;i++)
			c[i]+='0';

		for ( int i = 0; i <=k; i++ )
			c[ i ] = c[ i + begin];
	return;
}
