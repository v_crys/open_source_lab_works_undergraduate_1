function [ scal ] = scalar( F1, F2 )

scal = 0;

if ( size( F1' ) ~= size( F2' ) )
    error( 'F1 != F2' );
end

for i = 1 : size( F1' )
    scal = scal + (F1( i ) * F2( i ));
end

end

