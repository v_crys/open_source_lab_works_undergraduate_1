function [ out ] = calc_vector_func( number_f, T, Cnt_Point )

out = [ ];
coef = T / Cnt_Point ;
x = 0;

for i = 1 : Cnt_Point
    x = x + coef;
    out = [ out, garmonic_function( number_f, x, T ) / sqrt(Cnt_Point / T) ];
end

end

