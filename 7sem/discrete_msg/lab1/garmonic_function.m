function [ y ] = garmonic_function( number_f, t, T )

switch number_f
    case 1
        y = sqrt( 1 / T );
    case 2
        y = sqrt( 2 / T )*sin( 2*pi * t / T  );
    case 3 
        y = sqrt( 2 / T )*cos( 2*pi * t / T  );
    case 4
        y = sqrt( 2 / T )*sin( 4*pi * t / T  ); 
    case 5
        y = sqrt( 2 / T )*cos( 4*pi * t / T );
    case 6
        y = sqrt( 2 / T )*sin( 6*pi * t / T );
    case 7
        y = sqrt( 2 / T )*cos( 6*pi * t / T );
end

end

