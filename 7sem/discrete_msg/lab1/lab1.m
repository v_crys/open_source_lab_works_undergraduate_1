
while 1
    menu_item = menu( 'Menu', 'Input value',  'Test on harmonic basis', 'Show plot', 'Calc E, Norm, Dist', 'Generate signal', 'Signal param', 'Exit');
    
    switch menu_item
        case 1 
            T = input('T: ');
            Cnt_Point = input( 'Cnt_Point: ' );
            
            Func = zeros( 7, Cnt_Point );
            
            for i = 1 : 7
                Func( i, :) = calc_vector_func( i, T, Cnt_Point );
            end
            
        case 2
            Scal_matrix = zeros( 7, 7 );
            flag = 0;
            for i = 1 : 7
                for j = i : 7 
                    scal = scalar( Func( i, : ), Func( j, : ) );
                    Scal_matrix(i, j) = scal;
                    Scal_matrix(j, i) = scal;
                    
                    if ( flag == 0 )
                        if ( ( ( abs(scal) >= (10^-10) ) && ( i ~= j ) ) || ( ( (abs(scal) - 1) > 10^-10  ) && ( i == j ) )  )
                            disp( 'Not harmonic basis' );
                            flag = 1;       
                        end
                    end
                end
            end
            
            if (flag == 0)
                disp( 'Harmonic basis' );
            end
                    
            disp( 'Scalar_matrix: ');
            disp( Scal_matrix );
            
        case 3
           
            for i = 1 : 7
                subplot(8, 1, i);
                grid;
                plot( T / Cnt_Point : T / Cnt_Point : T, Func( i, : ) );
            end
            
            subplot(8, 1, 8);
            grid;
            hold on;
            
            color = ['b';'g';'r';'c';'m';'y';'k']; 
            for i = 1 : 7
                plot( T / Cnt_Point : T / Cnt_Point : T, Func( i, : ), color( i ) );
            end
            hold off;
            
        case 4
            Dist_matrix = zeros( 7, 7 );
            E_matrix = zeros( 7, 1 );
            Norm_matrix = zeros( 7, 1 );
            for i = 1 : 7
                for j = i : 7 
                    Dist = calc_distance( Func( i, : ), Func( j, : ) ); 
                    Dist_matrix( i, j ) = Dist;
                    Dist_matrix( j, i ) = Dist;
                end
                
                [ E_matrix( i ), Norm_matrix( i ) ] = calc_E_norma( Func( i, : ) );
            end
            
            
            
            disp( 'Distance: ');
            disp( Dist_matrix );
            
            disp( 'E: ' );
            disp( E_matrix );
            
            disp( 'Norma: ' );
            disp( Norm_matrix );
            
        case 5
            information = input( 'Inform msg: ');
            code_signal = [];
            if ( mod( size( information' ), 2)  ==  1 )
                information = [information 0];
            end
            
            
            
            for i = 1 : 2 : size( information' )
                if ( information( i:i+1 ) == [0 0] )
                    koef = -2;
                elseif ( information( i:i+1 ) == [0 1] )
                    koef = -1;
                elseif ( information( i:i+1 ) == [1 0] )
                    koef = 1;
                elseif ( information( i:i+1 ) == [1 1] )
                    koef = 2;
                end
             
                code_signal = [ code_signal;  zeros( Cnt_Point, 1 ) ];
                for j = 1 : Cnt_Point
                    code_signal( j + fix(i/2) * Cnt_Point ) = koef * Func(1, j );
                end
                
                
            end
            
            subplot( 1, 1, 1 );
            grid;
            plot( T / Cnt_Point : T / Cnt_Point : T * size( information' ) / 2, code_signal' );
            
        case 6
            Dist_matrix = zeros( 4, 4 );
            E_matrix = zeros( 4, 1 );
            Norm_matrix = zeros( 4, 1 );
            Signals = zeros( 4, Cnt_Point );
            
            Signals( 1, : ) = Func( 1, : ).*-2;
            Signals( 2, : ) = Func( 1, : ).*-1;
            Signals( 3, : ) = Func( 1, : ).*1;
            Signals( 4, : ) = Func( 1, : ).*2;
            
            for i = 1 : 4
                for j = i : 4
                    Dist = calc_distance( Signals( i, : ), Signals( j, : ) ); 
                    Dist_matrix( i, j ) = Dist;
                    Dist_matrix( j, i ) = Dist;
                end
                
                [ E_matrix( i ), Norm_matrix( i ) ] = calc_E_norma( Signals( i, : ) );
            end
            
            disp( 'Distance: ');
            disp( Dist_matrix );
            
            disp( 'E: ' );
            disp( E_matrix );
            
            disp( 'Norma: ' );
            disp( Norm_matrix );
            
        case 7
            break;
    end
end