function [ E, norma ] = calc_E_norma( F )

E = 0;
for i = 1 : size( F' )
   E = E + (F(i) * F(i));
end

norma = sqrt( E );

end

