function [ ] = Jeka( )
% lab2_1: 
    % Signal bases
    
    num_bits = 50;
    t_array = 0:0.01:1;
    t_array = t_array(1 : end - 1);
    %N = length(t_array);
    T = 1;
    Erro = 0;
    phi0 = sqrt(1/T)+(t_array-t_array);
    
    plot(t_array,phi0);
    
    x = t_array;
    sig = phi0;
    plot(x,sig);
    title('phi0');
    grid on
    
    sig = 0;

    sig_00 = -2*phi0;
    sig_01 = -1*phi0;
    sig_10 = phi0;
    sig_11 = 2*phi0;

    x = [t_array t_array+1 t_array+2 t_array+3];
    sig =[sig_00 sig_01 sig_10 sig_11];
    f2=figure;
    plot(x,sig);
    title('Graphs s(1),s(2),s(3),s(4)');
    grid on
    
    % signal sequence for a given set bit
    % signal set: s =[[1,0];[0,-1];[-1,0];[0,1]]
    
    n = rand(1,num_bits)<.5;
    %n = [0 0 0 1 1 0 0 0 1 1 0 0]
    graf1 = 0;
    
    for t=1:2:length(n)-1
        if((n(t) == 0) && (n(t+1) == 0)) 
            graf1 = [graf1 sig_00];
        end
        if((n(t) == 0) && (n(t+1) == 1)) 
            graf1 = [graf1 sig_01];
        end
        if((n(t) == 1) && (n(t+1) == 0)) 
            graf1 = [graf1 sig_10];
        end
        if((n(t) == 1) && (n(t+1) == 1)) 
            graf1 = [graf1 sig_11];
        end
    end
    
    t_array = 0:0.01:(1*length(n)/2);
    
    f3=figure;
    plot(t_array,graf1);
    title('Graphs s(i)');
    grid on
%-------------------------------------------------------------------------
%lab2_2
    SNRdb = -25 : 1 : 0;
    %SNRdb = 1;
    Perr = 1:length(SNRdb);
    
    for k = 1 : length(SNRdb)
        SNR = 10^(SNRdb(k)/10);
        Psignal = sum(graf1.^2)/length(graf1);
        Pnoise = Psignal/SNR;
        AWGN = randn(1, length(t_array))*sqrt(Pnoise);
        sig_awgn = graf1+AWGN;
        Slr1 = [-2 -1 1 2];
        Slr2 = [0 0 0 0];
        
        if(k == 1)
            f4=figure;
            plot(t_array ,AWGN);
            title('Graphs AWGN');
            grid on
        
            f5=figure;
            plot(t_array,sig_awgn);
            title('Graphs sig (awgn)');
            grid on
    
            x = 0 : length(xcorr(AWGN))-1;
            f6=figure;
            plot(x,xcorr(AWGN));
            title('Correlation fun');
            grid on
%-------------------------------------------------------------------------
%lab2_3
            f7 =figure;
            %scatter(Slr1,Slr2,[],[1 6 4 5]);
            hold on
        end
        for i = 1 : (length(graf1)-1)/100
            Slr1(i) = (sum(phi0.*sig_awgn(((i-1)*100)+1:i*100)))/100;
            Slr2(i) = 0;
        end
        if( k == 1)
           %scatter(Slr1,Slr2);
        end
        j = 1;
        Erro = 0;
        
        for i = 1 : (length(graf1)-1)/100
            Slr(j) = (sum(sig_00.*sig_awgn(((i-1)*100)+1:i*100)))/100 - 2;
            Slr(j+1) = (sum(sig_01.*sig_awgn(((i-1)*100)+1:i*100)))/100 - 0.5;
            Slr(j+2) = (sum(sig_10.*sig_awgn(((i-1)*100)+1:i*100)))/100 - 0.5;
            Slr(j+3) = (sum(sig_11.*sig_awgn(((i-1)*100)+1:i*100)))/100 - 2;
            sig_dots(i) = max(Slr);
            
            if(sig_dots(i) == Slr(1))
                    sig_dots_x(i) = -2;
                    sig_dots_y(i) = 0;
                    scatter(Slr1(i),Slr2(i), 'r');
                    if(n(i*2-1) ~= 0 && n(i*2) ~= 0) Erro = Erro + 1; end
                end
                if(sig_dots(i) == Slr(2))
                    sig_dots_x(i) = -1;
                    sig_dots_y(i) = 0;
                    scatter(Slr1(i),Slr2(i), 'g');
                    if(n(i*2-1) ~= 0 && n(i*2) ~= 1) Erro = Erro + 1; end
                end
                if(sig_dots(i) == Slr(3))
                    sig_dots_x(i) = 1;
                    sig_dots_y(i) = 0;
                    scatter(Slr1(i),Slr2(i), 'c');
                    if(n(i*2-1) ~= 1 && n(i*2) ~= 0) Erro = Erro + 1; end
                end
                if(sig_dots(i) == Slr(4))
                    sig_dots_x(i) = 2;
                    sig_dots_y(i) = 0;
                    scatter(Slr1(i),Slr2(i), 'b');
                    if(n(i*2-1) ~= 1 && n(i*2) ~= 1) Erro = Erro + 1; end
                end
            
        end
        Perr(k) = Erro/length(n)/2;
 %       if(k == 1)
  %          hold off
 %           f8 = figure;
 %           scatter(sig_dots_x,sig_dots_y);
 %       end
    end
    f9=figure;
    semilogy(SNRdb,Perr);
    title('Graphs Perr');
    grid on
    
    tmpG = 0;
    t_array = 0:0.01:(1*length(n)/2);

    for t = 1 : (length(graf1)-1)/100
        if((sig_dots_x(t) == -2) && (sig_dots_y(t) == 0)) 
            tmpG = [tmpG sig_00];
        end
        if((sig_dots_x(t) == -1) && (sig_dots_y(t) == 0)) 
            tmpG = [tmpG sig_01];
        end
        if((sig_dots_x(t) == 1) && (sig_dots_y(t) == 0)) 
            tmpG = [tmpG sig_10];
        end
        if((sig_dots_x(t) == 2) && (sig_dots_y(t) == 0)) 
            tmpG = [tmpG sig_11];
        end
    end
    
    f10=figure;
    plot(t_array,tmpG);
    title('Graphs s(i)');
    grid on
end