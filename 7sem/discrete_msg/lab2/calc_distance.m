function [ dist ] = calc_distance( F1, F2 )

if ( size( F1' ) ~= size( F2' ) )
    error( 'F1 != F2' );
end

dist = 0;

for i = 1 : size( F1' )
   dist = dist + ( F1(i)  - F2(i) )^2;
end

dist = sqrt( dist );

end

