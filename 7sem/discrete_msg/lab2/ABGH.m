function [ noise, corel_func, signal_with_noise ] = ABGH( SNR_db, signal )

noise = randn( size(signal, 1), 1 );
SNR = 10 ^ ( SNR_db / 10 );

P_signal = 0;
for i = 1 : size( signal, 1 )
    P_signal = P_signal + (signal( i ) ^ 2);
end
P_signal = P_signal / size(signal, 1);

P_noise = P_signal / SNR;

P_noise_sqrt = sqrt( P_noise );
for i = 1 : size( noise, 1 )
        noise(i, 1) = noise(i, 1) * P_noise_sqrt;
end

Test_SNR = snr( signal, noise );
disp( 'Test_SNR:' );
disp( Test_SNR );

corel_func = xcorr( noise );

signal_with_noise = [];
for i = 1 : size( signal, 1 )
    signal_with_noise = [ signal_with_noise (noise( i ) + signal( i )) ];
end

end

