
while 1
    menu_item = menu( 'Menu', 'Input value',  'Test on harmonic basis', 'Show plot', 'Calc E, Norm, Dist', 'Generate signal', 'ABGH', 'Correlation receiver', 'Probability Err', 'Exit');
    
    switch menu_item
        case 1 
            T = input('T: ');
            Cnt_Point = input( 'Cnt_Point: ' );
            
            Func = zeros( 7, Cnt_Point );
            
            for i = 1 : 7
                Func( i, :) = calc_vector_func( i, T, Cnt_Point );
            end
            
        case 2
            Scal_matrix = zeros( 7, 7 );
            flag = 0;
            for i = 1 : 7
                for j = i : 7 
                    scal = scalar( Func( i, : ), Func( j, : ) );
                    Scal_matrix(i, j) = scal;
                    Scal_matrix(j, i) = scal;
                    
                    if ( flag == 0 )
                        if ( ( ( abs(scal) >= (10^-10) ) && ( i ~= j ) ) || ( ( (abs(scal) - 1) > 10^-10  ) && ( i == j ) )  )
                            disp( 'Not harmonic basis' );
                            flag = 1;       
                        end
                    end
                end
            end
            
            if (flag == 0)
                disp( 'Harmonic basis' );
            end
                    
            disp( 'Scalar_matrix: ');
            disp( Scal_matrix );
            
        case 3
           
            for i = 1 : 7
                subplot(8, 1, i);
                grid;
                plot( T / Cnt_Point : T / Cnt_Point : T, Func( i, : ) );
            end
            
            subplot(8, 1, 8);
            grid;
            hold on;
            
            color = ['b';'g';'r';'c';'m';'y';'k']; 
            for i = 1 : 7
                plot( T / Cnt_Point : T / Cnt_Point : T, Func( i, : ), color( i ) );
            end
            hold off;
            
        case 4
            Dist_matrix = zeros( 7, 7 );
            E_matrix = zeros( 7, 1 );
            Norm_matrix = zeros( 7, 1 );
            for i = 1 : 7
                for j = i : 7 
                    Dist = calc_distance( Func( i, : ), Func( j, : ) ); 
                    Dist_matrix( i, j ) = Dist;
                    Dist_matrix( j, i ) = Dist;
                end
                
                [ E_matrix( i ), Norm_matrix( i ) ] = calc_E_norma( Func( i, : ) );
            end
            
            disp( 'Distance: ');
            disp( Dist_matrix );
            
            disp( 'E: ' );
            disp( E_matrix );
            
            disp( 'Norma: ' );
            disp( Norm_matrix );
            
        case 5
            information = input( 'Inform msg: ');
            code_signal = [];
            if ( mod( size( information' ), 2)  ==  1 )
                information = [information 0];
            end
            
            
            
            for i = 1 : 2 : size( information' )
                if ( information( i:i+1 ) == [0 0] )
                    koef = -2;
                elseif ( information( i:i+1 ) == [0 1] )
                    koef = -1;
                elseif ( information( i:i+1 ) == [1 0] )
                    koef = 1;
                elseif ( information( i:i+1 ) == [1 1] )
                    koef = 2;
                end
             
                code_signal = [ code_signal;  zeros( Cnt_Point, 1 ) ];
                for j = 1 : Cnt_Point
                    code_signal( j + fix(i/2) * Cnt_Point ) = koef * Func(1, j );
                end
                
                
            end
            
            subplot( 1, 1, 1 );
            grid;
            plot( T / Cnt_Point : T / Cnt_Point : T * size( information' ) / 2, code_signal' );
            
            
        case 6
            % ABGH generate
            SNR_db = input( 'Input SNR (in dB): ' );
            [noise, corel_func, signal_with_noise ] = ABGH( SNR_db, code_signal );
            
            subplot( 4, 1, 1 );
            grid;
            plot( T / Cnt_Point : T / Cnt_Point : T * size( information' ) / 2, code_signal' );
            
            
            subplot( 4, 1, 2 );
            grid;
            plot( T / Cnt_Point : T / Cnt_Point : T * size( information' ) / 2, noise' );
            
            subplot( 4, 1, 3 );
            grid;
            plot( T / Cnt_Point : T / Cnt_Point : T * size( information' ) / 2, signal_with_noise' );
            
            subplot( 4, 1, 4 );
            grid;
            plot( 1 : size( corel_func, 1 ), corel_func' );
            
        case 7
            % generate all signals
            all_signals = zeros( 4, Cnt_Point );
            all_signals( 1, : ) = -2 * Func(1, : );
            all_signals( 2, : ) = -1 * Func(1, : );
            all_signals( 3, : ) = 1 * Func(1, : );
            all_signals( 4, : ) = 2 * Func(1, : );
            
            
            % Correlation receiver
            cnt_receive_points = size(signal_with_noise', 1) /  Cnt_Point;
            rec_inf = zeros( cnt_receive_points * 2, 1 ) ;
            
            signal_point_rec = zeros( 1, cnt_receive_points );
            col_points = [];
            
            for i = 1 : 2 : cnt_receive_points * 2
                signal_point_rec( 1 + (i/2-0.5) ) = sum( Func( 1, : ).* signal_with_noise( 1 + (i/2-0.5) * Cnt_Point : Cnt_Point * (i/2+0.5)));
                
                switch Correlation_Receiver( signal_with_noise( 1 + (i/2-0.5) * Cnt_Point : Cnt_Point * (i/2+0.5) ), all_signals  )
                    case 1
                        rec_inf( i ) = 0;
                        rec_inf( i + 1 ) = 0;
                        col_points = [col_points, 'r' ];
                        
                    case 2
                        rec_inf( i ) = 0;
                        rec_inf( i + 1 ) = 1;
                        col_points = [col_points, 'g' ];
                        
                    case 3 
                        rec_inf( i ) = 1;
                        rec_inf( i + 1 ) = 0;
                        col_points = [col_points, 'b' ];
                        
                    case 4
                        rec_inf( i ) = 1;
                        rec_inf( i + 1 ) = 1;
                        col_points = [col_points, 'c' ];
                        
                end
            end
            
            disp( 'Receive msg: ' );    
            disp( rec_inf );
            
            % disp graphics points
            hold on;
            x = [-2, -1, 1, 2, signal_point_rec ];
            y = zeros( 1, 4 + cnt_receive_points );
            c = ['r', 'g', 'b', 'c', col_points ];
            for i = 1 : 4 + cnt_receive_points
                scatter( x(i), y( i ), c( i ) );
            end
            hold off;
            
        
        case 8
            % probab Err
            
            % generate all signals
            all_signals = zeros( 4, Cnt_Point );
            all_signals( 1, : ) = -2 * Func(1, : );
            all_signals( 2, : ) = -1 * Func(1, : );
            all_signals( 3, : ) = 1 * Func(1, : );
            all_signals( 4, : ) = 2 * Func(1, : );
            
            Diap_SNR = -35 : 1 : -1;
            
            Test_signal = code_signal; 
            
            P_err = zeros( size( Diap_SNR, 2 ) , 1 );
            
            Cnt_Tests = 50;
            
            for i = 1 : size( Diap_SNR, 2 )
                for j = 1 : Cnt_Tests %cnt experiments
                    % generate noise
                    [T_noise, T_corel_func, T_signal_with_noise ] = ABGH( Diap_SNR(i), Test_signal );
                    
                    % decode signal         
                    % Correlation receiver
                    cnt_receive_points = size(T_signal_with_noise', 1) /  Cnt_Point;
                    rec_inf = zeros( cnt_receive_points * 2, 1 ) ;
            
                    signal_point_rec = zeros( 1, cnt_receive_points );
                    col_points = [];
            
                    for k = 1 : 2 : cnt_receive_points * 2
                        signal_point_rec( 1 + (k/2-0.5) ) = sum( Func( 1, : ).* T_signal_with_noise( 1 + (k/2-0.5) * Cnt_Point : Cnt_Point * (k/2+0.5)));
                
                        switch Correlation_Receiver( T_signal_with_noise( 1 + (k/2-0.5) * Cnt_Point : Cnt_Point * (k/2+0.5) ), all_signals  )
                            case 1
                                rec_inf( k ) = 0;
                                rec_inf( k + 1 ) = 0;

                            case 2
                                rec_inf( k ) = 0;
                                rec_inf( k + 1 ) = 1;
                              
                            case 3 
                                rec_inf( k ) = 1;
                                rec_inf( k + 1 ) = 0;
                               
                            case 4
                                rec_inf( k ) = 1;
                                rec_inf( k + 1 ) = 1;
                                
                        end
                    end
                    
                    % compare trnsmite and receive data
                    for k = 1 : 2 : size( rec_inf, 1 )
                        if (( rec_inf( k ) == information( k )) && ( ( rec_inf( k + 1) == information( k + 1 ) ) ) )
                            continue;
                        end
                        P_err( i ) = P_err( i ) + 1;
                    end       
                end
                
                P_err( i ) = P_err( i ) / (Cnt_Tests * size( rec_inf, 1 ) / 2 );
            end
            
            subplot( 1, 1, 1 );
            grid;
            semilogy( Diap_SNR, P_err );
            
        case 9 
            break;
            
    end
end