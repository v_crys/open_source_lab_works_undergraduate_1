function [ inf ] = Correlation_Receiver( Input_sig, sig_points )

Cnt_points = size( sig_points, 1 );

% calc C
C = zeros( Cnt_points, 1 );
for i = 1 : Cnt_points
    [C( i ), buf] = calc_E_norma( sig_points( i, : ) );
    C( i ) = C( i ) / 2;
end


% calc correlaton

cor_value = zeros( Cnt_points, 1 );

max = -9999999999999;
cnt_max = 0;
for i = 1 : Cnt_points
    cor_value( i ) = Correlator( Input_sig, sig_points( i, : )) - C( i );
    
    % find max
    if ( max < cor_value( i ) )
        max = cor_value( i );
        cnt_max = i;
    end
end


inf = cnt_max;

end

