function [ out ] = Correlator( Sig_Noise, Signal )
% Sig_Noise = r = s + n (s - signal, n - noise)
out = 0;

for i = 1 : size( Signal' )
    out = out + (Sig_Noise(i) * Signal(i));
end

end

