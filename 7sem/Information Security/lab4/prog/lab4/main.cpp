#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>



struct Sig_struct
{
    // a, b - signature pair
    long a;
    long b;
    long msg; // hash from msg
};
typedef struct Sig_struct Sig_struct;

class Signature
{
    // var
    long X; // close key
    long Y; // open key

    long P; // prime
    long G; // primitive root

    // for demonstration
    long K; // secret K (delete after use)
    long G_M; // G^M
    long Y_a; // Y^a
    long a_b; // a^b
    long Y_a__a_b; // Y^a * a^b

    // math function
    long anti_log_small( long a, long pow_, long mod_ );
    long anti_log( long a, long pow_, long mod_ ); // function for calculate: (a ^ pow_) mod mod_
    long gcd (long a, long b, long * x, long * y); // Extended Euclide

    long calc_comp( long a, long b, long mod ); // calculation compare: ax = m (mod n)

    long calc_open_key(); // generate open key Y

public:
    Signature( long P_, long G_, long X_ ) : P(P_), G(G_), X(X_)  {  Y = calc_open_key(); }

    void calc( long msg, Sig_struct *sig ); //calculation signature for msg
    int verify( Sig_struct sig ); // verification of signature /return 1 - if valid or 0 - if inalid

    long get_X() { return X; }
    long get_Y() { return Y; }
    long get_P() { return P; }
    long get_G() { return G; }

    long get_K() { return K; }
    long get_G_M() { return G_M; }
    long get_Y_a() { return Y_a; }
    long get_a_b() { return a_b; }
    long get_Y_a__a_b() { return Y_a__a_b; }
};

int factorization(long n, long *a );

int main()
{
    int buf;
    Signature sig( 3559	, 11, 1001);
    Sig_struct my_sig[ 10 ];

    srand( (unsigned) time( NULL ) );

    for ( int i = 0; i < 5 ; i++)
        my_sig[ i ].msg = rand() % (sig.get_P() - 1);


    printf( "P: %d\tG: %d\tX: %d\tY: %d\n\n", sig.get_P(), sig.get_G(), sig.get_X(), sig.get_Y() );

    for ( int i = 0; i < 5; i++ )
    {
        sig.calc( my_sig[ i ].msg, &(my_sig[ i ]) );
        buf = sig.verify( my_sig[ i ] );
        printf( "M: %d\tK: %d\ta: %d\tb: %d\tG^M: %d\tY^a: %d\ta^b: %d\tY^a*a^b: %d", my_sig[ i ].msg, sig.get_K(), my_sig[ i ].a, my_sig[ i ].b, sig.get_G_M(), sig.get_Y_a(), sig.get_a_b(), sig.get_Y_a__a_b() );
        printf( "\tVerify: %d\n", buf );
    }

    return 0;
}

int Signature::verify( Sig_struct sig )
{

    //buf1 = (long)(pow( Y, sig.a ) * pow(sig.a, sig.b)) % P;
    Y_a = anti_log( Y, sig.a, P );
    a_b = anti_log( sig.a, sig.b, P );

    Y_a__a_b = (Y_a * a_b) % P;

    G_M = anti_log( G, sig.msg, P );

    return G_M == Y_a__a_b;
}

long Signature::calc_open_key( )
{
    return anti_log( G, X, P );
}

long Signature::calc_comp( long a, long b, long mod )
{
    long buf1 = 0, buf2 = 0;
    long d = gcd( a, b, &buf1, &buf2 ) ;
    long a_buf, b_buf, n_buf;

    a_buf = a / d;
    b_buf = b / d;
    n_buf = mod / d;

    gcd( a_buf, n_buf, &buf1, &buf2 );

    return (b_buf * buf1) % n_buf;

}

void Signature::calc( long msg, Sig_struct *sig)
{
    K = 0;

    while( gcd( K, P - 1, &(sig->a), &(sig->b) ) != 1 )
        K = rand() % (P - 1);

    sig->msg = msg;
    sig->a = anti_log( G, K, P );
    sig->b = calc_comp( K, (msg - X * (sig->a)), P - 1 );
    if ( sig->b < 0 ) sig->b += P - 1;
}

/*
long Signature::anti_log( long a, long pow_, long mod_ )
{
    //return (long)pow( a, pow_ ) % mod_;
    if (pow_ == 0) return 1;
    long z = anti_log(a, pow_ / 2, mod_);
    if (pow_ % 2 == 0)
      return (z*z) % mod_;
    else
      return (a*z*z) % mod_;
}
*/

long Signature::anti_log( long a, long pow_, long mod_)
{
   anti_log_small( a, pow_, mod_ );
}

long Signature::anti_log_small( long a, long pow_, long mod_ )
{
    long t;
    long d;
    long t_new;
    long d_new;
    int i;

    d = 1;
    t = a;

    for ( i = 0; i < sizeof(pow_)*8; i++)
    {
        t_new = (t * t) % mod_;

        if ( (pow_ >> i) & 1 )
        {
            d_new = (d * t) % mod_;
        } else {
            d_new = d;
        }

        d = d_new;
        t = t_new;
    }
    return d;
}


long Signature::gcd (long a, long b, long * x, long * y)
{
    if (a == 0) {
        *x = 0; *y = 1;
        return b;
    }
    long x1, y1;
    long d = gcd (b%a, a, &x1, &y1);
    *x = y1 - (b / a) * x1;
    *y = x1;
    return (d < 0) ? d * -1 : d;
}

