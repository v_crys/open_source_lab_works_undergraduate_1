#include <stdio.h>
#include <string.h>

#define ROUNDS 16

unsigned char F( unsigned char L, unsigned char K );
void Raund_Feistel( unsigned char L_in, unsigned char R_in, unsigned char K, unsigned char *L_out, unsigned char *R_out);

void Coding( unsigned char *msg, unsigned char msg_len, unsigned char *key, unsigned char *out, unsigned char *out_len );

void Gen_DecKey( unsigned char *Key, unsigned char cnt_rounds, unsigned char *Dec_Key );

int main()
{
    int i, j;
    unsigned char enc_value[ 100 ];
    unsigned char dec_value[ 100 ];

    unsigned char MSG[] = "Who does not work shall not eat.";
    unsigned char KEY[] = "eight";
    unsigned char DEC_KEY[100];

    Gen_DecKey( KEY, ROUNDS, DEC_KEY );

    unsigned char enc_value_len;
    unsigned char dec_value_len;

    Coding( MSG, strlen( MSG ), KEY, enc_value, &enc_value_len );

    Coding( enc_value, enc_value_len, DEC_KEY, dec_value, &dec_value_len );

    printf("Msg: %s\nKey: %s\nDec_Key: %s\n\n", MSG, KEY, DEC_KEY );

    printf( "Mgs in 16: " );
    for (i = 0; i < strlen( MSG ); i++ )
        printf( "%x ", MSG[ i ] );

    printf( "\n\nKey in 16: " );
    for (i = 0; i < strlen( KEY ); i++ )
        printf( "%x ", KEY[ i ] );

    printf( "\n\nDec_key in 16: " );
    for (i = 0; i < strlen( DEC_KEY ); i++ )
        printf( "%x ", DEC_KEY[ i ] );

    printf( "\n\nEncoding value: " );

    for ( i = 0; i < enc_value_len; i++ )
        putchar( enc_value[ i ] );

    printf( "\n\nEncoding value in 16: ");
    for (i = 0; i < enc_value_len ; i++ )
        printf( "%x ", enc_value[ i ] );


    printf( "\n\nDecoding value: ");

    for ( i = 0; i < dec_value_len; i++ )
        putchar( dec_value[ i ] );

    printf( "\n\nDecoding value in 16: " );
    for ( i = 0; i < dec_value_len; i++ )
        printf( "%x ", dec_value[ i ] );

    printf( "\n" );

    /// For demonstration
    printf( "\n\n\nDemonstration\n" );

    unsigned char   L_local,
                    R_local;

    L_local = MSG[ 0 ];
    R_local = MSG[ 1 ];

    printf( "id\tKey \tObraz F\tLeft \tRight\n");
     printf( "\t\t\t%X %c\t%X %c\n", L_local, L_local, R_local, R_local );
    // coding rounds
    for ( j = 0; j < ROUNDS; j++ )
    {
        Raund_Feistel( L_local, R_local, KEY[ j % strlen( KEY ) ], &L_local, &R_local );
        printf( "%d\t%c \t%X \t%X %c\t%X %c\n" , j, KEY[ j % strlen( KEY ) ], (L_local^KEY[ j % strlen( KEY ) ]), L_local, L_local, R_local, R_local );
    }

    j = L_local;
    L_local = R_local;
    R_local = j;

    printf( "Res\t\t\t%d %c\t%d %c\n", L_local, L_local, R_local, R_local );

    printf( "\n\n\nDecoding\n" );

    printf( "id\tKey \tObraz F\tLeft \tRight\n");
        printf( "Res\t\t\t%d %c\t%d %c\n", L_local, L_local, R_local, R_local );
    // coding rounds
    for ( j = 0; j < ROUNDS; j++ )
    {
        Raund_Feistel( L_local, R_local, DEC_KEY[ j % strlen( DEC_KEY ) ], &L_local, &R_local );
        printf( "%d\t%c \t%X \t%X %c\t%X %c\n", j, KEY[ j % strlen( KEY ) ], (L_local^KEY[ j % strlen( KEY ) ]), L_local, L_local, R_local, R_local );
    }

    j = L_local;
    L_local = R_local;
    R_local = j;

    printf( "Res\t\t\t%d %c\t%d %c\n", L_local, L_local, R_local, R_local );

    return 0;
}

/**
 * @brief F - Generated function
 * @param L - parametr
 * @param K - key
 * @return
 */
unsigned char F( unsigned char L, unsigned char K )
{
    unsigned char K_buf = K & 0x7;

    return (L << K_buf) | ( L >> (8 - K_buf) );
}

/**
 * @brief Raund_Feistel - function to calculate one round
 * @param L_in
 * @param R_in
 * @param K - Key
 * @param L_out
 * @param R_out
 */
void Raund_Feistel( unsigned char L_in, unsigned char R_in, unsigned char K, unsigned char *L_out, unsigned char *R_out)
{
    *L_out = F( L_in, K ) ^ R_in;
    *R_out = L_in;
}

/**
 * @brief Coding - function for coding Feistel alghoritm
 * @param msg   - input msg
 * @param msg_len - len msg
 * @param key   - key
 * @param out   - output msg
 * @param out_len - length output msg
 */
void Coding( unsigned char *msg, unsigned char msg_len, unsigned char *key, unsigned char *out, unsigned char *out_len )
{
    int i, j;
    unsigned char   L_local,
                    R_local;

    const unsigned int K_len = strlen( key );

    for ( i = 0; ; i+=2 )
    {
        // Coding pair
        if ( i >= msg_len )
        {
            out[ i ] = 0;
            *out_len = i;
            break;
        }

        L_local = msg[ i ];
        R_local = msg[ i + 1 ];

        // coding rounds
        for ( j = 0; j < ROUNDS; j++ )
            Raund_Feistel( L_local, R_local, key[ j % K_len ], &L_local, &R_local );

        // save encoders value (with swap)
        out[ i + 1 ] = L_local;
        out[ i ] = R_local;


    }
}

/**
 * @brief Gen_DecKey - generate key for decoding
 * @param Key - key used in encoding
 * @param cnt_rounds - Number of rounds
 * @param Dec_Key - Returned value (Key for decoding)
 */
void Gen_DecKey( unsigned char *Key, unsigned char cnt_rounds, unsigned char *Dec_Key )
{
    int i;
    int key_len = strlen( Key );
    for ( i = 0; i < key_len; i++ )
        Dec_Key[ key_len - i  -1 ] = Key[ ((i + cnt_rounds) % key_len) ];
    Dec_Key[ i ] = 0;
}
