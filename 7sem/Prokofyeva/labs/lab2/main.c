#include <stdio.h>

struct CPUID
{
	unsigned int EAX;
	unsigned int EBX;
	unsigned int ECX;
	unsigned int EDX;
} typedef CPUID;

CPUID get_CPUID( unsigned int cmd)
{
	CPUID cpu;

	asm(
		"movl %1, %%eax\n\r" 
		"cpuid\n\r" 
		"movl %%eax, %0\n\r" 
		: "=r"(cpu.EAX) 
		: "r"(cmd)
	);

	asm(
		"movl %%ebx, %0\n\r"
		: "=r"(cpu.EBX)
	);

	asm(
		"movl %%ecx, %0\n\r"
		: "=r"(cpu.ECX)
	);

	asm(
		"movl %%edx, %0\n\r"
		: "=r"(cpu.EDX)
	);


	return cpu;
}

int main( void )
{
	CPUID cpu;
	unsigned int cmd;

	while( 1 )
	{
		printf( "input cmd: " );
		scanf( "%x", &cmd );

		if ( cmd == -1 )
			break;

		cpu = get_CPUID(cmd);

		printf( "EAX = %x\nEBX = %x\nECX = %x\nEDX = %x\n", cpu.EAX, cpu.EBX, cpu.ECX, cpu.EDX );
	}
	return 0;
}
