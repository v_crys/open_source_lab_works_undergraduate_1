#include <stdio.h> 
#include <string.h> 
#include <stdlib.h>
#include <unistd.h>


int Daemon(void); 

int main(int argc, char* argv[])
{
  pid_t parpid;
  if((parpid=fork())<0) 
  {                  
    printf("\ncan't fork"); 
    exit(1);               
  }
  else if (parpid!=0) 
  {
    printf( "Demon pid: %d\n", parpid );
    exit(0);            
  }
          
  setsid();          
  Daemon();          
      
  return 0;
}


int Daemon()
{
  printf( "Deamon start\n" );

  close(0);
  close(1);
  close(2);

  while(1)
  {

  }

}