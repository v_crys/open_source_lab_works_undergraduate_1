#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>  
#include <string.h>

#define SIZE 256  

int main (int argc, char *argv[]) {
    int mypipe[2];
    pipe(mypipe);

    switch(fork()) {
        case -1: /* error */
            perror("fork");
            exit(EXIT_FAILURE);
        case 0: /* child process */
            dup2( mypipe[ 0 ], 0 );
            dup2(mypipe[1], 1); /* stdout to pipe out */
            while(1){
            sleep( 1 );  
            printf( "t");}
            //execlp(".\\test", NULL);

            //close(mypipe[1]);
            //close(mypipe[0]);
            _exit(EXIT_SUCCESS);
        }  

    /* parent process */
    char buf[SIZE] = "";
    int n;
    while(1)
    {

        //sleep( 1 );
        n = read(mypipe[0], buf, 1); /* read from pipe in */
        printf("Child output [%d]:\n%s\n", n, buf);
/*
        sleep(1);
        buf[ 0 ] = '1';
        buf[ 1 ] = '2';
        buf[3] = 0;
        printf( "child in: %s\n", buf) ;
        write( mypipe[ 1 ], buf, 3 );*/
  
	//	scanf( "%s", buf );
	//	write( mypipe_in[ 1], buf, strlen(buf) );
	}
    close(mypipe[0]);
    close(mypipe[1]);

    return EXIT_SUCCESS;
}