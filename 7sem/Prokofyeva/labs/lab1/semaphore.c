#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/socket.h>

int main( int argc, char *argv[] )
{
	key_t key = ftok("./", 'a');
	int semid;
	/* Пытаемся получить доступ по ключу к массиву семафоров, если он
	существует, или создать его из одного семафора, если он ещё не существует, с
	правами доступа read & write для всех пользователей */

	if((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0){
		printf("Can\'t get semid\n");
		exit(-1);
	}

	//Инициализация структуры:
	struct sembuf sem_lock = { 0, -1, IPC_NOWAIT };

	/*Трансляция структуры sem_lock добавит -1 к семафору 0 из множества
	семафоров. Т.е., одна единица ресурсов будет получена от конкретного (нулевого)
	семафора из множества. IPC_NOWAIT установлен, поэтому либо вызов пройдёт
	немедленно, либо будет провален, если ресурс занят. Рассмотрим пример
	инициализации sembuf вызовом semop (D(semid,1) для нашего массива семафоров):
	*/

	if(semop(semid, &sem_lock, 1) == 1)
		perror("semop2");

	/*Третий аргумент (nsops) говорит, что мы выполняем только одну (1) операцию
	(есть только одна структура sembuf в нашем массиве операций).27
	semid является IPC идентификатором для нашего множества семафоров.
	Пример операции А(semid,1) для нашего массива семафоров:
	*/

/*
	struct sembuf sem_lock = { 0, 1, IPC_NOWAIT };

	if(semop(semid, &sem_lock, 1) == -1)
		perror("semop");
*/
	return 0;
}