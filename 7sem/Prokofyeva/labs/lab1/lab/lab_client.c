#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>

void send_str( char * str, char * out );

int main() 
{
	char buf[ 100 ];
	char result[ 100000 ];
	char line_params[ 100000 ];
	int cnt_params;

	line_params[ 0 ] = 0;

	printf( "Input cnt params: " );
	scanf( "%d", &cnt_params );

	printf( "Input params for program: " );

	for ( int i = 0; i < cnt_params; i++ )
	{
		scanf( "%s", buf );
		strcat( line_params, buf );
		strcat( line_params, " " );
	}

	printf( "Send str: %s\n", line_params );

	send_str( line_params, result );

	printf( "Receive answer:\n %s\n", result );

	return 0;
}

void send_str( char * str, char * out )
{
	int sockfd;
	struct sockaddr_in address;
	int len_adr;
	

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if ( sockfd < 0 )
	{
		perror("socket");
        exit(1);
	}

	address.sin_family = AF_INET;
	address.sin_port = htons( 9999 );
	address.sin_addr.s_addr = htonl( INADDR_LOOPBACK );

	/*if (inet_aton(INADDR_LOOPBACK, &address.sin_addr)==0) 
	{
		perror( "inet_aton() failed\n" );
		exit(1);
	}*/

	if(sendto(sockfd, str, strlen( str )+1, 0,(struct sockaddr *) &address, sizeof( address )) < 0)
	{
        printf(  "sendto error\n" );
	    exit(0);
    }

    if(recvfrom( sockfd, out, 99999, 0,(struct sockaddr *) &address, &len_adr) < 0)
	{
	    printf( "recvfrom error\n" );
	    
    }
    close(sockfd);
}