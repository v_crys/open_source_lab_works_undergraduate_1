#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>

/*
 *	Problem with first run. Data send with error only first connect.
 */

struct SETTINGS
{
	int type;
	int port;
	char path[ 256 ];
} typedef SETTINGS;


int Daemon(void); 
void run_exec( char *path, char ** in, char * out );
void Gen_answer( char *path, char * in, char * out );
void line_to_array( char *path, char *in, char *** out, int *len );
void close_deamon();
void handler(int signum);
void unparse_settings( SETTINGS *s );

FILE * log;
FILE * f_SET;
int server_sockfd;

#define SIZE_BUF 100000

int main(int argc, char* argv[])
{
  	pid_t parpid;

  	if ( argc != 2 )
  	{
  		printf( "Input param not path\n" );
  		exit( 0 );
  	}
  	
  	f_SET = fopen( argv[ 1 ], "rt" );
  	if ( f_SET <= 0 )
  	{
  		printf( "Error open settings file\n" );
  		exit(0);
  	}

  	if( (parpid = fork()) < 0 ) 
  	{                  
    	printf("\ncan't fork"); 
    	exit(1);               
  	}
  	else if (parpid!=0) 
  	{
    	printf( "Demon pid: %d\n", parpid );
	    exit(0);            
  	}
          
  	setsid();    

  	printf( "Deamon start\n" );
  	log = fopen( "log.txt", "wt" );

/// close standart threads
  	close(0);
  	close(1);
  	close(2);

/// signal HUP perehvat
	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_handler = handler;
	sigset_t   set; 
	sigemptyset(&set);                                                             
	sigaddset(&set, SIGUSR1); 
	act.sa_mask = set;
	sigaction(SIGUSR1, &act, 0);



  	Daemon();          
      
  	return 0;
}

void unparse_settings( SETTINGS *s  )
{
	char buf[ 100 ];

	fscanf( f_SET, "%s", buf );

	if ( strcmp( buf, "UDP" ) == 0 )
		s->type = 0;
	else if ( strcmp( buf, "SEM" ) == 0 )
		s->type = 1;
	else
	{
		fprintf( log, "Not correct format settings file\n" );
		exit( 0 );
	}

	fscanf( f_SET, "%d", &(s->port) );

	fscanf( f_SET, "%s", s->path );

}

void close_deamon()
{
	fclose( log );
	close( server_sockfd );
	exit(0);
}

/// kill -USR1 pid
void handler(int signum)
{
	fprintf( log, "Signal USR1 - Exit\n");
	close_deamon();
}

int Daemon()
{
	SETTINGS sett;
	unparse_settings( &sett );
  	// main deamon function
  	while(1)
  	{
  		char buf[ 100 ];
  		char arr[ SIZE_BUF ];
  		char answer[ SIZE_BUF ];
  		int len;
  		int len_adr, n;
  		int cnt_request = 0;
  		
		struct sockaddr_in server_address, client_address;


		//unlink("server_socket");
		server_sockfd = socket(AF_INET, SOCK_DGRAM, 0); //< UDP use 
		if ( server_sockfd < 0 )
		{
			fprintf( log, "create socket\n");
			close_deamon();
		}

		server_address.sin_family = AF_INET;
		server_address.sin_port = htons( sett.port );
		server_address.sin_addr.s_addr = htonl( INADDR_ANY );

		if ( bind(server_sockfd, (struct sockaddr *)&server_address, sizeof(server_address)) < 0 )
		{
			fprintf( log, "error bind\n" );
			close_deamon();
		}

		fprintf( log, "Binding OK\n" );


		while(1) 
		{
			if((n = recvfrom( server_sockfd, arr, SIZE_BUF - 1, 0,(struct sockaddr *) &client_address, &len_adr)) < 0)
			{
	            fprintf( log, "recvfrom error\n" );
	            //close_deamon();
        	}

        	inet_ntop( AF_INET, &(client_address.sin_addr), buf, INET_ADDRSTRLEN );
        	fprintf( log, "Address: %s\n", buf );
        	fprintf( log, "Receive text [%d byte]: \n", n);
        	fprintf( log, "%s\n", arr);
			fprintf( log, "\n");

			for ( int i = 0; i < SIZE_BUF; i++ )
				answer[ i ] = 0;

			Gen_answer( sett.path, arr, answer );
			fprintf( log, "Answer prog: \n%s\n", answer );


			if(sendto(server_sockfd, answer, strlen( answer )+1, 0,(struct sockaddr *) &client_address, sizeof( client_address )) < 0)
			{
          	 	fprintf( log, "sendto error\n" );
	            //close_deamon();
        	}
		}

  	}

}

void line_to_array( char *path, char *in, char *** out, int *len )
{
	int i, j, k;
	int flag = 0;
	int len_in = strlen( in );

	*len = 0;
	for ( i = 0; i < len_in; i++ )
	{
		if ( ( in[ i ] == ' ' ) || ( in[ i ] == '\t' ) || ( i == (len_in - 1) )  )
		{
			if ( flag == 1 )
				continue;
		
			(*len)++;
			flag = 1;
		} else
			flag = 0;
	}


	*out = (char **) calloc( (*len) + 2, sizeof( char * ) );
	for ( i = 0; i < (*len)+1; i++ )
		(*out)[ i ] = (char *) calloc( 10, sizeof( char ) );
	(*out)[ (*len)+1 ] = NULL;

	strcpy( (*out)[0], path );

	j = 1;
	k = 0;
	for ( i = 0; i < len_in+1; i++ )
	{
		if ( ( in[ i ] == ' ' ) || ( in[ i ] == '\t' ) || ( i == len_in) )
		{
			if ( flag == 1 )
				continue;
		
			(*out)[ j ][ k ] = 0;

			j++;
			flag = 1;
			k = 0;
		} else {
			(*out)[ j ][ k++ ] = in[ i ];
			flag = 0;
		}
	}
}

void Gen_answer( char *path, char * in, char * out )
{
	char ** params;
	int cnt_params;

	line_to_array( path, in, &params, &cnt_params );

	run_exec( path, params, out );

	for ( int i = 0; i < cnt_params; i++ )
		free( params[ i ] );

	free( params );
}

void run_exec( char *path, char ** in, char * out )
{
	pid_t pid_p;
	int mypipe[2];
    pipe(mypipe);  

    pid_p = fork();
    switch( pid_p ) {
        case -1: /* error */
            perror("fork");
            exit(EXIT_FAILURE);
        case 0: /* child process */
            close(mypipe[0]); /* close unused in */
            dup2(mypipe[1], 1); /* stdout to pipe out */
            execvp(path, in); ///< run program with params
            //execlp("ls", "ls", NULL );
            close(mypipe[1]);
            _exit(EXIT_SUCCESS);
        }  

    /* parent process */
    close(mypipe[1]); /* close unused out */
        sleep(1);
    read(mypipe[0], out, SIZE_BUF); /* read from pipe in */
    close(mypipe[0]);
   // printf( "Child output:\n%s\n", out);  

    //kill( pid_p, 1 ); // kill potomok

}