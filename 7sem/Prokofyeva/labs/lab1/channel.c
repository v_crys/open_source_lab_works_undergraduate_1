#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] )
{
	int fd[2], nbytes;
	pid_t childpid;

	char string[] = "Hello, world!\n";
	
	char readbuffer[80];

	pipe(fd);

	if((childpid = fork()) == -1) {
		perror("fork");
		exit(1);
	}

	if(childpid == 0) {
		/* Потомок закрывает вход */
		close(fd[0]);
		/* Посылаем "string" через выход канала */
		write(fd[1], string, strlen(string));
		exit(0);
	} else {
		/* Родитель закрывает выход */
		close(fd[1]);
		/* Чтение строки из канала */
		nbytes = read(fd[0], readbuffer, sizeof(readbuffer));
		printf("Received string: %s", readbuffer);
	}
}