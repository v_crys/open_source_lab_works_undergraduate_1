#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void Print_word( void *path)
{
	char b = 0;
	int cnt = 0;
	char buf[ 8 ];

	printf( "pthread for files '%s' created\n", path );

	FILE *f = fopen( path, "rt" );

	while ( 1 )
	{
		if ( b == EOF )
			break;

		b = fgetc( f );
		if (( b == ' ') || ( b == '\n') || ( b == '\t')  || ( b == EOF ) )
		{
			if ( cnt == 7 )
			{
				buf[ cnt ] = 0;
				printf( "thread '%s' - %s\n", path, buf);
			}

			cnt = 0;
			continue;
		}

		buf[ cnt++ ] = b;


	}

	fclose( f );

	printf( "pthread for files '%s' conclusion\n", path );

	pthread_exit( 0 );
}

int main()
{
	FILE * files;
	int cnt_files = 0;
	char **name_files;

	int i;

	

	files = fopen( "f.txt", "rt" );

	fscanf( files, "%d", &cnt_files );
	name_files = (char **) calloc( cnt_files, sizeof( char *) );

	for ( i = 0; i < cnt_files; i++ )
		name_files[ i ] = (char *) calloc( 256, sizeof( char ) );


	pthread_t * ptr = calloc( cnt_files, sizeof( pthread_t ) );

	fgetc( files );

	printf( "worked files: \n" );
	for ( i = 0; i < cnt_files; i++ )
	{
		int cnt_char = 0;
		while ( 1 )
		{
			char b = fgetc( files );
			if (( b == '\n' ) || ( b == EOF ))
			{
				name_files[ i ][ cnt_char ] = 0;
				break;
			}

			name_files[ i ][ cnt_char++ ] = b;
		}

		printf( "%s\n", name_files[ i ] );
	}
	fclose( files );


	printf( "\n" );

	for ( i = 0; i < cnt_files; i++ )
		pthread_create( &(ptr[ i ]), NULL, Print_word, name_files[ i ] );

	for ( i = 0; i < cnt_files; i++ )
		pthread_join( ptr[ i ], NULL );
	

	return 0;
}