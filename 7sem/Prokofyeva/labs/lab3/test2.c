#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int size, rank;
	MPI_Init(&argc, &argv); /* Инициализируем библиотеку */

	MPI_Comm_size(MPI_COMM_WORLD, &size);

	/* Узнаем количество задач в запущенном приложении... */
	
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);

	/* ...и свой собственный номер: от 0 до (size-1) */
	if ((size > 1) && (rank == 0)) 
	{
		/* задача с номером 0 отправляет сообщение*/
		MPI_Send(argv[0], strlen(argv[0]), MPI_CHAR, 1, 1, MPI_COMM_WORLD);

		printf("Sent to process 1: \"%s\"\n", argv[0]);
	} 
	else if ((size > 1) && (rank == 1)) 
	{
		/* задача с номером 1 получает сообщение*/
		int count;
		MPI_Status status;
		char *buf;

		MPI_Probe(0, 1, MPI_COMM_WORLD, &status);

		MPI_Get_count(&status, MPI_CHAR, &count);

		buf = (char *) malloc(count * sizeof(char));

		MPI_Recv(buf, count, MPI_CHAR, 0, 1, MPI_COMM_WORLD, &status);
		
		printf("Received from process 0:\"%s\"\n", buf);
	}

	/* Все задачи завершают выполнение */
	MPI_Finalize();

	return 0;
}