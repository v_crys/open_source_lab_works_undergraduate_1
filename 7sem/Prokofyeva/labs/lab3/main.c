#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int size, rank;
    int err_code;
    int i;

    if ( MPI_Init(&argc, &argv) != MPI_SUCCESS )
    {
        printf( "Error init MPI lib\n" );
        return -1;
    }
  

    MPI_Comm_size(MPI_COMM_WORLD, &size);
  
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);

    if ( size != 2 )
    {
        if ( rank == 0 )
        {
            printf( "Error, -np use 2\n" );
            MPI_Abort( MPI_COMM_WORLD, -1 );
        }
    }


    if ( rank == 0 )
    {
        int cnt_elem;
        int *arr;
        MPI_Status mpi_stat;
        int sum;
        while( 1 )
        {
            MPI_Barrier( MPI_COMM_WORLD );

            printf( "\x1b[33mInput cnt elements: \x1b[0m" );
            scanf( "%d", &cnt_elem );

            if ( cnt_elem <= 0 )
            {
                printf( "\x1b[33mthr 0 - Exit\nthr 0 - send to thr 1 \"exit\"\n\x1b[0m" );
                MPI_Send( "1", 2, MPI_CHAR, 1, 3, MPI_COMM_WORLD);

                break;
            }
            printf( "\x1b[33mthr 0 - send to thr 1 \"no exit\"\n\x1b[0m" );
            MPI_Send( "2", 2, MPI_CHAR, 1, 3, MPI_COMM_WORLD);

            arr = (int *) calloc( cnt_elem, sizeof( int ) );

            printf( "\x1b[33minput elements: \n\x1b[0m" );
            for ( i = 0; i < cnt_elem; i++ )
                scanf( "%d", &(arr[ i ]) );

            printf( "\x1b[33mthr 0 - send arr to thr 1\n\x1b[0m" );

            MPI_Send( arr, cnt_elem, MPI_INT, 1, 1, MPI_COMM_WORLD);

            printf( "\x1b[33mthr 0 - wait rec sum\n\x1b[0m" );
            MPI_Recv( &sum, 1, MPI_INT, 1, 2, MPI_COMM_WORLD, &mpi_stat);

            printf( "\x1b[33mCnt_sum: %d\n\x1b[0m", sum );

            free( arr );

            

        }
    } else {
        int cnt_elem;
        int *arr;
        int sum;
        MPI_Status mpi_stat;
        char buf[ 2 ];

        while( 1 )
        {
            MPI_Barrier( MPI_COMM_WORLD );

            MPI_Recv(buf, 2, MPI_CHAR, 0, 3, MPI_COMM_WORLD, &mpi_stat);

            if ( buf[ 0 ] == '1' )
            {
                printf( "\x1b[36mthr 1 - rec from thr 1 \"exit\"\n\x1b[0m" );
                break;
            }

            MPI_Probe(0, 1, MPI_COMM_WORLD, &mpi_stat);

            MPI_Get_count(&mpi_stat, MPI_INT, &cnt_elem);

            arr = (int *) calloc( cnt_elem, sizeof( int ) );

            MPI_Recv(arr, cnt_elem, MPI_INT, 0, 1, MPI_COMM_WORLD, &mpi_stat);

            sum = 0;
            
            for ( i = 0; i < cnt_elem; i++ )
                sum += arr[ i ];

            MPI_Send( &sum, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);

            free( arr );

        }
    }

    MPI_Barrier( MPI_COMM_WORLD );
    MPI_Finalize();

  return 0;
}