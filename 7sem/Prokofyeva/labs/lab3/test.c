#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	int size, rank, i;
	MPI_Init(&argc, &argv); /* Инициализируем библиотеку */
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	/* Узнаем количество задач в запущенном приложении... */
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	
	/* ...и свой собственный номер: от 0 до (size-1) */
	/* задача с номером 0 сообщает пользователю размер группы, коммуникационный контекст которой описывает коммуникатор
	MPI_COMM_WORLD, т.е. число ветвей в приложении */

	if (rank == 0)
		printf("Total processes count = %d\n", size );
	
	/* Осуществляется барьерная синхронизация */
	MPI_Barrier(MPI_COMM_WORLD);
	/* Теперь каждая задача выводит на экран свой номер */
	printf("Hello! My rank in MPI_COMM_WORLD = %d\n", rank);
	/* Осуществляется барьерная синхронизация */
	MPI_Barrier(MPI_COMM_WORLD);
	/* затем ветвь c номером 0 печатает аргументы командной строки. */

	if (rank == 0) 
	{
		printf( "Command line of process 0:\n" );
		for(i = 0; i < argc; i++) 
			printf("%d: \"%s\"\n", i, argv[i]);
	} 

	/* Все задачи завершают выполнение */
	MPI_Finalize();
	return 0; 
}