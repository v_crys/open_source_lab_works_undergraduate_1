-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 25 2019 г., 13:54
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `backet`
--

CREATE TABLE IF NOT EXISTS `backet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code_product` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`Code_product`,`ID_user`),
  KEY `ID_user` (`ID_user`),
  KEY `Code_product` (`Code_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `backet`
--

INSERT INTO `backet` (`ID`, `Code_product`, `ID_user`) VALUES
(1, 2, 1),
(2, 3, 1),
(3, 7, 2),
(4, 6, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Logo` text NOT NULL,
  `Description` text NOT NULL,
  `Data` date NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `manufacturer`
--

INSERT INTO `manufacturer` (`ID`, `Name`, `Logo`, `Description`, `Data`) VALUES
(0, 'Asus', 'asus.png', 'AsusTek Computer Inc. — расположенная на Тайване транснациональная компания, специализирующаяся на компьютерной электронике (как комплектующие, так и готовые продукты). Название торговой марки Asus происходит от слова Pegasus («Пегас»).', '1989-04-01'),
(1, 'Huawei', 'huawei.png', 'Huawei Technologies Co. Ltd. — одна из крупнейших мировых компаний в сфере телекоммуникаций. Основана бывшим инженером Народно-освободительной армии Китая Жэнем Чжэнфэем в 1987 году.', '1987-09-15'),
(2, 'Karcher', 'karcher.png', 'Alfred Kärcher SE & Co. KG (Керхер) — немецкая компания, крупнейший в мире производитель техники для уборки и очистки. Компания была основана Альфредом Керхером в 1935 году в Штутгарте как семейное предприятие.', '1935-01-01'),
(3, 'Xiaomi', 'mi.png', 'Xiaomi Corporation — китайская компания, основанная Лэй Цзюнем в 2010 году[6]. С 2018 года является публичной. Занимает шестое место в мире и четвёртое в Китае по объёму производства (продаж) смартфонов.', '2010-04-06');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cost` text NOT NULL,
  `Name` text NOT NULL,
  `Type` int(11) NOT NULL,
  `Picture` text NOT NULL,
  `Manufacturer` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`Type`,`Manufacturer`),
  KEY `Manufacturer` (`Manufacturer`),
  KEY `Type` (`Type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`ID`, `Cost`, `Name`, `Type`, `Picture`, `Manufacturer`) VALUES
(2, '19990', 'ZenFone 5', 2, 'asus_zenfone5.jpg', 0),
(3, '35000', 'Mate 20', 2, 'huawey_mate20.jpg', 1),
(4, '15000', 'Model 1', 1, 'karcher1.jpg', 2),
(5, '23990', 'WD 5', 1, 'karcher_wd5.jpg', 2),
(6, '39800', 'Mi 8', 2, 'mi_8.jpg', 3),
(7, '13990', 'Redmi Note 6', 1, 'redmi_note_6.jpg', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `specification`
--

CREATE TABLE IF NOT EXISTS `specification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_product` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Value` text NOT NULL,
  PRIMARY KEY (`ID`,`ID_product`),
  KEY `ID_product` (`ID_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `specification`
--

INSERT INTO `specification` (`ID`, `ID_product`, `Name`, `Value`) VALUES
(1, 2, 'Разрешение экрана', 'Full HD'),
(2, 2, 'Процессор', 'Qualcomm snapdragon'),
(3, 2, 'GPS', 'Есть'),
(4, 2, 'WiFi', 'Есть'),
(5, 3, 'Разрешение экрана', 'Fill HD'),
(6, 3, 'Камера', '40 МП'),
(7, 3, 'Процессор', 'Intell celerate'),
(8, 4, 'Мощность', '200 Вт'),
(9, 5, 'Мощность', '350 Вт'),
(10, 5, 'Вес', '2 кг'),
(11, 6, 'Процессор', 'MediaTek 605'),
(12, 6, 'Камера ', '13 Мп'),
(13, 7, 'Экран', 'Каплевидный FullHD'),
(14, 7, 'Процессор', '4 ядерный'),
(15, 7, 'Аккумулятор', '4000 мАч');

-- --------------------------------------------------------

--
-- Структура таблицы `type_product`
--

CREATE TABLE IF NOT EXISTS `type_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `type_product`
--

INSERT INTO `type_product` (`ID`, `Name`) VALUES
(1, 'Пылесос'),
(2, 'Телефон');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Login` text NOT NULL,
  `Check_pass` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`ID`, `Name`, `Login`, `Check_pass`) VALUES
(1, 'Михаил', 'misha', 123),
(2, 'Никита', 'nikita', 321);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `backet`
--
ALTER TABLE `backet`
  ADD CONSTRAINT `backet_ibfk_1` FOREIGN KEY (`ID_user`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `backet_ibfk_2` FOREIGN KEY (`Code_product`) REFERENCES `product` (`ID`);

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`Type`) REFERENCES `type_product` (`ID`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`Manufacturer`) REFERENCES `manufacturer` (`ID`);

--
-- Ограничения внешнего ключа таблицы `specification`
--
ALTER TABLE `specification`
  ADD CONSTRAINT `specification_ibfk_1` FOREIGN KEY (`ID_product`) REFERENCES `product` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
