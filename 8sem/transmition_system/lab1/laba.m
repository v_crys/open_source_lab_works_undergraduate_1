function [ ] = laba( )
% �������� ������:
% ������ ������ ������ W0 = 40���
% �������� ����������� �� ������� Tm = 4���
% ���������� ���������� �������� �������� 3��/�
% �������� ����������� �� ������� B_d = 40��
% ������� ����������� ������ ��� ����������� ������/��� � ���������
% 10..40��

% ��������:
% ������������ ������� �������� 1/T = W00 / 3 = 13,3 ���
% ����� ����� �� �������� 13,3 / 3 = 4,4 ����

% ����� ����������� ��������� �����������:
%   1. 4� ������� ��������� ���������
%   2. 2� ������� �������� ����������� � ��� �� ��������� 1/2
%   3. ��� �� ��������� 1/4

digits(100);


h = 10:40;      % �������� ����
W0 = 40 * 10^6;
Tm = 4*10^-6;
max_i = length(h);
B_d = 40;
R = 13.3 * 10^6; % �������� �������� ��� �����������

% ���� �� ���
l = 4;
for i = 1:max_i
    Pf = pf( Tm, W0, l) ;
    Pe(1,i) = pe( l, h(i), Pf );
end

% ���������� �� 2 �����, ��� 1/2
V = 2;
l = 2;
for i = 1:max_i
    Pf = pf( Tm, W0, l);
    Pt = pt(  R, V, B_d );
    D0_v = D0( h(i), V, l, Pf, Pt, 10^6 );% D0( h(i), V, l, Pf, Pt ); 
    Pe(2,i) = D0_v;
end

% ��� ����������, ��� 1/4
V = 4;
l = 1;
for i = 1:max_i
    Pf = pf( Tm, W0, l);
    Pt = pt(  R, V, B_d );
    D0_v = D0( h(i), V, l, Pf, Pt, 10^6 ); %D0( h(i), V, l, Pf, Pt ); 
    Pe(3,i) = D0_v;
end

V = 4;
l = 1;
for i = 1:max_i
    Pf = pf( Tm, W0, l);
    Pt = pt(  R, V, B_d );
    D0_v = D0( h(i), V, l, Pf, Pt, 10^5 ); %D0( h(i), V, l, Pf, Pt ); 
    Pe(4,i) = D0_v;
end

V = 4;
l = 1;
for i = 1:max_i
    Pf = pf( Tm, W0, l);
    Pt = pt(  R, V, B_d );
    D0_v = D0( h(i), V, l, Pf, Pt, 10^7 ); %D0( h(i), V, l, Pf, Pt ); 
    Pe(5,i) = D0_v;
end

rer(1) = 'r';
rer(2) = 'g';
rer(3) = 'b';

for i = 1 : 3
    Ent = 0;
    for j = 1 : max_i
        Ent(j) = Pe(i,j);
    end
    semilogy(h,Ent, rer(i));

    grid on
    hold on
end
    legend('���������� �� 4 �������','���������� �� 2 ����� � ��� 1/2','��� 1/4');
    xlabel('h, ��');
    ylabel('Pe');
    
    figure
    grid off
  hold off  
    
    for i = 3 : 5
    Ent = 0;
    for j = 1 : max_i
        Ent(j) = Pe(i,j);
    end
    semilogy(h,Ent, rer(6-i) );

    grid on
    hold on
end
    legend('10^6','10^5','10^7');
    xlabel('h, ��');
    ylabel('Pe');
    
    
end
