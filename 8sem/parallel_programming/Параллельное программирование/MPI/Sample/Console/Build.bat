@echo off

del *.exe /Q

call "C:\Program Files\Microsoft Visual Studio\VC98\Bin\VCVARS32.BAT"

"C:\Program Files\Microsoft Visual Studio\VC98\Bin\cl.exe" /nologo /MT /W3 /GX /O2 /I "C:\Program Files\MPICH\SDK\include" /D WIN32 /D NDEBUG /D _CONSOLE /D _MBCS /c /Foout.obj %1

"C:\Program Files\Microsoft Visual Studio\VC98\Bin\link.exe" ws2_32.lib mpich.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386 /out:"out.exe" /libpath:"C:\Program Files\MPICH\SDK\lib" out.obj

del *.obj /Q

"C:\Program Files\MPICH\mpd\bin\MPIRun.exe" -np %2 out.exe

@echo on
