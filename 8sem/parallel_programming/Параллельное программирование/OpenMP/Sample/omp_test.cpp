#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>


const int N = 13;
int i;

int main(int argc, char* argv[])
{
	omp_set_num_threads(4);
	
	#pragma omp parallel 
	{
        #pragma omp sections //�����. ������
		{
			#pragma omp section
			{
				printf("Sections(1): thread %d of %d\n", omp_get_thread_num(), omp_get_num_threads());
			}
			#pragma omp section
			{
				printf("Sections(2): thread %d of %d\n", omp_get_thread_num(), omp_get_num_threads());
			}
		}
	}

	#pragma omp parallel for private(i) schedule(static)
	for (i=0; i<N; i++)
	{
		printf("For: thread %d of %d\n", omp_get_thread_num(), omp_get_num_threads());
	}


	return 0;
}
