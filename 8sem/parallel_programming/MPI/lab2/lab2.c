/*
 *	Course: Parallel systems
 *	Theme: MPI
 *	Lab: 2
 * 	Variant: 2 (M = 4, N = 20, Char, multiply)
 */

#include <mpi.h>
#include <stdio.h>
#include <time.h>

#define M 20

int main(int argc, char **argv)
{
	double start_time, end_time;

	int size, rank, i;

	

	MPI_Init(&argc, &argv); 

	start_time = MPI_Wtime();

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);

	srand( time( 0 ) * rank );

	char s[ 100 ];
	char vector[ M ];
	char vector_out[ M];


	for ( i = 0; i < M; i++ )
		vector[ i ] = rand( ) % 10 ;	
	



	MPI_Reduce( vector, vector_out, M, MPI_CHAR, MPI_PROD, 0, MPI_COMM_WORLD );
		



	if ( rank == 0 )
	{
		sprintf( s, "\nResult: ");
		for ( i = 0; i < M; i++ )
			sprintf( s, "%s%d ", s, vector_out[ i ] );
		printf( "%s\n", s );
	} 

	sprintf( s, "Source [ %d ]: ", rank );
		for ( i = 0; i < M; i++ )
			sprintf( s, "%s%d ", s, vector[ i ] );

	printf( "%s\n", s );



	end_time = MPI_Wtime();
	MPI_Finalize();


	printf( "\nRank: %d\nStart_time: %f\nEnd_time: %f\nDelta_time: %f\n", rank, start_time, end_time, end_time - start_time );

	return 0; 
}