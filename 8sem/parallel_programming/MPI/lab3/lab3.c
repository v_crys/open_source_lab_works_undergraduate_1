/*
 *	Course: Parallel systems
 *	Theme: MPI
 *	Lab: 3
 * 	Variant: 2 (M = 9, N = 4, Short int, ascending)
 */

#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define M 9
#define N 4

#define TAG_FOR_SORT 0
#define TAG_SORTED 	 1
#define TAG_CNT_SORTED	2

void sort( char * arr, int len )
{
	for ( int i = 0; i < len - 1; i++ )
	{
		for ( int j = i + 1; j < len; j++ )
		{
			if ( arr[ i ] > arr[ j ] )
			{
				arr[ i ] += arr[ j ];
				arr[ j ] = arr[ i ] - arr[ j ];
				arr[ i ] -= arr[ j ];
			}
		}
	}
}

void copy( char *source, int len_source, char *res )
{
	for ( int i = 0; i < len_source; i++ )
		res[ i ] = source[ i ];	
}

int main(int argc, char **argv)
{
	double start_time, end_time;
	MPI_Status MPI_Stat;
	int size, rank, i, j;

	MPI_Init(&argc, &argv); 

	start_time = MPI_Wtime();

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);

	srand( time( 0 ) * rank );

	if ( rank == 0 )
	{
		char array[ N ][ M ];
		char array_res[ N ][ M ];
		char buf[ M ];

		// generated matrix
		for ( i = 0; i < N; i++ )
		{
			for ( j = 0; j < M; j++ )
			{
				array[ i ][ j ] = rand( ) % 10 ;
				printf( "%d ", array[ i ][ j ] );
			}		
			printf( "\n" );
		}

		int send_col = 0;
		for ( i = 0; i < size; i++ )
		{
			int cnt_send = (N - send_col) / ( size - i );

			if ( i == 0 )
			{
				// sort local branch
				for ( j = 0; j < cnt_send; j++ )
				{
					copy( &(array[ j ][ 0 ]), M, &(array_res[ j ][ 0 ]) ); 
					sort( &(array_res[ j ][ 0 ]), M );
				}
			} else {
				// send for sort other branch
				MPI_Send( &cnt_send, 1, MPI_INT, i, TAG_CNT_SORTED, MPI_COMM_WORLD );	// Cnt rows for sorted

				for ( j = 0; j < cnt_send; j++ )
					MPI_Send( &(array[ send_col + j ][ 0 ]), M, MPI_CHAR, i, TAG_FOR_SORT, MPI_COMM_WORLD );
			}
			send_col += cnt_send;
		}

		// receive sorted from other branch
		send_col = N / size;
		for ( i = 1; i < size; i++ )
		{
			int cnt_send = (N - send_col) / ( size - i );

			for ( j = 0; j < cnt_send; j++ )
				MPI_Recv( &(array_res[ send_col + j ][ 0 ]), M, MPI_CHAR, i, TAG_SORTED, MPI_COMM_WORLD, &MPI_Stat );

			send_col += cnt_send;
		}

		printf( "\nResult: \n");
		for ( i = 0; i < N; i++ )
		{
			for ( j = 0; j < M; j++ )
				printf( "%d ", array_res[ i ][ j ] );
			
			printf( "\n" );
		}
	} else {
		int cnt_sorted = 0;

		// receive cnt rows
		MPI_Recv( &cnt_sorted, 1, MPI_INT, 0, TAG_CNT_SORTED, MPI_COMM_WORLD, &MPI_Stat );

		// receive rows
		char **arr = (char **) calloc( sizeof( char *), cnt_sorted );
		for ( i = 0; i < cnt_sorted; i++ )
		{
			arr[ i ] = (char *) calloc( sizeof( char ), M );

			MPI_Recv( &(arr[ i ][ 0 ]), M, MPI_CHAR, 0, TAG_FOR_SORT, MPI_COMM_WORLD, &MPI_Stat );
		}

		// sorted
		for ( i = 0; i < cnt_sorted; i++ )
			sort( &(arr[ i ][ 0 ]), M );

		// send
		for ( i = 0; i < cnt_sorted; i++ )
			MPI_Send( &(arr[ i ][ 0 ]), M, MPI_CHAR, 0, TAG_SORTED, MPI_COMM_WORLD );

		// free memory
		for ( i = 0; i < cnt_sorted; i++ )
			free( arr [ i ] );

		free( arr );
	}

		
	end_time = MPI_Wtime();
	MPI_Finalize();


	printf( "\nRank: %d\nStart_time: %f\nEnd_time: %f\nDelta_time: %f\n", rank, start_time, end_time, end_time - start_time );

	return 0; 
}