/*
 *	Course: Parallel systems
 *	Theme: MPI
 *	Lab: 1
 * 	Variant: 2 (M = 5, N = 4, MPI_Bcast)
 */

#include <mpi.h>
#include <stdio.h>
#include <time.h>

#define M 5

int main(int argc, char **argv)
{
	double start_time, end_time;

	int size, rank, i;

	srand( time( 0 ) );

	MPI_Init(&argc, &argv); 
	start_time = MPI_Wtime();

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);

	int vector[ M ];
	char s[ 100 ];

	if ( rank == 0 )
	{
		for ( i = 0; i < M; i++ )
			vector[ i ] = rand( ) % 100 ;	
	}

	MPI_Bcast( vector, M, MPI_INT, 0, MPI_COMM_WORLD );
		
	for ( i = 0; i < M; i++ )
		sprintf(s, "%s%d ", s, vector[ i ] );
	sprintf( s, "%s\n", s );

	if ( rank == 0 ) 
		printf( "0: send = %s", s );
	else
		printf( "%d: recv = %s", rank, s );



	end_time = MPI_Wtime();
	MPI_Finalize();

	printf( "Rank: %d\nStart_time: %f\nEnd_time: %f\nDelta_time: %f\n", rank, start_time, end_time, end_time - start_time );
	return 0; 
}