/*
 *	Course: Parallel systems
 *	Theme: OpenMP
 *	Lab: 1
 * 	Variant: 2 (N = 7, CHAR, sum vectors)
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 7

int main () 
{
	int i;

	char Vector_1[ N ];
	char Vector_2[ N ];
	char res[ N ];

	srand( time( 0 ) );

	for ( i = 0; i < N; i++ )
	{
		Vector_1[ i ] = rand( ) % 50;
		Vector_2[ i ] = rand( ) % 50;
	}

	printf( "Vector 1: " );
	for ( i = 0; i < N; i++ )
		printf( "%d ", Vector_1[ i ] );
	
	printf( "\nVector 2: " );
	for ( i = 0; i < N; i++ )
		printf( "%d ", Vector_2[ i ] );

	printf( "\n" );

	#pragma omp parallel for shared( Vector_1, Vector_2, res )  private( i ) schedule( static ) 
	for ( i = 0; i < N; i++ ) 
	{
		printf( "Thread: %d\n", omp_get_thread_num() );
		res[ i ] = Vector_1[ i ] + Vector_2[ i ];
	}

	printf( "Result: " );

	for ( i = 0; i < N; i++ )
		printf( "%d ", res[ i ] );

	printf( "\n" );

	return 0;
}