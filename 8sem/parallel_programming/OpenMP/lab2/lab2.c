/*
 *	Course: Parallel systems
 *	Theme: OpenMP
 *	Lab: 2
 * 	Variant: 2 (N = 20*60, CHAR, sorting rows)
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 100
#define M 10000


int main () 
{
	int i, j, k;

	clock_t begin = clock();

	char **Matrix;

	srand( time( 0 ) );

	printf( "Matrix: %dx%d\n", N, M );
	//printf( "Source matrix: \n" );

	for ( int cnt_threads = 1; cnt_threads < 1000; cnt_threads+=50 )
	{
		omp_set_num_threads( cnt_threads );

		clock_t begin = clock();

		Matrix = (char **) calloc( N, sizeof( char *) );
		for ( i = 0; i < N; i++ )
		{
			Matrix[ i ] = (char *) calloc( M, sizeof( char ) );
			for ( j = 0; j < M; j++ )
			{
				Matrix[ i ][ j ] = rand() % 10;
				//printf( "%d ", Matrix[ i ][ j ] ); 
			}
			//printf( "\n" );
		}

			#pragma omp parallel for shared( Matrix) private( i, j, k ) schedule( static ) 
			for ( i = 0; i < M; i++ )
			{
				printf( "Thread: %d\n", omp_get_thread_num() );
				for ( j = 0; j < N-1; j++ )
				{
					for ( k = j + 1; k < N; k++ )
					{
						if ( Matrix[ j ][ i ] > Matrix[ k ][ i ] )
						{
							char buf = Matrix[ j ][ i ];
							Matrix[ j ][ i ] = Matrix[ k ][ i ];
							Matrix[ k ][ i ] = buf;
						}
					}
				}
			}	
			

		/*
		printf( "Result: \n" );

		for ( i = 0; i < N; i++ )
		{
			for ( j = 0; j < M; j++ )
			{
				printf( "%d ", Matrix[ i ][ j ] );
			}
			printf( "\n" );
		}
		*/

		for ( i = 0; i < N; i++ )
			free( Matrix[ i ] );
		free( Matrix );

		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

		printf( "Threads: %d, Elapse time: %f\n", cnt_threads, time_spent ); 
	}
	return 0;
}