x = (0:0.2:10);
A = trimf(x,[2 5 8]);
B = psigmf(x,[3 6 6 4]);
figure
subplot(4,2,1);
plot (x, A, x, B); grid;
title('�������� ��');
unionAB = max(A,B);

subplot(4,2,2);
plot (x, A, x, B, x, unionAB); grid;
title('����������� ��(�������� max)');

intersecAB = min(A,B);
subplot(4,2,3);
plot(x, A, x, B, x,intersecAB); grid;
title('����������� ��(�������� min)');

alsAB = A + B - (A.*B);
subplot(4,2,4);
plot(x, A, x, B, x, alsAB); grid;
title('�������������� �����');

alyAB =(A.*B);
subplot(4,2,5);
plot(x, A, x, B, x, alyAB); grid;
title('�������������� ������������');

raznAB =(A - B);
subplot(4,2,6);
plot(x, A, x, B, x, raznAB); grid;
title('��������');

symraznAB = max( min(A , 1 - B), min( 1 - A, B ) );
subplot(4,2,7);
plot(x, A, x, B, x, symraznAB); grid;
title('������������ ��������');