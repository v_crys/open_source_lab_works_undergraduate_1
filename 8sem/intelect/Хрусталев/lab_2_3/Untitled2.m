alphabet = [0 0 0 0  ;
            0 0 0 0 ;
            1 1 1 1 ;
            0 0 0 0 ;
            0 0 0 0;
            
            0 0 0 0 ;
            1 1 0 0 ;
            1 0 0 1 ;
            1 0 1 0 ;
            0 0 0 0 ;
            
            1 1 1 1 ;
            0 1 1 0 ;
            1 1 1 1 ;
            0 1 1 0 ;
            1 1 1 1 ;
            
            0 0 0 0 ;
            0 1 0 1  ;
            1 0 0 1 ;
            0 0 1 1 ;
            0 0 0 0 ;
            
            0 0 0 0 ;
            0 0 0 0 ;
            1 1 1 1 ;
            0 0 0 0 ;
            0 0 0 0 ];
        

targets = eye(4);
plotletters2(alphabet);
% [10 26]
net = newff(minmax(alphabet),[10 size(targets,1)],{'logsig' 'logsig'},'traingdx');
P = alphabet;
T = targets;
net.trainParam.epochs = 500;
[net, tr] = train(net, P, T);
 
noisyP = alphabet + randn(size(alphabet))*0.1;
plotletters2(noisyP);
A2 = sim(net, noisyP);
 
for j=1:4
    A3 = compet(A2(:,j));
    answer1(j) = find(compet(A3) == 1);
end
 
NetLetters1 = alphabet(:,answer1);
plotletters2(NetLetters1);
