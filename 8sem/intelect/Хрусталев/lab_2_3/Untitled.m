[alphabet, targets] = prprob;
plotletters(alphabet);
% [10 26]
net = newff(minmax(alphabet),[20 26],{'logsig' 'logsig'},'traingdx');
P = alphabet;
T = targets;
net.trainParam.epochs = 1000;
[net, tr] = train(net, P, T);
 
noisyP = alphabet + randn(size(alphabet))*0.2;
plotletters(noisyP);
A2 = sim(net, noisyP);
 
for j=1:26
    A3 = compet(A2(:,j));
    answer(j) = find(compet(A3) == 1);
end
 
NetLetters = alphabet(:,answer);
plotletters(NetLetters);
