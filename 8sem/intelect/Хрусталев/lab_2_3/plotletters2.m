function plotletters2( alphabet )

fprintf( 'plotletters is plotting the first 25 letters\n');
[m,n] = size( alphabet );

figure
MM = colormap( gray );
MM = MM( end:-1:1,:);
colormap( MM );

nn = min( [n, 25] );
for j = 1:nn
    subplot( 5, 5, j );
    imagesc( reshape(alphabet(:,j),5,5)');
    axis equal;
    axis off;

end
end

