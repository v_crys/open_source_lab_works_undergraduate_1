x = 0 : 2*pi/20 : 2*pi;
y = 0 : 1/ 20 : 1;

z = zeros( size(x,2), size( y, 2 ) );
for i = 1:size(x,2)
    for j = 1:size(y,2)
        temp1 = sin( i );
        temp2 = exp( -3*j );
        z(i,j) = temp1 * temp2;
    end
end
figure;
mesh(x, y, z);
xlabel('x');
ylabel('y');
zlabel('z');
 
P = [x;y];
T = z;
netw = newff([0 2*pi; 0 1],[25 size( y, 2 )],{'tansig' 'purelin'},'trainlm');
net.trainParam.show = 50;
net.trainParam.lr = 0.05;
net.trainParam.epochs = 100;
net.trainParam.goal = 0.001;
netw1 = train(netw, P, T);
A = sim(netw1, P);
figure;
mesh(x, y, A);
