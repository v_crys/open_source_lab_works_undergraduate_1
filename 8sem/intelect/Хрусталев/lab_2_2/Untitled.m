x = 1 : 0.1 : 10;
y = x.*sin(0.2*x.^2) + (15 \ (x.^3));
 
for i = 1 : 10
    t(i) = i * 9;
    f(i) = x(t(i));
    z(i) = y(t(i));
end
figure;
plot(f, z, 'ko', x, y, 'm');
legend('����� ������������','������ �������');
xlabel('��� x');
ylabel('��� y');
net = newff([1 10],[20 1],{'tansig', 'purelin'});
net.trainParam.goal = 0.01;
net.trainParam.epochs = 100;
net = train(net, f, z);
X = linspace(1, 10);
Y = sim(net, X);
figure;
plot(f, z, 'ko', X, Y);
