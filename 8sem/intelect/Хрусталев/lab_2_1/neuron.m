in = [0 0 0 0 1 1 1 1; 0 0 1 1 0 0 1 1; 0 1 0 1 0 1 0 1];
out = [1 0 1 0 1 0 0 1];
 
network = newff([0 1; 0 1; 0 1],[10 1],{'logsig','logsig'});
network = init(network);
network.trainParam.epochs = 15;
network.trainParam.lr = 0.05;
network = train(network,in,out);

sim(network, in )
