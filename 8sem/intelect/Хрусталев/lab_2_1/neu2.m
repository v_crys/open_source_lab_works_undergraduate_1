inp = [ 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1; 
        0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1; 
        0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1; 
        0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 ];

out = zeros( 3, 16);

for i = 1: 16  
    proizv = (inp( 1, i ) * 2 + inp(2, i )) + (inp( 3, i ) * 2 + inp(4, i ));
    out( 1, i ) = proizv / 2;
    out( 2, i ) = mod( proizv, 2);
    out( 3, i ) = mod( proizv, 4);
end


network = newff([0 1; 0 1; 0 1; 0 1],[50 3],{'logsig','logsig'});
network = init(network);
network.trainParam.epochs = 200;
network.trainParam.lr = 0.05;
network.trainParam.gradient = 10^-10;

network = train(network,inp,out);
sim( network, inp )