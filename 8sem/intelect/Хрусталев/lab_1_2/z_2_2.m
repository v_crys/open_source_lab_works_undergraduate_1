%��������� �������� ��������� ������� �� ������� 1, ��������� ���������� � ��������
%�������� ���������� ��� ���� ������ �������� �������� �� ������� �����������.
%��������� �������������� ��� ������� ��������� ���������� � ������� DCOG.

% 1
x = 1:1:10;
A=[1 1 1  0.9 0.7 0.5 0.4 0.2 0.1 0];
B=[0 0.2 0.3 0.4 0.6 0.7 0.9 1 1 1];
R=zeros([10 10]);
for i=1:length(A)
    for j=1:length(B)
        R(i,j)=min(A(i),B(j));
    end;
end;

%2
for i=1:length(A)
    for j=1:length(A)
        C(j)=min(A(j),R(j,i)); 
    end;
    B1(i)=max(C);
end;
Numerator=0;
Denominator=0;
for i=1:length(B1)
    Denominator = Denominator+B1(i);
    Numerator = Numerator+B1(i)*i;
end
DCOG=Numerator/Denominator;


A1 = [1 1 1  0.9 0.7 0.5 0.4 0.2 0.1 0];
B1 = zeros([1 10]);
C = zeros([1 10]);
for i=1:length(A1)
    for j=1:length(A1)
        C(j) = min( A1(j), R(j,i) );
    end
    B1(i) = max(C);
end
figure
subplot(4, 1, 1);
plot(x, A, x, B);
legend('A', 'B');
title('A � B');
subplot(4, 1, 2);
plot(x, A1, x, B1)
newB1 = sum(B1.*x)/sum(B1);
h11 = line([newB1 newB1], [0 1], 'Color', 'black');
legend('A1', 'B1', 'DCOG');
%%%
A1 = [1 0.9 0.85  0.7 0.7 0.5 0.4 0.2 0.1 0];
B1 = zeros([1 10]);
C = zeros([1 10]);
for i=1:length(A1)
    for j=1:length(A1)
        C(j) = min( A1(j), R(j,i) );
    end
    B1(i) = max(C);
end
subplot(4, 1, 3);
plot(x, A1, x, B1)
newB1 = sum(B1.*x)/sum(B1);
h11 = line([newB1 newB1], [0 1], 'Color', 'black');
legend('A1', 'B1', 'DCOG');
%%%
A1 = [1 0.9 0.8  0.8 0.7 0.5 0.4 0.2 0.1 0];
B1 = zeros([1 10]);
C = zeros([1 10]);
for i=1:length(A1)
    for j=1:length(A1)
        C(j) = min( A1(j), R(j,i) );
    end
    B1(i) = max(C);
end
subplot(4, 1, 4);
plot(x , A1, x, B1)
newB1 = sum(B1.*x)/sum(B1);
h11 = line([newB1 newB1], [0 1], 'Color', 'black');
legend('A1', 'B1', 'DCOG');


