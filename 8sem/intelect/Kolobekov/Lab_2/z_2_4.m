%��������� �� �� ������� �������� ��������������, ���������� � ������������ �
%��������� ������� ��� ����������� ������� �������������� � ������������ �����������.
%��������� �������������� ����� �� ����� 5-� ��������� � Matlab. ����������� ����������
%�������� � ������.
%������� �������������� gaussmf; zmf; trimf
x = -10:0.01:10;
mf1 = gaussmf(x, [-10 -8 -2 2]); 
mf2 = zmf(x, [-5 -3 2 4]); 
mf3 = trimf(x, [2 3 8 9]); 
mf = max(mf2, max(mf1, mf3));
figure(1);
plot(x,mf1,x,mf2,x,mf3);
grid
title('��� �������')
figure(2);
plot(x,mf,'r','LineWidth',3);
grid
axis([-10 10 0 1]);
title('Defuzzification Methods')
xlabel('x')
ylabel('membership(x)')

x1 = defuzz(x,mf,'centroid');
h1 = line([x1 x1],[0 1],'Color','g', 'LineWidth',1);
t1 = text(x1,0.5,'centroid','Color','k','FontWeight','bold');

x2 = defuzz(x,mf,'bisector');
h2 = line([x2 x2],[0 1],'Color','b', 'LineWidth',1);
t2 = text(x2,0.2,'bisector','Color','k','FontWeight','bold');

x3 = defuzz(x,mf,'mom');% �������
h3 = line([x3 x3],[0 1],'Color','r', 'LineWidth',1);
t3 = text(x3,0.7,'mom','Color','k','FontWeight','bold');

x4 = defuzz(x,mf,'lom');% ����� ���������
h4 = line([x4 x4],[0 1],'Color','c', 'LineWidth',1);
t4 = text(x4,0.3,'lom','Color','k','FontWeight','bold');

x5 = defuzz(x,mf,'som'); % ����� ������� �� ������������ 
h5 = line([x5 x5],[0 1],'Color','m','LineWidth',1);
t5 = text(x5,0.5,'som','Color','k','FontWeight','bold');