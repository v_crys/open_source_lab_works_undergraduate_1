%x=-2:0.05:2;
%y=-1:0.05:1;
%[Xx,Yy] = meshgrid(-2:0.05:2, -1:0.05:1);
x=-1:0.05:1;
y=-1:0.05:1;
[Xx,Yy] = meshgrid(x, y);
Zz = (((Xx.^2) * (Yy.^2)) + (2*Xx*Yy)-3)/((Xx.^2)+(Yy.^2)+1);
figure(1);
mesh(Xx,Yy,Zz)
%surf(Xx,Yy,Zz)
P=[x;y];
T=Zz;
net=newff([-1 1; -1 1],[25 41],{'tansig','purelin'},'trainlm');
net.trainParam.show=1; %���������� ���� ����� ���������
net.trainParam.lr=0.05; %�������� ��������
net.trainParam.epochs=1000; %������������ ���������� ���� ����������
net.trainParam.goal=0.001; %������� ��������� �� ���������� �� �������
net1=train(net,P,T);
A=sim(net1,P);
figure(2);
mesh(x,y,A);