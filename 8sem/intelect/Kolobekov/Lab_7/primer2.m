x=-2:0.25:2;
y=-2:0.25:2;
z=cos(x)'*sin(y);
figure(1);
mesh(x,y,z);
P=[x;y];
T=z;
net=newff([-2 2; -2 2],[25 17],{'tansig','purelin'},'trainlm');
net.trainParam.show=50; %���������� ���� ����� ���������
net.trainParam.lr=0.05; %�������� ��������
net.trainParam.epochs=300; %������������ ���������� ���� ����������
net.trainParam.goal=0.001; %������� ��������� �� ���������� �� �������
net1=train(net,P,T);
A=sim(net1,P);
figure(2);
mesh(x,y,A);
