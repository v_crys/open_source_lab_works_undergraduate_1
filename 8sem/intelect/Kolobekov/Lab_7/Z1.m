x=-2:0.1:3;
y=(3*sin(x).^3)-(2*cos(x).^2);
figure(1);
plot(x,y);
title('�������� ������ ������� ����� ����������'); 
xlabel('The coordinate of Ox'); 
ylabel('The coordinate of Oy'); 
grid;
for i=1:22
    t(i)=i*2;
    f(i)=x(t(i));
    z(i)=y(t(i));
end
figure(2);
plot(f,z,'ko',x,y)
title('�� �������� ������� �������� ����� ��� �������������'); 
xlabel('The coordinate of Ox'); 
ylabel('The coordinate of Oy');
grid;
net=newff([-2 3],[15 1],{'tansig','purelin'});
net.trainParam.goal=0.01;
net.trainParam.epochs=50;
net=train(net,f,z);
X=linspace(-2,3);
Y=sim(net,X);
figure(3);
plot(f,z,'ko',X,Y)
title('������ ������������� ������� ����� ���������'); 
xlabel('The coordinate of Ox'); 
ylabel('The coordinate of Oy');
grid;