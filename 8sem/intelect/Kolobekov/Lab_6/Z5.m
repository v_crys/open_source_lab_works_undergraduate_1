%������� ������ ��� ���������
inp = [0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1;
       0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1;
       0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1;
       0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1];
%�������� ������
out = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1; 
       0 0 0 0 0 0 0 0 0 0 1 1 0 0 1 0;
       0 0 0 0 0 0 1 1 0 1 0 1 0 1 1 0;
       0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1];
%�������� ����������� ���� ������� ���������������:
network = newff([0 1; 0 1; 0 1; 0 1],[6 4],{'logsig','logsig'});
%�� ������ �������� ����������� ������������� ��:
network = init(network);
%������� ����� ����, �������� � �������� ��������:
network.trainParam.epochs = 500;
network.trainParam.lr = 0.05;
network = train(network, inp, out);
%�������� ������ ����:
y = sim(network,inp);
