﻿#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
#include <windows.h>

struct kadr{
    int NK[8];
    int AP[8];
    int AO[8];
    int PU[1];
    int DD[10];
    int DATA[8];
    int FCS[8];
};

void printKadr(struct kadr);

char* func(struct kadr);

int main(){
    setlocale(LC_ALL, "Russian");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    printf("\n\n\tStarting tests.\n");
    printf("\tGenerate frames.\n");

    printf("Test 1:\n");
    printf("Modeling: receive frame, but in field NK\nincorrect value\n");
    struct kadr kadrilla_1 ={
        .NK =   {0, 1, 1, 0, 0, 1, 1, 0}, // начало кадра
        .AP =   {0, 0, 0, 0, 0, 1, 0, 1}, // адрес получателя
        .AO =   {0, 0, 0, 0, 0, 0, 0, 0}, // адрес отправителя
        .PU =   {0},                      // тип пакета
        .DD =   {0},                      // длина данных
        .DATA = {0},                      // сами данные
        .FCS =  {1, 1, 1, 1, 1, 0, 1, 0}  // контрольная сумма (250)
    };

    printKadr(kadrilla_1);
    char* result = func(kadrilla_1);
    printf("%s\n\n", result);

    printf("Test 2:\n");
    printf("Modeling: receive frame, NK is correct,\n but CRC incorrect\n");
    struct kadr kadrilla_2 ={
        .NK =   {0, 1, 1, 1, 1, 1, 1, 0}, // начало кадра
        .AP =   {0, 0, 0, 0, 0, 1, 0, 1}, // адрес получателя
        .AO =   {0, 0, 0, 0, 0, 0, 0, 0}, // адрес отправителя
        .PU =   {0},                      // тип пакета
        .DD =   {0},                      // длина данных
        .DATA = {0},                      // сами данные
        .FCS =  {1, 1, 1, 1, 1, 0, 0, 1}  // контрольная сумма (249) норма (248)
    };

    printKadr(kadrilla_2);
    result = func(kadrilla_2);
    printf("%s\n\n", result);

    printf("Test 3:\n");
    printf("Modeling: receive frame, NK correct,\nCRC correct, address foreign\n");
    struct kadr kadrilla_3 ={
        .NK =   {0, 1, 1, 1, 1, 1, 1, 0}, // начало кадра
        .AP =   {0, 0, 0, 0, 0, 1, 1, 0}, // адрес получателя
        .AO =   {0, 0, 0, 0, 0, 0, 0, 0}, // адрес отправителя
        .PU =   {0},                      // тип пакета
        .DD =   {0},                      // длина данных
        .DATA = {0},                      // сами данные
        .FCS =  {1, 1, 1, 1, 1, 0, 0, 0}  // контрольная сумма норма (248)
    };

    printKadr(kadrilla_3);
    result = func(kadrilla_3);
    printf("%s\n\n", result);

    printf("Test 4:\n");
    printf("Modeling: receive frame, all correct, frame for us\n");
    struct kadr kadrilla_4 ={
        .NK =   {0, 1, 1, 1, 1, 1, 1, 0}, // начало кадра
        .AP =   {0, 0, 0, 0, 0, 1, 0, 1}, // адрес получателя
        .AO =   {0, 0, 0, 0, 0, 0, 0, 0}, // адрес отправителя
        .PU =   {0},                      // тип пакета
        .DD =   {0},                      // длина данных
        .DATA = {0},                      // сами данные
        .FCS =  {1, 1, 1, 1, 1, 0, 0, 0}  // контрольная сумма норма (248)
    };

    printKadr(kadrilla_4);
    result = func(kadrilla_4);
    printf("%s\n\n", result);

    return 0;
}

char* func(struct kadr kadrilla){
    const int ideal_NK[8]  = {0, 1, 1, 1, 1, 1, 1, 0};
    const int my_adress[8] = {0, 0, 0, 0, 0, 1, 0, 1};
    //проверка коррректности поля NK
    register int i = 0;
    for(i=0; i<8; i++){
        if(kadrilla.NK[i] != ideal_NK[i]){
            return "Field NK incorrect!";
        }
    }

    //проверка коррректности поля FCS
    int summ_ones = 0;
    for(i=0; i<8; i++){
        summ_ones = summ_ones + kadrilla.NK[i] + kadrilla.AO[i] + kadrilla.AP[i] + kadrilla.DATA[i];
    }
    for(i=0; i<10; i++){
        summ_ones = summ_ones + kadrilla.DD[i];
    }
    summ_ones = summ_ones + kadrilla.PU[0];

    summ_ones = summ_ones % 256;
    //printf("%i\n", summ_ones); количесвто едениц в пакете кроме поля FCS

    int n_FCS = 0;
    for(i=0; i<8; i++){
        n_FCS += kadrilla.FCS[7-i] * (int)pow(2, i);
    }
    //printf("%i\n", n_FCS); FCS в десятичной СС

    if((n_FCS + summ_ones) % 256 != 0){
        return "Field FCS incorrect!";
    }

    int flag = 0;
    //проверка поля AP
    for(i=0; i<8; i++){
        if(my_adress[i] != kadrilla.AP[i]){
            flag = 1;
            break;
        }
    }

    if(flag == 1){
        return "Address foreign. Return frame to NET";
    }
    else{
        return "Frame for us!";
    }

    return "Bad deal!";
}

void printKadr(struct kadr kadrilla){
    printf("-------------------------------------------------------------\n");
    printf("|   NK   |   AP   |   AO   | PU |    DD    |  DATA  |   FCS  |\n");
    printf("-------------------------------------------------------------\n");
    printf("|%i%i%i%i%i%i%i%i|", kadrilla.NK[0], kadrilla.NK[1], kadrilla.NK[2], kadrilla.NK[3], kadrilla.NK[4], kadrilla.NK[5], kadrilla.NK[6], kadrilla.NK[7]);
    printf("%i%i%i%i%i%i%i%i|", kadrilla.AP[0], kadrilla.AP[1], kadrilla.AP[2], kadrilla.AP[3], kadrilla.AP[4], kadrilla.AP[5], kadrilla.AP[6], kadrilla.AP[7]);
    printf("%i%i%i%i%i%i%i%i|", kadrilla.AO[0], kadrilla.AO[1], kadrilla.AO[2], kadrilla.AO[3], kadrilla.AO[4], kadrilla.AO[5], kadrilla.AO[6], kadrilla.AO[7]);
    printf("  %i |", kadrilla.PU[0]);
    printf("%i%i%i%i%i%i%i%i%i%i|", kadrilla.DD[0], kadrilla.DD[1], kadrilla.DD[2], kadrilla.DD[3], kadrilla.DD[4], kadrilla.DD[5], kadrilla.DD[6], kadrilla.DD[7], kadrilla.DD[8], kadrilla.DD[9]);
    printf("%i%i%i%i%i%i%i%i|", kadrilla.DATA[0], kadrilla.DATA[1], kadrilla.DATA[2], kadrilla.DATA[3], kadrilla.DATA[4], kadrilla.DATA[5], kadrilla.DATA[6], kadrilla.DATA[7]);
    printf("%i%i%i%i%i%i%i%i|\n", kadrilla.FCS[0], kadrilla.FCS[1], kadrilla.FCS[2], kadrilla.FCS[3], kadrilla.FCS[4], kadrilla.FCS[5], kadrilla.FCS[6], kadrilla.FCS[7]);
    printf("-------------------------------------------------------------\n");
}
