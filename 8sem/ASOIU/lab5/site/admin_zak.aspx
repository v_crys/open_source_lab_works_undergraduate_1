﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="admin_zak.aspx.cs" Inherits="admin_zak" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="Номер_пользователя" HeaderText="Номер_пользователя" SortExpression="Номер_пользователя" />
            <asp:BoundField DataField="Номер_продукта" HeaderText="Номер_продукта" SortExpression="Номер_продукта" />
            <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
            <asp:BoundField DataField="id_заказа" HeaderText="id_заказа" SortExpression="id_заказа" />
        </Columns>
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#E3EAEB" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F8FAFA" />
        <SortedAscendingHeaderStyle BackColor="#246B61" />
        <SortedDescendingCellStyle BackColor="#D4DFE1" />
        <SortedDescendingHeaderStyle BackColor="#15524A" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" DeleteCommand="DELETE FROM [Заказы] WHERE [id] = @id" InsertCommand="INSERT INTO [Заказы] ([Номер пользователя], [Номер продукта], [Количество], [id_заказа]) VALUES (@Номер_пользователя, @Номер_продукта, @Количество, @id_заказа)" SelectCommand="SELECT [id], [Номер пользователя] AS Номер_пользователя, [Номер продукта] AS Номер_продукта, [Количество], [id_заказа] FROM [Заказы]" UpdateCommand="UPDATE [Заказы] SET [Номер пользователя] = @Номер_пользователя, [Номер продукта] = @Номер_продукта, [Количество] = @Количество, [id_заказа] = @id_заказа WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Номер_пользователя" Type="Int32" />
            <asp:Parameter Name="Номер_продукта" Type="Int32" />
            <asp:Parameter Name="Количество" Type="Int32" />
            <asp:Parameter Name="id_заказа" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Номер_пользователя" Type="Int32" />
            <asp:Parameter Name="Номер_продукта" Type="Int32" />
            <asp:Parameter Name="Количество" Type="Int32" />
            <asp:Parameter Name="id_заказа" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="Код" DataSourceID="SqlDataSource2">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
            <asp:BoundField DataField="Код" HeaderText="Код" InsertVisible="False" ReadOnly="True" SortExpression="Код" />
            <asp:BoundField DataField="Адрес_доставки" HeaderText="Адрес_доставки" SortExpression="Адрес_доставки" />
            <asp:BoundField DataField="Дата_доставки" HeaderText="Дата_доставки" SortExpression="Дата_доставки" />
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
        <RowStyle BackColor="White" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <SortedAscendingCellStyle BackColor="#EDF6F6" />
        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
        <SortedDescendingCellStyle BackColor="#D6DFDF" />
        <SortedDescendingHeaderStyle BackColor="#002876" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" DeleteCommand="DELETE FROM [инф_заказа] WHERE [Код] = @Код" InsertCommand="INSERT INTO [инф_заказа] ([Адрес доставки], [Дата доставки]) VALUES (@Адрес_доставки, @Дата_доставки)" SelectCommand="SELECT [Код], [Адрес доставки] AS Адрес_доставки, [Дата доставки] AS Дата_доставки FROM [инф_заказа]" UpdateCommand="UPDATE [инф_заказа] SET [Адрес доставки] = @Адрес_доставки, [Дата доставки] = @Дата_доставки WHERE [Код] = @Код">
        <DeleteParameters>
            <asp:Parameter Name="Код" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Адрес_доставки" Type="String" />
            <asp:Parameter Name="Дата_доставки" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Адрес_доставки" Type="String" />
            <asp:Parameter Name="Дата_доставки" Type="String" />
            <asp:Parameter Name="Код" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>

