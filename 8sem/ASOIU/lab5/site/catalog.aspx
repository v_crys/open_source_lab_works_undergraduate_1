﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="catalog.aspx.cs" Inherits="catalog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="auto-style7">
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged1">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                        <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
                        <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
                        <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
                        <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
                        <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" Visible="False" />
                        <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
                        <asp:ImageField DataImageUrlField="Путь_картинки">
                        </asp:ImageField>
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                    <SortedDescendingHeaderStyle BackColor="#820000" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Производитель], [Путь_картинки], [Количество] FROM [Товар]"></asp:SqlDataSource>
                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" Visible="False">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                        <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
                        <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
                        <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
                        <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" Visible="False" />
                        <asp:ImageField DataImageUrlField="Путь_картинки">
                        </asp:ImageField>
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                    <SortedDescendingHeaderStyle BackColor="#820000" />
                </asp:GridView>
                <br />
            </td>
            <td>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [id], [Название], [Описание], [Дата создания] AS Дата_создания, [Путь_логотип] FROM [Производитель]"></asp:SqlDataSource>
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource4" ForeColor="#333333" GridLines="None" Visible="False">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
                        <asp:BoundField DataField="Номер_товара" HeaderText="Номер_товара" SortExpression="Номер_товара" Visible="False" />
                        <asp:BoundField DataField="Название_характеристики" HeaderText="Название_характеристики" SortExpression="Название_характеристики" />
                        <asp:BoundField DataField="Значение_характеристики" HeaderText="Значение_характеристики" SortExpression="Значение_характеристики" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Номер товара] AS Номер_товара, [Название характеристики] AS Название_характеристики, [Значение характеристики] AS Значение_характеристики FROM [Характеристики] WHERE ([Номер товара] = @Номер_товара)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="0" Name="Номер_товара" SessionField="Prod" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" OnSelecting="SqlDataSource2_Selecting" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Путь_картинки] FROM [Товар] WHERE ([Производитель] = @Производитель)">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="1" Name="Производитель" SessionField="Manuf" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Добавить в корзину" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Название" DataValueField="id" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1" Visible="False">
                </asp:DropDownList>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Фильтр включить" />
            </td>
            <td>
                <asp:GridView ID="GridViewType" runat="server" AutoGenerateColumns="False" DataKeyNames="Код" DataSourceID="SqlDataSource5" Visible="False">
                    <Columns>
                        <asp:BoundField DataField="Код" HeaderText="Код" InsertVisible="False" ReadOnly="True" SortExpression="Код" />
                        <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="GridViewMan" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource6" Visible="False">
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                        <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
                        <asp:BoundField DataField="Описание" HeaderText="Описание" SortExpression="Описание" />
                        <asp:BoundField DataField="Дата_создания" HeaderText="Дата_создания" SortExpression="Дата_создания" />
                        <asp:BoundField DataField="Путь_логотип" HeaderText="Путь_логотип" SortExpression="Путь_логотип" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [id], [Название], [Описание], [Дата создания] AS Дата_создания, [Путь_логотип] FROM [Производитель]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [Код], [Название] FROM [Тип товара]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

