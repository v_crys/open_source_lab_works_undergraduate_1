﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Visible_auth(bool auth)
    {
        //Label1.Visible = true ^ auth;

        LabelLog.Visible = true ^ auth;
        LabelPass.Visible = true ^ auth;
        TextBoxLogin.Visible = true ^ auth;
        TextBoxPass.Visible = true ^ auth;
        ButtonEntry.Visible = true ^ auth;
        ButtonReg.Visible = true ^ auth;

        LinkButtonBucket.Visible = false ^ auth;
        ButtonExit.Visible = false ^ auth;

        ButtonLK.Visible = false ^ auth;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //получаем код пользователя
        long iduser = Convert.ToInt64(Session["iduser"]);

        bool auth = (iduser !=  0);

        if (iduser == 0)
            Label1.Text = "Авторизация";
        else
            Label1.Text = "Здравствуйте, " + Session["myuser"];

        Visible_auth(auth);

       

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        long iduser = Convert.ToInt64(Session["iduser"]);

        if ( iduser == 3 )
            Response.Redirect("index_admin.aspx");
        else
            Response.Redirect("index.aspx");
    }
protected void LinkButton2_Click(object sender, EventArgs e)
{
    Response.Redirect("catalog.aspx");
}
protected void LinkButton3_Click(object sender, EventArgs e)
{
    Response.Redirect("bucket.aspx");
}
protected void ButtonEntry_Click(object sender, EventArgs e)
{
    string myuser;
    long iduser;
    string dd, qq;
    dd = TextBoxLogin.Text;
    qq = TextBoxPass.Text;
    DataClassesDataContext db = new DataClassesDataContext();
    Label1.Text = "";
    try
    {
        var SelectedUser = (from item in db.Пользовательs
                            where item.Логин == dd && item.Хеш_пароля == qq
                            select item).Single();
        myuser = SelectedUser.Имя;
        Session["myuser"] = myuser;
        iduser = SelectedUser.ID;
        Session["iduser"] = iduser;
        //после автортзации выводим приветствие
        Label1.Text = "Hello" + myuser + " " + iduser;
        //прячем поля для ввода логина и пароля

        Visible_auth( true );

        if (iduser == 3)
            Response.Redirect("index_admin.aspx");
    }
    catch (Exception ex)
    {
        Label1.Text = ex.Message;
    }
}
protected void ButtonExit_Click(object sender, EventArgs e)
{
    Session.Clear();
    Response.Redirect("index.aspx");
}
protected void ButtonReg_Click(object sender, EventArgs e)
{
    Response.Redirect("registration.aspx");
}
protected void ButtonLK_Click(object sender, EventArgs e)
{
    Response.Redirect("LK.aspx");
}
}
