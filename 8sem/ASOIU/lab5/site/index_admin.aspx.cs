﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index_admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long iduser = Convert.ToInt64(Session["iduser"]);

        if ( iduser != 3 )
            Response.Redirect("index.aspx");
    }
}