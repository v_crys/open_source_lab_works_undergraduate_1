﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        TextBoxRegLogin.Text = "";
        TextBoxRegName.Text = "";
        TextBoxRegPass.Text = "";
        TextBoxRegPass2.Text = "";
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        //проверка срабатывания валидаторов
        //иначе данные не заполненой формы будут сохранены
        // Page.Validate(); можно добавить

        if ((TextBoxRegLogin.Text == "") || (TextBoxRegName.Text == "") || (TextBoxRegPass.Text == "") || (TextBoxRegPass2.Text == ""))
        {
            Label2.Text = "Поля не заполнены";
            return;
        }

        if (TextBoxRegPass.Text != TextBoxRegPass2.Text)
        {
            Label2.Text = "Пароли различнаются";
            return;
        }



        if (Page.IsValid)
        {
            try
            {
                Пользователь cust = new Пользователь();
                cust.Логин = TextBoxRegLogin.Text;
                cust.Имя = TextBoxRegName.Text;
                cust.Хеш_пароля = TextBoxRegPass.Text;

                //сведения о клиенте добавляются в таблицу
                db.Пользовательs.InsertOnSubmit(cust);
                // в этой точке s_user объект добавляется в объектную модель
                //изменения не будут выполнены пока не будет вызван метод SubmitChanges
                db.SubmitChanges();
                Label2.Text = "данные пользователя успешно внесены в БД";
            }
            catch
            {
                Label2.Text = "ошибка";
            }
        }
    }
}