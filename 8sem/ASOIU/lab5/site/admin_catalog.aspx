﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="admin_catalog.aspx.cs" Inherits="admin_catalog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" AllowPaging="True">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
            <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
            <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
            <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" />
            <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" DeleteCommand="DELETE FROM [Товар] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Товар] ([Цена], [Название], [Тип], [Производитель], [Путь_картинки], [Количество]) VALUES (@Цена, @Название, @Тип, @Производитель, @Путь_картинки, @Количество)" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Производитель], [Путь_картинки], [Количество] FROM [Товар]" UpdateCommand="UPDATE [Товар] SET [Цена] = @Цена, [Название] = @Название, [Тип] = @Тип, [Производитель] = @Производитель, [Путь_картинки] = @Путь_картинки, [Количество] = @Количество WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Цена" Type="Decimal" />
            <asp:Parameter Name="Название" Type="String" />
            <asp:Parameter Name="Тип" Type="Int32" />
            <asp:Parameter Name="Производитель" Type="Int32" />
            <asp:Parameter Name="Путь_картинки" Type="String" />
            <asp:Parameter Name="Количество" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Цена" Type="Decimal" />
            <asp:Parameter Name="Название" Type="String" />
            <asp:Parameter Name="Тип" Type="Int32" />
            <asp:Parameter Name="Производитель" Type="Int32" />
            <asp:Parameter Name="Путь_картинки" Type="String" />
            <asp:Parameter Name="Количество" Type="Int32" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="125px" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" AllowPaging="True" AutoGenerateRows="False" DataKeyNames="ID">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <Fields>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
            <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
            <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
            <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" />
            <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
</asp:DetailsView>

</asp:Content>

