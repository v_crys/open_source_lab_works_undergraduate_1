﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="auto-style7">
    <tr>
        <td>
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" DataSourceID="SqlDataSource5" DataTextField="Название" DataValueField="id">
        <asp:ListItem Selected="True" Value="10">All</asp:ListItem>
    </asp:DropDownList>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource2" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
            <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
            <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
            <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
            <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" Visible="False" />
            <asp:ImageField DataImageUrlField="Путь_картинки">
            </asp:ImageField>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />
    </asp:GridView>
        </td>
        <td>
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource6">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" Visible="False" />
                    <asp:BoundField DataField="Номер_товара" HeaderText="Номер_товара" SortExpression="Номер_товара" Visible="False" />
                    <asp:BoundField DataField="Название_характеристики" HeaderText="Название_характеристики" SortExpression="Название_характеристики" />
                    <asp:BoundField DataField="Значение_характеристики" HeaderText="Значение_характеристики" SortExpression="Значение_характеристики" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Номер товара] AS Номер_товара, [Название характеристики] AS Название_характеристики, [Значение характеристики] AS Значение_характеристики FROM [Характеристики] WHERE ([Номер товара] = @Номер_товара)">
                <SelectParameters>
                    <asp:SessionParameter DefaultValue="1" Name="Номер_товара" SessionField="ID_tovar" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="ID" DataSourceID="SqlDataSource3" Height="50px" Width="125px">
        <Fields>
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
            <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
            <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
            <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" />
        </Fields>
    </asp:DetailsView>
        </td>
        <td>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Добавить в корзину" />
        </td>
    </tr>
</table>
    <br>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Производитель], [Путь_картинки] FROM [Товар] WHERE ([Производитель] = @Производитель)">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="Производитель" SessionField="Proizvod" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [id], [Название], [Описание], [Дата создания] AS Дата_создания, [Путь_логотип] FROM [Производитель]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Производитель], [Путь_картинки] FROM [Товар] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="1" Name="ID" SessionField="ID_tovar" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Картинка], [Производитель] FROM [Товар]"></asp:SqlDataSource>
</asp:Content>

