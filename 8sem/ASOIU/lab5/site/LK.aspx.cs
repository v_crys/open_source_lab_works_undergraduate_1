﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LK : System.Web.UI.Page
{


    String Login, Name, Pass;

    protected void Page_Load(object sender, EventArgs e)
    {
        DataClassesDataContext db;
        Пользователь SelectedUser;

        if (!IsPostBack) // WARNING!!!
        {
            db = new DataClassesDataContext();
            int IDUser = Convert.ToInt16(Session["iduser"]);

            if (IDUser == 0)
                Response.Redirect("index.aspx");

            SelectedUser = (from item in db.Пользовательs
                            where item.ID == IDUser
                            select item).Single();

            try
            {
                Login = SelectedUser.Логин;
                Name = SelectedUser.Имя;
                Pass = SelectedUser.Хеш_пароля;


                TextBoxLogin.Text = Login;
                TextBoxName.Text = Name;
                TextBoxPass.Text = Pass;
                TextBoxPass2.Text = Pass;



            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        TextBoxLogin.Text = "";
        TextBoxName.Text = "";
        TextBoxPass.Text = "";
        TextBoxPass2.Text = "";
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if ((TextBoxLogin.Text == "") || (TextBoxName.Text == "") || (TextBoxPass.Text == "") || (TextBoxPass2.Text == ""))
        {
            Label2.Text = "Ошибка";
            return;
        }

        if (TextBoxPass.Text != TextBoxPass2.Text)
        {
            Label2.Text = "Ошибка";
            return;
        }

    

        DataClassesDataContext db = new DataClassesDataContext();
        int IDUser = Convert.ToInt16(Session["iduser"]);
        var UpdateData = (from item in db.Пользовательs
                             where item.ID == IDUser
                             select item).Single();

        try
        {
            
            UpdateData.Имя = TextBoxName.Text;
            UpdateData.Логин = TextBoxLogin.Text;
            UpdateData.Хеш_пароля = TextBoxPass.Text;
            db.SubmitChanges();

            Session["myuser"] = TextBoxName.Text;
            
            Label2.Text = "Изменено";
        }
        catch (Exception ex)
        {
            Label2.Text = "ошибка";
        }
    }
}