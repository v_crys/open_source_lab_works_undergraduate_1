﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="bucket.aspx.cs" Inherits="bucket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Visible="False">
    <Columns>
        <asp:BoundField DataField="Код_товара" HeaderText="Код_товара" SortExpression="Код_товара" />
        <asp:BoundField DataField="Код_пользователя" HeaderText="Код_пользователя" SortExpression="Код_пользователя" />
        <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
    </Columns>
</asp:GridView>
<asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style1" DataKeyNames="ID" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" AllowPaging="True" AllowSorting="True">
    <AlternatingRowStyle BackColor="White" />
    <Columns>
        <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
        <asp:BoundField DataField="Цена" HeaderText="Цена" SortExpression="Цена" />
        <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
        <asp:BoundField DataField="Тип" HeaderText="Тип" SortExpression="Тип" />
        <asp:BoundField DataField="Производитель" HeaderText="Производитель" SortExpression="Производитель" />
        <asp:BoundField DataField="Путь_картинки" HeaderText="Путь_картинки" SortExpression="Путь_картинки" Visible="False" />
        <asp:BoundField DataField="Количество" HeaderText="Количество" SortExpression="Количество" />
        <asp:ImageField DataImageUrlField="Путь_картинки">
        </asp:ImageField>
    </Columns>
    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
    <SortedAscendingCellStyle BackColor="#FDF5AC" />
    <SortedAscendingHeaderStyle BackColor="#4D0000" />
    <SortedDescendingCellStyle BackColor="#FCF6C0" />
    <SortedDescendingHeaderStyle BackColor="#820000" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" DeleteCommand="DELETE FROM [Товар] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Товар] ([Цена], [Название], [Тип], [Производитель], [Путь_картинки], [Количество]) VALUES (@Цена, @Название, @Тип, @Производитель, @Путь_картинки, @Количество)" SelectCommand="SELECT [ID], [Цена], [Название], [Тип], [Производитель], [Путь_картинки], [Количество] FROM [Товар]" UpdateCommand="UPDATE [Товар] SET [Цена] = @Цена, [Название] = @Название, [Тип] = @Тип, [Производитель] = @Производитель, [Путь_картинки] = @Путь_картинки, [Количество] = @Количество WHERE [ID] = @ID">
    <DeleteParameters>
        <asp:Parameter Name="ID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Цена" Type="Decimal" />
        <asp:Parameter Name="Название" Type="String" />
        <asp:Parameter Name="Тип" Type="Int32" />
        <asp:Parameter Name="Производитель" Type="Int32" />
        <asp:Parameter Name="Путь_картинки" Type="String" />
        <asp:Parameter Name="Количество" Type="Int32" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="Цена" Type="Decimal" />
        <asp:Parameter Name="Название" Type="String" />
        <asp:Parameter Name="Тип" Type="Int32" />
        <asp:Parameter Name="Производитель" Type="Int32" />
        <asp:Parameter Name="Путь_картинки" Type="String" />
        <asp:Parameter Name="Количество" Type="Int32" />
        <asp:Parameter Name="ID" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [Код товара] AS Код_товара, [Код пользователя] AS Код_пользователя, [Количество] FROM [Корзина] WHERE ([Код пользователя] = @Код_пользователя)">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="Код_пользователя" SessionField="iduser" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
    Дата доставки: <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
    Адрес доставки: <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    <br />

<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Удалить" />
<asp:Button ID="Button2" runat="server" Text="Заказать" OnClick="Button2_Click" />

    <br />
    <br />
    <asp:GridView ID="GridViewType" runat="server" AutoGenerateColumns="False" DataKeyNames="Код" DataSourceID="SqlDataSource3" Visible="False">
        <Columns>
            <asp:BoundField DataField="Код" HeaderText="Код" InsertVisible="False" ReadOnly="True" SortExpression="Код" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
        </Columns>
    </asp:GridView>
    <asp:GridView ID="GridViewMan" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource4" Visible="False">
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="Название" HeaderText="Название" SortExpression="Название" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [id], [Название] FROM [Производитель]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:Shop_2ConnectionString %>" SelectCommand="SELECT [Код], [Название] FROM [Тип товара]"></asp:SqlDataSource>

</asp:Content>

