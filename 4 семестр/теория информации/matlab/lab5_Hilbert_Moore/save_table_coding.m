function [ ] = save_table_coding( ax, ay, az, Table_in )
    
    alf = {ax( 1 )};
    for i = 2 : size( ax, 2 )
        alf = [ alf, {ax( i )} ];
    end
    
    alf = [ alf, ay, az ];
    
    File = table( alf', Table_in' );
    writetable( File, 'coding.xls' );

end

