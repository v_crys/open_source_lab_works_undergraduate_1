function [ a, Nz ] = statistics_z(z,N)
%a = string (' ');
a = unique (z);
Nz = zeros(size(a,2),1);

for i = 1:size(a,2)
    for j = 1:N-2
        if(a(i) == z(j))
            Nz(i) = Nz(i)+1;
        end
    end
end

end


