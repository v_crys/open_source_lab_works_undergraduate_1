function [ Table_coding ] = Hilbert_Moone_coder( Px )
    %----- create servic matrix
    Len = zeros( 1, size( Px, 2 ) );
    Q = zeros( 1, size( Px, 2 ) );
    Q_code_word = zeros( 1, size( Px, 2 ) );
    
    %----- calculated Len, Q, Q_code_word
    Q( 1 ) = 0;
    for i = 1 : size( Px, 2 )   
        if ( i > 1 ) 
            Q( i ) = Q( i - 1 ) + Px( i - 1 );
        end
        
        Q_code_word( i ) = Q( i ) + Px( i ) / 2;
        
        Len( i ) = -floor( log( Px( i ) / 2 ) / log( 2 ) ); 
    end
    
    %------ expension in two pow (create table coding)
    Table_coding = zeros( 1, size( Px, 2 ) );
    for i = 1 : size( Px, 2 )
        Table_coding( i ) = 2;
        my_P = Q_code_word( i );
        for j = 1 : Len( i )
            Table_coding( i ) = Table_coding( i ) * 10;
            if ( my_P - ( 2 ^ ( -j ) ) >= 0 )
                Table_coding( i ) = Table_coding( i ) + 1;
                my_P = my_P - ( 2 ^ ( -j ) );
            end
        end
    end
    
end

