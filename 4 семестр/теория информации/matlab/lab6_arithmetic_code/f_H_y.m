function [ H_y H_y_n H_conv_y] = f_H_y( PyIy, Hx_conv )
    
    H_y = 0;
    for i = 1:size( PyIy, 2 )
       H_y = H_y + PyIy( i ); 
    end

    H_y_n = H_y / 2;
    H_conv_y = H_y - Hx_conv;
end

