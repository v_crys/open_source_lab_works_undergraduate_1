function [ ] = Save_Xaffman_Table( ax, Nx, ay, Ny, az, Nz, Table_Se, Table_De, Table_Te, N )
    
    alf = {ax( 1 )};
    for i = 2 : size( ax, 2 )
        alf = [ alf, {ax( i )} ];
    end
    
    LenX = zeros( 1, size( Nx, 2 ) );
    e_X = 0;
    for i = 1 : size( Nx, 2)
        LenX( i ) = round( log( Table_Se( i ) ) / log( 10 ) );
        e_X = e_X + (LenX( i ) * Nx( i ) / N);
    end
    
    LenY = zeros( 1, size( Ny, 2 ) );
    e_Y = 0;
    for i = 1 : size( Ny, 2)
        LenY( i ) = round( log( Table_De( i ) ) / log( 10 ) );
        e_Y = e_Y + LenY( i ) * Ny( i ) / ( N - 1 );
    end
    
    
    LenZ = zeros( 1, size( Nz, 2 ) );
    e_Z = 0;
    for i = 1 : size( Nz, 2)
        LenZ( i ) = round( log( Table_Te( i ) ) / log( 10 ) );
        e_Z = e_Z + LenZ( i ) * Nz( i ) / ( N - 2 );
    end
    
    dop_X = zeros( 1, size( Nx, 2 ) );
    dop_X( 1 ) = e_X ;%/ size( Nx, 2 );
    dop_X( 2 ) = dop_X( 1 );
    File_Se = table( alf', Nx' , Table_Se', LenX', dop_X' );
    writetable( File_Se, 'C_Xaffman_Se.xls' );

    dop_Y = zeros( 1, size( Ny, 2 ) );
    dop_Y( 1 ) = e_Y ;%/ size( Ny, 2 );
    dop_Y( 2 ) = dop_Y( 1 ) / 2;
    File_De = table( ay', Ny', Table_De', LenY', dop_Y' );
    writetable( File_De, 'C_Xaffman_De.xls' );
    
    dop_Z = zeros( 1, size( Nz, 2 ));
    dop_Z( 1 ) = e_Z ;%/ size( Nz, 2 );
    dop_Z( 2 ) = dop_Z( 1 ) / 3;
    File_Te = table( az', Nz', Table_Te', LenZ', dop_Z' );
    writetable( File_Te, 'C_Xaffman_Te.xls' );
end

