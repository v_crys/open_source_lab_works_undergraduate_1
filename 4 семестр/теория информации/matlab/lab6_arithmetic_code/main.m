%------ lab 1
global c N  x y z   ax ay az    Nx Ny Nz    Px Py Pz    Ix Iy Iz    PxIx PyIy PzIz 
     
[ c N x y z ] = ReadFromFile;

[ ax, Nx, Px, Ix, PxIx ] = statistics_x( x, N );
[ ay, Ny, Py, Iy, PyIy ] = statistics_y( y, N );
[ az, Nz, Pz, Iz, PzIz ] = statistics_z( z, N );

WriteToFileX( ax, Nx, Px, Ix, PxIx );
WriteToFileY( ay, Ny, Py, Iy, PyIy );
WriteToFileZ( az, Nz, Pz, Iz, PzIz );

%------------- lab 2
global H_x H_x_n H_x_conv      H_y H_y_n H_y_conv        H_z H_z_n H_z_conv  

[ H_x H_x_n H_x_conv ] = f_H_x( PxIx );
[ H_y H_y_n H_y_conv ] = f_H_y( PyIy, H_x_conv );
[ H_z H_z_n H_z_conv ] = f_H_z( PzIz, H_x_conv, H_y_conv );

%------------- lab 3
global table_xaffman_SE  table_xaffman_DE  table_xaffman_TE

[ table_xaffman_SE ] = Xaffman_coder( Nx, N );
[ table_xaffman_DE ] = Xaffman_coder( Ny, N );
[ table_xaffman_TE ] = Xaffman_coder( Nz, N );
Save_Xaffman_Table( ax, Nx, ay, Ny, az, Nz, table_xaffman_SE, table_xaffman_DE, table_xaffman_TE, N );


%------------- lab 4
global table_Shannon_SE SI_x table_Shannon_DE SI_y table_Shannon_TE SI_z
[ table_Shannon_SE SI_x] = Shannon_coder( Px );
[ table_Shannon_DE SI_y] = Shannon_coder( Py );
[ table_Shannon_TE SI_z] = Shannon_coder( Pz );
Save_Shannon_Table( ax, Nx, ay, Ny, az, Nz, table_Shannon_SE, SI_x, table_Shannon_DE, SI_y, table_Shannon_TE, SI_z, N );


%------------- lab 5
global table_Hilbert_Moone_SE table_Hilbert_Moone_DE table_Hilbert_Moone_TE
[ table_Hilbert_Moone_SE ] = Hilbert_Moone_coder( Px );
[ table_Hilbert_Moone_DE ] = Hilbert_Moone_coder( Py );
[ table_Hilbert_Moone_TE ] = Hilbert_Moone_coder( Pz );
Save_Hilbert_Moone_Table( ax, Nx, ay, Ny, az, Nz, table_Hilbert_Moone_SE, table_Hilbert_Moone_DE, table_Hilbert_Moone_TE, N );

%----------- lab 6
global len_algebr code_algebr
[ code_algebr len_algebr ] = Arithmetic_coder( c, Px, ax, 1 );
[ code_algebr len_algebr ] = Arithmetic_coder( c, Py, ay, 2 );
[ code_algebr len_algebr ] = Arithmetic_coder( c, Pz, az, 3 );