function [ Code_word Len ] = Arithmetic_coder( Msg_in, Px, ax, flag_ )
    Msg_in_buf = Msg_in;
    
    if ( flag_ == 1 )
        fileID = fopen('out_lab6_X.txt','w');
        
        ax_cell = {ax( 1 )};
        for i = 2 : size( ax, 2 )
            ax_cell = [ ax_cell, {ax( i )} ];
        end
        ax = ax_cell;
    else if (flag_ == 2 )
            fileID = fopen('out_lab6_Y.txt','w');
        else if ( flag_ == 3 )
                fileID = fopen('out_lab6_Z.txt','w');
            else 
                printf( 'ERROR' );
            end
        end
    end
    
            
    if ( fileID == -1 ) 
        error( 'File is not opened' );
    end
    
  %  fprintf( fileID, 'CodeWord: ');
    
for glob_i = 0 : size( Msg_in, 2 ) / 6 - 1
    Msg_in = Msg_in_buf( (glob_i * 6 + 1) : ((glob_i + 1) * 6 ) );
    

    %--------- create massage propability
    Msg = zeros( 1, size( Msg_in, 2 ) / flag_ );
    for i = 1 : size( Msg_in, 2 ) / flag_
        for j = 1 : size( ax, 2 )
            if ( ax{ j } == Msg_in( i : i + flag_ - 1 ) )
                Msg( i ) = j;
                break;
            end
        end
    end
    
    Msg = Msg';

    %--------- cumulative probability
    Q = zeros( 1, size( ax, 2 ) );
    Q( 1 ) = 0;
    for i = 2 : size( ax, 2 ) + 1
            Q( i ) = Q( i - 1 ) + Px( i - 1 );
    end
    
    %--------- calculate intervals
    left_border = zeros( 1, size( Msg, 1 ) + 1 );
    right_border = zeros( 1, size( Msg, 1 ) + 1 );
    radius_interval = zeros( 1, size( Msg, 1 ) + 1 );
    
    left_border( 1 ) = 0;
    right_border( 1 ) = 1;
    radius_interval( 1 ) = 1;
    
    
    for i = 2 : size( Msg, 1 ) + 1
        Symbol = Msg( i - 1 );
        left_border( i ) = left_border( i - 1 ) + Q( Symbol ) * radius_interval( i - 1 );
        right_border( i ) = left_border( i - 1 ) + Q( Symbol + 1 ) * radius_interval( i - 1 );
        radius_interval( i ) = right_border( i ) - left_border( i );
    end
    
    Len =-floor( log( radius_interval( i ) ) / log( 2 ) ); 
        
    %----------- write output file, code word
    Code_word = 2;
    my_P = left_border( size( Msg, 1 ) + 1 );
    for j = 1 : Len
        Code_word = Code_word * 10;
        if ( my_P - ( 2 ^ ( -j ) ) >= 0 )
            Code_word = Code_word + 1;
            my_P = my_P - ( 2 ^ ( -j ) );
            
            fprintf( fileID, '1' );
        else
            fprintf( fileID, '0' );
        end
    end
    
end
    fclose(fileID);

end

