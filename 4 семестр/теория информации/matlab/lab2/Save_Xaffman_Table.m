function [ ] = Save_Xaffman_Table( ax, ay, az, Table_Se, Table_De, Table_Te )
    
    alf = {ax( 1 )};
    for i = 2 : size( ax, 2 )
        alf = [ alf, {ax( i )} ];
    end
    
    File_Se = table( alf', Table_Se' );
    writetable( File_Se, 'C_Xaffman_Se.xls' );

    File_De = table( ay', Table_De' );
    writetable( File_De, 'C_Xaffman_De.xls' );
    
    File_Te = table( az', Table_Te' );
    writetable( File_Te, 'C_Xaffman_Te.xls' );
end

