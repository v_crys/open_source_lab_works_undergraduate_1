global c N  x y z   ax ay az    Nx Ny Nz    Px Py Pz    Ix Iy Iz    PxIx PyIy PzIz 

[ c N x y z ] = ReadFromFile;

[ ax, Nx, Px, Ix, PxIx ] = statistics_x( x, N );
[ ay, Ny, Py, Iy, PyIy ] = statistics_y( y, N );
[ az, Nz, Pz, Iz, PzIz ] = statistics_z( z, N );

WriteToFileX( ax, Nx, Px, Ix, PxIx );
WriteToFileY( ay, Ny, Py, Iy, PyIy );
WriteToFileZ( az, Nz, Pz, Iz, PzIz );