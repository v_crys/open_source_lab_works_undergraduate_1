//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------

#define round( X ) ( (X) + 0.5 )

#define L_LEFT 	65
#define L_TOP   87
#define L_RIGHT	68
#define L_DOWN 	83
#define L_ANG_L 81
#define L_ANG_R	69
#define L_SCL_P 90
#define L_SCL_M 88


#define STEP_MOVE 10
#define STEP_ANG  10
#define STEP_SCALE 10

int L_X1 		= 50,
	L_Y1 		= 50,
	L_ANG 		= 0,
	L_SCALE 	= 100;

//int FIGURE_X[ 5 ] = { 10, 10, 40, 40, 40 };
//int FIGURE_Y[ 5 ] = { -10, -10, 0, 0, 0 };
//int FIGURE_X[ 5 ] = { 10, 30, 40, 40, 10 };
//int FIGURE_Y[ 5 ] = { -10, -10, 0, 0, 10 };

int FIGURE_X[ 5 ] = { 10, 30, 40, 30, 10 };
int FIGURE_Y[ 5 ] = { -10, -10, 0, 10, 10 };

void Print_figure( TImage *img, int X1, int Y1, int ang, int scale);


//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void Print_figure( TImage *img, int X1, int Y1, int ang, int scale )
{
	//-------- calculation the end line
	float ang_rad = ( (float)(ang * M_PI) ) / 180 ;
	int X2[ 5 ];
	int Y2[ 5 ];
	int i;

	img->Canvas->MoveTo( X1, Y1 );
	for (i = 0; i < 5; i++) {
		float ang_dop = atan2( FIGURE_Y[ i ] , FIGURE_X[ i ] );
		float scale_2 = sqrt( FIGURE_X[ i ] * FIGURE_X[ i ] + FIGURE_Y[ i ] * FIGURE_Y[ i ] );
		X2[ i ] = round( cos( ang_rad + ang_dop ) * scale_2 * scale / 100 + X1 );
		Y2[ i ] = round( sin( ang_rad + ang_dop ) * scale_2 * scale / 100 + Y1 );


		img->Canvas->LineTo( X2[ i ], Y2[ i ] );
	}
	img->Canvas->LineTo( X1, Y1 );

	//------- print debug
   /*	Form3->Label1->Caption = "X2 = " + IntToStr( X2 );
	Form3->Label2->Caption = "Y2 = " + IntToStr( Y2 ); */
	Form3->Label4->Caption = "ANGLE = " + IntToStr( ang );



	return;
}

void __fastcall TForm3::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)

{
	//------- print key down
	Label3->Caption = "Press key: " + IntToStr( Key );

	switch ( Key ) {
	//------ left line
		 //------------- MOVE
		 case L_LEFT:
			L_X1 -= STEP_MOVE;
			break;

		 case L_RIGHT:
			L_X1 += STEP_MOVE;
			break;

		 case L_TOP:
			L_Y1 -= STEP_MOVE;
			break;

		 case L_DOWN:
			L_Y1 += STEP_MOVE;
			break;

		 //-------------- ANGEL
		 case L_ANG_L:
			L_ANG -= STEP_ANG;
			break;

		 case L_ANG_R:
			L_ANG += STEP_ANG;
			break;

		 //--------------- SCALE
		 case L_SCL_P:
			L_SCALE += STEP_SCALE;
			break;

		 case L_SCL_M:
			L_SCALE -= STEP_SCALE;
			break;
	}

	Image1->Canvas->FillRect(Rect(0,0,Image1->Width,Image1->Height));
	Print_figure( Image1, L_X1, L_Y1, L_ANG, L_SCALE );
}
//---------------------------------------------------------------------------

