//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
//---------------------------------

#define round( X ) ( (X) + 0.49 )

#define L_LEFT 	65
#define L_TOP   87
#define L_RIGHT	68
#define L_DOWN 	83
#define L_ANG_L 81
#define L_ANG_R	69
#define L_SCL_P 90
#define L_SCL_M 88


#define STEP_MOVE 10
#define STEP_ANG  10
#define STEP_SCALE 10

int L_X1 		= 50,
	L_Y1 		= 50,
	L_ANG 		= 0,
	L_SCALE 	= 100;


//int FIGURE_X[ 5 ] = { 10, 10, 0, 0, 0 };
//int FIGURE_Y[ 5 ] = { 0, 10, 10, 10, 10 };
#define POINTS 5
int FIGURE_X[ POINTS ] = { 10, 10, 10, 10, 10 };
int FIGURE_Y[ POINTS ] = { -10, 10, 10, 10, 10 };

//int FIGURE_X[ POINTS ] = { 10, 30, 40, 30, 10 };
//int FIGURE_Y[ POINTS ] = { -10, -10, 0, 10, 10 };

void Print_figure( TImage *img, int X1, int Y1, int ang, int scale);

void coloring_figure_line( TImage *img, int *arr_point_X, int *arr_point_Y, int points );
void coloring_figure_pixels( TImage *img, int X, int Y );

void find_two_cros_line( int *arr_point_X, int *arr_point_Y, int points, int Y,
	int *loc_X1, int *loc_X2 );

//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void Print_figure( TImage *img, int X1, int Y1, int ang, int scale )
{
	//-------- calculation the end line
	float ang_rad = ( (float)(ang * M_PI) ) / 180 ;
	int X2[ POINTS + 1 ];
	int Y2[ POINTS + 1 ];
	int i;


	for (i = 0; i < POINTS; i++) {
		float ang_dop = atan2( FIGURE_Y[ i ] , FIGURE_X[ i ] );
		float scale_2 = sqrt( FIGURE_X[ i ] * FIGURE_X[ i ] + FIGURE_Y[ i ] * FIGURE_Y[ i ] );
		X2[ i ] = round( cos( ang_rad + ang_dop ) * scale_2 * scale / 100 + X1 );
		Y2[ i ] = round( sin( ang_rad + ang_dop ) * scale_2 * scale / 100 + Y1 );
	}

	X2[ 5 ] = X1;
	Y2[ 5 ] = Y1;

	//----------- liner
	/*coloring_figure_line( img, X2, Y2, POINTS + 1 );

	*/
	img->Canvas->Pen->Color = clBlue;
	img->Canvas->MoveTo( X1, Y1 );
	for (i = 0; i < POINTS + 1; i++)
		img->Canvas->LineTo( X2[ i ], Y2[ i ] );
	img->Canvas->Pen->Color = clBlack;



	//-------- pixels
	for (i = 0; i < POINTS + 1; i++) {
		if ( X2[ i ] >= img->Width) X2[ i ] = img->Width;
		if ( X2[ i ] <= 0 ) X2[ i ] = 0;
		if ( Y2[ i ] >= img->Height ) Y2[ i ] = img->Height;
		if ( Y2[ i ] <= 0 ) Y2[ i ] = 0;
	}

	//------------ find center pixel
	int min = Y2[ 0 ],
		max = Y2[ 0 ];
	for (i = 0; i < POINTS + 1; i++) {
		if ( Y2[ i ] > max) max = Y2[ i ];
		if ( Y2[ i ] < min ) min = Y2[ i ];
	}

	int X_left, X_right;
	find_two_cros_line( X2, Y2, POINTS + 1, ( min + max ) >> 1, &X_left, &X_right);

  	if ( X_left < 0 ) X_left = 0;
	if ( X_right < 0 ) X_right = 0;
	if ( min < 0 ) min = 0;
	if ( max < 0 )  max = 0;

	if ( (( X_left <= 0 ) && ( X_right <= 0 )) || ( (min <= 0 ) && ( max <= 0 ) ) )
		return;

	if ( X_left >= img->Width ) X_left = img->Width;
	if ( X_right >= img->Width ) X_right = img->Width;
	if ( min >= img->Height ) min = img->Height;
	if ( max >= img->Height )  max = img->Height;

	if ( ( ( X_left >= img->Width ) && ( X_right >= img->Width ) ) ||
		  ( ( min >= img->Height ) && ( max >= img->Height )) )
			return;

	img->Canvas->Pixels[ ( X_left + X_right ) >> 1 ][ ( min + max ) >> 1 ] = clBlue;
	coloring_figure_pixels( img, ( X_left + X_right ) >> 1, ( min + max ) >> 1 );



	return;
}

void coloring_figure_line( TImage *img, int *arr_point_X, int *arr_point_Y, int points )
{
	int i, j;


	int min = arr_point_Y[ 0 ],
		max = arr_point_Y[ 0 ],
		pos_min = 0,
		pos_max = 0;

	for (i = 0; i < points; i++) {
		if ( arr_point_Y[ i ] < min ) {
			min = arr_point_Y[ i ];
			pos_min = i;
		}

		 if ( arr_point_Y[ i ] > max ) {
			max = arr_point_Y[ i ];
			pos_max = i;
		}

	}

	for (i = min + 1; i < max; i++) {
		//------- find two points
		int loc_X1;
		int loc_X2;

		find_two_cros_line( arr_point_X, arr_point_Y, points , i, &loc_X1, &loc_X2 );

		img->Canvas->Pen->Color = clBlue;
		img->Canvas->MoveTo( loc_X1, i );
		img->Canvas->LineTo( loc_X2, i );
		img->Canvas->Pen->Color = clBlack;
	}
	return;
}

void coloring_figure_pixels( TImage *img, int X, int Y )
{
	int i;

	if ( img->Canvas->Pixels[ X + 1 ][ Y ] == clWhite ) {
		img->Canvas->Pixels[ X + 1 ][ Y ] = clBlue;
		coloring_figure_pixels( img, X + 1, Y );
	}

	if ( img->Canvas->Pixels[ X - 1 ][ Y ] == clWhite ) {
		img->Canvas->Pixels[ X - 1 ][ Y ] = clBlue;
		coloring_figure_pixels( img, X - 1, Y );
	}

	if ( img->Canvas->Pixels[ X ][ Y + 1 ] == clWhite ) {
		img->Canvas->Pixels[ X ][ Y + 1 ] = clBlue;
		coloring_figure_pixels( img, X , Y + 1 );
	}

	if ( img->Canvas->Pixels[ X ][ Y - 1 ] == clWhite ) {
		img->Canvas->Pixels[ X ][ Y - 1 ] = clBlue;
		coloring_figure_pixels( img, X, Y - 1 );
	}
}


void find_two_cros_line( int *arr_point_X, int *arr_point_Y, int points, int Y,
	int *loc_X1, int *loc_X2 )
{
int j;
int flag = 0;
		for (j = 0; j < points - 1; j++ ) {
			if ( (( arr_point_Y[ j ] > Y ) && ( arr_point_Y[ j + 1 ] <= Y ) ) ||
				(( arr_point_Y[ j ] < Y ) && ( arr_point_Y[ j + 1 ] >= Y ) ) ) {

				float d_x =  arr_point_X[ j ] - arr_point_X[ j + 1 ] ;
				float d_y =  arr_point_Y[ j ] - arr_point_Y[ j + 1 ] ;
				float k = (float)d_x / d_y;

				if ( flag == 0) {
					*loc_X1 = round( k * ( Y - arr_point_Y[ j ] ) ) + arr_point_X[ j ] ;
				} else
				{
					*loc_X2 = round( k * ( Y - arr_point_Y[ j ] ) ) + arr_point_X[ j ];
					break;
				}

				flag++;
			}
		}

		if ( (( arr_point_Y[ points - 1 ] > Y ) && ( arr_point_Y[ 0 ] <= Y ) ) ||
				(( arr_point_Y[ points - 1 ] < Y ) && ( arr_point_Y[ 0 ] >= Y ) ) ) {

				float d_x =  arr_point_X[ points - 1 ] - arr_point_X[ 0 ] ;
				float d_y =  arr_point_Y[ points - 1 ] - arr_point_Y[ 0 ] ;
				float k = (float)d_x / d_y;

				*loc_X2 = round( k * ( Y - arr_point_Y[ points - 1 ] ) ) + arr_point_X[ points - 1 ];
		}


}

void __fastcall TForm3::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)

{
	//------- print key down
  //	Label3->Caption = "Press key: " + IntToStr( Key );

	switch ( Key ) {
	//------ left line
		 //------------- MOVE
		 case L_LEFT:
			L_X1 -= STEP_MOVE;
			break;

		 case L_RIGHT:
			L_X1 += STEP_MOVE;
			break;

		 case L_TOP:
			L_Y1 -= STEP_MOVE;
			break;

		 case L_DOWN:
			L_Y1 += STEP_MOVE;
			break;

		 //-------------- ANGEL
		 case L_ANG_L:
			L_ANG -= STEP_ANG;
			break;

		 case L_ANG_R:
			L_ANG += STEP_ANG;
			break;

		 //--------------- SCALE
		 case L_SCL_P:
			L_SCALE += STEP_SCALE;
			break;

		 case L_SCL_M:
			L_SCALE -= STEP_SCALE;
			break;
	}

	Image1->Canvas->FillRect(Rect(0,0,Image1->Width,Image1->Height));
	Print_figure( Image1, L_X1, L_Y1, L_ANG, L_SCALE );
}
//---------------------------------------------------------------------------

