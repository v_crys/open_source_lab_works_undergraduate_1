object Form3: TForm3
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Lab_4'
  ClientHeight = 209
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 460
    Height = 160
    Align = alClient
    ExplicitTop = -6
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 160
    Width = 460
    Height = 49
    Align = alBottom
    Caption = 'Debug'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 112
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Label2'
    end
    object Label3: TLabel
      Left = 224
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Label3'
    end
    object Label4: TLabel
      Left = 336
      Top = 24
      Width = 31
      Height = 13
      Caption = 'Label4'
    end
  end
end
