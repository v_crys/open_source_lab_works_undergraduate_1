object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Lab_2'
  ClientHeight = 190
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 447
    Height = 132
    Align = alClient
    AutoSize = True
    ExplicitLeft = 96
    ExplicitTop = 16
    ExplicitWidth = 105
    ExplicitHeight = 105
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 132
    Width = 447
    Height = 58
    Align = alBottom
    Caption = 'Debug'
    TabOrder = 0
    ExplicitTop = 142
    ExplicitWidth = 457
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 64
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label2'
    end
    object Label3: TLabel
      Left = 128
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Label3'
    end
    object Label4: TLabel
      Left = 384
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label4'
    end
    object Label5: TLabel
      Left = 8
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Label5'
    end
    object Label6: TLabel
      Left = 64
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Label6'
    end
    object Label7: TLabel
      Left = 128
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Label7'
    end
    object Label8: TLabel
      Left = 180
      Top = 15
      Width = 31
      Height = 13
      Caption = 'Label8'
    end
    object Label9: TLabel
      Left = 180
      Top = 34
      Width = 31
      Height = 13
      Caption = 'Label9'
    end
    object Edit1: TEdit
      Left = 273
      Top = 34
      Width = 121
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 0
      Text = '0'
    end
    object Button1: TButton
      Left = 400
      Top = 32
      Width = 54
      Height = 25
      Caption = 'Draw'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
end
