
//using namespace TVector_SUAI_V_crys;

double & _fastcall TVector_v::operator[]( int Number )
{
	if ( ( Number >= _size ) || ( Number < 0 ) )
		throw Exception( "Vector::Error index []" );

	return (_data[ Number ]);
}

_fastcall TVector_v::TVector_v( const TVector_v &src ) :
    _size( 0 ), _data( NULL )
{
    int i;
    set_size( src._size );

    for ( i = 0; i < src._size; i++ )
        _data[ i ] = src._data[ i ];
}

TVector_v & _fastcall TVector_v::operator=( const TVector_v &src )
{
    if ( this == &src )
        return *this;


    set_size( src._size );
    int i;

    for ( i = 0; i < src._size; i++ )
        _data[ i ] = src._data[ i ];

    return *this;
}


TVector_v _fastcall TVector_v::operator+( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator+: size don't equals" );

    TVector_v res( _size );
    int i;
    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] + src_r._data[ i ];

    return res;
}

TVector_v _fastcall TVector_v::operator-( )
{
    TVector_v res( _size );
    int i;
    for ( i = 0; i < _size; i++ )
        res._data[ i ] = -_data[ i ];

    return res;
}

TVector_v _fastcall TVector_v::operator-( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator-: size don't equals" );

    TVector_v res( _size );
    res = src_r;
    res = *this + ( -res );

    return res;
}

TVector_v _fastcall TVector_v::operator*( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator*: size don't equals" );

    int i;
    TVector_v res( _size );

    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] * src_r._data[ i ];

    return res;
}

TVector_v _fastcall TVector_v::operator*( int r )
{
    TVector_v res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        res._data[ i ] = _data[ i ] * r;

    return res;
}

TVector_v _fastcall operator*( int l, const TVector_v &src_r )
{
    TVector_v res( src_r._size );
    res = src_r;
    res = res * l;
    return res;
}

TVector_v _fastcall TVector_v::operator>>( int SHR )
{
    if ( SHR < 0 )
		throw Exception( "Vector::Operator>>: arg < 0" );

    TVector_v res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        if ( i < SHR )
            res[ i ] = 0;
        else
            res[ i ] = _data[ i - SHR ];

    return res;
}

TVector_v _fastcall TVector_v::operator<<( int SHL )
{
	if ( SHL < 0 )
		throw Exception( "Vector::Operator<<: arg < 0" );

    TVector_v res( _size );
    int i;

    for ( i = 0; i < _size; i++ )
        if ( i + SHL >= _size )
            res[ i ] = 0;
        else
            res[ i ] = _data[ i + SHL ];

	return res;
}

bool _fastcall operator== ( const TVector_v &src_l, const TVector_v &src_r )
{
    int i;
    if ( src_l._size != src_r._size )
        return false;

    for ( i = 0; i < src_l._size; i++ )
        if ( src_l._data[ i ] != src_r._data[ i ] )
            return false;

    return true;
}

bool _fastcall operator!=( const TVector_v &src_l, const TVector_v &src_r )
{
    return !( src_l == src_r );
}

TVector_v & _fastcall TVector_v::operator+= ( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator+=: size don't equals"  );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] += src_r._data[ i ];

    return *this;
}

TVector_v & _fastcall TVector_v::operator-= ( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator-=: size don't equals"  );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] -= src_r._data[ i ];

    return *this;
}

TVector_v & _fastcall TVector_v::operator*= ( const TVector_v &src_r )
{
    if ( _size != src_r._size )
		throw Exception( "Vector::Operator*=: size don't equals"  );

    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] *= src_r._data[ i ];

    return *this;
}

TVector_v & _fastcall TVector_v::operator*= ( int src )
{
    int i;
    for ( i = 0; i < _size; i++ )
        _data[ i ] *= src;

    return *this;
}


TVector_v & _fastcall TVector_v::operator<<= ( int SHL )
{
    int i;

    if ( SHL < 0 )
		throw Exception( "Vector::Operator<<=: arg < 0" );

    for ( i = 0; i < _size; i++ )
        if ( i + SHL >= _size )
            _data[ i ] = 0;
        else
            _data[ i ] = _data[ i + SHL ];

    return *this;
}

TVector_v & _fastcall TVector_v::operator>>= ( int SHR )
{
    int i;

	if ( SHR < 0 )
		throw Exception( "Vector::Operator>>=: arg < 0" );

    for ( i = _size - 1; i >= 0; i-- )
        if ( i < SHR )
            _data[ i ] = 0;
        else
            _data[ i ] = _data[ i - SHR ];

    return *this;
}


//----------------- private methods
void _fastcall TVector_v::set_size( int SIZE )
{
    if ( _data != NULL )
    {
        delete _data;
        _data = NULL;
    }

    if ( SIZE <= 0 )
    {
        _size = 0;
        _data = NULL;
        return;
    }

    _size = SIZE;
    _data = new double [ SIZE ];
}



int _fastcall TVector_v::get_size()
{
    return _size;
}

TVector_v TVector_v::ret_part( int x1, int x2 )
{
    if ( ( x1 > x2 ) || ( x1 < 0 ) || ( x2 >= _size ) )
        throw Exception( "Vector::ret_part, index error" );

    int i;
    TVector_v buf( x2 - x1 + 1 );

    for ( i = x1; i <= x2; i++ )
        buf[ i - x1 ] = _data[ i ];

    return buf;
}

TVector_v TVector_v::del_elem( int ind )
{
    if ( ( ind < 0 ) || ( ind >= _size ) )
        throw Exception( "Vector::del_elem, index error" );

	TVector_v buf( _size - 1 );
    int i;

    for ( i = 0; i < ind; i++ )
        buf[ i ] = _data[ i ];

    for ( i = ind + 1; i < _size; i++ )
        buf[ i - 1 ] = _data[ i ];


    return buf;
}

