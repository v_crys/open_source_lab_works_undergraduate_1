#ifndef MATRIX_H
#define MATRIX_H

#include "Vector.h"

class Matrix
{
	private:
		TVector_v *_vectors;
        int _size;

        void set_size( int N );

    public:
        Matrix() : _vectors( NULL ), _size( 0 ) {}
        Matrix( int N ) : _vectors( NULL ), _size( 0 ) { set_size( N ) ; }
        Matrix( int Vectors, int Len_vect );
        ~Matrix();

    //------- constructor copy
        Matrix( const Matrix &src );

    //------- binary operators
        Matrix & operator=( const Matrix &src );
        Matrix operator+( const Matrix &src_r );
        Matrix operator-( );
        Matrix operator-( const Matrix &src_r );
        Matrix operator*( const Matrix &src_r );
        Matrix operator*( int );
        friend Matrix operator*( int l, const Matrix &src_r );

		TVector_v &operator[]( int );

    //-------- thread operators
	//    friend std::ostream & operator<< ( std::ostream &OS, const Matrix &src_r );
    //    friend std::istream & operator>> ( std::istream &OS, Matrix &src_r );

    //-------------
        friend bool operator== ( const Matrix &src_l, const Matrix &src_r );
        friend bool operator!= ( const Matrix &src_l, const Matrix &src_r );
        Matrix & operator+= ( const Matrix &src_r );
        Matrix & operator-= ( const Matrix &src_r );
        Matrix & operator*= ( const Matrix &src_r );
        Matrix & operator*= ( int src );

        Matrix operator!( );

        Matrix Minor( int x, int y );
        Matrix ret_part( int x1, int y1, int x2, int y2 );
        double Determinant( );
		void rotate_matr( int ang );

		int Matrix::get_size();

		void Print_grid( TStringGrid *out );

};
#include "Matrix.cpp"
#endif // MATRIX_H
