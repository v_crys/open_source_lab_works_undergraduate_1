//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;

//---------------------------------------------------------------------------

#define CUB
//#define PIRAMIDA
//#define piram_3

#define round( X ) ( (X) + 0.49 )
#define RASST( X, Y ) ( sqrt( (X)*(X) + (Y)*(Y) ))

#define L_LEFT 	65
#define L_TOP   87
#define L_RIGHT	68
#define L_DOWN 	83

#define L_Z_DOWN 81
#define L_Z_UP 69

#define SCALE_PLUS  90
#define SCALE_MINUS  88


#define ANG_X_L 85
#define ANG_X_R 73
#define ANG_Y_L 74
#define ANG_Y_R 75
#define ANG_Z_L 77
#define ANG_Z_R 188


#define STEP_MOVE 10
#define STEP_ANG  10
#define STEP_SCALE 10

#define DEF_POLYGONS 6

void Draw();
void rotate_matr( int ang_x, int ang_y, int ang_z);
void coloring_3d( TImage *img );
void calculated_Matrix( void );
void set_figure( void );

void coloring_figure_pixels( TImage *img, int X, int Y, TColor );
void coloring_figure_line( TImage *img, int *arr_point_X, int *arr_point_Y, int points,
				TColor color1, TColor color2 );

void find_two_cros_line( int *arr_point_X, int *arr_point_Y, int points, int Y,
	int *loc_X1, int *loc_X2 );

#include "Vector.h"
#include "Matrix.h"


Matrix MOVE_3d( 4, 4 );

Matrix SCALE_3d( 4, 4 );

Matrix ROTATE_X_3d( 4, 4 );
Matrix ROTATE_Y_3d( 4, 4 );
Matrix ROTATE_Z_3d( 4, 4 );

Matrix POINT_FIG( 8, 4 );

struct TPOLYGONE {
	Matrix FIGURE_3d;
	Matrix disp_FIGURE_3d;
	Matrix disp_FIGURE_3d_roberts;
	Matrix V;

	TColor cl1;
	TColor cl2;
};

struct TPOLYGONE POLYGONE[ DEF_POLYGONS ];

int Polygons = DEF_POLYGONS;

int ANG_X = 0,
	ANG_Y = 0,
	ANG_Z = 0;


int Mouse_flag = 0,
	mouse_x_old = 0,
	mouse_y_old = 0;

int flag_animation = 0;

void Draw( TImage *img )
{
	int i, j;
	img->Canvas->FillRect(Rect(0,0,img->Width,img->Height));

	if ( Form3->RadioButton4->Checked ) {
		img->Canvas->Pen->Color = clBlack;
		for (i = 0; i < Polygons; i++) {
			for (j = 0; j < 3; j++) {
				img->Canvas->MoveTo( POLYGONE[ i ].disp_FIGURE_3d[ j ][ 0 ],
						   POLYGONE[ i ].disp_FIGURE_3d[ j ][ 1 ] );
				img->Canvas->LineTo( POLYGONE[ i ].disp_FIGURE_3d[ j + 1 ][ 0 ],
						   POLYGONE[ i ].disp_FIGURE_3d[ j + 1 ][ 1 ] );
			}
			img->Canvas->LineTo( POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 0 ],
					   POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 1 ] );
		}
	} else
		coloring_3d( img );

}


void coloring_3d( TImage *img )
{
  int i, j ,k, z ;
	//---- create V matrix
	for (i = 0; i < Polygons; i++) {
        	//-------- copy xyz coordinate for single polygone
			Matrix buf( 3, 3 );
			for (k = 0; k < 3; k++) {
					for (z = 0; z < 3; z++)
						buf[ k ][ z ] = POLYGONE[ i ].disp_FIGURE_3d_roberts[ k ][ z ];
			}

			//------ SLAY Calculation
			Matrix B1 = buf;
			B1[ 0 ][ 0 ] = -1;
			B1[ 1 ][ 0 ] = -1;
			B1[ 2 ][ 0 ] = -1;

			Matrix B2 = buf;
			B2[ 0 ][ 1 ] = -1;
			B2[ 1 ][ 1 ] = -1;
			B2[ 2 ][ 1 ] = -1;

			Matrix B3 = buf;
			B3[ 0 ][ 2 ] = -1;
			B3[ 1 ][ 2 ] = -1;
			B3[ 2 ][ 2 ] = -1;

			double det_a1 = buf.Determinant();
			if (det_a1 == 0) det_a1 = 0.000001;

			POLYGONE[ i ].V[ 0 ][ 0 ] = B1.Determinant() / det_a1;
			POLYGONE[ i ].V[ 0 ][ 1 ] = B2.Determinant() / det_a1;
			POLYGONE[ i ].V[ 0 ][ 2 ] = B3.Determinant() / det_a1;
			POLYGONE[ i ].V[ 0 ][ 3 ] = 1;
	}

	//------ point in figure
	Matrix S( 1, 4 );
	if ( Form3->RadioButton6->Checked || Form3->RadioButton7->Checked ) {
		for (i = 0; i < 3; i++) {
			S[ 0 ][ i ] = ( POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 0 ][ i ] +
						POLYGONE[ 1 ].disp_FIGURE_3d_roberts[ 2 ][ i ] ) / 2;
		}
		S[ 0 ][ 3 ] = 1;
	}

	if ( Form3->RadioButton8->Checked ) {
		for (i = 0; i < 3; i++) {
			S[ 0 ][ i ] = ( POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 0 ][ i ] +
						POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 1 ][ i ] +
						POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 2 ][ i ] +
						POLYGONE[ 1 ].disp_FIGURE_3d_roberts[ 2 ][ i ] ) / 4;
		}
		S[ 0 ][ 3 ] = 1;
	}

	if ( Form3->RadioButton9->Checked ) {
		for (i = 0; i < 3; i++) {
			S[ 0 ][ i ] = ( POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 1 ][ i ] +
							POLYGONE[ 0 ].disp_FIGURE_3d_roberts[ 3 ][ i ] +
							POLYGONE[ 1 ].disp_FIGURE_3d_roberts[ 2 ][ i ] ) / 3 ;
		}
		S[ 0 ][ 3 ] = 1;
	}

	//----- create V
	Matrix V( 4, Polygons );
	for (i = 0; i < Polygons; i++) {
		V[ 0 ][ i ] = POLYGONE[ i ].V[ 0 ][ 0 ];
		V[ 1 ][ i ] = POLYGONE[ i ].V[ 0 ][ 1 ];
		V[ 2 ][ i ] = POLYGONE[ i ].V[ 0 ][ 2 ];
		V[ 3 ][ i ] = POLYGONE[ i ].V[ 0 ][ 3 ];
	}

	//------- Test normals
	Matrix Test = S * V;
	for (i = 0; i < Test[ 0 ].get_size(); i++) {
		if ( Test[ 0 ][ i ] > 0 ) {
			for (j = 0; j < 4; j++) {
				V[ j ][ i ] *= -1;
			}
		}
	}

	V.Print_grid( Form3->StringGrid1 );


	//----- draw view
	int rebr = 0;
	for (i = 0; i < V[ 0 ].get_size(); i++) {
		if ( V[ 2 ][ i ] > 0 ) {
			rebr++;

			int Points_X[ 4 ];
			int Points_Y[ 4 ];

			int X_sred = 0,
				Y_sred = 0;

			int max_X = POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 0 ];
			int min_X = POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 0 ];
			int max_Y = POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 1 ];
			int min_Y = POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 1 ];

			for (j = 0; j < 4; j++) {
				if (j != 3)
				if ( Form3->RadioButton2->Checked || Form3->RadioButton3->Checked ) {
					img->Canvas->MoveTo( POLYGONE[ i ].disp_FIGURE_3d[ j ][ 0 ],
						   POLYGONE[ i ].disp_FIGURE_3d[ j ][ 1 ] );
					img->Canvas->LineTo( POLYGONE[ i ].disp_FIGURE_3d[ j + 1 ][ 0 ],
							   POLYGONE[ i ].disp_FIGURE_3d[ j + 1 ][ 1 ] );
				}

				Points_X[ j ] = POLYGONE[ i ].disp_FIGURE_3d[ j ][ 0 ];
				Points_Y[ j ] = POLYGONE[ i ].disp_FIGURE_3d[ j ][ 1 ];

				X_sred += POLYGONE[ i ].disp_FIGURE_3d[ j ][ 0 ];
				Y_sred += POLYGONE[ i ].disp_FIGURE_3d[ j ][ 1 ];

			}

			X_sred /= 4;
			Y_sred /= 4;

			if ( Form3->RadioButton2->Checked || Form3->RadioButton3->Checked )
				img->Canvas->LineTo( POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 0 ],
					   POLYGONE[ i ].disp_FIGURE_3d[ 0 ][ 1 ] );


			if ( Form3->RadioButton1->Checked )
				coloring_figure_line( img, Points_X, Points_Y, 4, POLYGONE[ i ].cl1, POLYGONE[ i ].cl2 );

			if ( Form3->RadioButton2->Checked )
				if (
					( img->Canvas->Pixels[ X_sred ][ Y_sred ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred  ][ Y_sred + 1 ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred ][ Y_sred - 1 ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred + 1 ][ Y_sred ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred + 1 ][ Y_sred + 1 ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred + 1][ Y_sred - 1 ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred - 1 ][ Y_sred ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred - 1 ][ Y_sred + 1 ] == clWhite ) &&
					( img->Canvas->Pixels[ X_sred - 1][ Y_sred - 1 ] == clWhite )
				)
					coloring_figure_pixels( img, X_sred, Y_sred, POLYGONE[ i ].cl1 );
		}
	}
	Form3->Label2->Caption = "Visible sides: " + IntToStr( rebr );


}

//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}


//---------------------------------------------------------------------------
void __fastcall TForm3::Button1Click(TObject *Sender)
{
Form3->SetFocusedControl( Form3 );
set_figure();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button1KeyDown(TObject *Sender, WORD &Key, TShiftState Shift)

{
int i, j;

	Label1->Caption = "Key down: " + IntToStr( Key );
	switch ( Key ) {
	//------ left line
		 //------------- MOVE
		 case L_LEFT:
			MOVE_3d[ 3 ][ 0 ] -= STEP_MOVE;
			break;

		 case L_RIGHT:
			MOVE_3d[ 3 ][ 0 ] += STEP_MOVE;
			break;

		 case L_TOP:
			MOVE_3d[ 3 ][ 1 ] -= STEP_MOVE;
			break;

		 case L_DOWN:
			MOVE_3d[ 3 ][ 1 ] += STEP_MOVE;
			break;

		 case L_Z_DOWN:
			MOVE_3d[ 3 ][ 2 ] -= STEP_MOVE;
			break;

		 case L_Z_UP:
			MOVE_3d[ 3 ][ 2 ] += STEP_MOVE;
			break;


		//-------- scale
		case SCALE_PLUS:
			SCALE_3d[ 0 ][ 0 ] += STEP_SCALE;
			SCALE_3d[ 1 ][ 1 ] += STEP_SCALE;
			SCALE_3d[ 2 ][ 2 ] += STEP_SCALE;
			break;

		case SCALE_MINUS:
			SCALE_3d[ 0 ][ 0 ] -= STEP_SCALE;
			SCALE_3d[ 1 ][ 1 ] -= STEP_SCALE;
			SCALE_3d[ 2 ][ 2 ] -= STEP_SCALE;
			break;

		//------- rotate
		case ANG_X_L:
            ANG_X += STEP_ANG;
			break;

		case ANG_X_R:
            ANG_X -= STEP_ANG;
			break;

		case ANG_Y_L:
			ANG_Y += STEP_ANG;
			break;

		case ANG_Y_R:
			ANG_Y -= STEP_ANG;
			break;

		case ANG_Z_L:
			ANG_Z += STEP_ANG;
			break;

		case ANG_Z_R:
            ANG_Z -= STEP_ANG;
			break;

	}

	rotate_matr( ANG_X, ANG_Y, ANG_Z );

	MOVE_3d.Print_grid( StringGrid2 );
	SCALE_3d.Print_grid( StringGrid3 );

	ROTATE_X_3d.Print_grid( StringGrid4 );
	ROTATE_Y_3d.Print_grid( StringGrid5 );
	ROTATE_Z_3d.Print_grid( StringGrid6 );

	calculated_Matrix();

	Draw( Image1 );
}
//---------------------------------------------------------------------------

void calculated_Matrix( void )
{
    int i, j;
	for (i = 0; i < DEF_POLYGONS; i++) {
		for (j = 0; j < 4; j++ ) {
			Matrix buf(1, 4);
			buf[ 0 ][ 0 ] = POLYGONE[ i ].FIGURE_3d[ j ][ 0 ];
			buf[ 0 ][ 1 ] = POLYGONE[ i ].FIGURE_3d[ j ][ 1 ];
			buf[ 0 ][ 2 ] = POLYGONE[ i ].FIGURE_3d[ j ][ 2 ];
			buf[ 0 ][ 3 ] = POLYGONE[ i ].FIGURE_3d[ j ][ 3 ];

			buf = buf * SCALE_3d * ROTATE_X_3d * ROTATE_Y_3d * ROTATE_Z_3d;

			POLYGONE[ i ].disp_FIGURE_3d_roberts[ j ][ 0 ] = buf[ 0 ][ 0 ] - 5;
			POLYGONE[ i ].disp_FIGURE_3d_roberts[ j ][ 1 ] = buf[ 0 ][ 1 ] - 5;
			POLYGONE[ i ].disp_FIGURE_3d_roberts[ j ][ 2 ] = buf[ 0 ][ 2 ] - 5;
			POLYGONE[ i ].disp_FIGURE_3d_roberts[ j ][ 3 ] = buf[ 0 ][ 3 ];

			buf = buf * MOVE_3d;

			POLYGONE[ i ].disp_FIGURE_3d[ j ][ 0 ] = buf[ 0 ][ 0 ];
			POLYGONE[ i ].disp_FIGURE_3d[ j ][ 1 ] = buf[ 0 ][ 1 ];
			POLYGONE[ i ].disp_FIGURE_3d[ j ][ 2 ] = buf[ 0 ][ 2 ];
			POLYGONE[ i ].disp_FIGURE_3d[ j ][ 3 ] = buf[ 0 ][ 3 ];


		}
	}
}

//---------------------------------------------------------------------------
void __fastcall TForm3::FormCreate(TObject *Sender)
{
	int i, j;

	StringGrid1->Cells[ 0 ][ 0 ] = "V matrix";
	StringGrid1->Cells[ 0 ][ 1 ] = "a";
	StringGrid1->Cells[ 0 ][ 2 ] = "b";
	StringGrid1->Cells[ 0 ][ 3 ] = "c";
	StringGrid1->Cells[ 0 ][ 4 ] = "d";

	StringGrid2->Cells[ 0 ][ 0 ] = "Move";

	StringGrid3->Cells[ 0 ][ 0 ] = "Scale";

	StringGrid4->Cells[ 0 ][ 0 ] = "Rot_X";
	StringGrid5->Cells[ 0 ][ 0 ] = "Rot_Y";
	StringGrid6->Cells[ 0 ][ 0 ] = "Rot_Z";

	//---- resize matrix
	for (i = 0; i < DEF_POLYGONS; i++) {
		Matrix buf( 4, 4 );
		Matrix buf_V( 1, 4 );

		POLYGONE[ i ].FIGURE_3d = buf;
		POLYGONE[ i ].disp_FIGURE_3d = buf;
		POLYGONE[ i ].disp_FIGURE_3d_roberts = buf;
		POLYGONE[ i ].V = buf_V;
	}


	set_figure();



	//-------- move
	MOVE_3d[ 0 ][ 0 ] = 1;
	MOVE_3d[ 0 ][ 1 ] = 0;
	MOVE_3d[ 0 ][ 2 ] = 0;
	MOVE_3d[ 0 ][ 3 ] = 0;

	MOVE_3d[ 1 ][ 0 ] = 0;
	MOVE_3d[ 1 ][ 1 ] = 1;
	MOVE_3d[ 1 ][ 2 ] = 0;
	MOVE_3d[ 1 ][ 3 ] = 0;

	MOVE_3d[ 2 ][ 0 ] = 0;
	MOVE_3d[ 2 ][ 1 ] = 0;
	MOVE_3d[ 2 ][ 2 ] = 1;
	MOVE_3d[ 2 ][ 3 ] = 0;

	MOVE_3d[ 3 ][ 0 ] = STEP_MOVE;
	MOVE_3d[ 3 ][ 1 ] = STEP_MOVE;
	MOVE_3d[ 3 ][ 2 ] = STEP_MOVE;
	MOVE_3d[ 3 ][ 3 ] = 1;

	//-------- scale
	SCALE_3d[ 0 ][ 0 ] = STEP_SCALE;
	SCALE_3d[ 0 ][ 1 ] = 0;
	SCALE_3d[ 0 ][ 2 ] = 0;
	SCALE_3d[ 0 ][ 3 ] = 0;

	SCALE_3d[ 1 ][ 0 ] = 0;
	SCALE_3d[ 1 ][ 1 ] = STEP_SCALE;
	SCALE_3d[ 1 ][ 2 ] = 0;
	SCALE_3d[ 1 ][ 3 ] = 0;

	SCALE_3d[ 2 ][ 0 ] = 0;
	SCALE_3d[ 2 ][ 1 ] = 0;
	SCALE_3d[ 2 ][ 2 ] = STEP_SCALE;
	SCALE_3d[ 2 ][ 3 ] = 0;

	SCALE_3d[ 3 ][ 0 ] = 0;
	SCALE_3d[ 3 ][ 1 ] = 0;
	SCALE_3d[ 3 ][ 2 ] = 0;
	SCALE_3d[ 3 ][ 3 ] = 1;

	rotate_matr( ANG_X, ANG_Y, ANG_Z );



   /*	TColor my_cl = clBlue;
	int delta_R = ( (clGreen % 0xFF ) - ( my_cl % 0xFF ) ) / 16;
	int delta_G = ( ( (clGreen >> 8 ) %0xFF) - ( ( my_cl >> 8 ) %0xFF ) ) /400;
	int delta_B = ( ( (clGreen >> 16 ) %0xFF) - ( ( my_cl >> 16 ) %0xFF ) ) /400;

	for (i = 0; i < 400; i++) {
		Form3->Image1->Canvas->MoveTo(0, i);
		Form3->Image1->Canvas->Pen->Color = my_cl;
		Form3->Image1->Canvas->LineTo( 200, i );

		if ( i % 16 == 0 )
		my_cl = my_cl + delta_R + (delta_G << 8 ) + ( delta_B << 16 );
	}   */
}
//---------------------------------------------------------------------------


void set_figure( void )
{
      	//------ points of cube
	Polygons = DEF_POLYGONS;

	POINT_FIG[ 0 ][ 0 ] = -1;
	POINT_FIG[ 0 ][ 1 ] = -1;
	POINT_FIG[ 0 ][ 2 ] = -1;
	POINT_FIG[ 0 ][ 3 ] = 1;

	POINT_FIG[ 1 ][ 0 ] = 1;
	POINT_FIG[ 1 ][ 1 ] = -1;
	POINT_FIG[ 1 ][ 2 ] = -1;
	POINT_FIG[ 1 ][ 3 ] = 1;

	POINT_FIG[ 2 ][ 0 ] = 1;
	POINT_FIG[ 2 ][ 1 ] = -1;
	POINT_FIG[ 2 ][ 2 ] = 1;
	POINT_FIG[ 2 ][ 3 ] = 1;

	POINT_FIG[ 3 ][ 0 ] = -1;
	POINT_FIG[ 3 ][ 1 ] = -1;
	POINT_FIG[ 3 ][ 2 ] = 1;
	POINT_FIG[ 3 ][ 3 ] = 1;

	POINT_FIG[ 4 ][ 0 ] = -1;
	POINT_FIG[ 4 ][ 1 ] = 1;
	POINT_FIG[ 4 ][ 2 ] = -1;
	POINT_FIG[ 4 ][ 3 ] = 1;

	POINT_FIG[ 5 ][ 0 ] = 1;
	POINT_FIG[ 5 ][ 1 ] = 1;
	POINT_FIG[ 5 ][ 2 ] = -1;
	POINT_FIG[ 5 ][ 3 ] = 1;

	POINT_FIG[ 6 ][ 0 ] = 1;
	POINT_FIG[ 6 ][ 1 ] = 1;
	POINT_FIG[ 6 ][ 2 ] = 1;
	POINT_FIG[ 6 ][ 3 ] = 1;

	POINT_FIG[ 7 ][ 0 ] = -1;
	POINT_FIG[ 7 ][ 1 ] = 1;
	POINT_FIG[ 7 ][ 2 ] = 1;
	POINT_FIG[ 7 ][ 3 ] = 1;

	if ( Form3->RadioButton7->Checked ) {
		POINT_FIG[ 4 ][ 0 ] = -1;
		POINT_FIG[ 4 ][ 1 ] = 2;
		POINT_FIG[ 4 ][ 2 ] = -1;
		POINT_FIG[ 4 ][ 3 ] = 1;

		POINT_FIG[ 5 ][ 0 ] = 1;
		POINT_FIG[ 5 ][ 1 ] = 2;
		POINT_FIG[ 5 ][ 2 ] = -1;
		POINT_FIG[ 5 ][ 3 ] = 1;

		POINT_FIG[ 6 ][ 0 ] = 1;
		POINT_FIG[ 6 ][ 1 ] = 2;
		POINT_FIG[ 6 ][ 2 ] = 1;
		POINT_FIG[ 6 ][ 3 ] = 1;

		POINT_FIG[ 7 ][ 0 ] = -1;
		POINT_FIG[ 7 ][ 1 ] = 2;
		POINT_FIG[ 7 ][ 2 ] = 1;
		POINT_FIG[ 7 ][ 3 ] = 1;
	}

	POLYGONE[ 0 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
	POLYGONE[ 0 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
	POLYGONE[ 0 ].FIGURE_3d[ 2 ] = POINT_FIG[ 5 ];
	POLYGONE[ 0 ].FIGURE_3d[ 3 ] = POINT_FIG[ 4 ];
	POLYGONE[ 0 ].cl1 = 0x0b5ea2;
	POLYGONE[ 0 ].cl2 = 0x16f2f2;

	POLYGONE[ 1 ].FIGURE_3d[ 0 ] = POINT_FIG[ 1 ];
	POLYGONE[ 1 ].FIGURE_3d[ 1 ] = POINT_FIG[ 2 ];
	POLYGONE[ 1 ].FIGURE_3d[ 2 ] = POINT_FIG[ 6 ];
	POLYGONE[ 1 ].FIGURE_3d[ 3 ] = POINT_FIG[ 5 ];
	POLYGONE[ 1 ].cl1 = clBlue;
	POLYGONE[ 1 ].cl2 = clGray;

	POLYGONE[ 2 ].FIGURE_3d[ 0 ] = POINT_FIG[ 2 ];
	POLYGONE[ 2 ].FIGURE_3d[ 1 ] = POINT_FIG[ 3 ];
	POLYGONE[ 2 ].FIGURE_3d[ 2 ] = POINT_FIG[ 7 ];
	POLYGONE[ 2 ].FIGURE_3d[ 3 ] = POINT_FIG[ 6 ];
	POLYGONE[ 2 ].cl1 = clYellow;
	POLYGONE[ 2 ].cl2 = clLtGray;

	POLYGONE[ 3 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
	POLYGONE[ 3 ].FIGURE_3d[ 1 ] = POINT_FIG[ 3 ];
	POLYGONE[ 3 ].FIGURE_3d[ 2 ] = POINT_FIG[ 7 ];
	POLYGONE[ 3 ].FIGURE_3d[ 3 ] = POINT_FIG[ 4 ];
	POLYGONE[ 3 ].cl1 = clGreen;
	POLYGONE[ 3 ].cl2 = clNavy;

	POLYGONE[ 4 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
	POLYGONE[ 4 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
	POLYGONE[ 4 ].FIGURE_3d[ 2 ] = POINT_FIG[ 2 ];
	POLYGONE[ 4 ].FIGURE_3d[ 3 ] = POINT_FIG[ 3 ];
	POLYGONE[ 4 ].cl1 = clLime;
	POLYGONE[ 4 ].cl2 = clOlive;

	POLYGONE[ 5 ].FIGURE_3d[ 0 ] = POINT_FIG[ 4 ];
	POLYGONE[ 5 ].FIGURE_3d[ 1 ] = POINT_FIG[ 5 ];
	POLYGONE[ 5 ].FIGURE_3d[ 2 ] = POINT_FIG[ 6 ];
	POLYGONE[ 5 ].FIGURE_3d[ 3 ] = POINT_FIG[ 7 ];
	POLYGONE[ 5 ].cl1 = clAqua;
	POLYGONE[ 5 ].cl2 = clPurple;

	//----- piramida
	if ( Form3->RadioButton8->Checked ) {
		Polygons = 4;

		POINT_FIG[ 0 ][ 0 ] = -2;
		POINT_FIG[ 0 ][ 1 ] = -2;
		POINT_FIG[ 0 ][ 2 ] = -2;
		POINT_FIG[ 0 ][ 3 ] = 1;

		POINT_FIG[ 1 ][ 0 ] = 2;
		POINT_FIG[ 1 ][ 1 ] = -2;
		POINT_FIG[ 1 ][ 2 ] = -2;
		POINT_FIG[ 1 ][ 3 ] = 1;

		POINT_FIG[ 2 ][ 0 ] = 0;
		POINT_FIG[ 2 ][ 1 ] = -2;
		POINT_FIG[ 2 ][ 2 ] = 2;
		POINT_FIG[ 2 ][ 3 ] = 1;

		POINT_FIG[ 3 ][ 0 ] = 0;
		POINT_FIG[ 3 ][ 1 ] = 2;
		POINT_FIG[ 3 ][ 2 ] = 0;
		POINT_FIG[ 3 ][ 3 ] = 1;

		POLYGONE[ 0 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 0 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
		POLYGONE[ 0 ].FIGURE_3d[ 2 ] = POINT_FIG[ 2 ];
		POLYGONE[ 0 ].FIGURE_3d[ 3 ] = POINT_FIG[ 0 ];

		POLYGONE[ 1 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 1 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
		POLYGONE[ 1 ].FIGURE_3d[ 2 ] = POINT_FIG[ 3 ];
		POLYGONE[ 1 ].FIGURE_3d[ 3 ] = POINT_FIG[ 0 ];

		POLYGONE[ 2 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 2 ].FIGURE_3d[ 1 ] = POINT_FIG[ 2 ];
		POLYGONE[ 2 ].FIGURE_3d[ 2 ] = POINT_FIG[ 3 ];
		POLYGONE[ 2 ].FIGURE_3d[ 3 ] = POINT_FIG[ 0 ];

		POLYGONE[ 3 ].FIGURE_3d[ 0 ] = POINT_FIG[ 1 ];
		POLYGONE[ 3 ].FIGURE_3d[ 1 ] = POINT_FIG[ 2 ];
		POLYGONE[ 3 ].FIGURE_3d[ 2 ] = POINT_FIG[ 3 ];
		POLYGONE[ 3 ].FIGURE_3d[ 3 ] = POINT_FIG[ 1 ];

	}

	//-------- piramida 4
	if ( Form3->RadioButton9->Checked ) {
		Polygons = 5;

		POINT_FIG[ 0 ][ 0 ] = -2;
		POINT_FIG[ 0 ][ 1 ] = -2;
		POINT_FIG[ 0 ][ 2 ] = -2;
		POINT_FIG[ 0 ][ 3 ] = 1;

		POINT_FIG[ 1 ][ 0 ] = -2;
		POINT_FIG[ 1 ][ 1 ] = -2;
		POINT_FIG[ 1 ][ 2 ] = 2;
		POINT_FIG[ 1 ][ 3 ] = 1;

		POINT_FIG[ 2 ][ 0 ] = 2;
		POINT_FIG[ 2 ][ 1 ] = -2;
		POINT_FIG[ 2 ][ 2 ] = 2;
		POINT_FIG[ 2 ][ 3 ] = 1;

		POINT_FIG[ 3 ][ 0 ] = 2;
		POINT_FIG[ 3 ][ 1 ] = -2;
		POINT_FIG[ 3 ][ 2 ] = -2;
		POINT_FIG[ 3 ][ 3 ] = 1;

		POINT_FIG[ 4 ][ 0 ] = 0;
		POINT_FIG[ 4 ][ 1 ] = 2;
		POINT_FIG[ 4 ][ 2 ] = 0;
		POINT_FIG[ 4 ][ 3 ] = 1;

		POLYGONE[ 0 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 0 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
		POLYGONE[ 0 ].FIGURE_3d[ 2 ] = POINT_FIG[ 2 ];
		POLYGONE[ 0 ].FIGURE_3d[ 3 ] = POINT_FIG[ 3 ];

		POLYGONE[ 1 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 1 ].FIGURE_3d[ 1 ] = POINT_FIG[ 1 ];
		POLYGONE[ 1 ].FIGURE_3d[ 2 ] = POINT_FIG[ 4 ];
		POLYGONE[ 1 ].FIGURE_3d[ 3 ] = POINT_FIG[ 0 ];

		POLYGONE[ 2 ].FIGURE_3d[ 0 ] = POINT_FIG[ 1 ];
		POLYGONE[ 2 ].FIGURE_3d[ 1 ] = POINT_FIG[ 2 ];
		POLYGONE[ 2 ].FIGURE_3d[ 2 ] = POINT_FIG[ 4 ];
		POLYGONE[ 2 ].FIGURE_3d[ 3 ] = POINT_FIG[ 1 ];

		POLYGONE[ 3 ].FIGURE_3d[ 0 ] = POINT_FIG[ 2 ];
		POLYGONE[ 3 ].FIGURE_3d[ 1 ] = POINT_FIG[ 3 ];
		POLYGONE[ 3 ].FIGURE_3d[ 2 ] = POINT_FIG[ 4 ];
		POLYGONE[ 3 ].FIGURE_3d[ 3 ] = POINT_FIG[ 2 ];

		POLYGONE[ 4 ].FIGURE_3d[ 0 ] = POINT_FIG[ 0 ];
		POLYGONE[ 4 ].FIGURE_3d[ 1 ] = POINT_FIG[ 3 ];
		POLYGONE[ 4 ].FIGURE_3d[ 2 ] = POINT_FIG[ 4 ];
		POLYGONE[ 4 ].FIGURE_3d[ 3 ] = POINT_FIG[ 0 ];
	}



    int i;
	for (i = 0; i < DEF_POLYGONS; i++) {
			POLYGONE[ i ].disp_FIGURE_3d = POLYGONE[ i ].FIGURE_3d;
            POLYGONE[ i ].disp_FIGURE_3d_roberts = POLYGONE[ i ].FIGURE_3d;
	}
}

void rotate_matr( int ang_x, int ang_y, int ang_z)
{
    	//---------- rotate X
	float loc_cos = cos( ang_x * (3.14 / 180) );
	float loc_sin = sin( ang_x * (3.14 / 180) );

	ROTATE_X_3d[ 0 ][ 0 ] = 1;
	ROTATE_X_3d[ 0 ][ 1 ] = 0;
	ROTATE_X_3d[ 0 ][ 2 ] = 0;
	ROTATE_X_3d[ 0 ][ 3 ] = 0;

	ROTATE_X_3d[ 1 ][ 0 ] = 0;
	ROTATE_X_3d[ 1 ][ 1 ] = loc_cos;
	ROTATE_X_3d[ 1 ][ 2 ] = loc_sin;
	ROTATE_X_3d[ 1 ][ 3 ] = 0;

	ROTATE_X_3d[ 2 ][ 0 ] = 0 ;
	ROTATE_X_3d[ 2 ][ 1 ] = -loc_sin;
	ROTATE_X_3d[ 2 ][ 2 ] = loc_cos;
	ROTATE_X_3d[ 2 ][ 3 ] = 0;

	ROTATE_X_3d[ 3 ][ 0 ] = 0;
	ROTATE_X_3d[ 3 ][ 1 ] = 0;
	ROTATE_X_3d[ 3 ][ 2 ] = 0;
	ROTATE_X_3d[ 3 ][ 3 ] = 1;


	loc_cos = cos( ang_y * (3.14 / 180) );
	loc_sin = sin( ang_y * (3.14 / 180) );

	ROTATE_Y_3d[ 0 ][ 0 ] = loc_cos;
	ROTATE_Y_3d[ 0 ][ 1 ] = 0;
	ROTATE_Y_3d[ 0 ][ 2 ] = loc_sin;
	ROTATE_Y_3d[ 0 ][ 3 ] = 0;

	ROTATE_Y_3d[ 1 ][ 0 ] = 0;
	ROTATE_Y_3d[ 1 ][ 1 ] = 1;
	ROTATE_Y_3d[ 1 ][ 2 ] = 0;
	ROTATE_Y_3d[ 1 ][ 3 ] = 0;

	ROTATE_Y_3d[ 2 ][ 0 ] = -loc_sin;
	ROTATE_Y_3d[ 2 ][ 1 ] = 0 ;
	ROTATE_Y_3d[ 2 ][ 2 ] = loc_cos;
	ROTATE_Y_3d[ 2 ][ 3 ] = 0;

	ROTATE_Y_3d[ 3 ][ 0 ] = 0;
	ROTATE_Y_3d[ 3 ][ 1 ] = 0;
	ROTATE_Y_3d[ 3 ][ 2 ] = 0;
	ROTATE_Y_3d[ 3 ][ 3 ] = 1;

	loc_cos = cos( ang_z * (3.14 / 180) );
	loc_sin = sin( ang_z * (3.14 / 180) );

	ROTATE_Z_3d[ 0 ][ 0 ] = loc_cos;
	ROTATE_Z_3d[ 0 ][ 1 ] = -loc_sin;
	ROTATE_Z_3d[ 0 ][ 2 ] = 0;
	ROTATE_Y_3d[ 0 ][ 3 ] = 0;

	ROTATE_Z_3d[ 1 ][ 0 ] = loc_sin;
	ROTATE_Z_3d[ 1 ][ 1 ] = loc_cos;
	ROTATE_Z_3d[ 1 ][ 2 ] = 0;
	ROTATE_Y_3d[ 1 ][ 3 ] = 0;

	ROTATE_Z_3d[ 2 ][ 0 ] = 0;
	ROTATE_Z_3d[ 2 ][ 1 ] = 0;
	ROTATE_Z_3d[ 2 ][ 2 ] = 1;
	ROTATE_Y_3d[ 2 ][ 3 ] = 0;

	ROTATE_Z_3d[ 3 ][ 0 ] = 0;
	ROTATE_Z_3d[ 3 ][ 1 ] = 0;
	ROTATE_Z_3d[ 3 ][ 2 ] = 0;
	ROTATE_Z_3d[ 3 ][ 3 ] = 1;
}





//----------------------- mouse

void __fastcall TForm3::Image1MouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y)
{
	if ( ( CheckBox1->Checked ) && ( Mouse_flag ) ) {
		Label1->Caption = "Mouse X: " + IntToStr( X ) + ", Y: " + IntToStr( Y );

	ANG_X -= Y - mouse_x_old;
	ANG_Y -= X - mouse_y_old;

	mouse_x_old = Y;
	mouse_y_old = X;

	rotate_matr( ANG_X, ANG_Y, ANG_Z );

	MOVE_3d.Print_grid( StringGrid2 );
	SCALE_3d.Print_grid( StringGrid3 );

	ROTATE_X_3d.Print_grid( StringGrid4 );
	ROTATE_Y_3d.Print_grid( StringGrid5 );
	ROTATE_Z_3d.Print_grid( StringGrid6 );

	calculated_Matrix();

   	Draw( Image1 );

	}
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Image1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	Mouse_flag = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Image1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	Mouse_flag = 1;
	mouse_x_old = Y;
    mouse_y_old = X;
}
//---------------------------------------------------------------------------


//---------- coloring
void coloring_figure_line( TImage *img, int *arr_point_X, int *arr_point_Y, int points,
				TColor color1, TColor color2 )
{
	int i, j;


	int min = arr_point_Y[ 0 ],
		max = arr_point_Y[ 0 ],
		pos_min = 0,
		pos_max = 0;



	for (i = 0; i < points; i++) {
		if ( arr_point_Y[ i ] < min ) {
			min = arr_point_Y[ i ];
			pos_min = i;
		}

		 if ( arr_point_Y[ i ] > max ) {
			max = arr_point_Y[ i ];
			pos_max = i;
		}

	}

	TColor my_cl = color1;
	float loc_R = ( color1 & 0xFF );
	float loc_G = ( color1 >> 8 ) &0xFF ;
	float loc_B =  ( color1 >> 16 ) &0xFF ;
	float delta_R = (float)( (color2 & 0xFF ) - ( color1 & 0xFF ) ) / (float)( max - min );
	float delta_G = (float)( ( (color2 >> 8 ) &0xFF) - ( ( color1 >> 8 ) &0xFF ) ) / (float)( max - min );
	float delta_B = (float)( ( (color2 >> 16 ) &0xFF) - ( ( color1 >> 16 ) &0xFF ) ) / (float)(max- min );


	for (i = min + 1; i < max; i++) {
		//------- find two points
		int loc_X1;
		int loc_X2;

		find_two_cros_line( arr_point_X, arr_point_Y, points , i, &loc_X1, &loc_X2 );

		img->Canvas->Pen->Color = my_cl;
		img->Canvas->MoveTo( loc_X1, i );
		img->Canvas->LineTo( loc_X2, i );

		loc_R += delta_R;
		loc_G += delta_G;
		loc_B += delta_B;

		if ( Form3->CheckBox2->Checked )
			my_cl = loc_R + ( (int)loc_G << 8 ) + ( (int)loc_B << 16 );

	}
	img->Canvas->Pen->Color = clBlack;


	img->Canvas->MoveTo( arr_point_X[ points - 1 ], arr_point_Y[ points - 1 ] );
	for (i = 0; i < points; i++) {
		img->Canvas->LineTo( arr_point_X[ i ], arr_point_Y[ i ] );
	}

	return;
}


void coloring_figure_pixels( TImage *img, int X, int Y, TColor color )
{
	int i;

	//if ( img->Canvas->Pixels[ X ][ Y ] != clWhite )
	//	return;

	if ( img->Canvas->Pixels[ X + 1 ][ Y ] == clWhite ) {
		img->Canvas->Pixels[ X + 1 ][ Y ] = color;
		coloring_figure_pixels( img, X + 1, Y, color );
	}

	if ( img->Canvas->Pixels[ X - 1 ][ Y ] == clWhite ) {
		img->Canvas->Pixels[ X - 1 ][ Y ] = color;
		coloring_figure_pixels( img, X - 1, Y, color );
	}

	if ( img->Canvas->Pixels[ X ][ Y + 1 ] == clWhite ) {
		img->Canvas->Pixels[ X ][ Y + 1 ] = color;
		coloring_figure_pixels( img, X , Y + 1, color );
	}

	if ( img->Canvas->Pixels[ X ][ Y - 1 ] == clWhite ) {
		img->Canvas->Pixels[ X ][ Y - 1 ] = color;
		coloring_figure_pixels( img, X, Y - 1, color );
	}
}



void find_two_cros_line( int *arr_point_X, int *arr_point_Y, int points, int Y,
	int *loc_X1, int *loc_X2 )
{
int j;
int flag = 0;
		for (j = 0; j < points - 1; j++ ) {
			if ( (( arr_point_Y[ j ] > Y ) && ( arr_point_Y[ j + 1 ] <= Y ) ) ||
				(( arr_point_Y[ j ] < Y ) && ( arr_point_Y[ j + 1 ] >= Y ) ) ) {

				float d_x =  arr_point_X[ j ] - arr_point_X[ j + 1 ] ;
				float d_y =  arr_point_Y[ j ] - arr_point_Y[ j + 1 ] ;
				float k = (float)d_x / d_y;

				if ( flag == 0) {
					*loc_X1 = round( k * ( Y - arr_point_Y[ j ] ) ) + arr_point_X[ j ] ;
				} else
				{
					*loc_X2 = round( k * ( Y - arr_point_Y[ j ] ) ) + arr_point_X[ j ];
					break;
				}

				flag++;
			}
		}

		if ( (( arr_point_Y[ points - 1 ] > Y ) && ( arr_point_Y[ 0 ] <= Y ) ) ||
				(( arr_point_Y[ points - 1 ] < Y ) && ( arr_point_Y[ 0 ] >= Y ) ) ) {

				float d_x =  arr_point_X[ points - 1 ] - arr_point_X[ 0 ] ;
				float d_y =  arr_point_Y[ points - 1 ] - arr_point_Y[ 0 ] ;
				float k = (float)d_x / d_y;

				*loc_X2 = round( k * ( Y - arr_point_Y[ points - 1 ] ) ) + arr_point_X[ points - 1 ];
		}


}



void __fastcall TForm3::Timer1Timer(TObject *Sender)
{
	if ( flag_animation ) {
		ANG_X += SCALE_3d[ 0 ][ 0 ] / 30;
		ANG_Y -= SCALE_3d[ 0 ][ 0 ] / 30;
		SCALE_3d[ 0 ][ 0 ] += 1;
		SCALE_3d[ 1 ][ 1 ] += 1;
		SCALE_3d[ 2 ][ 2 ] += 1;
	} else {
		ANG_X -= SCALE_3d[ 0 ][ 0 ] / 30;
		ANG_Y += SCALE_3d[ 0 ][ 0 ] / 30;
		SCALE_3d[ 0 ] [ 0 ] -= 1;
		SCALE_3d[ 1 ] [ 1 ] -= 1;
		SCALE_3d[ 2 ] [ 2 ] -= 1;
	}

	if ( ( SCALE_3d[ 0 ][ 0 ] > 200 ) || (  SCALE_3d[ 0 ][ 0 ] < 20)) {
		flag_animation = !flag_animation;
	}

	rotate_matr( ANG_X, ANG_Y, ANG_Z );
	calculated_Matrix();
	Draw( Form3->Image1 );
}
//---------------------------------------------------------------------------

void __fastcall TForm3::CheckBox3Click(TObject *Sender)
{
	if ( CheckBox3->Checked ) {
		Timer1->Enabled = True;
	} else {
        Timer1->Enabled = False;
    }
}
//---------------------------------------------------------------------------

