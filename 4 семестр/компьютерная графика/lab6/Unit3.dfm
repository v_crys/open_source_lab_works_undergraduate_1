object Form3: TForm3
  Left = 342
  Top = 55
  BorderStyle = bsDialog
  Caption = 
    'LAB6 of  computer graphics - V. Khrustalev, A. Prosandeeva, G. Z' +
    'aytsev, M. Fedorova'
  ClientHeight = 724
  ClientWidth = 765
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 548
    Height = 440
    Align = alClient
    OnMouseDown = Image1MouseDown
    OnMouseMove = Image1MouseMove
    OnMouseUp = Image1MouseUp
    ExplicitWidth = 440
    ExplicitHeight = 153
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 440
    Width = 765
    Height = 284
    Align = alBottom
    Caption = 'Matrix window'
    TabOrder = 0
    ExplicitTop = 288
    ExplicitWidth = 598
    object StringGrid1: TStringGrid
      Left = 3
      Top = 16
      Width = 238
      Height = 129
      ColCount = 7
      DefaultColWidth = 32
      TabOrder = 0
    end
    object StringGrid2: TStringGrid
      Left = 247
      Top = 16
      Width = 170
      Height = 129
      DefaultColWidth = 32
      TabOrder = 1
    end
    object StringGrid3: TStringGrid
      Left = 423
      Top = 16
      Width = 170
      Height = 129
      DefaultColWidth = 32
      TabOrder = 2
    end
    object StringGrid4: TStringGrid
      Left = 3
      Top = 151
      Width = 174
      Height = 130
      DefaultColWidth = 32
      TabOrder = 3
    end
    object StringGrid5: TStringGrid
      Left = 183
      Top = 151
      Width = 170
      Height = 130
      DefaultColWidth = 32
      TabOrder = 4
    end
    object StringGrid6: TStringGrid
      Left = 359
      Top = 151
      Width = 236
      Height = 130
      DefaultColWidth = 32
      TabOrder = 5
    end
  end
  object GroupBox2: TGroupBox
    Left = 548
    Top = 0
    Width = 217
    Height = 440
    Align = alRight
    Caption = 'Settings'
    TabOrder = 1
    ExplicitLeft = 381
    ExplicitHeight = 288
    object Label1: TLabel
      Left = 6
      Top = 250
      Width = 54
      Height = 13
      Caption = 'Key down: '
    end
    object Label2: TLabel
      Left = 6
      Top = 269
      Width = 63
      Height = 13
      Caption = 'Visible sides: '
    end
    object GroupBox3: TGroupBox
      Left = 2
      Top = 15
      Width = 213
      Height = 58
      Align = alTop
      Caption = 'Coloring'
      TabOrder = 0
      object RadioButton3: TRadioButton
        Left = 149
        Top = 25
        Width = 52
        Height = 17
        Caption = 'None'
        TabOrder = 0
      end
      object RadioButton1: TRadioButton
        Left = 4
        Top = 25
        Width = 51
        Height = 17
        Caption = 'Liner'
        Checked = True
        TabOrder = 1
        TabStop = True
      end
      object RadioButton2: TRadioButton
        Left = 78
        Top = 25
        Width = 65
        Height = 17
        Caption = 'Pixels'
        TabOrder = 2
      end
    end
    object Button1: TButton
      Left = 6
      Top = 216
      Width = 203
      Height = 31
      Caption = 'Draw'
      TabOrder = 1
      OnClick = Button1Click
      OnKeyDown = Button1KeyDown
    end
    object GroupBox5: TGroupBox
      Left = 2
      Top = 73
      Width = 213
      Height = 48
      Align = alTop
      TabOrder = 2
      object RadioButton5: TRadioButton
        Left = 111
        Top = 16
        Width = 73
        Height = 17
        Caption = 'Roberts'
        TabOrder = 0
      end
      object RadioButton4: TRadioButton
        Left = 17
        Top = 16
        Width = 80
        Height = 17
        Caption = 'Carcass'
        Checked = True
        TabOrder = 1
        TabStop = True
      end
    end
    object CheckBox1: TCheckBox
      Left = 22
      Top = 193
      Width = 67
      Height = 17
      Caption = 'Mouse'
      TabOrder = 3
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 121
      Width = 213
      Height = 66
      Align = alTop
      Caption = 'Figure'
      TabOrder = 4
    end
    object RadioButton6: TRadioButton
      Left = 24
      Top = 137
      Width = 41
      Height = 17
      Caption = 'CUB'
      Checked = True
      TabOrder = 5
      TabStop = True
    end
    object RadioButton7: TRadioButton
      Left = 119
      Top = 144
      Width = 57
      Height = 17
      Caption = 'PARALL'
      TabOrder = 6
    end
    object RadioButton8: TRadioButton
      Left = 24
      Top = 160
      Width = 89
      Height = 17
      Caption = 'PYRAMID _3'
      TabOrder = 7
    end
    object RadioButton9: TRadioButton
      Left = 120
      Top = 160
      Width = 113
      Height = 17
      Caption = 'PYRAMID_4'
      TabOrder = 8
    end
    object CheckBox2: TCheckBox
      Left = 120
      Top = 193
      Width = 97
      Height = 17
      Caption = 'Gradient'
      TabOrder = 9
    end
    object CheckBox3: TCheckBox
      Left = 128
      Top = 264
      Width = 97
      Height = 17
      Caption = 'CheckBox3'
      TabOrder = 10
      OnClick = CheckBox3Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 20
    OnTimer = Timer1Timer
    Left = 472
    Top = 8
  end
end
