/*
 * Name Project: Class for Vectors
 * IDE: Code::Blocks
 * Compiler: GCC
 * Verion: 1.0
 * Generated: 26.03.2017
 * Author: V.Hrustalev
 */


#ifndef VECTOR_H
#define VECTOR_H

//#include <iostream>
//#include <stdlib.h>

//namespace TVector_v_SUAI_V_crys{

class TVector_v {
//-------- variables
    float *_data;
    int _size;

public:
//------- constructors & destructor
	_fastcall TVector_v() : _data( NULL ), _size( 0 ) {}
	_fastcall TVector_v( int SIZE ) : _data( NULL ), _size( SIZE ) { set_size( SIZE ); }
    _fastcall ~TVector_v() { set_size( 0 ); };

//------- constructor copy
    _fastcall TVector_v( const TVector_v &src );

//------- binary operators
	TVector_v & _fastcall operator=( const TVector_v &src );
	TVector_v _fastcall operator+( const TVector_v &src_r );
	TVector_v _fastcall operator-( );
	TVector_v _fastcall operator-( const TVector_v &src_r );
	TVector_v _fastcall operator*( const TVector_v &src_r );
	TVector_v _fastcall operator*( int );
    friend TVector_v _fastcall operator*( int l, const TVector_v &src_r );

	TVector_v _fastcall operator>>( int );
    TVector_v _fastcall operator<<( int );

//-------- thread operators
    //friend std::ostream & operator<< ( std::ostream &OS, const TVector_v &src_r );
   //	friend std::istream & operator>> ( std::istream &OS, TVector_v &src_r );

//-------------
	friend bool _fastcall operator== ( const TVector_v &src_l, const TVector_v &src_r );
	friend bool _fastcall operator!= ( const TVector_v &src_l, const TVector_v &src_r );
	TVector_v & _fastcall operator+= ( const TVector_v &src_r );
	TVector_v & _fastcall operator-= ( const TVector_v &src_r );
	TVector_v & _fastcall operator*= ( const TVector_v &src_r );
	TVector_v & _fastcall operator*= ( int src );
	TVector_v & _fastcall operator<<= ( int src );
	TVector_v & _fastcall operator>>= ( int src );

    float & _fastcall operator[]( int );

    TVector_v ret_part( int x1, int x2 );
    TVector_v del_elem( int ind );
    void _fastcall set_size( int SIZE );
    int _fastcall get_size();
private:
    void Print();
};

//}

#include "Vector.cpp"

#endif // TVector_v_H
