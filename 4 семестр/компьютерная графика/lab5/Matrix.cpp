#include "Matrix.h"

Matrix::Matrix( int Vectors, int Len_vect )
    : _vectors( NULL ), _size( 0 )
{
    set_size( Vectors );
    for ( int i = 0; i < _size; i ++ )
        _vectors[ i ].set_size( Len_vect );

}

Matrix::~Matrix()
{
	set_size( 0 );
	return;

    for ( int i = 0; i < _size; i ++ )
		_vectors[ i ].set_size( 0 );

	//delete _vectors;
	set_size( 0 );
    _size = 0;
}

void Matrix::set_size( int N )
{
    if ( _vectors != NULL )
	{
		for ( int i = 0; i < _size; i ++ )
			_vectors[ i ].set_size( 0 );

		//delete &_vectors;
		//delete _vectors;
        _vectors = NULL;
    }

    if ( N <= 0 )
    {
        _size = 0;
        return;
    }

    _size = N;
    _vectors = new TVector_v [ N ];

}


TVector_v & Matrix::operator[]( int Number )
{
    if ( ( Number < 0) || ( Number >= _size ) )
        throw Exception( "Matrix::Error index []" );

    return _vectors[ Number ];
}


//------- constructor copy
Matrix::Matrix( const Matrix &src ) :
    _size( 0 ), _vectors( NULL )
{
    set_size( src._size );
    for ( int i = 0; i < _size; i ++ )
    {
        _vectors[ i ].set_size( src._vectors[ i ].get_size() );
        _vectors[ i ] = src._vectors[ i ];
    }
}

//------- binary operators
Matrix & Matrix::operator=( const Matrix &src )
{
    if ( this == &src )
        return *this;

    set_size( src._size );
    for ( int i = 0; i < _size; i++ )
    {
        _vectors[ i ].set_size( src._vectors[ i ].get_size() ) ;
        _vectors[ i ] = src._vectors[ i ];
    }

    return *this;
}


Matrix Matrix::operator+( const Matrix &src_r )
{
    if ( _size != src_r._size )
        throw Exception( "Matrix::Operator+: size don't equals" );

    Matrix res( src_r._size );
    for ( int i = 0; i < src_r._size; i++ )
    {
        res._vectors[ i ].set_size( _size );
        res._vectors[ i ] = _vectors[ i ] + src_r._vectors[ i ];
    }

    return res;
}


Matrix Matrix::operator-( )
{
    Matrix res( _size );
    for ( int i = 0; i < _size; i++ )
    {
        res._vectors[ i ].set_size( _size );
        res._vectors[ i ] = -_vectors[ i ];
    }

    return res;
}


Matrix Matrix::operator-( const Matrix &src_r )
{
    if ( _size != src_r._size )
        throw Exception( "Matrix::Operator-: size don't equals" );

    Matrix res( _size, _vectors[ 0 ].get_size() );
    res = src_r;
    res = *this + ( -res );

    return res;
}

Matrix Matrix::operator*( const Matrix &src_r )
{
    int i, j, k;

    if ( _vectors[ 0 ].get_size() != src_r._size )
	    throw Exception( "Matrix::Operator*: column != row" );

    Matrix res( _size, src_r._vectors[ 0 ].get_size() );

    for ( i = 0; i < _size; i++ )
    {
        for ( j = 0; j < src_r._vectors[ 0 ].get_size(); j++ )
        {
            int sum = 0;
			for ( k = 0; k < _vectors[ 0 ].get_size(); k++ )
				sum += _vectors[ i ][ k ] * src_r._vectors[ k ][ j ];

            res[ i ][ j ] = sum;
        }
    }

    return res;
}



Matrix Matrix::operator*( int Val )
{
    int i;

    Matrix res( _size, _vectors[ 0 ].get_size() );

    for ( i = 0; i < _size; i++ )
        res._vectors[ i ] = _vectors[ i ] * Val;

    return res;
}


Matrix operator*( int Val, const Matrix &src_r )
{
    int i;

    Matrix res( src_r._size, src_r._vectors[ 0 ].get_size() );

    for ( i = 0; i < src_r._size; i++ )
        res._vectors[ i ] = src_r._vectors[ i ] * Val;

    return res;
}



//-------------------------------
bool operator== ( const Matrix &src_l, const Matrix &src_r )
{
    int i;

    if ( src_l._size != src_r._size )
        return false;

    for ( i = 0; i < src_l._size; i++)
        if ( src_l._vectors[ i ] != src_r._vectors[ i ] )
            return false;

    return true;
}

bool operator!= ( const Matrix &src_l, const Matrix &src_r )
{
    return !( src_l == src_r );
}

Matrix & Matrix::operator+= ( const Matrix &src_r )
{
    if ( _size != src_r._size )
        throw Exception( "Matrix::Operator+=: size don't equals" );

    int i;
    for ( i = 0; i < _size; i++ )
        _vectors[ i ] += src_r._vectors[ i ];

    return *this;
}

Matrix & Matrix::operator-= ( const Matrix &src_r )
{
    if ( _size != src_r._size )
        throw Exception( "Matrix::Operator-=: size don't equals" );

    int i;
    for ( i = 0; i < _size; i++ )
        _vectors[ i ] -= src_r._vectors[ i ];

    return *this;
}

Matrix & Matrix::operator*= ( const Matrix &src_r )
{

    Matrix buf = *this;
    *this = buf * src_r;

    return *this;
}

Matrix & Matrix::operator*= ( int src )
{
    int i;
    for ( i = 0; i < _size; i++ )
        _vectors[ i ] *= src;

    return *this;
}

Matrix Matrix::operator!( )
{
    Matrix res( _vectors[ 0 ].get_size(), _size );
    int i, j;

    for ( i = 0; i < _size; i++ )
    {
        for ( j = 0; j < _vectors[ 0 ].get_size(); j++ )
        {
            res[ j ][ i ] = _vectors[ i ][ j ];
        }
    }

    return res;
}


Matrix Matrix::ret_part( int x1, int y1, int x2, int y2 )
{
    if ( ( x1 > x2 ) || ( y1 > y2 ) ||
        ( x1 < 0 ) || ( y1 < 0 ) || ( y2 >= _size ) )
            throw Exception( "Matrix::ret_part: index error" );


    Matrix buf( y2 - y1 + 1, x2 - x1 + 1 );
    int i;

    for ( i = y1; i <= y2; i++ )
        buf[ i - y1 ] = _vectors[ i ].ret_part( x1, x2 );

    return buf;
}

Matrix Matrix::Minor( int x, int y )
{
    if ( ( x < 0 ) || ( y < 0 ) || ( y >= _size ) )
		throw Exception( "Matrix::Minor: error index" );

    int i;
    Matrix buf( _size - 1, _vectors[ 0 ].get_size() - 1 );

    for ( i = 0; i < y; i++ )
        buf[ i ] = _vectors[ i ].del_elem( x );

    for ( i = y + 1; i < _size; i++ )
        buf[ i - 1 ] = _vectors[ i ].del_elem( x );

    return buf;
}


int Matrix::Determinant( )
{
    if ( _size == 1 )
        return _vectors[ 0 ][ 0 ];

    int i, res;

    res = 0;
    for ( i = 0; i < _vectors[ 0 ].get_size(); i++ )
    {
        Matrix loop_b = Minor( i, 0 );
        res += ( ( i % 2 == 1 ) ? -1 : 1 ) * loop_b.Determinant( ) * _vectors[ 0 ][ i ];
    }
    return res;
}

void Matrix::rotate_matr( int ang )
{
    if ( ang > 3 ) ang %= 4;
    if ( ang == 0 ) return;

    if ( ang == -1 ) ang = 3;
        else if ( ang == -2 ) ang = 2;
            else if ( ang == -3 ) ang = 1;

    int i, j, k ;

    for ( i = 0; i < ang; i++ )
    {
        Matrix buf( _vectors[ 0 ].get_size(), _size );
        for ( j = 0; j < _size; j++ )
        {
            for ( k = 0; k < _vectors[ 0 ].get_size(); k++ )
            {
                buf[ k ][ j ] = _vectors[ _size - j - 1 ][ k ];
            }
        }
        *this = buf;
    }


    return;
}

int Matrix::get_size(){ return _size; }
