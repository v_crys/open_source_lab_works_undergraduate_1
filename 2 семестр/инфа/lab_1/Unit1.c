//---------------------------------------------------------------------------
#include <stdio.h>

//---------------------------------------------------------------------------
#define N 999999999999999

int main(void)
{
        unsigned int i = 0;
        long int up = 3;
        long int down = 1;
        double S = 0;

        if ((N < 0) || (sizeof(N) > 4)) {
                printf("Error!\n");
                getch();
                return 0;
        }

        while (i < N) {
                if ((i & 1) == 0) {
                        S += (float)up / (float)down;
                } else {
                        S -= (float)up / (float)down;
                }

                up++;
                down += 3;
                i++;
        }
        printf("S=%10.9f\n",S);
        getch();
        return 0;
}
//---------------------------------------------------------------------------
