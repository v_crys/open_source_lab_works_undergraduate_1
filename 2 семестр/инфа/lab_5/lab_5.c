#include <stdio.h>

#define _N 6
#define _K 3

int main(void)
{
        int data[_N][_K];
        int i, j, k;

        //Input
        for (i = 0; i < _K; i++)
                for (j = 0; j < _N; j++)
                        scanf("%d", &data[j][i]);


        for (j = 0; j < _K; j++){ //Line bust
                for (i = 0; i < (_N / 2); i++){ //Element bust
                        if (data[i][j] != data[_N - i - 1][j]) break;

                        if (i == ((_N / 2) - 1)) {//if last element
                                //Zero line
                                for (k = 0; k < _N; k++) data[k][j] = 0;
                        }
                }
        }

        //Output
        printf("OUT:\n");
        for (i = 0; i < _K; i++){
                for (j = 0; j < _N; j++) printf("%d ", data[j][i]);
                putchar('\n');
        }

        getch();
        return 0;
}
//---------------------------------------------------------------------------

