#include <stdio.h>

#define  testbit(var, bit)   (((var) & (1 <<(bit))) >> (bit))
#define  setbit(var, bit)    ((var) |= (1 << (bit)))
#define  clrbit(var, bit)    ((var) &= ~(1 << (bit)))

#define _BYTE 8

#define _NOT_SIGNED

int main(void)
{
        int i;
        unsigned long int num;
        unsigned char buf;

        unsigned int size_num = sizeof(num) * _BYTE;
        #ifdef _NOT_SIGNED
                size_num -= 1;
        #endif
        scanf("%d", &num);


        for (i = 0; i < (size_num / 2); i++){
                buf = testbit(num, i);

                if (testbit(num, size_num - i - 1) == 1)
                        setbit(num, i);
                else
                        clrbit(num, i);

                if (buf == 1)
                        setbit(num, size_num - i - 1);
                else
                        clrbit(num, size_num - i - 1);
        }

        printf("%d", num);
        getch();
        return 0;
}











