#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 100

#define SPACE_ON 1
#define SPACE_OFF 0

char *lines[MAX_LEN];
int ind_line = 0;

int main(void)
{
        FILE *fpi;
        FILE *fpo;

        char *start, *max_p, *start_word;
        int i, j, max, len_word;

        char flag = SPACE_ON;

        char rd_str(FILE *);
        void wr_str(FILE *, char *, char *);

        fpi = fopen("i.txt", "r");
        fpo = fopen("o.txt", "w");

        //input
        while ((rd_str(fpi)) && (ind_line < MAX_LEN));

        //output
        for (i = 0; i < ind_line; i++){
                start = lines[i];
                max = 0;
                len_word = 0;


                for (j = 0; j < MAX_LEN; j++)
                {
                        //Find max len word
                        if ((*lines[i] == ' ') || (*lines[i] == ',') || (*lines[i] == '.') || (*lines[i] == '\0')){
                                if ((len_word > max) && (flag == SPACE_OFF)) {
                                        max = len_word;
                                        max_p = start_word;
                                }
                                len_word = 0;
                                flag = SPACE_ON;
                        } else {
                                if (flag == SPACE_ON){
                                        flag = SPACE_OFF;
                                        start_word = lines[i];
                                }
                                len_word++;
                        }

                        if (*lines[i] == '\0') break;
                        lines[i]++;
                }

                lines[i] = start;
                wr_str(fpo, lines[i], max_p);
        }

        fclose(fpi);
        fclose(fpo);

        getchar();
        return 0;
}


void wr_str(FILE *ofp, char *data, char *p_start_w){
        char *buf;
        char flag = SPACE_OFF;

        buf = data;
        while (*buf != '\0'){
                if (p_start_w == buf)
                        flag = SPACE_ON;

                if ((*buf == ' ') || (*buf == ',') || (*buf =='.'))
                        flag = SPACE_OFF;

                if (flag == SPACE_OFF)
                        putc(*buf, ofp);

                buf++;
        }

        putc('\n', ofp);
        return;
}

char rd_str(FILE *ifp){
        char c;
        char *start;

        //allocation memory
        lines[ind_line] = (char *) calloc(MAX_LEN, sizeof(char));
        //save pointer on zero element str
        start = lines[ind_line];


        while (((c = getc(ifp)) != '\n') && (c != EOF) && ((lines[ind_line] - start) < (MAX_LEN-1)))
                *lines[ind_line]++ = c;

        *lines[ind_line] = '\0';
        lines[ind_line] = start;        //restore pointer

        ind_line++;

        if (c == EOF) return 0;
        else return 1;
}

//---------------------------------------------------------------------------



