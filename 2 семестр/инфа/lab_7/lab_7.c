#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 100

int main(void)
{
        FILE *fpi;
        FILE *fpo;
        char *array;

        void filecopy(FILE *, FILE *);
        char *rd_str(FILE *);
        void wr_str(FILE *, char *);

        fpi = fopen("i.txt", "r");
        fpo = fopen("o.txt", "w");

        array = (char *) calloc(MAX_LEN, sizeof(char));
        array = rd_str(fpi);

        
        wr_str(fpo, array);

        fclose(fpi);
        fclose(fpo);

        getchar();
        return 0;
}


void wr_str(FILE *ofp, char *data){
        char *buf;

        buf = data;
        while (*buf != '\0') putc(*buf++, ofp);
        putc('\n', ofp);
        return;
}

char *rd_str(FILE *ifp){
        char c;
        char *str;
        char *st;

        str = (char *) calloc(MAX_LEN, sizeof(char));
        st = str;

        while (((c = getc(ifp)) != '\n') && (c != EOF))
                *str++ = c;
        *str = '\0';
        return st;
}

void filecopy(FILE *ifp, FILE *ofp){
        int c;

        while((c = getc(ifp)) != EOF)
                putc(c, ofp);
}
//---------------------------------------------------------------------------


