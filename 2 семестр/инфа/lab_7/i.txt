The MPLAB XC Compilers are complete ANSI C
compilers for all of Microchip’s 8, 16, and 32-bit MCU
and DSC devices. These compilers provide powerful
integration capabilities, superior code optimization and
ease of use. MPLAB XC Compilers run on Windows,
Linux or MAC OS X.
For easy source level debugging, the compilers provide
debug information that is optimized to the MPLAB X
IDE.
The free MPLAB XC Compiler editions support all
devices and commands, with no time or memory
restrictions, and offer sufficient code optimization for
most applications.
MPLAB XC Compilers include an assembler, linker and
utilities. The assembler generates relocatable object
files that can then be archived or linked with other relocatable
object files and archives to create an executable
file. MPLAB XC Compiler uses the assembler to
produce its object file. Notable features of the assembler
include:
• Support for the entire device instruction set
• Support for fixed-point and floating-point data
• Command-line interface
• Rich directive set
• Flexible macro language
• MPLAB X IDE compatibility