#include <stdio.h>

#define _LEN 10

int main(void)
{
        int i;
        int data[_LEN];
        int buf;

        //Input data
        for (i = 0; i < _LEN; i++) scanf("%d", &data[i]);

        //inversion
        for (i = 0; i < (_LEN / 2); i++){
                buf = data[i];
                data[i] = data[_LEN - i - 1];
                data[_LEN - i - 1] = buf;
        }

        //Output data
        for (i = 0; i < _LEN; i++) printf("%d\n", data[i]);

        getch();
        return 0;
}
//---------------------------------------------------------------------------
 