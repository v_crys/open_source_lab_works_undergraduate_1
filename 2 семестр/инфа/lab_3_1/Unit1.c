/*
 * Subject: Basics of programming
 * Number lab: 3
 * Name Project: Removing Words with recurring characters
 * IDE: Borland C++ Builder 6
 * Verion: 1.0
 * Generated: 07/04/2016
 * Developer: student from group 1542 of the SUAI (St. Petersburg)
 */
#include <stdio.h>

#define AMOUNT_ELEM 200

#define IN_WORD 1
#define OUT_WORD 0

int duplate_chars(int, int);
char arr[AMOUNT_ELEM];

int main(void)
{
	int len = 0;
	int i, j;
	char flag_w = OUT_WORD;
	int pos_start = 0;
	
	//Input text
	while ((arr[len++] = getchar()) != EOF );
	
	for (i = 0; i < len; i++) { //Begining all chars 
		if ((arr[i] == ' ') || (arr[i] == ',') || (arr[i] =='.') || (arr[i] == '\n') || (arr[i] == EOF)) {
			if (flag_w == IN_WORD) { //if the privious char was the end of a word
				if (duplate_chars(pos_start, i - 1) == 1) {
						//if privious word consisted of a different chars, then print it
						for (j = pos_start; j < i; j++) putchar(arr[j]);
                        
						putchar(' '); //add space after word
				}
				
				flag_w = OUT_WORD;
			}
		} else {
			if (flag_w == OUT_WORD) {
				pos_start = i; //if it is the first char words then save it 
				flag_w = IN_WORD;
			}
		}
	}

        getch();
	return;
}

/*
 * This is method return zero if found  
 * recuring characters, and return one
 * if not found recuring characters.
 *
 * Checks the array begining with starting 
 * at stop inclusive. 
 */
int duplate_chars(int start, int stop)
{
	int i, j;
	
	for (i = start; i < stop; i++) 
		for (j = (i + 1); j <= stop; j++) 
			if (arr[i] == arr[j]) return 0;
	
	return 1;
}