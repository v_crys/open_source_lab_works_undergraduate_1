#include <stdio.h>

#define LEN 256
#define N_SIGN 6

int main(void)
{
        char data[LEN];
        int i_len = 0;
        int j, k;
        int word_len = 0;
        int pos_old = -1;

        //input
        while (((data[i_len++] = getchar()) != EOF) && (i_len < LEN));

        for (j = 0; j < i_len; j++){
                //detect the end of word
                if ((data[j] == ' ') || (data[j] == ',') ||
                                        (data[j] == '.') || (data[j] == EOF) || (data[j] == '\n')){

						if (word_len <= N_SIGN) {
							for (k = (pos_old + 1); k < j; k++) putchar(data[k]);
							if (data[j] != EOF) putchar(data[j]);
						} else {
							if (data[j] != '\n') putchar(data[j]);		
						}
                        word_len = 0;
						pos_old = j;
						
                        continue;
                }

                word_len++;
				
        }

        getch();
        return 0;
}
//---------------------------------------------------------------------------
 