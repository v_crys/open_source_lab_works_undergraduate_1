#include <stdio.h>

#define _NEW 0 //new word (treated, incremented)
#define _OLD 1 //old word

int main(void)
{
        int sign = 0;
        unsigned int words = 0;
        char flag_word = 0; //0 - new word; 1 - old word

        while ((sign = getchar()) != EOF) {
                if ((sign == ' ') || (sign == '.') || (sign == ',')){ //detect  new word
                        flag_word = _NEW;
                        continue;
                }

                if ((sign == 'a') || (sign == 'A') || (sign == 'e') || (sign == 'E')
                        || (sign == 'i') || (sign == 'I') || (sign == 'o') || (sign == 'O')
                        || (sign == 'u') || (sign == 'U') || (sign == 'y') || (sign == 'Y')) {

                        if (flag_word == _NEW){
                                words++;
                                flag_word = _OLD;
                        }
                }
        }

        printf("words = %d\n",words);

        getch();
        return 0;
}
//---------------------------------------------------------------------------
 