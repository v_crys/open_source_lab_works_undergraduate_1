#include <stdio.h>
#include <stdlib.h>
#include <windows.h>



#define FIELD_SIZE  8
#define MAX_POS     8


#define X           0
#define Y           0


#define FIELD_( var ) ( ( var >= 0 ) && ( var <= ( FIELD_SIZE - 1 ) ) )

//

//#define SHOW

struct vector{
    int x;
    int y;
};

void print_chess( int flag_clr, int vec_x, int vec_y );


int chess[ FIELD_SIZE ][ FIELD_SIZE ];
struct vector   moves[ MAX_POS ];

int flag = 0;
HANDLE console;

int cnt_out = 0;


int main()
{
    console = GetStdHandle(STD_OUTPUT_HANDLE);

    moves[ 0 ].x = 1;
    moves[ 0 ].y = -2;

    moves[ 1 ].x = 2;
    moves[ 1 ].y = -1;

    moves[ 2 ].x = 2;
    moves[ 2 ].y = 1;

    moves[ 3 ].x = 1;
    moves[ 3 ].y = 2;

    moves[ 4 ].x = -1;
    moves[ 4 ].y = 2;

    moves[ 5 ].x = -2;
    moves[ 5 ].y = 1;

    moves[ 6 ].x = -2;
    moves[ 6 ].y = -1;

    moves[ 7 ].x = -1;
    moves[ 7 ].y = -2;

    int i, j;
    struct vector  location;

    location.x = X;
    location.y = Y;

    for ( i = 0; i < FIELD_SIZE; i++ )
        for ( j = 0; j < FIELD_SIZE; j++ )
            chess[ i ][ j ] = 0;

    chess[ location.x ][ location.y ] = 1;
    step( location );


    return 0;
}



void step( struct vector loc )
{
    int i, j, k;
    int buf = 0;
    int flag_call = 0;
    struct vector vec;

    //--- finished?
    for ( i = 0; i < FIELD_SIZE; i++ )
        for ( j = 0; j < FIELD_SIZE; j++ )
            if ( chess[ i ][ j ] == 0 )
                buf = 1;

    if ( !buf ) {
            printf( "Print answer:\n");
            print_chess( 0, loc.x, loc.y );
            chess[ loc.x ][ loc.y ] = 0;
            flag = 1;
            return;
    }



    for ( i = 0; i < MAX_POS; i++ )
    {
        vec.x = loc.x + moves[ i ].x;
        vec.y = loc.y + moves[ i ].y ;

        #ifdef SHOW
        if ( flag_call )
            print_chess( 1, loc.x, loc.y );
        flag_call = 0;
        #endif

        if ( FIELD_( vec.x ) && FIELD_( vec.y) )
            if ( chess[ vec.x ][ vec.y ] == 0 )
            {
                chess[ vec.x ][ vec.y ] = 1;

                #ifdef SHOW
                print_chess( 1, vec.x, vec.y );
                #endif


                step( vec );


                if ( flag )
                {
                    //printf( "Step: %d %d\n", loc.x, loc.y );
                    chess[ loc.x ][ loc. y ] = 0;

                    printf( "\n" );
                    print_chess( 0, loc.x, loc.y );
                    cnt_out++;

                    if ( cnt_out > 9 ) { getch(); cnt_out = 0; }

                    return;
                }

                flag_call = 1;
            }
    }

    chess[ loc.x ][ loc.y ] = 0;
    #ifdef SHOW
    print_chess( 1, loc.x, loc.y );
    #endif
    return;


}

void print_chess( int flag_clr, int vec_x, int vec_y )
{
    int i, j;


    if ( flag_clr )
        system( "cls" );

    for ( i = 0; i < FIELD_SIZE; i++ )
    {
        for ( j = 0; j < FIELD_SIZE; j++ )
        {
            if ( ( i == vec_x ) && ( j == vec_y ) )
                SetConsoleTextAttribute(console, 2);
            else
            {
                if ( chess[ i ][ j ] == 1 )
                    SetConsoleTextAttribute(console, 0x4);
                else
                    SetConsoleTextAttribute(console, 0xF);
            }


            printf( "%d ", chess[ i ][ j ] );
        }

    printf( "\n" );

    }

    #ifdef SHOW
                sleep( 1 );
    #endif
}


