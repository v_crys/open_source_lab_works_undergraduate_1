/*
 * Name Project: Lab 1 ( code highlighting )
 * IDE: Code Blocks
 * Compiler: MinGW
 * Version: 1.0
 * Generated: 03.09.2016
 * Developer: V. Hrustalev
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

//#define INPUT_FILE  "flash_for_public.c"
#define INPUT_FILE  "EE_MSSP.h"
//#define     INPUT_FILE  "main.c"
//#define     INPUT_FILE      "blink_wifi.c"
//#define        INPUT_FILE   "blink_stop_loader.c"
//#define     INPUT_FILE      "test.c"

#define SIZE_BUF    100
#define LINES       20

#define NOW_I       51
#define NEXT_I      52
#define BACK_I      50

#define NOW_CHAR    in_buf[ NOW_I ]
#define NEXT_CHAR   in_buf[ NEXT_I ]
#define BACK_CHAR   in_buf [ BACK_I ]
#define NEW_CHAR    in_buf[ SIZE_BUF - 1 ]


#define KEY_WORD_CNT    13
char *KEY_WORD[ ] = { "if", "for", "return", "switch", "while", "continue", "break", "case", "struct", "else", "goto", "extern", "volatile" };

#define TYPE_WORD_CNT   10
char *TYPE_WORD[ ] = { "int", "char", "float", "double", "signed", "unsigned", "long", "short", "const", "void" };

char *SPACE = "=*-+/%$&^\'\" \t;,])[(<>" ;

char FindChar( char *line, char c );

int main()
{
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);

    //-------- global flag description ( 0 - normal, 1 - "/*", 2 - "//", 3 - '"', 4 - function, 5 - key word, 6 - TYPE WORD
    //-------- 7 - macros, 8 - dec )
    char glob_flag = 0;
    int counter_figure = 0;     //----- counter '{' or '}'

    FILE *fi = fopen( INPUT_FILE, "r" );
    int in_buf[ SIZE_BUF ];     //----- buffer input data
    char buff[ SIZE_BUF ];      //----- templated buffer

    int cnt_out = 0;
    int i, j, c;
    int tmp = BACK_I - 2;
    int delay_color = 0;

    char flag_end = 0;
    int end_file;

    c = 0;

    if ( fi == NULL )
    {
        printf( "File path don't correct" );
        getchar();
        return 0;
    }


    //--------------- main loop
    while ( 1 )
    {
        //--------- correcting finish
        if ( c != EOF )
            c = getc( fi );
        else
        {
            if ( flag_end )
                end_file--;
            else
            {
                flag_end = 1;
                end_file = 48;
            }

            if ( !end_file ) break;
        }


        //---------- buffer <<
        for ( i = 0; i < SIZE_BUF - 1; i++ )
            in_buf[ i ] = in_buf[ i + 1 ];

        NEW_CHAR = c;

        //--------- delay fill buffer
        if ( tmp != 0 )
        {
            tmp--;
            continue;
        }

        //--------- detecting now char
        switch( NOW_CHAR )
        {
            case 0x0A:
                if ( ( glob_flag == 2 ) || ( glob_flag == 5 ) || ( glob_flag == 6 ) || ( glob_flag == 7 ) || ( glob_flag == 8 ) )
                    glob_flag = 0;
                break;

            case ' ':
                if ( BACK_CHAR == '\'' ) break;

                if ( ( glob_flag == 5 ) || ( glob_flag == 6 ) || ( glob_flag == 8 ) )
                    glob_flag = 0;
                break;

            case '\t':
                if ( BACK_CHAR == '\'' ) break;

                if ( ( glob_flag == 5 ) || ( glob_flag == 6 ) || ( glob_flag == 8 ) )
                    glob_flag = 0;
                break;

            case '/':
                if ( BACK_CHAR == '\'' ) break;

                if ( glob_flag == 8 )
                    glob_flag = 0;

                switch( NEXT_CHAR )
                {
                    case '/':
                        if ( ( glob_flag == 0 ) || ( glob_flag == 5 ) || ( glob_flag == 6 ) || ( glob_flag == 7) || ( glob_flag == 8 ) )
                            glob_flag = 2;

                        break;

                    case '*':
                        if ( ( glob_flag == 0 ) || ( glob_flag == 5 ) || ( glob_flag == 6 ) || ( glob_flag == 7) || ( glob_flag == 8 ) )
                            glob_flag = 1;

                        break;
                }
                break;

            case '*':
                if ( glob_flag == 8 )
                    glob_flag = 0;

                if ( ( NEXT_CHAR == '/' ) && ( glob_flag == 1 ) )
                {
                    delay_color = 2;
                    glob_flag = 0;
                }

                break;

            case '"':
                if ( ( BACK_CHAR != '\\' ) && ( BACK_CHAR != '\'' ) )
                {
                    if ( ( glob_flag == 0 ) || ( glob_flag == 3 ) )
                    {
                        if ( glob_flag == 0 )
                        {
                            glob_flag = 3;
                        } else {
                            glob_flag = 0;
                            delay_color = 1;
                        }
                    }
                }
                break;

            case '{':
                if ( BACK_CHAR == '\'' ) break;

                if ( ( glob_flag == 0 ) || ( glob_flag == 4 ) || ( glob_flag == 5 ) || ( glob_flag == 6 ) )
                    counter_figure++;
                break;

            case '}':
                if ( BACK_CHAR == '\'' ) break;

                if ( glob_flag == 8 )
                    glob_flag = 0;

                if ( ( glob_flag == 0 ) || ( glob_flag == 4 ) || ( glob_flag == 5 ) || ( glob_flag == 6 ) )
                    counter_figure--;
                break;

            case '(':
                if ( BACK_CHAR == '\'' ) break;

                if ( ( glob_flag == 4 ) )
                    glob_flag = 0;
                break;

            case '#':
                if ( BACK_CHAR == '\'' ) break;

                if ( ( glob_flag == 0 ) && ( BACK_CHAR != '\'' ) )
                    glob_flag = 7;
                break;

            case '-':
            case '+':
            case ':':
            case ']':
            case '\'':
            case ')':
            case ',':
            case ';':
                if ( BACK_CHAR == '\'' ) break;

                if ( glob_flag == 8 )
                    glob_flag = 0;
                break;


        }


        if ( glob_flag == 0 )
        {
            //--------------- KEY WORD
            for ( i = 0; i < KEY_WORD_CNT; i++ )
            {
                if ( ( ( BACK_CHAR >= 'a' ) && ( BACK_CHAR <= 'z' ) ) || ( BACK_CHAR == '_' ) )
                    break;

                for ( j = 0; j < ( SIZE_BUF / 2 ); j++ )
                    if ( ( ( in_buf[ j + NOW_I ] >= 'a' ) && ( in_buf[ j + NOW_I ] <= 'z' ) ) ||
                                        ( in_buf[ j + NOW_I ] == '_' ) || ( in_buf[ j + NOW_I ] == ';' ) )
                    {
                        if ( in_buf[ j + NOW_I ] == ';' )
                        {
                            buff[ j ] = 0;
                            break;
                        }

                        buff[ j ] = in_buf[ j + NOW_I ];
                    }
                    else {
                        buff[ j ] = 0;
                        break;
                    }

                if ( stricmp( ( const char * ) buff, KEY_WORD[ i ] ) == 0 )
                    glob_flag = 5;
            }

            //-------------------- TYPE WORD
            for ( i = 0; i < TYPE_WORD_CNT; i++ )
            {
                if ( ( ( BACK_CHAR >= 'a' ) && ( BACK_CHAR <= 'z' ) ) || ( BACK_CHAR == '_' ) )
                    break;

                for ( j = 0; j < ( SIZE_BUF / 2 ); j++ )
                    if ( ( ( in_buf[ j + NOW_I ] >= 'a' ) && ( in_buf[ j + NOW_I ] <= 'z' ) ) || ( in_buf[ j + NOW_I ] == '_' ) ||
                                            ( in_buf[ j + NOW_I ] == ';' ) )
                    {
                        if ( in_buf[ j + NOW_I ] == ';' )
                        {
                            buff[ j ] = 0;
                            break;
                        }

                        buff[ j ] = in_buf[ j + NOW_I ];
                    }
                    else {
                        buff[ j ] = 0;
                        break;
                    }

                if ( stricmp( ( const char * ) buff, TYPE_WORD[ i ] ) == 0 )
                    glob_flag = 6;
            }

        }

        //------------ clearing KEY and TYPE word flag
        if ( !( ( NOW_CHAR >= 'a' ) && ( NOW_CHAR <= 'z' ) ) )
            if ( ( glob_flag == 5 ) || ( glob_flag == 6 ) )
                glob_flag = 0;


        //---------------- function light
        if ( ( glob_flag == 0 ) && ( counter_figure ) )
        {
            for ( i = NOW_I ; i >= 0; i-- )
            {
                if ( in_buf[ i ] == '(' )
                        goto label1;

                //if ( ( in_buf[ i ] != ' ' ) && ( in_buf[ i ] != '\t' )  && ( in_buf[ i ] != '_' ))
                if ( !FindChar( SPACE, in_buf[ i ] ) )
                        break;
            }

            for ( i = NOW_I; i < SIZE_BUF; i++ )
            {
                if ( ( in_buf[ i ] == '_' ) /*|| ( in_buf[ i ] == ' ' ) || ( in_buf[ i ] == '\t' )*/ || ( in_buf[ i ] == '(' ) ||
                            ( ( in_buf[ i ] >= 'a' ) && ( in_buf[ i ] <= 'z' ) ) ||
                            ( ( in_buf[ i ] >= 'A' ) && ( in_buf[ i ] <= 'Z' ) ) ||
                            ( ( in_buf[ i ] >= '0' ) && ( in_buf[ i ] <= '9' ) ) )
                {
                    if ( in_buf[ i ] == '(' )
                    {
                            glob_flag = 4;
                            break;
                    }
                } else break;

            }

            label1:;
        }

        if ( glob_flag == 0 )
        {
            if ( ( NOW_CHAR >= '0' ) && ( NOW_CHAR <= '9') )

            for ( i = NOW_I; i >= 0; i-- )
            {
                if ( ( ( ( in_buf[ i ] >= 'a' ) && ( in_buf[ i ] <= 'z' ) ) || ( in_buf[ i ] == '_' ) || ( ( in_buf[ i ] >= 'A' ) && ( in_buf[ i ] <= 'Z' ) ) ) || ( BACK_CHAR == '\'' ) )
                        break;

                if ( FindChar( SPACE, in_buf[ i ] ) )
                {
                    glob_flag = 8;
                    break;
                }
            }

        }

        //--------------- set color
        if ( delay_color == 0 ){
            switch ( glob_flag )
            {
                case 0:
                    SetConsoleTextAttribute(console, 0xF);
                    break;

                case 1:
                    SetConsoleTextAttribute(console, 1);
                    break;

                case 2:
                    SetConsoleTextAttribute(console, 2);
                    break;

                case 3:
                    SetConsoleTextAttribute(console, 3);
                    break;

                case 4:
                    SetConsoleTextAttribute(console, 9);
                    break;

                case 5:
                    SetConsoleTextAttribute(console, 4);
                    break;

                case 6:
                    SetConsoleTextAttribute(console, 5);
                    break;

                case 7:
                    SetConsoleTextAttribute(console, 6);
                    break;

                case 8:
                    SetConsoleTextAttribute(console, 8);
                    break;
            }

        } else delay_color--;

        //--------- out char
        putchar( NOW_CHAR );


        //--------- listing page
        if ( c == '\n' )
            cnt_out++;

        if ( cnt_out == LINES )
        {
            cnt_out = 0;
            getchar();
        }

    }

    getchar( );

    fclose( fi );


    return 0;
}

char FindChar( char *line,  char c )
{
    int i;
    for ( i = 0; line[ i ] != 0; i++ )
        if ( line[ i ] == c )
            return 1;

    return 0;
}
