#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main()
{
    FILE    *fi     =   fopen( "in.txt", "rt" );
    FILE    *fo     =   fopen( "out.txt", "wt" );
    char    **text  =   NULL;
    char    *p_buf  =   NULL;
    int     lines   =   0;
    int     len     =   0;
    int     i       =   0;
    int     j       =   0;
    char    flag_s  =   0;
    char    c;

    //-------------- verify corrected files
    if ( !(fi && fo) )
    {
        printf( "Error!" );
        return 0;
    }

    //------------- input from in.txt
    text    =   ( char ** )realloc( text, ( ++lines * sizeof( char * ) ) );
    text[ lines - 1 ]   =   NULL;
    while ( ( c = getc( fi ) ) != EOF )
    {
        text[ lines - 1 ]   =   ( char * )realloc( text[ lines - 1 ], ++len );
        text[ lines - 1 ][ len - 1 ]    =   c;

        if ( c == '\n' )
        {
            text[ lines - 1 ][ len - 1 ]    =   0;
            len     =   0;
            text    =   ( char ** )realloc( text, ( ++lines * sizeof( char * ) ) );
            text[ lines - 1 ]   =   NULL;
            text[ lines - 1 ]   =   ( char * )realloc( text[ lines - 1 ], 1 );
            text[ lines - 1 ][ 0 ]    =   0;
        }
    }

    //------------ sort
    for ( i = 0; i < lines; i++ )
    {
        flag_s  =   1;
        for ( j = i + 1; j < lines; j++ )
        {
            if ( strcasecmp( text[ i ], text[ j ] ) > 0 )
            {
                p_buf       =   text[ i ];
                text[ i ]   =   text[ j ];
                text[ j ]   =   p_buf;

                flag_s      =   0;
            }
        }
        if ( flag_s ) break;
    }



    //------------ output in out.txt
    for ( i = 0; i < lines; i++ )
        fprintf( fo, ( i == ( lines - 1 ) ) ? "%s" : "%s\n", text[ i ] );

    //------------ free used memory
    for ( i = 0; i < lines; i++ )
        free( text[ i ] );

    free( text );

    fclose( fi );
    fclose( fo );
    return 0;
}
