/*
 *  Name project:   Lab2
 *  Generated:      15/10/2016
 *
 *  Description:
 *      file for storing the parameters and description of
 *      composite types
 */

#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

    #define AUTHORS     "A. Prosandeeva, M. Fedorova, V. Khrustalev"
    #define NAME_PRJ    "Labs 2 ( OP )"

    #define PRINT_MENU  system( "cls" ); \
                        printf( "-------------------------------------------------------------------------\n" );\
                        printf( "|\t\tName project: " NAME_PRJ "\t\t\t\t|\n" );                           \
                        printf( "|\t\tAuthors: " AUTHORS "\t|\n");                                  \
                        printf( "-------------------------------------------------------------------------\n" );\
                        printf( "\n\nMain menu (top on the number):\n" );                 \
                        printf( "\t0 - DB open\n" ); \
                        printf( "\t1 - Add record\n" ); \
                        printf( "\t2 - Delete record\n" ); \
                        printf( "\t3 - Print DB\n" ); \
                        printf( "\t4 - Selection of the minimum basket\n" );\
                        printf( "\t5 - Exit\n" );

    #define PRINT_0     system( "cls" ); \
                        printf( "DB open:\n" );

    #define PRINT_1     system( "cls" ); \
                        printf( "Add record:\n" );

    #define PRINT_2     system( "cls" ); \
                        printf( "Delete record:\n" );

    #define PRINT_3     system( "cls" ); \
                        printf( "Print DB:\n" );

    #define PRINT_4     system( "cls" ); \
                        printf( "Selection of the minimum basket:\n" );

    #define PRINT_5     system( "cls" ); \
                        printf( "Do you wont to save the change?\n") ;\
                        printf( "Y - Yes\tN - No\tC - Close\n" );


    #define MAX_LEN_NAME 100
    #define MAX_PRODUCTS 100
    #define MAX_COAST    1000000

    typedef struct s_Product{
            char            name[ MAX_LEN_NAME ];
            unsigned int    coast;
            unsigned char   category;
            unsigned int    number;
        } Product;




#endif // SETTINGS_H_INCLUDED
