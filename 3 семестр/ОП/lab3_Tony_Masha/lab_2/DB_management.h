/*
 *  Name project:   Lab2
 *  Generated:      15/10/2016
 *
 *  Description:
 *      library for working database, add and delete records
 */

#ifndef DB_MANAGEMENT_H_INCLUDED
#define DB_MANAGEMENT_H_INCLUDED

    #include <stdio.h>

    #include "settings.h"

    #include "DB_management.c"


    void add_record( Product *p_prod, Product *p_added_prod, unsigned int *p_prod_size );
    void del_record( Product *p_prod, unsigned int number_prod, unsigned int *p_prod_size );


#endif // DB_MANAGEMENT_H_INCLUDED
