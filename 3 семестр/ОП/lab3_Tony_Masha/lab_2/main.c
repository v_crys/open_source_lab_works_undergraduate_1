/*
 *  Name project:   Lab2
 *  Generated:      15/10/2016
 *  Authors:
 */

#include <stdio.h>
#include <stdlib.h>

#include "settings.h"
#include "DB_management.h"
#include "DB_print.h"
#include "selection_product.h"

int main()
{
    Product my_products[ MAX_PRODUCTS ];
    unsigned int my_products_i = 0;


    Product buf_product;

    char c;
    unsigned int buf_int;
    unsigned int i;

    unsigned char category_arr[ MAX_PRODUCTS ];


    FILE *f_p;
    char path_DB[ MAX_LEN_NAME ];

    PRINT_MENU;

    while ( 1 )
    {
        c = getch();
        switch ( c )
        {
            case '0':
                PRINT_0;
                printf( "\tPrint path to file: " );
                scanf( "%s", path_DB );
                f_p = fopen( path_DB, "r" );
                if ( f_p == NULL )
                {
                    printf( "File don't open!" );
                    getch();
                    break;
                }

                fread( my_products, sizeof( my_products ), 1, f_p);
                my_products_i = 0;
                while( my_products[ my_products_i++ ].coast );
                my_products_i--;
                fclose( f_p );

                printf( "\nDB download!" );
                getch();
                break;

            case '1':
                PRINT_1;
                printf( "\tPrint name product: " );
                scanf( "%s", buf_product.name );
                printf( "\tPrint number: " );
                scanf( "%d", &(buf_product.number) );
                printf( "\tPrint coast: " );
                scanf( "%d", &(buf_product.coast) );
                printf( "\tPrint category: " );
                scanf( "%d", &(buf_product.category) );

                add_record( my_products, &buf_product, &my_products_i );

                printf( "\nRecord adding!" );
                getch();
                break;

            case '2':
                PRINT_2;
                printf( "\tPrint index of deleting: " );
                scanf( "%d", &buf_int );
                del_record( my_products, buf_int, &my_products_i );
                printf( "\nRecord deleting!" );
                getch();
                break;

            case '3':
                PRINT_3;
                print_DB( my_products, 0, my_products_i );
                printf( "\n\n" );

                printf( "Print category: " );
                scanf( "%d", &buf_int );
                print_DB_alf( my_products, buf_int, my_products_i );

                getch();
                break;

            case '4':
                PRINT_4;
                printf( "\tPrint count products: " );
                scanf( "%d", &buf_int );
                for ( i = 0; i < buf_int; i++ )
                {
                    printf( "\t\tPrint category: ");
                    scanf( "%d", &category_arr[ i ] );
                }

                select_prod( my_products, category_arr, my_products_i, buf_int );

                getch();
                break;

            case '5':
                PRINT_5;
                c = getch();
                switch ( c )
                {
                    case 'Y':
                    case 'y':
                        printf( "Print file path to save: " );
                        scanf( "%s", path_DB );
                        f_p = fopen( path_DB, "w" );
                        fwrite( my_products, sizeof( my_products), 1, f_p );
                        fclose( f_p );
                        return 0;

                    case 'N':
                    case 'n':
                        return 0;
                }

                getch();
                break;

        }

        PRINT_MENU;
    }

    return 0;
}
