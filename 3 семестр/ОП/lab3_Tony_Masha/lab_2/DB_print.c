#include <stdio.h>

void print_DB( Product *p_prod, unsigned int categ, unsigned int p_prod_size )
{
    unsigned int i;
    printf( "-------------------------------------------------------------------------\n");
    printf( "|\tName\t|\tCoast\t|\tCategory\t|\tNumber\t|\n" );
    printf( "-------------------------------------------------------------------------\n");
    for ( i = 0; i < p_prod_size; i++ )
    {
        printf( "|\t%-8s|\t%-8d|\t%-8d\t|\t%-8d|\n", p_prod[ i ].name, p_prod[ i ].coast,
               p_prod[ i ].category, p_prod[ i ].number );
    }
    printf( "-------------------------------------------------------------------------\n");
    return;
}

void print_DB_alf( Product *p_prod, unsigned int categ, unsigned int p_prod_size )
{
    unsigned int i, j;
    unsigned int buf_i;


    printf( "Alfavit view\n" );
    printf( "-------------------------------------------------------------------------\n");
    printf( "|\tName\t|\tCoast\t|\tCategory\t|\tNumber\t|\n" );
    printf( "-------------------------------------------------------------------------\n");
    for ( i = 0; i < p_prod_size; i++ )
    {
        if ( ( p_prod[ i ].category != categ )  )
                continue;

        for ( j = 0; j < p_prod_size; j++ )
            if ( ( p_prod[ j ].category == categ ) && ( !( p_prod[ j ].name[ MAX_LEN_NAME - 1] == 100 ) ) ) break;

        buf_i = j;
        for ( j = 0; j < p_prod_size; j++ )
        {
            if ( ( p_prod[ j ].category != categ ) || ( p_prod[ j ].name[ MAX_LEN_NAME - 1 ] == 100 ))
                continue;

            if ( strcmp( p_prod[ buf_i ].name, p_prod[ j ].name ) > 0 )
                buf_i = j;
        }

         printf( "|\t%-8s|\t%-8d|\t%-8d\t|\t%-8d|\n", p_prod[ buf_i ].name, p_prod[ buf_i ].coast,
               p_prod[ buf_i ].category, p_prod[ buf_i ].number );

        p_prod[ buf_i ].name[ MAX_LEN_NAME - 1] = 100;
    }

    for ( i = 0 ; i < p_prod_size; i++ )
        p_prod[ i ].name[ MAX_LEN_NAME - 1 ] = 0;
    printf( "-------------------------------------------------------------------------\n");
    return;
}
