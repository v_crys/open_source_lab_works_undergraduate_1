#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

    #define LEFT_BUTT   75//-3275
    #define RIGHT_BUTT  77//-3277
    #define TOP_BUT     72//-3272
    #define DOWN_BUF    80//224//22480//-3280

    #define ENTER       13
    #define ESC         27

    #define FOLDER 16

    #define MAX_DEPTH 10

    #define YELLOW  14
    #define BLUE    11

    #define DEFAULT_FOLDERS 100
    #define DEFAULT_FILES   100
    #define MAX_LEN_NAME    1000

#endif // SETTINGS_H_INCLUDED
