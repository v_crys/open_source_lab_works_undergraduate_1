/*
 * Project: File explorer
 * Author: V. Khrustalev
 * Generated: 29/10/2016
 */

#include <stdio.h>
#include <sys/types.h>

#include <windows.h>
#include <dirent.h>
#include <string.h>

#include "settings.h"

typedef char * string;

typedef struct simple_folder
{
    char *path;
    string *folders;
    unsigned int folder_size;
    string *files;
    unsigned int files_size;
} Folder;

int download_dir( Folder *fold, char *path);
void clear_dir( Folder *fold);

void print_dir( Folder *fold, unsigned int index );

int main() {
    Folder my_fold;
    unsigned int location = 0;
    int i;

    char buf_path[ MAX_LEN_NAME ];
    char buf2_path[ MAX_LEN_NAME ];
    unsigned int c;

    printf( "Print start path: " );
    scanf( "%s", buf_path );

    download_dir( &my_fold, buf_path );
    print_dir( &my_fold, location );


    while( 1 )
    {
        c = getch();
        switch( (unsigned int) c)
        {

            case RIGHT_BUTT:
            case ENTER:
                if ( my_fold.folder_size == 0 ) break;

                strcpy( buf_path, my_fold.path );
                strcat( buf_path, "\\" );
                strcat( buf_path, my_fold.folders[ location ] );

                location = 0;
                clear_dir( &my_fold );
                download_dir( &my_fold, buf_path );

                print_dir( &my_fold, location );
                break;

            case LEFT_BUTT:
                strcpy( buf2_path, my_fold.path );

                for ( i = strlen( my_fold.path ) - 1;
                    ( ( my_fold.path[ i ] != '\\' )  )
                     && ( i >= 0 );
                    i-- );

                if ( ( strstr( my_fold.path, "\\" ) - my_fold.path ) == i )
                {
                    if ( my_fold.path[ i + 1 ] == 0 )
                        break;
                    else
                        my_fold.path[ i + 1 ] = 0;
                }  else
                    my_fold.path[ i ] = 0;

                strcpy( buf_path, my_fold.path );
                location = 0;
                clear_dir( &my_fold );
                if ( download_dir( &my_fold, buf_path ) )
                {
                    system( "cls" );
                    printf( "Error! Don't correct path." );
                    getch();
                    clear_dir( &my_fold );
                    download_dir( &my_fold, buf2_path );
                }

                print_dir( &my_fold, location );
                break;

            case TOP_BUT:
                if ( location != 0 )
                    location--;
                print_dir( &my_fold, location );
                break;

            case DOWN_BUF:
                if ( location < my_fold.folder_size - 1 )
                    location++;
                print_dir( &my_fold, location );
                break;

            case ESC:
                return 0;

        }
    }

    return 0;
}

void print_dir( Folder *fold, unsigned int index )
{
    int i;

    HANDLE console = GetStdHandle( STD_OUTPUT_HANDLE );
    SetConsoleTextAttribute(console, 10);

    system( "cls" );
    printf( "Path my folder: %s \n(Folders: %d, Files: %d)\n\n", fold->path, fold->folder_size, fold->files_size );

    for ( i = 0; i < fold->folder_size; i++ )
    {
        SetConsoleTextAttribute(console, 10);
        if ( i == index )
            printf( "      - " );
        else
            printf( "\t" );

        SetConsoleTextAttribute(console, YELLOW);
        printf( "%s\n", fold->folders[ i ] );
    }

    if ( fold->folder_size == 0 )
        printf( "\n\n\tEmpty folder!" );

    SetConsoleTextAttribute(console, BLUE);
    printf( "\n" );
    for ( i = 0; i < fold->files_size; i++ )
        printf( "\t%s\n", fold->files[ i ] );

    SetConsoleTextAttribute(console, YELLOW);
    return;
}

int download_dir( Folder *fold, char *path)
{
    DIR *dir;
    struct dirent *entry;

    system( "cls" );
    printf( "Wait..." );

    fold->files_size = 0;
    fold->folder_size = 0;

    fold->path = ( char * ) calloc( MAX_LEN_NAME, sizeof( char ) );
    strcpy( fold->path, path );

    fold->files = ( string * ) calloc( DEFAULT_FILES, sizeof( string ) );
    fold->folders = ( string * ) calloc( DEFAULT_FOLDERS, sizeof( string ) );

    dir = opendir( path );
    if ( !dir )
    {
        printf( "Path don't correct");
        return 1;
    }

    while ( (entry = readdir( dir )) != NULL )
    {
        if (  entry->d_type >> 4  )
        {
            if ( fold->folder_size >= DEFAULT_FOLDERS )
              fold->folders = realloc( fold->folders, ( fold->folder_size + 1 ) * sizeof( string ) );

            fold->folders[ fold->folder_size ] = ( char * ) calloc( MAX_LEN_NAME, sizeof( char ) );

            strcpy( fold->folders[ fold->folder_size ], entry->d_name );
            fold->folder_size++;
        } else {
            if ( fold->files_size >= DEFAULT_FILES )
                fold->files = realloc( fold->files , ( fold->files_size + 1 ) * sizeof( string ) );


            fold->files[ fold->files_size ] = ( char * ) calloc( MAX_LEN_NAME, sizeof( char ) );
            strcpy( fold->files[ fold->files_size ], entry->d_name );
            fold->files_size++;
        }
    }

    closedir( dir );

    return 0;
}

void clear_dir( Folder *fold )
{
    int i;

    free( fold->path );

    for ( i = 0; i < fold->files_size; i++ )
        free( fold->files[ i ] );
    free( fold->files );

    for ( i = 0; i < fold->folder_size; i++ )
        free( fold->folders[ i ] );
    free( fold->folders );

    return;
}
